require 'rubygems'
require 'find'

task :required do
    #puts "Checking GEMs required..."
    isOk = true
    begin
        require 'json'
    rescue LoadError
        puts "JSON gem not found.\nInstall it by running 'gem install json'"
        isOk = false
    end
    begin
        require 'closure-compiler'
    rescue LoadError
        puts "closure-compiler gem not found.\nInstall it by running 'gem install closure-compiler'"
        isOk = false
    end
    begin
        require 'compass'
    rescue LoadError
        puts "compass gem not found.\nInstall it by running 'gem install compass'"
        isOk = false
    end
    begin
        require 'zip'
    rescue LoadError
        puts "Zip Tools not found.\nInstall it by running 'gem install rubyzip'"
        isOk = false
    end
    if !isOk
        abort
    end
    #puts "DONE"
end

task :dir do
    puts "Preparing Directories..."
    system "rm -rf build/"
    system "rm -rf docs/"
    system "rm -rf violations/"
    system "rm -rf dist/"
    Dir.mkdir('build') unless File.exists?('build')
    Dir.mkdir('build/js') unless File.exists?('build/js')
    Dir.mkdir('build/css') unless File.exists?('build/css')
    Dir.mkdir('build/img') unless File.exists?('build/img')
    #Dir.mkdir('build/fonts') unless File.exists?('build/fonts')
    Dir.mkdir('docs') unless File.exists?('docs')
    Dir.mkdir('violations') unless File.exists?('violations')
    Dir.mkdir('dist') unless File.exists?('dist')
    puts "DONE"
end

desc "Generates the project .js file"
task :js => [:required, :dir] do
    puts "Generating .js files..."
    Rake::Task['required'].execute

    buildConfig = File.read File.dirname(__FILE__) + '/config/build.json'
    buildFiles = JSON.parse buildConfig
    i = 0
    buildFiles.each do |bFile|

        extension = bFile['extension']
        name = bFile['name']
        if bFile['include_version']
            name += '-' + getVersion
        end
        write_path = bFile['target_path']
        buffer_file = ""
        sourceFiles = bFile['files']
        sourceFiles.each do |source|
            buffer_file += File.read source
            buffer_file += "\n"
        end
        if bFile['construct']
            #puts "construct"
            base_path = bFile['construct']['base_file']
            replace_pattern = bFile['construct']['replace_pattern']
            base_file = File.read base_path
            #The next line is necessay to be added, 'cause the gsub escapes the backslashes
            buffer_file = buffer_file.gsub("\\", "\\\\\\");
            buffer_file = base_file.gsub(replace_pattern, buffer_file)
            if bFile['construct']['version_pattern']
                buffer_file = buffer_file.gsub(bFile['construct']['version_pattern'], getVersion)
            end
        end
        fileName = write_path + name + '.' + extension
        File.open(fileName, 'w+') do |write_file|
            write_file.write buffer_file
        end
        puts " - File: " + fileName + " has been generated correctly."
        if (bFile['minify'] && $pmBuildMode == 'production')
            write_path += 'min/'
            Dir.mkdir(write_path) unless File.exists?(write_path)
            fileName = write_path + name + '.min.' + extension
            File.open(fileName, 'w+') do |write_file|
                write_file.write Closure::Compiler.new.compress(buffer_file)
            end
            puts " - File: " + fileName + " has been generated correctly."
        end
        if bFile['copy_extra']
            copy_path = bFile['copy_extra']['target_path']
            Dir.mkdir(copy_path) unless File.exists?(copy_path)
            paths = bFile['copy_extra']['paths']
            paths.each do |path|
                system "cp -r " + path + " " + copy_path + "."
            end
        end
    end
    #puts "DONE"
end

desc "Generates the stylesheet"
task :css => [:required] do
    Rake::Task['compileTheme'].invoke("themes/default")
end

task :doc => [:required] do
    appName = getAppName
    version = getVersion
    docConfig = File.read 'config/docs.json'
    docs = JSON.parse(docConfig)
    docs.each do |doc_file|
        doc_list = ""
        doc_target = doc_file['build_dir']
        src_dir = doc_file['src_dir']
        example_template = doc_file['live_example_template']
        files = doc_file['files']
        files.each do |file|
            doc_list += " " + file
        end
        system "jsduck --eg-iframe " + example_template + " -o " + doc_target + doc_list
        Dir.mkdir(doc_target + '/' + appName) unless File.exists?(doc_target + '/' + appName)
        system "cp -rf build/js/ " + doc_target + "/" + appName + "/js"
        system "cp -rf build/css/ " + doc_target + "/" + appName + "/css"
        system "cp -rf build/img/ " + doc_target + "/" + appName + "/img"
        system "cp -rf libraries/ " + doc_target + "/" + appName + "/libraries"
        html = File.read doc_target + '/eg-iframe.html'
        while html['##VERSION##'] do
            html['##VERSION##'] = version
        end
        while html['##APPNAME##'] do
            html['##APPNAME##'] = appName
        end
        while html['##THEME##'] do
            html['##THEME##'] = 'default'
        end
        while html['##THEMECSS##'] do
            html['##THEMECSS##'] = 'pmui-default.css'
        end
        File.open(doc_target + '/eg-iframe.html', 'w+') do |file|
            file.write html
        end
    end
end

task :package => [:required] do
    zip_config = File.read 'config/dist.json'
    zips = JSON.parse(zip_config)
    zips.each do |zip|
        zip_path = zip['copy_to']
        fileName = zip['fileName']
        type = zip['type']
        version = getVersion
        archive = zip_path + fileName + "-" + version + "-" + type + ".zip";
        FileUtils.rm archive, :force=>true
        Zip::File.open(archive, Zip::File::CREATE) do |zipfile|
            files = zip['files']
            files.each do |file|
                if File.directory?(file)
                    Find.find(file) do |path|
                        Find.prune if File.basename(path)[0] == ?.
                        begin
                            zipfile.add(path, path) #if dest
                        rescue Zip::ZipEntryExistsError
                        end
                    end
                else
                    add_files = FileList.new(file)
                    add_files.each do |afile|
                        zipfile.add(afile,afile)
                    end
                end
            end
            puts "File: " + archive + " has been created correctly!"
        end
    end
end

task :violations do
  Dir.mkdir('violations') unless File.exists?('violations')
  Dir.mkdir('violations/js') unless File.exists?('violations/js')
  Dir.mkdir('violations/css') unless File.exists?('violations/css')
  system "nodelint build/js/*.js --config config/jslint/jslint.js --reporter=xml > violations/js/jslint.xml"
  system "csslint build/css/*.css --format=checkstyle-xml --ignore=box-sizing,fallback-colors > violations/css/checkstyle.xml"
  system "csslint build/css/*.css --format=lint-xml --ignore=box-sizing,fallback-colors > violations/css/csslint.xml"
end

desc  "Copy libraries to the Michelangelo project"
task :mafe do
    copy_path = "../michelangelofe/lib/pmUI/"
    system "rm -rf ../michelangelofe/lib/pmUI/"
    Dir.mkdir("../michelangelofe/lib/pmUI/") unless File.exist?('../michelangelofe/lib/pmUI/')
    system "cp -r build/js/pmui-1.0.0.js " + copy_path + "."
    system "cp -r build/css/pmui-1.0.0.css " + copy_path + "."
    puts "Files: pmui-1.0.0.js and pmui-1.0.0.css have been copied correctly"

    copy_path = "../michelangelofe/lib/restclient/"
    system "rm -rf ../michelangelofe/lib/restclient/"
    Dir.mkdir("../michelangelofe/lib/restclient/") unless File.exist?('../michelangelofe/lib/restclient/')
    system "cp -r libraries/restclient/restclient-min.js " + copy_path + "."
    system "cp -r libraries/restclient/restclient.js " + copy_path + "."
    puts "Files: restclient-min.js and restclient.js have been copied correctly"
end

task :jasmine_ci do
    puts "Generating the spec file..."
    system "rm -rf spec"
    Dir.mkdir('spec') unless File.exists?('spec')
    buffer_file = ""
    specConfig = File.read "config/spec.json"
    specConfig = JSON.parse specConfig
    base_file = File.read specConfig["base_file"]

    filename = specConfig["name"]
    files = specConfig["files"]
    files.each do |file|
        buffer_file += File.read file
        buffer_file += "\n"
    end

    replace_pattern = specConfig["pattern"]
    buffer_file = base_file.gsub(replace_pattern, buffer_file)
    File.open(specConfig["destination"] + "/" + filename, 'w+') do |write_file|
        write_file.write buffer_file
    end
    puts "DONE"
    puts "Starting JASMINE testing..."
    system "jasmine-node spec/ --junitreport"
    puts "JASMINE testing...DONE"
end

desc "Run Jasmine Tests"
task :jasmine, [:spec] => [:required] do |t, args|
    puts "Generating the spec file..."
    system "rm -rf spec"
    Dir.mkdir('spec') unless File.exists?('spec')
    buffer_file = ""
    specConfig = File.read "config/spec.json"
    specConfig = JSON.parse specConfig
    base_file = File.read specConfig["base_file"]
    if(args['spec'])
        filename = args["spec"] + ".spec.js"
        buffer_file += File.read "src/spec/" + args["spec"] + ".spec.js"
    else
        filename = specConfig["name"]
        files = specConfig["files"]
        files.each do |file|
            buffer_file += File.read file
            buffer_file += "\n"
        end
    end
    replace_pattern = specConfig["pattern"]
    buffer_file = base_file.gsub(replace_pattern, buffer_file)
    File.open(specConfig["destination"] + "/" + filename, 'w+') do |write_file|
        write_file.write buffer_file
    end
    puts "DONE"
    system "jasmine-node --matchall --verbose spec"
end

task :build_ci do
    Rake::Task['required'].execute
    Rake::Task['dir'].execute
    Rake::Task['js'].execute
    Rake::Task['css'].execute
    Rake::Task['example'].execute
    Rake::Task['doc'].execute
    Rake::Task['package'].execute
    Rake::Task['jasmine_ci'].execute
    Rake::Task['violations'].execute
end

desc "Set the library's version"
task :version, :version do |t,args|
    if (args['version'])
        File.open('VERSION.txt', 'w+') do |file|
            file.write args['version']
        end
    end
end

desc "Generate PMUI Files"
task :files do
    Rake::Task['required'].execute
    Rake::Task['dir'].execute
    Rake::Task['js'].execute
    Rake::Task['css'].execute
end

desc "Generate PMUI Documentation"
task :docs =>[:required] do
    #Rake::Task['required'].execute
    #Rake::Task['dir'].execute
    Dir.mkdir('docs') unless File.exists?('docs')
    Rake::Task['doc'].execute
end

desc "Build the PMUI example"
task :example, [:theme] =>[:required, :js] do |t, args|
    args.with_defaults(:theme =>'default')
    theme = args['theme']
    themecss = 'pmui-' + theme + '.css'
    exampleConfig = File.read 'config/example.json'
    exampleArray = JSON.parse exampleConfig
    exampleArray.each do |exampleFiles|
        directory = exampleFiles["copy_to"]
        from = exampleFiles["copy_from"]
        version = getVersion
        appName = getAppName
        exampleFiles["files"].each do |file|
            html = File.read from + "/" + file
            while html['##VERSION##'] do
                html['##VERSION##'] = version
            end
            while html['##APPNAME##'] do
                html['##APPNAME##'] = appName
            end
            while html['##THEME##'] do
                html['##THEME##'] = theme
            end
            while html['##THEMECSS##'] do
                html['##THEMECSS##'] = themecss
            end
            File.open(directory + "/" + file, 'w+') do |file|
                file.write html
            end
        end
        system "cp -rf img/* build/img"
    end
    puts "DONE"
    puts "copying all the font files to the build's fonts folder..."
    configFile = File.read 'config/fonts.json'
    configFile = JSON.parse configFile
    Dir.mkdir('build/css/fonts') unless File.exists?('build/css/fonts')
    configFile.each do |fontFamily|
        fontFolder = fontFamily['font_folder']
        destinyFolder = 'build/css/fonts/' + fontFolder
        Dir.mkdir(destinyFolder) unless File.exists?(destinyFolder)
        system "cp -rf fonts/" + fontFolder + "/* build/css/fonts/" + fontFolder + "/" 
    end
    puts "DONE"
end

desc "Build PMUI library"
task :build, :version do |t, args|
    if args['version']
        Rake::Task['version'].invoke(args['version'])
    end
    Rake::Task['required'].execute
    Rake::Task['dir'].execute
    Rake::Task['js'].execute
    Rake::Task['css'].execute
    Rake::Task['example'].execute
    #Rake::Task['doc'].execute
    Rake::Task['package'].execute
    #Rake::Task['fonts'].execute
    puts "PMUI " + getVersion + " has been build correctly."
end

desc "Default Task - Build Library"
task :default do
  Rake::Task['build'].execute
end

desc "Run JSLint tests"
task :jslint, :js do |t, args|
    if args['file']
        system "find src/ -name \"" + args['file'] + "\" -print0 | xargs -0 jslint --sloppy"
    else
        system "find src/ -name \"*.js\" -print0 | xargs -0 jslint --sloppy"
    end
end

task :get_version do
    puts "The version is: " + getVersion
end

desc "Generates a list of the translatable labels"
task :labels => [:js] do    
    puts "Searching for translatable strings..."
    labelsList = {}
    file = File.read 'build/js/pmui-' + getVersion + '.js'
    labels = file.scan(/'.[^;']+(?=\'.translate\()|".[^;"]+(?=\".translate\()/)
    labels.each do |label|
        labelsList[label.slice(1, label.length)] = ''
    end
    buffer_file = "<?php" + "\n"
    buffer_file += "$labels = [" + "\n";
    i = 0;
    labelsList.each_key do |key|
        buffer_file += "\t" + '"' + key + '",' + "\n"
        i += 1
    end
    if i > 0
        buffer_file = buffer_file.slice(0, buffer_file.length - 2);
    end
    buffer_file += "\n" + "];" + "\n";
    buffer_file += "?>";
    File.open('labels.php', 'w+') do |write_file|
        write_file.write buffer_file
    end
    puts "DONE, #{i} label(s) found!"
end

desc "Creates a new theme for PMUI, it receives the following parameters:\n * themeName: the name for the theme.\n * [themeDir]: the directory in which the theme will be created. If it doesn't supplied then it will be created inside the pmUI's project directory.\n * [force] (default value: false): specifies if the theme will be created even if the directory supplied already exists, use true for create, any other value for skip creation. NOTE: use this parameter with caution."
task :createTheme, [:themeName, :themeDir, :forceCreation] => [:required] do |t, args|
    currentDir = Dir.pwd
    if currentDir[-1] === '/'
        currentDir[0, currentDir.length - 1]
    end
    themeName = args['themeName']
    themeDir = args['themeDir']
    force = args["forceCreation"] === 'true' ? true : false
    if !themeName
        raise "You must specify a name for your new theme"
    end
    if !themeDir
        themeDir = 'themes/'
    else
        themeDir = themeDir.strip
        while(themeDir[-1] === '/')
            themeDir = themeDir[0, themeDir.length - 1]
        end
        if !File.directory?(themeDir)
            raise "The destination folder for the theme doesn't exists."
        else
            if themeDir[0] != '/'
                raise "The directory for the new theme must be specified by supplying an absolute path for it"
            end
            if themeDir[0,  currentDir.length] === currentDir
                raise "You can't create a theme inside the pmUI project when you're supplying a directory." + " To create a theme inside the pmUI project please don't supply any directory."
            end
        end
    end
    themeDir = themeDir + "/" + themeName

    if File.directory?(themeDir)   #Dir.exists?(themeDir)
        if force
            system "rm -rf " + themeDir
        else
            raise "A theme with that name already exists! Please choose another name for it or change the directory for the new theme."
        end
    end
    #If the validations are passed we create the directory structure
    Dir.mkdir(themeDir)
    Dir.mkdir(themeDir + '/fonts')
    Dir.mkdir(themeDir + '/images')
    Dir.mkdir(themeDir + '/pmui-sprite')
    Dir.mkdir(themeDir + '/var')

    jsonConfigFile = {
        "theme_name" => themeName
    };
    File.open(themeDir + '/config.json', 'w') do |f|
        f.write(jsonConfigFile.to_json)
    end

    puts themeName + " theme created success"
end

desc "Compiles a PMUI theme, it receives an argument: the absolute path to the theme directory, but if you are attempting to compile a theme that resides inside the pmUI's project directory you must specify only the theme's directory name."
task :compileTheme, [:themeName] => [:required] do |t, args|
    themeName = args['themeName']

    if themeName.nil? || themeName.strip == ''
        raise "Error: Argument not supplied."
    end

    theme_dir = File.expand_path(themeName)
    configRBFile = theme_dir + "/config.rb"

    currentDir = Dir.pwd
    if theme_dir[0, currentDir.length] === currentDir
        configRBFile = "config.rb"
    end

    while theme_dir[-1] === '/'
        theme_dir = theme_dir[0, theme_dir.length - 1]
    end

    if !File.exists?(theme_dir + "/config.json")
        raise "Error: the config file for the theme can't be found on " + theme_dir + "/config.json"
    end

    configFile = File.read theme_dir + "/config.json"
    configFile = JSON.parse configFile
    themeName = configFile["theme_name"]
    themeName = themeName.strip

    if !themeName || themeName === ""
        raise "theme_name setting not found in config file " + theme_dir + "/config.json"
    end

    if !File.directory?(theme_dir) #!Dir.exists?(theme_dir)
        raise "The folder for the " + themeName + " theme wasn't found. (" + theme_dir + ")"
    end
    puts "Compiling theme " + theme_dir + " ..."
    if(theme_dir[-1] != '/')
        theme_dir += "/"
    end
    begin
        includes = "";

        system "rm -rf " + theme_dir + "build/"
        Dir.mkdir(theme_dir + "build")
        Dir.mkdir(theme_dir + "build/images")
        Dir.mkdir(theme_dir + "build/images/pmui-sprite")

        var_entries = Dir.entries(theme_dir + "var/");
        var_entries.each do |entry|
            if(!File.directory?(entry))
                includes += (/^_\w+.scss$/ === entry) ? "@import \"var/#{entry}\";\n" : '';
            end
        end
        base_file = File.read "./sass/_pmui-theme-base.scss"
        includes = base_file.gsub('###USER_VARS###', includes)
        File.open("./sass/pmui-" + themeName + ".scss", 'w+') do |write_file|
            write_file.write includes
        end

        system "cp -rf ./sass/images/* " + theme_dir + "build/images"
        system "cp " + theme_dir + "pmui-sprite/* " + theme_dir + "build/images/pmui-sprite"

        command = 'compass compile'
        command += ' --sass-dir "./sass/"'
        command += ' --css-dir "' + theme_dir + 'build"'
        command += ' --fonts-dir "' + theme_dir + 'fonts"'
        command += ' --images-dir "' + theme_dir + 'build/images/"'
        command += ' --import-path "' + theme_dir + '"'
        command += ' --config "' + configRBFile + '" '
        command += ' --force'
        system command
    rescue
        system "rm -rf " + theme_dir + "build/*"        
    ensure
        system "rm -f ./sass/pmui-" + themeName + ".scss"
        system "rm -rf " + theme_dir + "build/images/pmui-sprite"
    end
    puts "Theme building finished!"
end

task :version do
    puts getVersion
end

task :log do
    puts getLog
end

def getVersion
    if File.exists?'VERSION.txt'
        version = File.read 'VERSION.txt'
    else
        hash = `git rev-parse HEAD`[0..6]
        branch = `git branch`
        branch = branch[2..(branch.length-2)]
        version = branch+"."+hash
    end

    return version
end

def getLog
    output = `git log --pretty='[%cr] %h %d %s <%an>'  --since=8.weeks --no-merges`
    return output
end

def getAppName
    appname = File.read 'APP.txt'
    return appname
    exit
end
