(function () {
    /**
     * @class PMUI.grid.GridPanel
     * Class to display data in a grid format.
     * @extends PMUI.core.Container
     *

     *
     * Single grid example:

     *
     *     @example
     *     var g;

     *
     *     $(function(){
     *         g = new PMUI.grid.GridPanel({
     *             pageSize: 6,
     *             columns:[
     *                 {
     *                     title:'columna1',
     *                     dataType:'string',
     *                     columnData: "name"
     *                 },
     *                 {
     *                     title:'columna2',
     *                     dataType:'number',
     *                     width : 150,
     *                     columnData: "lastName",
     *                     sortable: true
     *                 },
     *                 {
     *                     title:'columna3',
     *                     dataType:'button',
     *                     width : "200px",
     *                     onButtonClick: function(row, grid) {
     *                         console.log(row, grid);
     *                     }
     *                 }
     *             ],
     *             dataItems: [
     *                 {
     *                     name: "Daniel",
     *                     lastName: "Canedo"
     *                 }, {
     *                     name: "John",
     *                     lastName: "McAllister"
     *                 }, {
     *                     name: "Ximena",
     *                     lastName: "Jimenez"
     *                 }, {
     *                     name: "Fernando",
     *                     lastName: "Fernandez"
     *                 }, {
     *                     name: "Rodrigo",
     *                     lastName: "Rodriguez"
     *                 }, {
     *                     name: "Andrea",
     *                     lastName: "Postigo"
     *                 }, {
     *                     name: "Hernando",
     *                     lastName: "Hernandez"
     *                 }, {
     *                     name: "Ramiro",
     *                     lastName: "Ramirez"
     *                 }, {
     *                     name: "Domingo",
     *                     lastName: "Dominguez"
     *                 }, {
     *                     name: "Gonzalo",
     *                     lastName: "Gonzales"
     *                 }

     *             ]                       
     *         });
     *         document.body.appendChild(g.getHTML());
     *         g.defineEvents();
     *     });
     *
     * Example with two grids with interchangeable rows.
     *
     *      @example
     *      var g, g2;

     *
     *      $(function(){
     *          g2 = new PMUI.grid.GridPanel({
     *              behavior: 'dragdropsort',
     *              pageSize: 0,
     *              columns: [
     *                  {
     *                      title: 'lastName',
     *                      columnData: 'lastName',
     *                      sortable: true
     *                  }
     *              ],
     *              dataItems: [
     *                  {
     *                      name: "Ramiro",
     *                      lastName: "Ramirez"
     *                  }, {
     *                      name: "Pedro",
     *                      lastName: "Pedraza"
     *                  }, {
     *                      name: "Domingo",
     *                      lastName: "Dominguez"
     *                  }, {
     *                      name: "Oliva",
     *                      lastName: "Olivares"
     *                  }, {
     *                      name: "Gonzalo",
     *                      lastName: "Gonzales"
     *                  }, {
     *                      name: "Enrique",
     *                      lastName: "Enriquez"
     *                  }
     *              ],
     *             onDrop: function(container, draggableItem) {
     *                 var subject = draggableItem.getData();
     *                 subject = subject.name + " " + subject.lastName;
     *                 console.log(subject + " was dropped on grid 1");
     *             },
     *             onSort: function(container, item, index) {
     *                 var subject = item.getData();
     *                 subject = subject.name + " " + subject.lastName;
     *                 console.log(subject + "'s index has changed to " + index);

     *             }  
     *          });
     *          g = new PMUI.grid.GridPanel({
     *              behavior: 'dragdropsort',
     *              pageSize:6,
     *              columns:[
     *                  {
     *                      title: 'Initials',
     *                      columnData: function(data) {
     *                          return data.name.substr(0, 1) + " " + data.lastName.substr(0, 1);
     *                      }
     *                  },
     *                  {
     *                      title:'Name',
     *                      dataType:'string',
     *                      columnData: "name"
     *                  },
     *                  {
     *                      title:'LastName',
     *                      dataType:'number',
     *                      width : 150,
     *                      columnData: "lastName",
     *                      sortable: true
     *                  },
     *                  {
     *                      title:'columna3',
     *                      dataType:'button',
     *                      width : "200px",
     *                      onButtonClick: function(row, grid) {
     *                          console.log(row, grid);
     *                      }
     *                  }
     *              ],
     *              dataItems: [
     *                  {
     *                      name: "Daniel",
     *                      lastName: "Canedo"
     *                  }, {
     *                      name: "John",
     *                      lastName: "McAllister"
     *                  }, {
     *                      name: "Ximena",
     *                      lastName: "Jimenez"
     *                  }, {
     *                      name: "Fernando",
     *                      lastName: "Fernandez"
     *                  }, {
     *                      name: "Rodrigo",
     *                      lastName: "Rodriguez"
     *                  }, {
     *                      name: "Andrea",
     *                      lastName: "Postigo"
     *                  }, {
     *                      name: "Hernando",
     *                      lastName: "Hernandez"
     *                  }
     *              ],
     *             onDrop: function(container, draggableItem) {
     *                 var subject = draggableItem.getData();
     *                 subject = subject.name + " " + subject.lastName;
     *                 console.log(subject + " was dropped on grid 2");
     *             },
     *             onSort: function(container, item, index) {
     *                 var subject = item.getData();
     *                 subject = subject.name + " " + subject.lastName;
     *                 console.log(subject + "'s index has changed to " + index);

     *             }                     
     *          });
     *          document.body.appendChild(g.getHTML());
     *          document.body.appendChild(g2.getHTML());
     *          g.defineEvents();
     *          g2.defineEvents();
     *      });
     *
     * @constructor
     * Creates a new instance of GridPanel class.
     * @param {Object} [settings=null] A JSON object with the config options.
     *
     * @cfg {Array} [columns=[]] An Array where each element is an object literal or an instance of
     * {@link PMUI.grid.GridPanelColumn}. In case to be an object literal it must have {@link PMUI.grid.GridPanelColumn
     * the config options for GridPanelColumn creation}.
     * @cfg {Array} [items=[]] An Array where each of its elements can be an instance of PMUI.grid.GridPanelRow or a
     * JSON object with the config options to build a new one.
     * @cfg {Array} [dataItems=[]] An Array where each of its elements can be a JSON object with the data available for
     * the item (row) that will be created.
     * @cfg {Number} [pageSize=0] The size for every page in the table, if it is set to 0 (default) then there will no
     * exists pagination.
     * @cfg {Boolean} [draggable=false] If the grid will apply the row dragging behavior.
     * @cfg {Boolean} [filterable=true] If the grid will have the filtering function enabled or not.
     * @cfg {Number} [initialPage=0] The page number to show initially.
     * @cfg {Function} [customStatusBar=null] The function to generate a custom message in the grid's status bar. This
     * function must return a string with the text to be used as the message to be displayed in thegrid's status bar.
     * When the function is called 5 parameters will be sent:
     *
     * - The current page (Number).
     * - The page size (Number).
     * - The number of items (Number).
     * - If a criteria is being applied to the grid (Boolean).
     * - The filter criteria applied to the grid (String).
     *
     * If you need access more grid's properties you can use 'this' to refer the grid since the function is called in
     * the grid's context.
     * @cfg {Object|PMUI.util.Style|null} [rowStyle=null] The style to be applied to the grid's rows. It can be:
     *
     * - An object literal: in this case the object can have the properties:
     *     - cssClasses: an array of strings, each string is a css class that will be applied to each row's html.
     *     - cssProperties: an object literal in which each property is a valid css property and its value is a valid
     *     css value for the respective css property.
     *
     * - An instance of the {@link PMUI.util.Style Style} class.
     * - null, in this case no custom style will be applied to the html of the grid's rows.
     *
     * @cfg {String} [filterPlaceholder] Sets the placeholder for the filter textbox.
     * @cfg {Boolean} [visibleHeaders=true] Turns on/off the visibility of the column headers.
     * @cfg {String} [sortableItems='> tr'] Specifies which items inside the element should be sortable in case a
     * sort behavior is applied. It must be a jQuery selector.
     * @cfg {Function|null} [onEmpty=null] A callback function to be called everytime the grid's last item is removed.
     * The value for this config option can be a function or the null constant for no callback execution. For more
     * info about the paramerters sent to the function please read the {@link #event-onEmpty onEmpty} event
     * documentation.
     * @cfg {Function|null} [onRowClick=null] The callback function to be executed everytime the
     * {@link #event-onRowClick onRowClick} event is fired. Read about this event to know about the parameters received
     * by the callback function.
     * @cfg {String|Function|null} [emptyMessage=null] The message to show when the grid is showing no items. It
     * can be:
     *
     * - A String, in this case the string will be displayed in both cases, when the grid has no items and when no
     * grid's item meet the filter.
     * - A Function, in this case the function must return a string or an HTML Element to be displayed. The function
     * will receive two parameters: the {@link PMUI.grid.Grid grid}, a boolean If it is true it means the returned
     * value will be used when a filter is applied, otherwise it means that the returned value will be used when
     * there are no items in the grid.
     * - null, in this case a default message will be used for each situation.
     */
    var GridPanel = function (settings) {
        /**
         * A JSON object that contains the object's DOM components.
         * @type {Object}
         * @private
         */
        this.dom = {};
        /**
         * The grid's list of {@link PMUI.grid.GridPanelColumn column objects}.
         * @type {PMUI.ui.ArrayList}
         * @private
         */
        this.columns = new PMUI.util.ArrayList();
        GridPanel.superclass.call(this, jQuery.extend({
            factory: {
                products: {
                    "GridPanelRow": PMUI.grid.GridPanelRow
                },
                defaultProduct: "GridPanelRow"
            }
        }, settings, {
            elementTag: "div"
        }));
        this.pages = null;
        this.displayedPages = null;
        this.prevText = null;
        this.nextText = null;
        this.invertPageOrder = null;
        this.labelMap = null;
        this.hrefTextPrefix = null;
        this.hrefTextSuffix = null;
        this.nextAtFront = null;
        this.edges = null;
        this.useStartEdge = null;
        this.ellipseText = null;
        this.selectOnClick = null;
        this.useEndEdge = null;
        /**
         * The number of rows to show in every page, 0 means no limit.
         * @type {Number}
         * @readonly
         */
        this.pageSize = null;
        /**
         * If the grid has the filtering functiionality enabled or not.
         * @type {Boolean}
         * @readonly
         */
        this.filterable = null;
        /**
         * The current filter criteria.
         * @type {String}
         * @readonly
         */
        this.filterCriteria = null;
        /**
         * The current page being displayed.
         * @type {Boolean}
         * @readonly
         */
        this.currentPage = null;
        /**
         * If the column sorting functionloty is enabled or not.
         * @type {Boolean}
         * @readonly
         */
        this.sortable = null;
        /**
         * The {@link PMUI.control.TextControl TextControl} object that is playing the filter text input control role.
         * @type {PMUI.control.TextControl}
         * @private
         */
        this.filterControl = null;
        /**
         * The {@link PMUI.ui.Button Button} to executes the filtering.
         * @type {[type]}
         */
        this.filterButton = null;
        /**
         * An {@link PMUI.util.ArrayList ArrayList} that contains the rows ordered by the
         * {@link #property-filterCriteria filterCriteria}.
         * @type {PMUI.util.ArrayList}
         * @private
         */
        this.usableItemsList = null;
        /**
         * An {@link PMUI.util.ArrayList ArrayList} that contains only the rows that meet the filter criteria.
         * @type {PMUI.util.ArrayList}
         * @private
         */
        this.filteredItems = null;
        /**
         * @property {Function} statusBarHandler The function to generate a custom message in the grid's status bar.
         */
        this.customStatusBar = null;
        /**
         * The style to be applied to all the rows in the grid.
         * @type {PMUI.util.Style}
         * @private
         */
        this.rowStyle = null;
        /**
         * The placeholder to be used in the text control for filtering. It is set by the
         * {@link #cfg-filterPlaceholder filterPlaceholder} config option and the
         * {@link #method-setFilterPlaceholer setFilterPlaceholer()} method.
         * @type {String}
         */
        this.filterPlaceholder = null;
        /**
         * The data about the sorting that is currently applied to the grid.
         * @type {Object}
         */
        this.sortingData = null;
        /**
         * @event onRowClick
         * Fired everytime a grid's row is clicked.
         * @param {PMUI.grid.GridPanelRow} row The row object the click was performed on.
         * @param {Object} data The row's data
         */
        this.onRowClick = null;
        /**
         * @event onEmpty
         * Fired everytime the Grid removes its last item.
         * @param {PMUI.grid.GridPanel} grid The GridPanel object.
         */
        this.onEmpty = null;
        /**
         * If the column headers are visible or not, it is set by the {@link #cfg-visibleHeaders visibleHeaders} config
         * option and the {@link #method-hideHeaders hideHeaders()} and {@link #method-showHeaders showHeaders()}.
         * @type {Boolean}
         */
        this.visibleHeaders = null;
        /**
         * [selectedRow description]
         * @type {[type]}
         */
        this.selectedRow = null;
        /**
         * [selectable description]
         * @type {[type]}
         */
        this.selectable = null;
        /**
         * If the footer is visible or not.
         * @type {Boolean}
         */
        this.visibleFooter = null;
        /**
         * The message to be displayed in the grid when there are no items to display. Set by the
         * {@link #cfg-emptyMessage emptyMessage config option} and the
         * {@link #method-setEmptyMessage setEmptyMessage()} method.
         * @type {String|Function}
         */
        this.emptyMessage = null;
        this._dynamicLoad = null;
        this.keys = null;
        this.customDataRest;
        /**
         * Height for the HTML element tableContainer, it can be a number or a string with the following format:
         ##px when ## is a number.
         * @type {Number|String}
         * @readonly
         */
        this.tableContainerHeight = "";
        GridPanel.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom("PMUI.core.Container", GridPanel);
    /**
     * The object type.
     * @type {String}
     */
    GridPanel.prototype.type = "GridPanel";
    /**
     * The object's family.
     * @type {String}
     */
    GridPanel.prototype.family = "GridPanel";
    /**
     * Initialize the object.
     * @param  {Object} settings A JSON object with the setting options.
     * @private
     */
    GridPanel.prototype.init = function (settings) {
        var defaults = {
            columns: [],
            items: [],
            dataItems: null,
            pages: 0,
            displayedPages: 3,
            prevText: 'Prev'.translate(),
            nextText: 'Next'.translate(),
            invertPageOrder: false,
            labelMap: [],
            hrefTextPrefix: '#page-',
            hrefTextSuffix: '',
            nextAtFront: false,
            edges: 2,
            useStartEdge: true,
            ellipseText: '&hellip;',
            selectOnClick: true,
            useEndEdge: true,
            pageSize: 0,
            filterable: true,
            inititalPage: 0,
            customStatusBar: null,
            rowStyle: null,
            visibleHeaders: true,
            filterPlaceholder: "",
            onRowClick: null,
            sortableItems: '> tr:not(.pmui-nodrag)',
            onEmpty: null,
            emptyMessage: null,
            selectable: false,
            visibleFooter: true,
            dynamicLoad: false,
            keys: {},
            tableContainerHeight: "auto"
        };

        jQuery.extend(true, defaults, settings);
        this.totalRows = 0;

        this.usableItemsList = new PMUI.util.ArrayList();
        this.sortingData = {
            criteria: null,
            type: null
        };
        this.rowStyle = new PMUI.util.Style();
        this.filterControl = new PMUI.control.TextControl({
            onKeyUp: this.onFilterControlChangeHandler(),
            style: {
                cssClasses: [
                    "pmui-gridpanel-search"
                ]
            }
        });
        this.filterButton = new PMUI.ui.Button({
            text: 'Search',
            style: {
                cssClasses: [
                    "pmui-gridpanel-buttonsearch"
                ]
            }
        });
        this.filteredItems = new PMUI.util.ArrayList();

        this.setPageSize(defaults.pageSize)
            .setCurrentPage(defaults.inititalPage);

        this.setSortableItems(defaults.sortableItems)
            .setSelectable(defaults.selectable)
            .setEmptyMessage(defaults.emptyMessage)
            .setOnEmptyHandler(defaults.onEmpty)
            .setRowStyle(defaults.rowStyle)
            .setColumns(defaults.columns)
            .setCustomStatusBar(defaults.customStatusBar)
            .setFilterPlaceholder(defaults.filterPlaceholder)
            .setOnRowClick(defaults.onRowClick)
            .setVisibleFooter(defaults.visibleFooter)
            .setDisplayedPages(defaults.displayedPages)
            .setPages(defaults.pages)
            .setPrevText(defaults.prevText)
            .setNextText(defaults.nextText)
            .setInvertPageOrder(defaults.invertPageOrder)
            .setLabelMap(defaults.labelMap)
            .setHrefTextPrefix(defaults.hrefTextPrefix)
            .setHrefTextSuffix(defaults.hrefTextSuffix)
            .setNextAtFront(defaults.nextAtFront)
            .setEdgesPagination(defaults.edges)
            .setUseStartEdge(defaults.useStartEdge)
            .setEllipseText(defaults.ellipseText)
            .setSelectOnClick(defaults.selectOnClick)
            .setUseEndEdge(defaults.useEndEdge)
            .setDynamicLoad(defaults.dynamicLoad)
            .setCustomDataRest(defaults.customDataRest)
            .setTableContainerHeight(defaults.tableContainerHeight);

        if (jQuery.isArray(defaults.dataItems)) {
            this.setDataItems(defaults.dataItems);
        } else {
            this.setItems(defaults.items);
        }


        if (defaults.filterable) {
            this.enableFiltering();
        } else {
            this.disableFiltering();
        }
        if (defaults.visibleHeaders) {
            this.showHeaders();
        } else {
            this.hideHeaders();
        }
        this.updateUsableItemsList();
    };
    /**
     * Set the height for the HTML element tableContainer
     * @param {Number|String} height it can be a number or a string.
     * In case of using a String you only can use 'auto' or 'inherit' or ##px or ##% or ##em when ## is a number.
     * @chainable
     */
    GridPanel.prototype.setTableContainerHeight = function(height){
        if (typeof height === 'number') {
            height = height+"px";
        } else if (/^\d+(\.\d+)?px$/.test(height)) {
            height = height;
        } else if (/^\d+(\.\d+)?%$/.test(height)) {
            height = height;
        } else if (/^\d+(\.\d+)?em$/.test(height)) {
            height = height;
        } else if (height === 'auto' || height === 'inherit') {
            height = height;
        } else {
            throw new Error('setHeight: height param is not a number');
        }
        this.tableContainerHeight = height;
        return this;
    };

    GridPanel.prototype.setCustomDataRest = function (handlder) {
        if (typeof handlder === "function") {
            this.customDataRest = handlder;
        }
        return this;
    };

    GridPanel.prototype.setKeys = function (keys) {
        if (!(keys.server && keys.projectID && keys.workspace && keys.accessToken && keys.endPoint)) {
            throw new Error('setKeys(): The parameter, in case of being an object, must define server, projectId, authBearer, URL, pageParam, totalPages;');
        }
        this.keys = keys || {};
        this.url = keys.server;
        this.url = this.url + "/api/1.0/" + keys.workspace + "/";
        this.url = this.url + keys.endPoint;
        return this;
    };
    GridPanel.prototype.setDynamicLoad = function (dynamicLoad) {
        this._dynamicLoad = !!dynamicLoad ? dynamicLoad : false;
        if (this._dynamicLoad.hasOwnProperty("keys")) {
            this.setKeys(this._dynamicLoad["keys"]);
        }
        return this;
    };
    /**
     * @inherirdoc
     */
    GridPanel.prototype.setDataItems = function (dataItems) {
        if (this.usableItemsList) {
            GridPanel.superclass.prototype.setDataItems.call(this, dataItems);
        }
        return this;
    };
    /**
     * @inheritdoc
     */
    GridPanel.prototype.setItems = function (items) {
        if (this.usableItemsList) {
            GridPanel.superclass.prototype.setItems.call(this, items);
        }
        return this;
    };
    /**
     * Sets the message to display when there are no items to display in the grid.
     * @param {String|Function|null} emptyMessage It can be:
     *
     * - A String, in this case the string will be displayed in both cases, when the grid has no items and when no
     * grid's item meet the filter.
     * - A Function, in this case the function must return a string or an HTML Element to be displayed. The function
     * will receive two parameters: the {@link PMUI.grid.Grid grid}, a boolean If it is true it means the returned
     * value will be used when a filter is applied, otherwise it means that the returned value will be used when
     * there are no items in the grid.
     * - null, in this case a default message will be used for each situation.
     * @chainable
     */
    GridPanel.prototype.setEmptyMessage = function (emptyMessage) {
        if (!(emptyMessage === null || typeof emptyMessage === 'string' || typeof emptyMessage === 'function')) {
            throw new Error("setEmptyMessage(): the parameter must be a string, a function or null.");
        }
        this.emptyMessage = emptyMessage;
        return this;
    };
    /**
     * Disables the behaviors and column's buttons.
     * @chainable
     */
    GridPanel.prototype.disable = function () {
        this.disableColumn();
        return GridPanel.superclass.prototype.disable.call(this);
    };
    /**
     * Enables the behaviors and column's buttons.
     * @chainable
     */
    GridPanel.prototype.enable = function () {
        this.enableColumn();
        return GridPanel.superclass.prototype.enable.call(this);
    };
    /**
     * @inheritdoc
     */
    GridPanel.prototype.getBehavioralItems = function () {
        var items = [],
            initialIndex = this.currentPage * this.pageSize,
            finalIndex = initialIndex + this.pageSize,
            i;

        for (i = initialIndex; i < finalIndex && this.getItem(i); i += 1) {
            items.push(this.getItem(i));
        }
        return items;
    };
    /**
     * Enables one or all the columns.
     * @param {String|Number|PMUI.grid.GridPanelColumn} [column] A criteria to select the column that will be
     * enabled. It supports the following data types:
     *
     * - String, in this case the parameter is used as the column's id.
     * - Number, in this case the parameter is used as the column's index.
     * - An instance of {@link PMUI.grid.GridPanelColumn GridPanelColumn}, in this case the supplied object must be
     * a child of the grid.
     * - [no value], since this parameter is optional, you can skip it. In this case all the columns in the grid will
     * be enabled.
     * @chainable
     */
    GridPanel.prototype.enableColumn = function (column) {
        var i,
            targetColumn = this.getColumns(column);

        for (i = 0; i < targetColumn.length; i += 1) {
            if (targetColumn[i].dataType === 'button') {
                targetColumn[i].enable();
            }
        }
        this.disabled = false;
        this.style.removeClasses(['pmui-disabled']);
        return this;
    };
    /**
     * Disables one or all the columns.
     * @param {String|Number|PMUI.grid.GridPanelColumn} [column] A criteria to select the column that will be
     * disabled. It supports the following data types:
     *
     * - String, in this case the parameter is used as the column's id.
     * - Number, in this case the parameter is used as the column's index.
     * - An instance of {@link PMUI.grid.GridPanelColumn GridPanelColumn}, in this case the supplied object must be
     * a child of the grid.
     * - [no value], since this parameter is optional, you can skip it. In this case all the columns in the grid will
     * be disabled.
     * @chainable
     */
    GridPanel.prototype.disableColumn = function (column) {
        var i,
            targetColumn = this.getColumns(column);

        for (i = 0; i < targetColumn.length; i += 1) {
            if (targetColumn[i].dataType === 'button') {
                targetColumn[i].disable();
            }
        }
        return this;
    };
    /**
     * Sets the width to one or all the columns.
     * @param {Number|String} width  The width to be applied to the column(s).
     * @param {String|Number|PMUI.grid.GridPanelColumn} [column] A criteria to select the column the width will be
     * applied. It supports the following data types:
     *
     * - String, in this case the parameter is used as the column's id.
     * - Number, in this case the parameter is used as the column's index.
     * - An instance of {@link PMUI.grid.GridPanelColumn GridPanelColumn}, in this case the supplied object must be
     * a child of the grid.
     * - [no value], since this parameter is optional, you can skip it. In this case the width will be applied to all
     * the columns in the grid.
     * @chainable
     */
    GridPanel.prototype.setColumnWidth = function (width, column) {
        var targetColumn = [],
            aux,
            i;

        if (typeof column === 'string') {
            aux = this.columns.find("id", column);
        } else if (typeof column === 'number') {
            aux = this.columns.get(column);
        } else if (column instanceof PMUI.grid.GridPanelColumn && this.isDirectParentOf(column)) {
            targetColumn.push(column);
        } else if (column === undefined) {
            targetColumn = this.column.asArray();
        }
        if (aux) {
            targetColumn.push(aux);
        }
        for (i = 0; i < targetColumn.length; i += 1) {
            targetColumn[i].setWidth(width);
        }
        if (i > 0) {
            this.goToPage(this.currentPage);
        }
        return this;
    };
    /**
     * Sets the function callback to be called everytime the grid's last item is removed.
     * @param {Function|null} handler The callback function or the null constant for no callback execution. For more
     * info about the paramerters sent to the function please read the {@link #event-onEmpty onEmpty} event
     * documentation.
     * @chainable
     */
    GridPanel.prototype.setOnEmptyHandler = function (handler) {
        if (!(typeof handler === 'function' || handler === null)) {
            throw new Error('setOnEmptyHandler(): The parameter must be a function or null.');
        }
        this.onEmpty = handler;
        return this;
    };
    /**
     * Shows the column headers.
     * @chainable
     */
    GridPanel.prototype.showHeaders = function () {
        this.visibleHeaders = true;
        if (this.html) {
            this.dom.thead.style.display = '';
        }
        return this;
    };
    /**
     * Hide the column headers.
     * @chainable
     */
    GridPanel.prototype.hideHeaders = function () {
        this.visibleHeaders = false;
        if (this.html) {
            this.dom.thead.style.display = 'none';
        }
        return this;
    };
    /**
     * Updates the usable items list.
     * @chainable
     * @private
     */
    GridPanel.prototype.updateUsableItemsList = function () {
        if (this.usableItemsList) {
            this.usableItemsList.set(this.items.asArray());
        }
        return this;
    };
    /**
     * Sets the placeholder for the filter textbox.
     * @param {String} filterPlaceholder
     * @chainable
     */
    GridPanel.prototype.setFilterPlaceholder = function (filterPlaceholder) {
        if (typeof filterPlaceholder !== 'string') {
            throw new Error('setFilterPlaceholder(): The parameter must be a string.');
        }
        this.filterControl.setPlaceholder(this.filterPlaceholder = filterPlaceholder);
        return this;
    };
    /**
     * Sets the custom style to be applied to the grid's rows.
     * @param {Object|PMUI.util.Style|null} rowStyle It can be an object literal (with the properties
     * cssProperties and/or cssClasses), an instance of PMUI.util.Style or null (in this case no custom style will be
     * applied to the rows).
     */
    GridPanel.prototype.setRowStyle = function (rowStyle) {
        var i,
            rows,
            rowsLength;

        if (rowStyle !== null && typeof rowStyle !== 'object' && !(rowStyle instanceof PMUI.util.Style)) {
            throw new Error("setRowStyle(): The parameter must be null or an object literal or an instance of "
                + "PMUI.grid.GridPanelRow.");
        }
        this.rowStyle.clear();
        if (rowStyle instanceof PMUI.util.Style) {
            this.rowStyle = rowStyle;
        } else if (rowStyle) {
            this.rowStyle.addProperties(rowStyle.cssProperties || {})
                .addClasses(rowStyle.cssClasses || []);
        }
        rows = this.items.asArray();
        rowsLength = rows.length;
        for (i = 0; i < rowsLength; i += 1) {
            rows[i].setStyle({
                cssProperties: this.rowStyle.cssProperties,
                cssClasses: this.rowStyle.cssClasses
            });
        }
        return this;
    };
    /**
     * Sets a function to generate a custom message in the grid's status bar.
     * @param {Function|NULL} handler The function for generate the message to be shown in the grid's status bar. It
     * also can be NULL, in this case the message shown in the grid's status bar wiil be the default one.
     * For more info about the sent parameters to the function read the {@link #cfg-customStatusBar customStatusBar}
     * config option.
     */
    GridPanel.prototype.setCustomStatusBar = function (handler) {
        if (!(typeof handler === 'function' || handler === null)) {
            throw new Error('setCustomStatusBar(): this method only accepts as parameter a function or null.');
        }
        this.customStatusBar = handler;
        return this;
    };
    /**
     * Enables the filter functionality.
     * @chainable
     */
    GridPanel.prototype.enableFiltering = function () {
        this.filterable = true;
        if (this.dom.toolbar) {
            this.dom.toolbar.style.display = '';
            if (!this._dynamicLoad) {
                this.goToPage(this.currentPage);
            }

        }
        return this;
    };
    /**
     * Disables the filter functionality.
     * @chainable
     */
    GridPanel.prototype.disableFiltering = function () {
        this.filterable = false;
        if (this.dom.toolbar) {
            this.dom.toolbar.style.display = 'none';
            this.filterCriteria = "";
            this.filterControl.setValue("");
            if (!this._dynamicLoad) {
                this.goToPage(this.currentPage);
            }
        }
        return this;
    };
    /**
     * Returns the index in page for the specified row.
     * @param  {PMUI.grid.GridPanelRow|Number|String} row It can be:
     *
     * - {@link PMUI.grid.GridPanelRow}.
     * - Number, in this case it is interpreted as the global index for the row.
     * - String, it will be interpreted as the row's id.
     * @return {Number}
     */
    GridPanel.prototype.indexInPage = function (row) {
        var globalIndex = this.getItemIndex(row);

        if (this.pageSize !== 0 && globalIndex >= 0) {
            globalIndex = globalIndex % this.pageSize;
        }

        return globalIndex;
    };
    /**
     * Returns the handler to be executed when the filter changes.
     * @return {Function}
     * @private
     */
    GridPanel.prototype.onFilterControlChangeHandler = function () {
        var that = this;

        return function () {
            var nextFilter = this.getHTML().value;

            if (that.filterCriteria !== nextFilter) {
                that.filter(nextFilter);
            }
        };
    };
    /**
     * Sets the columns.
     * @param {Array} columns An Array where each element is an object literal with the
     * {@link PMUI.grid.GridPanelColumn the config options for GridPanelColumn creation} or a
     * {@link PMUI.grid.GridPanelColumn GridPanelColumn object}.
     * @chainable
     */
    GridPanel.prototype.setColumns = function (columns) {
        var i;

        this.clearColumns();
        if (!jQuery.isArray(columns)) {
            throw new Error("setColumns(): The parameter must be an Array");
        }

        for (i = 0; i < columns.length; i += 1) {
            this.addColumn(columns[i]);
        }
        return this;
    };
    /**
     * Returns one or all the grid's columns.
     * @param {String|Number|PMUI.grid.GridPanelColumn} [column] A criteria to select the column that will be
     * returned. It supports the following data types:
     *
     * - String, in this case the parameter is used as the column's id.
     * - Number, in this case the parameter is used as the column's index.
     * - An instance of {@link PMUI.grid.GridPanelColumn GridPanelColumn}, in this case the supplied object must be
     * a child of the grid.
     * - [no value], since this parameter is optional, you can skip it. In this case all the columns in the grid will
     * be returned.
     * @return {Array}
     */
    GridPanel.prototype.getColumns = function (column) {
        var targetColumn = [],
            aux;

        if (typeof column === 'string') {
            aux = this.columns.find("id", column);
        } else if (typeof column === 'number') {
            aux = this.columns.get(column);
        } else if (column instanceof PMUI.grid.GridPanelColumn && this.isDirectParentOf(column)) {
            targetColumn.push(column);
        } else if (column === undefined) {
            targetColumn = this.columns.asArray().slice(0);
        }
        if (aux) {
            targetColumn.push(aux);
        }
        return targetColumn;
    };
    /**
     * Paints a column into the grid's head.
     * @param  {PMUI.grid.GridPanelColumn} column
     * @chainable
     * @private
     */
    GridPanel.prototype.paintColumn = function (column) {
        if (this.dom.thead) {
            this.dom.thead.appendChild(column.getHTML());
        }
        return this;
    };
    GridPanel.prototype.paintColumns = function () {
        var i,
            columns = this.columns.asArray();

        for (i = 0; i < columns.length; i += 1) {
            this.paintColumn(columns[i]);
        }
        return this;
    };
    /**
     * Adds a column.
     * @param {PMUI.grid.GridPanelColumn|JSON} column A {@link PMUI.grid.GridPanelColumn} or a JSON Object with the
     * {@link PMUI.grid.GridPanelColumn#cfg config options} for the new column.
     */
    GridPanel.prototype.addColumn = function (column) {
        var that = this,
            columnToAdd,
            rows,
            i,
            defaults = {
                grid: this,
                title: '[untitled]',
                type: 'string',
                sortable: false,
                searchable: true,
                onSort: function (order) {
                    var criteria = this.columnData;
                    that.sort(criteria, order);
                }
            };

        if (column instanceof PMUI.grid.GridPanelColumn) {
            columnToAdd = column;
            columnToAdd.clearCells();
            columnToAdd.setGrid(this);
        } else if (typeof column === 'object') {
            jQuery.extend(true, defaults, column);
            columnToAdd = new PMUI.grid.GridPanelColumn(defaults);
        } else {
            throw new Error('addColumn(): The method only accepts an object or an instace of PMUI.grid.GridPanelColumn'
                + ' as parameter.');
        }

        //TODO control align and width
        //console.log("\tCOLUMN insert " + columnToAdd.title/*isd*/);
        this.columns.insert(columnToAdd);
        this.paintColumn(columnToAdd);
        rows = this.items.asArray();
        for (i = 0; i < rows.length; i += 1) {
            rows[i].addCell(columnToAdd);
        }

        return this;
    };
    /**
     * Clears all the columns for the grid.
     * @chainable
     */
    GridPanel.prototype.clearColumns = function () {
        while (this.columns.getSize()) {
            this.removeColumn(0);

        }
        return this;
    };
    /**
     * Removes a column and all its cells from the grid.
     * @param  {Number|String|PMUI.grid.GridPanelColumn} column This value is used to determine the row to remove:
     *
     * - String, in this case the string must be the id of a column in the grid.
     * - Number, in this case the number is the index of the column to remove.
     * - a {@link PMUI.grid.GridPanelColumn GridPanelColumn object}, if it's a column that belongs to the grid then
     * this column will be the one to remove.
     * @chainable
     */
    GridPanel.prototype.removeColumn = function (column) {
        var columnToRemove,
            i,
            row,
            size,
            index;

        if (column instanceof PMUI.grid.GridPanelColumn) {
            columnToRemove = column;
        } else if (typeof column === 'number') {
            columnToRemove = this.columns.get(column);
        } else if (typeof column === 'string') {
            columnToRemove = this.columns.find('id', column);
        }

        if (columnToRemove) {
            index = this.columns.indexOf(columnToRemove);
            size = this.items.getSize();
            for (i = 0; i < size; i += 1) {
                row = this.items.get(i);
                row.removeCell(index);
            }
            columnToRemove.grid = null;
            columnToRemove.clearCells();
            this.columns.remove(columnToRemove);
            jQuery(columnToRemove.getHTML()).remove();
        }
        return this;
    };
    /**
     * Adds a new Item
     * @param {Object|PMUI.grid.GridPanelRow} item An instance of {@link PMUi.grid.GridPanelRow GridPanelRow} or an
     * object with the properties to be used to create a new one.
     * @param {Number} [index] The insertion index position.
     */
    GridPanel.prototype.addItem = function (item, index) {
        var itemToBeAdded;

        if (typeof item !== 'object') {
            throw new Error('addItem(): The first parameter must be an object or an instance of PMUI.grid.GridPanel.');
        }
        item.onSelect = this.onRowSelectHandler();
        if (this.factory) {
            itemToBeAdded = this.factory.make(item);
        }
        if (itemToBeAdded && !this.isDirectParentOf(itemToBeAdded)) {
            itemToBeAdded.parent = this;
            itemToBeAdded.style.addClasses(['pmui-' + (this.items.getSize() % 2 ? 'odd' : 'even')]);
            if (itemToBeAdded.html || this.columns.asArray()) {
                itemToBeAdded.setCells();
            }

            if (typeof index === 'number') {
                this.items.insertAt(itemToBeAdded, index);
                if (this.usableItemsList) {
                    this.usableItemsList.insertAt(itemToBeAdded, index);
                }
                if (this.filterCriteria) {
                    this.filter(this.filterCriteria);
                } else {
                    this.goToPage(this.currentPage);
                }
            } else {
                this.items.insert(itemToBeAdded);
                this.usableItemsList.insert(itemToBeAdded);
                if (this.dom.tbody && !this.massiveAction) {
                    this.goToPage(this.currentPage);

                }
            }
            if (this.eventsDefined) {
                itemToBeAdded.defineEvents();
            }
        }
        return this;
    };
    /**
     * [onRowSelectHandler description]
     * @return {[type]} [description]
     */
    GridPanel.prototype.onRowSelectHandler = function () {
        var that = this;
        return function () {
            if (that.selectedRow) {
                that.selectedRow.deselectRow();
            }
            that.selectedRow = this;
        };
    };
    /**
     * Sets the callback function to be executed everytime the {@link #event-onRowClick onRowClick} event is fired.
     * Read about this event to know about the parameters received by the callback function.
     * @param {Function|null} handler
     * @chainable
     */
    GridPanel.prototype.setOnRowClick = function (handler) {
        if (typeof handler !== 'function' && handler !== null) {
            throw new Error("sdfsdfgsd");
        }
        this.onRowClick = handler;
        return this;
    };
    /**
     * Shows an empty row in grid.
     * @chainable
     * @private
     */
    GridPanel.prototype.showEmptyCell = function () {
        var tr,
            td,
            message;

        if (this.dom.tbody) {
            tr = PMUI.createHTMLElement('tr');
            td = PMUI.createHTMLElement('td');
            tr.className = 'pmui-gridpanel-emptyrow pmui-nodrag';
            td.colSpan = this.columns.getSize();
            tr.appendChild(td);
            $(this.dom.tbody).find('.pmui-gridpanel-emptyrow').remove().end().append(tr);
            if (typeof this.emptyMessage === 'function') {
                message = this.emptyMessage(this, !!this.filterCriteria);
            } else if (typeof this.emptyMessage === 'string') {
                message = this.emptyMessage;
            } else {
                if (this.filterCriteria) {
                    message = 'No matches found for \"' + this.filterCriteria + '\"';
                } else {
                    message = 'No records found.';
                }
            }
            if (typeof message === 'string') {
                td.appendChild(document.createTextNode(message));
            } else if (PMUI.isHTMLElement(message)) {
                td.appendChild(message);
            }
            if (this.items.getSize() === 0 && typeof this.onEmpty === 'function') {
                this.onEmpty(this);
            }
        }
        return this;
    };
    /**
     * Removes a row from the object.
     * @param  {PMUI.core.Element|String|Number} item It can be a string (id of the child to remove),
     a number (index of the child to remove) or a {Element} object
     * @chainable
     */
    GridPanel.prototype.removeItem = function (item) {
        var currentPage = this.currentPage;
        if (item.selected) {
            item.deselectRow();
        }

        GridPanel.superclass.prototype.removeItem.call(this, item);

        if (!this.massiveAction) {
            this.updateUsableItemsList();
            if (!this.items.getSize()) {
                this.showEmptyCell();
            }
            if (currentPage >= this.getTotalPages()) {
                currentPage -= 1;
                this.currentPage = currentPage < 0 ? 0 : currentPage;
            }
            if (this.dom.tbody) {
                this.goToPage(this.currentPage);
            }
        }
        return this;
    };
    /**
     * @inheritdoc
     */
    GridPanel.prototype.clearItems = function () {
        var currentPage = this.currentPage;
        GridPanel.superclass.prototype.clearItems.call(this);
        this.updateUsableItemsList();
        if (!this.items.getSize()) {
            this.showEmptyCell();
        }
        if (currentPage >= this.getTotalPages()) {
            currentPage -= 1;
            this.currentPage = currentPage < 0 ? 0 : currentPage;
        }
        if (this.dom.tbody) {
            this.goToPage(this.currentPage);
        }
        return this;
    };
    /**
     * Moves a row from its current position to another.
     * @param  {PMUI.grid.GridPanelRow|Number|String} item  It can be:

     *
     * - An instance of PMUI.grid.GridPanelRow, in this case this object must be a item of the object.
     * - A Number, in this case the number is interpereted as the index of the item to move.
     * - A String, in this case the string is interpreted as the id of the item to move.
     * @param  {Number} index The index in which the item will be moved to.
     * @chainable
     */
    GridPanel.prototype.moveItem = function (item, index) {
        var items = this.items,
            currentIndex,
            referenceObject;

        item = this.getItem(item);

        if (item instanceof PMUI.core.Element && this.isDirectParentOf(item)) {
            currentIndex = items.indexOf(item);
            items = items.asArray();
            referenceObject = this.items.get(index + (currentIndex < index ? 1 : 0));
            item = items.splice(currentIndex, 1)[0];
            if (this.items.indexOf(item) > -1) {
                this.items.remove(item);
            }
            this.items.insertAt(item, index);
            this.updateUsableItemsList();
            if (this.html) {
                if (!referenceObject) {
                    this.goToPage(this.currentPage);
                } else if (index === (this.currentPage * this.pageSize + this.pageSize - 1)
                    || (this.getTotalPages() === (this.currentPage + 1)
                    && index === (this.items.getSize() % this.pageSize) - 1)) {
                    this.containmentArea.appendChild(item.getHTML());
                } else {
                    this.containmentArea.insertBefore(item.getHTML(), referenceObject.getHTML());
                }
            }
        }
        return this;
    };
    /**
     * Returns the data for every rowe in the grid.
     * @return {Array}
     */
    GridPanel.prototype.getData = function () {
        var i,
            items = this.items.asArray(),
            data = [];

        for (i = 0; i < items.length; i += 1) {
            data.push(items[i].getData());
        }

        return data;
    };
    /**
     * @inheritdoc
     */
    GridPanel.prototype.paintItem = function (item, index) {
        var intitalItem = this.currentPage * this.pageSize,
            finalIndex = this.pageSize ? this.currentPage + this.pageSize - 1 : null;

        if (index === undefined) {
            index = this.items.getSize();
        }
        if (index >= initialIndex || (index <= finalIndex || finalIndex === null)) {
            this.goToPage(this.currentPage);
        }
        return this;
    };
    /**
     * @inheritdoc
     */
    GridPanel.prototype.paintItems = function () {
        return this.goToPage(this.currentPage);
    };
    /**
     * Creates the HTML for the Grid.
     * @return {HTMLElement}
     */
    GridPanel.prototype.createHTML = function () {
        var table,
            thead,
            tbody,
            toolbarDiv,
            footer,
            pager,
            statusBar,
            tableContainer,
            pager2;

        if (this.html) {
            return this.html;
        }

        GridPanel.superclass.superclass.prototype.createHTML.call(this);
        table = PMUI.createHTMLElement("table");
        table.className = 'pmui-gridpanel-table';
        table.cellSpacing = 0;
        table.cellPadding = 0;
        thead = PMUI.createHTMLElement("thead");
        tbody = PMUI.createHTMLElement("tbody");
        tableContainer = PMUI.createHTMLElement("div");
        tableContainer.className = "pmui-gridpanel-tableContainer";
        tbody.className = 'pmui-gridpanel-tbody';
        this.containmentArea = tbody;
        tableContainer.style.overflow = 'auto';
        toolbarDiv = PMUI.createHTMLElement("div");
        toolbarDiv.className = 'pmui-gridpanel-toolbar';
        toolbarDiv.appendChild(this.filterControl.getHTML());
        if (this._dynamicLoad) {
            var searchLoad = PMUI.createHTMLElement("span");
            searchLoad.className = "pmui-gridpanel-searchload";
            toolbarDiv.appendChild(searchLoad);
        }
        this.html.appendChild(toolbarDiv);

        this.dom.tableContainer = tableContainer;
        this.dom.thead = thead;
        this.dom.tbody = tbody;
        this.dom.table = table;
        this.dom.toolbar = toolbarDiv;


        table.appendChild(thead);
        table.appendChild(tbody);
        tableContainer.appendChild(table);
        this.html.appendChild(tableContainer);
        pager = PMUI.createHTMLElement('ul');
        pager.className = 'pmui-gridpanel-pager';

        statusBar = PMUI.createHTMLElement('div');
        statusBar.className = 'pmui-gridpanel-statusbar';

        footer = PMUI.createHTMLElement("div");
        footer.className = 'pmui-gridpanel-footer';
        pager2 = PMUI.createHTMLElement("ul");
        pager2.className = 'pmui-gridpanel-pager';
        if (this._dynamicLoad) {
            footer.appendChild(pager);
        } else {
            footer.appendChild(pager2);
        }

        this.html.appendChild(footer);

        this.dom.pager2 = pager2;

        this.dom.pager = pager;
        this.dom.statusBar = statusBar;
        this.dom.footer = footer;

        this.setVisibleFooter(this.visibleFooter);
        this.paintColumns()
            .setHeight(this.height);

        tableContainer.style.height = this.tableContainerHeight;

        this.style.applyStyle();

        if (this.filterable) {
            this.enableFiltering();
        } else {
            this.disableFiltering();
        }
        if (this.visibleHeaders) {
            this.showHeaders();
        } else {
            this.hideHeaders();
        }
        if (!this.items.getSize()) {
            this.showEmptyCell();
        }
        if (this.eventsDefined) {
            this.defineEvents();
        }
        if (this.sortingData.sortingCriteria) {
            this.sort(this.sortingData.criteria, this.sortingData.type);
        }

        return this.html;
    };
    /**
     * [updateStatusBar description]
     * @return {[type]} [description]
     */
    GridPanel.prototype.updateStatusBar = function () {
        var itemsLength,
            filterCriteria,
            b,
            currentPage,
            filtered,
            totalPages;

        filterCriteria = this.filterCriteria;
        currentPage = this.currentPage;
        filtered = !!(filterCriteria && this.filterable);
        itemsLength = filtered ? this.filteredItems.getSize() : this.items.getSize();
        jQuery(this.dom.statusBar).empty();
        totalPages = this.getTotalPages(filtered);
        if (typeof this.customStatusBar === 'function') {
            this.dom.statusBar.textContent =
                this.customStatusBar(currentPage, this.pageSize, itemsLength, filtered, filterCriteria || "");
        } else if (totalPages === 0) {
            b = PMUI.createHTMLElement('b');
            b.appendChild(document.createTextNode(""));
            this.dom.statusBar.appendChild(b);
        } else {
            b = PMUI.createHTMLElement('b');
            b.appendChild(document.createTextNode("PAGE " + (currentPage + 1)));
            this.dom.statusBar.appendChild(b);
            this.dom.statusBar.appendChild(document.createTextNode(" of " + totalPages
                + (filtered ? " filtered pages" : "")));
        }
        return this;
    };

    /**
     * [param] this-GridPanel
     * @return Object
     */
    GridPanel.prototype._getInterval = function (that) {
        return {
            start: Math.ceil(that.currentPage > that.halfDisplayed ? Math.max(Math.min(that.currentPage - that.halfDisplayed, (that.pages - that.displayedPages)), 0) : 0),
            end: Math.ceil(that.currentPage > that.halfDisplayed ? Math.min(that.currentPage + that.halfDisplayed, that.pages) : Math.min(that.displayedPages, that.pages))
        };
    };

    /**
     * [number] pageIndex
     * [object] opts
     */
    GridPanel.prototype._appendItem = function (pageIndex, opts) {
        var self = this.dom.footer,
            options,
            $link,
            $linkWrapper = $('<li></li>'),
            $ul = $(this.dom.footer).find('ul');

        pageIndex = pageIndex < 0 ? 0 : (pageIndex < this.pages ? pageIndex : this.pages - 1);

        options = {
            text: pageIndex + 1,
            classes: ''
        };

        if (this.labelMap.length && this.labelMap[pageIndex]) {
            options.text = this.labelMap[pageIndex];
        }

        options = $.extend(options, opts || {});

        if (pageIndex == this.currentPage || this.disabled) {
            if (this.disabled) {
                $linkWrapper.addClass('disabled');
            } else {
                $linkWrapper.addClass('active');
            }
            $link = (options.text == 'Prev') ? $('<span class="pmui-gridpanel-pager-current"><span style="font-size: 18px; line-height: normal;"> &lsaquo; </span>' + (options.text) + '</span>') : (options.text == 'Next') ? $('<span class="pmui-gridpanel-pager-current">' + (options.text) + '<span style="font-size: 18px; line-height: normal;"> &rsaquo; </span></span>') : $('<span class="pmui-gridpanel-pager-current">' + (options.text) + '</span>');

        } else {
            $link = (options.text == 'Prev') ? $('<a href="' + this.hrefTextPrefix + (pageIndex + 1) + this.hrefTextSuffix + '" class="page-link"><span style="font-size: 18px; line-height: normal;"> &lsaquo; </span>' + (options.text) + '</a>') : (options.text == 'Next') ? $('<a href="' + this.hrefTextPrefix + (pageIndex + 1) + this.hrefTextSuffix + '" class="page-link">' + (options.text) + '<span style="font-size: 18px; line-height: normal;"> &rsaquo; </span></a>') : $('<a href="' + this.hrefTextPrefix + (pageIndex + 1) + this.hrefTextSuffix + '" class="page-link">' + (options.text) + '</a>');
            var that = this;
            $link.click(function (event) {
                return that._selectPage.call(that, pageIndex, event);
            });
        }

        if (options.classes) {
            $link.addClass(options.classes);
        }

        $linkWrapper.append($link);

        if ($ul.length) {
            $(this.dom.pager2).append($linkWrapper);
        } else {
            $(this.dom.pager2).append($linkWrapper); //... review
        }
    };

    /**
     * Select number in a page.
     * @param  {Number} pageIndex The index of the page to go.
     * @param  {event} click event.
     * @chainable
     */
    GridPanel.prototype._selectPage = function (pageIndex, event) {
        this.currentPage = pageIndex;
        if (this.selectOnClick) {
            this.goToPage(pageIndex, true);
        }
    }

    /**
     * Change the grid page.
     * @param  {Number} index The index of the page to go.
     * @param  {Boolean} [force] If its true you go to the specified page even if there's no data in it.
     * @chainable
     */
    GridPanel.prototype.goToPage = function (index, force) {
        var modal;

        if (this._dynamicLoad) {
            var i, numDisplayedItems = 0, that = this;

            if (!((index < 0) && !force)) {
                this.currentPage = index;

                if (!this.html) {
                    return this;
                }
            }
            if (!this.html) {
                return this;
            }

            if (numDisplayedItems === 0) {
                this.showEmptyCell();
            }
            if (this.html) {
                $(this.dom.toolbar).find(".pmui-gridpanel-searchload").addClass("load");
                modal = document.createElement("div");
                modal.id = "dynamic-modal-load";
                $(modal).css({
                    height: $(this.dom.tableContainer).outerHeight(),
                    width: $(this.dom.tableContainer).outerWidth(),
                    background: "rgba(204, 197, 197, 0.4)",
                    position: "absolute",
                    zIndex: "200"
                });
                $(this.dom.tableContainer).prepend(modal);
            }
            $.ajax({
                url: this.url + "?filter=" + (this.filterCriteria ? this.filterCriteria : "") + "&start=" + (this.pageSize * index) + "&limit=" + this.pageSize + "&type=" + (this.typeList ? this.typeList : ""),
                type: "GET",
                async: true,
                contentType: "application/json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + that.keys.accessToken);
                },
                success: function (data) {
                    var i, newItem, numDisplayedItems, dataReceived, numDisplayedItems;
                    if (typeof that.customDataRest === "function") {
                        dataReceived = that.customDataRest(data["data"]);
                    } else {
                        dataReceived = data["data"];
                    }
                    that._dynamicLoad.totalPages = Math.ceil(data["total"] / that.pageSize);
                    $(that.dom.tbody).empty();
                    if (dataReceived.length) {
                        for (i = 0; i < dataReceived.length; i += 1) {
                            newItem = that.factory.make({
                                data: dataReceived[i]
                            });
                            newItem.parent = that;
                            newItem.style.addClasses(['pmui-' + (i % 2 ? 'odd' : 'even')]);
                            if (newItem.html || that.columns.asArray()) {
                                newItem.setCells();
                            }
                            that.dom.tbody.appendChild(newItem.updateCellsWidth().getHTML());
                            $(newItem.html).find(".pmui-gridpanelcell-content").each(function () {
                                if (this.children.length === 0 && this.textContent.length != 0) {
                                    this.title = this.textContent;
                                }
                            });
                            numDisplayedItems += 1;
                        }
                    } else {
                        that.showEmptyCell();
                    }
                    if (numDisplayedItems === 0) {
                        that.showEmptyCell();
                    }
                    that.setBehavior(that.behavior);
                    if (that.html) {
                        $(that.dom.toolbar).find(".pmui-gridpanel-searchload").removeClass("load");
                        $(that.dom.tableContainer).find("#dynamic-modal-load").remove();
                    }
                    return that.updatePager().updateStatusBar();
                    that.dom.footer.appendChild(this.dom.pager);
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
            this.setBehavior(this.behavior);
        } else {
            this.halfDisplayed = this.displayedPages / 2;
            this.currentPage = index;
            if (force && this.filterCriteria && this.filterable) {
                this.pages = Math.ceil(this.filteredItems.getSize() / this.pageSize) ? Math.ceil(this.filteredItems.getSize() / this.pageSize) : 1;
            } else {
                this.pages = Math.ceil(this.getItems().length / this.pageSize) ? Math.ceil(this.getItems().length / this.pageSize) : 1;
            }
            var interval = this._getInterval(this);
            //Updates the pager
            if (this.dom.pager2 && this.getItems().length != 0) {
                jQuery(this.dom.pager2).empty();

                //Generate Prev link
                if (this.prevText) {
                    this._appendItem.call(this, !this.invertPageOrder ? this.currentPage - 1 : this.currentPage + 1, {
                        text: this.prevText,
                        classes: 'prev'
                    });
                }


                //Generate Next link
                if (this.nextText && this.nextAtFront) {
                    this._appendItem.call(this, !this.invertPageOrder ? this.currentPage + 1 : this.currentPage - 1, {
                        text: this.nextText,
                        classes: 'next'
                    });
                }


                //Generate start edges
                if (!this.invertPageOrder) {
                    if (interval.start > 0 && this.edges > 0) {
                        if (this.useStartEdge) {
                            var end = Math.min(this.edges, interval.start);
                            for (i = 0; i < end; i += 1) {
                                this._appendItem.call(this, i);
                            }
                        }
                        if (this.edges < interval.start && (interval.start - this.edges != 1)) {
                            $(this.dom.pager2).append('<li class="disabled"><span class="pmui-gridpanel-pager-ellipse">' + this.ellipseText + '</span></li>');
                        } else if (interval.start - this.edges == 1) {
                            this._appendItem.call(this, this.edges);
                        }
                    }
                } else {
                    if (interval.end < this.pages && this.edges > 0) {
                        if (this.useStartEdge) {
                            var begin = Math.max(this.pages - this.edges, interval.end);
                            for (i = this.pages - 1; i >= begin; i -= 1) {
                                this._appendItem.call(this, i);
                            }
                        }
                        if (this.pages - this.edges > interval.end && (this.pages - this.edges - interval.end != 1)) {
                            $(this.dom.pager2).append('<li class="disabled"><span class="pmui-gridpanel-pager-ellipse">' + this.ellipseText + '</span></li>');
                        } else if (this.pages - this.edges - interval.end == 1) {
                            this._appendItem.call(this, interval.end);
                        }
                    }
                }


                //Generate interval links
                if (!this.invertPageOrder) {
                    for (i = interval.start; i < interval.end; i += 1) {
                        this._appendItem.call(this, i);
                    }
                } else {
                    for (i = interval.end - 1; i >= interval.start; i -= 1) {
                        this._appendItem.call(this, i);
                    }
                }

                //Generate end edges
                if (!this.invertPageOrder) {
                    if (interval.end < this.pages && this.edges > 0) {
                        if (this.pages - this.edges > interval.end && (this.pages - this.edges - interval.end != 1)) {
                            $(this.dom.pager2).append('<li class="disabled"><span class="pmui-gridpanel-pager-ellipse">' + this.ellipseText + '</span></li>');
                        } else if (this.pages - this.edges - interval.end == 1) {
                            this._appendItem.call(this, interval.end);
                        }
                        if (this.useEndEdge) {
                            var begin = Math.max(this.pages - this.edges, interval.end);
                            for (i = begin; i < this.pages; i += 1) {
                                this._appendItem.call(this, i);
                            }
                        }
                    }
                } else {
                    if (interval.start > 0 && this.edges > 0) {
                        if (this.edges < interval.start && (interval.start - this.edges != 1)) {
                            $(this.dom.pager2).append('<li class="disabled"><span class="pmui-gridpanel-pager-ellipse">' + this.ellipseText + '</span></li>');
                        } else if (interval.start - this.edges == 1) {
                            this._appendItem.call(this, this.edges);
                        }

                        if (this.useEndEdge) {
                            var end = Math.min(this.edges, interval.start);
                            for (i = end - 1; i >= 0; i -= 1) {
                                this._appendItem.call(this, i);
                            }
                        }
                    }
                }

                //Generate Next link
                if (this.nextText && !this.nextAtFront) {
                    this._appendItem.call(this, !this.invertPageOrder ? this.currentPage + 1 : this.currentPage - 1, {
                        text: this.nextText,
                        classes: 'next'
                    });
                }
            } else {
                if (this.getItems().length == 0) {
                    jQuery(this.dom.pager2).empty();
                }
            }
            //Show rows in gridPanel
            var i, initRow, lastRow, listTarget, size, numDisplayedItems = 0, totalPages;
            if (!((index < 0 || index >= this.getTotalPages(this.filterCriteria)) && !force)) {
                if (!this.html) {
                    return this;
                }
                if (this.filterCriteria && this.filterable) {
                    listTarget = this.filteredItems.asArray();
                } else {
                    listTarget = this.usableItemsList.asArray();
                }
                size = listTarget.length;

                totalPages = this.getTotalPages(!!this.filterCriteria);
                jQuery(this.dom.tbody).find('>tr').detach();
                if (force) {
                    initRow = this.currentPage * this.pageSize;
                    lastRow = this.pageSize !== 0 ? (initRow + this.pageSize) : size;
                } else {
                    initRow = (this.filterCriteria && this.filterable) ? 0 : this.pageSize !== 0 ? ((this.currentPage + 1) * this.pageSize) - this.pageSize : 0;
                    lastRow = this.pageSize !== 0 ? (initRow + this.pageSize) : size;
                }
                for (i = initRow; i < lastRow && i < size; i += 1) {
                    this.dom.tbody.appendChild(listTarget[i].updateCellsWidth().getHTML());
                    numDisplayedItems += 1;
                }
            }

            if (!this.html) {
                return this;
            }

            if (numDisplayedItems === 0) {
                this.showEmptyCell();
            }

            this.setBehavior(this.behavior);
            return this;
        }
    };

    /**
     * Deprecated...
     * Updates the pager.
     * @chainable
     * @private
     */
    GridPanel.prototype.updatePager = function () {
        var totalPages,
            page,
            i,
            inputPage,
            link,
            textContent,
            additionalClass,
            currentPage = this.currentPage,
            dataIndex,
            infoLink;

        if (this.dom.pager) {
            jQuery(this.dom.pager).empty();
            if (this._dynamicLoad) {

                totalPages = this._dynamicLoad.totalPages;
                var first, next, first, last, total;
                first = PMUI.createHTMLElement('li');
                first.className = "list-item-ajax";
                link = PMUI.createHTMLElement('a');
                link.className = 'pmui-gridpanel-pagelink pmui-gridpanel-previousbutton';
                textContent = "«";
                link.textContent = textContent;
                link.href = '#';
                first.appendChild(link);
                jQuery(link).css({
                    height: "auto"
                }).data({"index": 0});
                this.dom.pager.appendChild(first);
                if (this.currentPage !== 0) {
                    jQuery(link).removeClass("disable");
                } else {
                    jQuery(link).addClass("disable");
                }
                prev = PMUI.createHTMLElement('li');
                prev.className = "list-item-ajax";
                link = PMUI.createHTMLElement('a');
                link.className = 'pmui-gridpanel-pagelink';
                link.innerHTML = "&lt;";
                jQuery(link).addClass("pmui-gridpanel-previousbutton");
                link.href = '#';
                prev.appendChild(link);
                jQuery(link).css({
                    height: "auto"
                }).data({"index": this.currentPage - 1});
                this.dom.pager.appendChild(prev);
                if (this.currentPage !== 0) {
                    jQuery(link).removeClass("disable");
                } else {
                    jQuery(link).addClass("disable");
                }
                infoLink = PMUI.createHTMLElement('li');
                infoLink.className = "list-item-ajax";
                total = PMUI.createHTMLElement("a");
                total.className = "pmui-gridpanel-total";
                total.textContent = " Page ";
                infoLink.appendChild(total);
                this.dom.pager.appendChild(infoLink);

                infoLink = PMUI.createHTMLElement('li');
                infoLink.className = "list-item-ajax";
                inputPage = PMUI.createHTMLElement("input");
                jQuery(inputPage).addClass("pmui-gridpanel-gotoPage");
                infoLink.appendChild(inputPage);
                this.dom.pager.appendChild(infoLink);
                jQuery(inputPage).val(this.currentPage + 1);
                jQuery(infoLink).css({
                    "vertical-align": "top"
                });
                infoLink = PMUI.createHTMLElement('li');
                infoLink.className = "list-item-ajax";
                total = PMUI.createHTMLElement("a");
                total.className = "pmui-gridpanel-total";
                total.textContent = " of " + (totalPages ? totalPages : 1);
                infoLink.appendChild(total);
                this.dom.pager.appendChild(infoLink);

                next = PMUI.createHTMLElement('li');
                next.className = "list-item-ajax";
                link = PMUI.createHTMLElement('a');
                link.className = 'pmui-gridpanel-pagelink';
                link.innerHTML = "&gt;";
                jQuery(link).addClass("pmui-gridpanel-nextbutton");
                link.href = '#';
                next.appendChild(link);
                jQuery(link).css({
                    height: "auto"
                }).data({"index": this.currentPage + 1});
                this.dom.pager.appendChild(next);
                if (currentPage + 1 < totalPages) {
                    jQuery(link).removeClass("disable");
                } else {
                    jQuery(link).addClass("disable");
                }
                last = PMUI.createHTMLElement('li');
                last.className = "list-item-ajax";
                link = PMUI.createHTMLElement('a');
                link.className = 'pmui-gridpanel-pagelink pmui-gridpanel-nextbutton';
                textContent = "»";
                link.textContent = textContent;
                link.href = '#';
                last.appendChild(link);
                jQuery(link).css({
                    height: "auto"
                }).data({"index": this._dynamicLoad.totalPages - 1});
                this.dom.pager.appendChild(last);
                if (currentPage + 1 < totalPages) {
                    jQuery(link).removeClass("disable");
                } else {
                    jQuery(link).addClass("disable");
                }
            } else {
                totalPages = this.getTotalPages(!!this.filterCriteria);
                if (!totalPages) {
                    return this;
                }
                for (i = -1; i <= totalPages; i += 1) {
                    additionalClass = textContent = dataIndex = "";
                    if (i < 0) {
                        if (currentPage > 0) {
                            dataIndex = 'p';
                            additionalClass = 'pmui-gridpanel-previousbutton';
                        }
                    } else if (i === totalPages) {
                        if (currentPage < totalPages - 1) {
                            additionalClass = 'pmui-gridpanel-nextbutton';
                            dataIndex = 'n';
                        }
                    } else {
                        textContent = i + 1;
                        if (currentPage === i) {
                            additionalClass = 'pmui-active';
                        }
                        dataIndex = i;
                    }
                    if (dataIndex !== "") {
                        page = PMUI.createHTMLElement('li');
                        link = PMUI.createHTMLElement('a');
                        if (this._dynamicLoad) {
                            $(link).css({
                                height: 21
                            });
                        }
                        link.className = 'pmui-gridpanel-pagelink';
                        link.setAttribute("data-index", dataIndex);
                        if (textContent) {
                            link.textContent = textContent;
                        } else {
                            textContent = PMUI.createHTMLElement('div');
                            textContent.className = 'pmui-icon';
                            link.appendChild(textContent);
                        }
                        jQuery(link).addClass(additionalClass);
                        link.href = '#';
                        page.appendChild(link);
                        this.dom.pager.appendChild(page);
                    }
                }
            }
            return this;
        }
    };

    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setPageSize = function (pageSize) {
        var previousSize = this.pageSize;
        if (typeof pageSize !== 'number') {
            throw new Error("setPageSize(): The method only accepts a number as parameter.");
        }
        if (pageSize < 0) {
            throw new Error("setPageSize(): The method only accepts a number major or equal to zero (0).");
        }
        this.pageSize = pageSize;
        if (this.html && previousSize !== pageSize) {
            if (pageSize === 0) {
                jQuery(this.dom.pager).hide();
                this.currentPage = 0;
            } else {
                jQuery(this.dom.pager).show();
            }
            this.goToPage(this.currentPage);
        }
        return this;
    };

    /**
     * Sets the size for the grid's displayedPages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setDisplayedPages = function (displayedPages) {
        this.displayedPages = displayedPages;
        return this;
    };

    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setPages = function (pages) {
        this.pages = pages;
        return this;
    };

    /**
     * Sets Prev text in paginator.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setPrevText = function (prevText) {
        this.prevText = prevText;
        return this;
    };

    /**
     * Sets Next text in paginator.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setNextText = function (nextText) {
        this.nextText = nextText
        return this;
    };

    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setInvertPageOrder = function (invertPageOrder) {
        this.invertPageOrder = invertPageOrder
        return this;
    };

    /**
     * Sets numbers in a paginator.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setLabelMap = function (labelMap) {
        this.labelMap = labelMap
        return this;
    };

    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setHrefTextPrefix = function (hrefTextPrefix) {
        this.hrefTextPrefix = hrefTextPrefix
        return this;
    };


    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setHrefTextSuffix = function (hrefTextSuffix) {
        this.hrefTextSuffix = hrefTextSuffix
        return this;
    };


    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setNextAtFront = function (nextAtFront) {
        this.nextAtFront = nextAtFront
        return this;
    };


    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setEdgesPagination = function (edges) {
        this.edges = edges
        return this;
    };


    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setUseStartEdge = function (useStartEdge) {
        this.useStartEdge = useStartEdge
        return this;
    };


    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setEllipseText = function (ellipseText) {
        this.ellipseText = ellipseText
        return this;
    };


    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setSelectOnClick = function (selectOnClick) {
        this.selectOnClick = selectOnClick
        return this;
    };

    /**
     * Sets the size for the grid's pages.
     * @param {Number} pageSize
     * @chainable
     */
    GridPanel.prototype.setUseEndEdge = function (useEndEdge) {
        this.useEndEdge = useEndEdge
        return this;
    };

    /**
     * Returns the total number of pages on the grid.
     * @param  {Boolean} [filter=false] If the calculation will be done taking only the filtered results.
     * @return {Number}
     */
    GridPanel.prototype.getTotalPages = function (filter) {
        var targetListSize = filter ? this.filteredItems.getSize() : this.items.getSize();

        return !this.items.getSize() ? 0 : (this.pageSize === 0 ? 1 : Math.ceil(targetListSize / this.pageSize));
    };

    /**
     * It defines the events for the Grid.
     * @chainable
     */
    GridPanel.prototype.defineEvents = function () {
        var that = this,
            i,
            columnsNum = this.columns.getSize();

        this.removeEvents().eventsDefined = true;
        if (this.dom.pager) {
            this.filterControl.defineEvents();

            for (i = 0; i < columnsNum; i += 1) {
                this.columns.get(i).defineEvents();
            }

            /*for(i = 0; i < this.items.getSize(); i += 1) {
             this.items.get(i).defineEvents();
             }*/
            this.addEvent('click').listenWithDelegation(this.dom.pager, 'a', function (e) {
                var where;
                e.preventDefault();
                e.stopPropagation();
                where = jQuery(this).data("index");
                if (that._dynamicLoad) {
                    if ((where > -1 && where < that._dynamicLoad.totalPages) && (that.currentPage !== where)) {
                        that.goToPage(where === 'n' ? that.currentPage + 1 : (where === 'p' ? that.currentPage - 1 : where));
                    }
                } else {
                    that.goToPage(where === 'n' ? that.currentPage + 1 : (where === 'p' ? that.currentPage - 1 : where));
                }
            });
            this.addEvent('keypress').listenWithDelegation(this.dom.pager, 'input', function (e) {
                if (e.keyCode == 13 || e.keyCode == 32) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (/^\d*$/.test(this.value) && (this.value <= that._dynamicLoad.totalPages)) {
                        if (this.value > 0) {
                            that.goToPage(parseInt(this.value) - 1);
                        }

                    }
                }
            });
            this.addEvent('change').listenWithDelegation(this.dom.pager, 'input', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (this.value !== that.currentPage) {
                    this.value = that.currentPage + 1;
                }
            });
        }
        return this;
    };
    /**
     * Sets the current page to be displayed in the grid.
     * @param {Number} currentPage
     * @chainable
     */
    GridPanel.prototype.setCurrentPage = function (currentPage) {
        this.currentPage = currentPage;
        if (this.html) {
            this.goToPage(this.currentPage);
        }
        return this;
    };
    /**
     * Clears the filter.
     * @chainable
     */
    GridPanel.prototype.clearFilter = function () {
        this.filterCriteria = null;
        this.goToPage(this.currentPage);
        return this;
    };
    /**
     * Executes the sorting.
     * @param  {String} criteria The sorting criteria.
     * @param  {String} [sortType="asc"] The sorting order: "asc" or "desc".
     * @chainable
     */
    GridPanel.prototype.sort = function (criteria, sortType) {
        var items,
            theColumn,
            i,
            columns,
            evaluate;

        this.sortingData.criteria = criteria;
        this.sortingData.type = sortType;
        evaluate = function (a, b) {
            var dataA,
                dataB,
                res;

            if (typeof criteria === 'function') {
                dataA = criteria.call(a, a.getData()).toString();
                dataB = criteria.call(b, b.getData()).toString();
            } else {
                dataA = a.getData();
                dataB = b.getData();
                dataA = (dataA[criteria] && dataA[criteria].toString()) || "";
                dataB = (dataB[criteria] && dataB[criteria].toString()) || "";
            }
            if (dataA < dataB) {
                res = 1;
            } else if (dataA > dataB) {
                res = -1;
            } else {
                res = 0;
            }
            if (sortType === 'asc') {
                res *= -1;
            }
            return res;
        };
        if (!this.html) {
            return this;
        }
        this.updateUsableItemsList();
        items = this.usableItemsList.asArray();
        items.sort(evaluate);
        this.usableItemsList.set(items);
        columns = this.getColumns();
        for (i = 0; i < columns.length; i += 1) {
            columns[i].style.removeClasses(['pmui-sort-asc', 'pmui-sort-desc']);
            if (columns[i].columnData === criteria) {
                theColumn = columns[i];
            }
        }

        if (theColumn) {
            theColumn.style.addClasses(['pmui-sort-' + sortType]);
        }

        if (this.filterCriteria) {
            this.filter(this.filterCriteria);
        } else {
            this.goToPage(this.currentPage);
        }
        this.goToPage(this.currentPage);
        return this;
    };
    /**
     * Returns the function to be executed when an accepted draggedgable is dragged over the droppable area.
     * @return {Function}
     */
    GridPanel.prototype.onDragOver = function () {
        return function () {
        };
    };
    /**
     * Filters the rows using a criteria. The filter will be applied only in the columns in which its property
     * "searchable" is true.
     * @param  {String} criteria The criteria.
     * @chainable
     */
    GridPanel.prototype.filter = function (criteria) {
        var i,
            columns,
            j,
            cell,
            regExp,
            rowsCopy,
            visibleRows,
            pmuiObject,
            sortingCriteria = this.sortingData.criteria;

        if (typeof criteria === 'string') {
            if (criteria === "") {
                this.clearFilter();
                return this;
            }
            this.filterCriteria = criteria;
        } else if (typeof criteria === 'number') {
            this.filterCriteria = criteria.toString(10);
        }

        if (!this.dom.tbody) {
            return this;
        }

        columns = this.columns.asArray();
        regExp = new RegExp(this.filterCriteria.replace(/([\\\.\[\]\^\$\(\)\?\*\+\|\{\}])/g, "\\\$1"), "i");
        rowsCopy = this.usableItemsList.asArray().slice(0);

        visibleRows = jQuery(this.dom.tbody).find('tr');
        for (i = 0; i < visibleRows.length; i += 1) {
            pmuiObject = PMUI.getPMUIObject(visibleRows[i]);
            if (pmuiObject) {
                jQuery(pmuiObject.html).detach();
            }
        }
        this.filteredItems.clear();
        //if there is a sorting criteria set at the moment, first when use it to filter the rows
        //this is necessary to keep the current sorting order.
        if (sortingCriteria) {
            for (j = 0; j < rowsCopy.length; j += 1) {
                for (i = 0; i < columns.length; i += 1) {
                    if (columns[i].getDataType() !== 'button' && columns[i].isSearchable()) {
                        if (!rowsCopy[j].html) {
                            rowsCopy[j].getHTML();
                            rowsCopy[j].setCells();
                        }
                        cell = rowsCopy[j].getCells()[i];
                        if (regExp.test(cell.getContent()) && cell.visible) {
                            this.filteredItems.insert(rowsCopy.splice(j, 1)[0]);
                            j -= 1;
                            break;
                        }
                    }
                }
            }
        }
        if (rowsCopy.length) {
            for (i = 0; i < columns.length; i += 1) {
                if (columns[i].getDataType() !== 'button' && columns[i].isSearchable()) {
                    for (j = 0; j < rowsCopy.length; j += 1) {
                        if (!rowsCopy[j].html) {
                            rowsCopy[j].getHTML();
                            rowsCopy[j].setCells();
                        }
                        cell = rowsCopy[j].getCells()[i];
                        if (regExp.test(cell.getContent()) && cell.visible) {
                            this.filteredItems.insert(rowsCopy.splice(j, 1)[0]);
                            j -= 1;
                        }
                    }
                }
            }
        }
        this.goToPage(0, true);
        return this;
    };
    /**
     * [setSelectable description]
     * @param {[type]} value [description]
     */
    GridPanel.prototype.setSelectable = function (value) {
        if (typeof value !== 'boolean') {
            throw new Error('setSelectable(): the property is not valid, should be a boolean');
        }
        this.selectable = value;
        if (!value && this.selectedRow) {
            this.selectedRow.deselectRow();
            this.selectedRow = null;
        }
        return this;
    };
    /**
     * fixed the property visibleFooter, to hide or not the foot of GridPanel
     * @param {Boolean} value the property should be type 'boolean'
     */
    GridPanel.prototype.setVisibleFooter = function (value) {
        if (typeof value !== 'boolean') {
            throw new Error("setFooterHidden(): the value is no valid, should be a value type 'boolean'");

        }
        this.visibleFooter = value;
        if (this.html) {
            if (this.visibleFooter) {
                this.showFooter();
            } else {
                this.hiddenFooter();
            }
        }
        return this;
    };
    /**
     * hidden the footer of the GridPanel if is visible
     * @chainable
     */
    GridPanel.prototype.hiddenFooter = function () {
        this.dom.footer.style.display = 'none';
        return this;
    };
    /**
     * show the footer of the GridPanel if it has been hidden
     * @chainable
     */
    GridPanel.prototype.showFooter = function () {
        this.dom.footer.style.display = 'block';
        return this;
    };
    /**
     * Set the height for the HTML element
     * @param {Number|String} height it can be a number or a string.
     * In case of using a String you only can use 'auto' or 'inherit' or ##px or ##% or ##em when ## is a number.
     * also sets the height of the table being rendered, according to the high set in order gridPanel
     * @chainable
     */
    GridPanel.prototype.setHeight = function (height) {
        GridPanel.superclass.prototype.setHeight.call(this, height);


        if (this.dom.tableContainer) {
            if (typeof this.height !== "string") {
                this.dom.tableContainer.style.height = this.height - 74 + 'px';
            } else {
                this.dom.tableContainer.style.height = 'auto';
            }
        }
        return this;
    };

    PMUI.extendNamespace("PMUI.grid.GridPanel", GridPanel);

    if (typeof exports !== 'undefined') {
        module.exports === GridPanel;
    }
}());