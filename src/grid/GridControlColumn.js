(function () {
    var GridControlColumn = function (settings) {
        this.cells = new PMUI.util.ArrayList();
        GridControlColumn.superclass.call(this, jQuery.extend(settings, {elementTag: "th"}));
        /**
         * The name for the column.
         * @type {String}
         */
        this.name = null;
        /**
         * The text to be shown in the column header.
         * @type {String}
         */
        this.title = null;
        /**
         * The grid the column belongs to.
         * @type {PMUI.grid.GridPanel}
         */
        this.grid = null;
        /**
         * The HTML element in which the cell content will be appended.
         * @type {HTMLElement}
         * @private
         */
        this.contentTitle = null;
        /**
         * [dependentCols description]
         * @type {Array}
         */
        this.dependentCols = [];
        /**
         * [onChange description]
         * @type {[type]}
         */
        this.onChange = null;
        /**
         * [dependencyHandler description]
         * @type {[type]}
         */
        this.dependencyHandler = null;
        /**
         * [disabled description]
         * @type {[type]}
         */
        this.disabled = null;
        /**
         * [validatorFactory description]
         * @type {[type]}
         */
        this.validatorFactory = null;
        /**
         * [validators description]
         * @type {Object}
         */
        this.validators = {};
        /**
         * [required description]
         * @type {[type]}
         */
        this.required = null;
        /**
         * [requiredMessage description]
         * @type {[type]}
         */
        this.requiredMessage = null;
        GridControlColumn.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Element', GridControlColumn);

    GridControlColumn.prototype.CELL = null;

    GridControlColumn.prototype.init = function (settings) {
        var defaults = {
            name: this.id,
            title: '[untitled]',
            grid: null,
            cells: [],
            dependentCols: [],
            onChange: null,
            dependencyHandler: null,
            disabled: false,
            validators: [],
            required: false,
            requiredMessage: 'This cell is required.',
            validatorFactory: new PMUI.form.ValidatorFactory()
        };

        jQuery.extend(true, defaults, settings);

        this.setName(defaults.name)
            .setTitle(defaults.title)
            .setGrid(defaults.grid)
            .setRequired(defaults.required)
            .setDependentCols(defaults.dependentCols)
            .setOnChangeHandler(defaults.onChange)
            .setDependencyHandler(defaults.dependencyHandler)
            .setValidatorFactory(defaults.validatorFactory)
            .setValidators(defaults.validators)
            .setRequiredMessage(defaults.requiredMessage);

        if (defaults.disabled) {
            this.disable();
        } else {
            this.enable();
        }
    };

    GridControlColumn.prototype.setRequiredMessage = function (requiredMessage) {
        if (typeof requiredMessage !== 'string') {
            throw new Error('setRequiredMessage(): The parameter must be a strng.');
        }
        this.requiredMessage = requiredMessage;
        return this;
    };

    GridControlColumn.prototype.setRequired = function (required) {
        this.required = !!required;
        return this;
    };

    GridControlColumn.prototype.setValidatorFactory = function (factory) {
        if (factory instanceof PMUI.util.Factory) {
            this.validatorFactory = factory;
        } else {
            this.validatorFactory = new PMUI.form.ValidatorFactory(factory);
        }
        return this;
    };

    GridControlColumn.prototype.addValidator = function (validator) {
        var newValidator;
        if (this.validatorFactory) {
            if (this.validatorFactory.isValidClass(validator) || this.validatorFactory.isValidName(validator.pmType)) {
                newValidator = this.validatorFactory.make(validator);
            } else {
                throw new Error('Invalid validator to add.');
            }
        }

        if (newValidator && newValidator instanceof PMUI.form.Validator) {
            this.validators[newValidator.type] = newValidator;
        }

        return this;
    };

    GridControlColumn.prototype.clearValidators = function () {
        var key;

        for (key in this.validators) {
            if (this.validators.hasOwnProperty(key)) {
                this.validators[key] = null;
                delete this.validators[key];
            }
        }

        return this;
    };

    GridControlColumn.prototype.setValidators = function (validators) {
        var i = 0;

        if (jQuery.isArray(validators)) {
            this.clearValidators();
            for (i = 0; i < validators.length; i += 1) {
                this.addValidator(validators[i]);
            }
        }
        return this;
    };

    GridControlColumn.prototype.getCells = function () {
        return this.cells.asArray().slice(0);
    };

    GridControlColumn.prototype.setName = function (name) {
        if (typeof name !== 'string') {
            throw new Error("setName(): The parameter must be a string.");
        }
        this.name = name;
        return this;
    };

    GridControlColumn.prototype.setDependencyHandler = function (handler) {
        if (!(typeof handler === 'function' || handler === null)) {
            throw new Error("setDependencyHandler(): The parameter must be a function or null.");
        }
        this.dependencyHandler = handler;
        return this;
    };

    GridControlColumn.prototype.onChangeHandler = function (cell, newValue, oldValue) {
        var i,
            dependentCols = this.dependentCols,
            grid = this.grid,
            dependentCell,
            col,
            row;

        cell.row.updateData(cell);
        if (typeof this.onChange === 'function') {
            this.onChange(cell, newValue, oldValue);
        }
        if (grid) {
            for (i = 0; i < dependentCols.length; i += 1) {
                col = grid.getColumn(dependentCols[i]);
                if (!col) {
                    this.removeDependentCol(dependentCols[i]);
                    continue;
                }
                dependentCell = cell.row.getCell(col);
                col.fireDependencyHandler(dependentCell);
            }
        }
        return this;
    };

    GridControlColumn.prototype.fireDependencyHandler = function (cell) {
        var grid = this.grid,
            dependsOf,
            obj = {},
            i,
            targetRow,
            targetCells,
            j;

        if (grid) {
            targetCells = cell ? [cell] : this.cells.asArray();
            for (j = 0; j < targetCells.length; j += 1) {
                cell = targetCells[j];
                targetRow = cell.row;
                dependsOf = grid.dependencies[this.name];
                for (i = 0; i < dependsOf.length; i += 1) {
                    obj[dependsOf[i].name] = targetRow.getCell(dependsOf[i]);
                }
                if (typeof this.dependencyHandler === 'function') {
                    this.dependencyHandler.call(cell, cell, obj);
                }
            }
        }
        return this;
    };

    GridControlColumn.prototype.enable = function () {
        var i,
            cells = this.getCells();

        for (i = 0; i < cells.length; i += 1) {
            cells[i].enable();
        }
        this.disabled = false;
        return this;
    };

    GridControlColumn.prototype.disable = function () {
        var i,
            cells = this.getCells();

        for (i = 0; i < cells.length; i += 1) {
            cells[i].disable();
        }
        this.disabled = true;
        return this;
    };

    GridControlColumn.prototype.setOnChangeHandler = function (handler) {
        if (!(typeof handler === 'function' || handler === null)) {
            throw new Error("setOnChangeHandler(): the parameter must be a function or null.");
        }
        this.onChange = handler;
        return this;
    };

    GridControlColumn.prototype.setDependentCols = function (dependentCols) {
        if (!jQuery.isArray(dependentCols)) {
            throw new Error("setDependentCols(): The parameter must be an array.");
        }
        this.dependentCols = dependentCols;
        if (this.grid) {
            this.grid.updateDependencies();
        }
        return this;
    };

    GridControlColumn.prototype.removeDependentCol = function (columnName) {
        var i;

        for (i = 0; this.dependentCols.length; i += 1) {
            if (this.dependentCols[i] === columnName) {
                this.dependentCols.splice(i, 1);
                i--;
            }
        }
        return this;
    };

    GridControlColumn.prototype.removeCell = function (cell) {
        var cellToRemove;

        if (cell instanceof PMUI.grid.GridControlCell) {
            cellToRemove = cell;
        } else if (typeof cell === 'number') {
            cellToRemove = this.cells.get(cell);
        } else if (typeof cell === 'string') {
            cellToRemove = this.cells.find("id", cell);
        }

        if (cellToRemove) {
            cellToRemove.column = null;
            this.cells.remove(cellToRemove);
        }

        return this;
    };

    GridControlColumn.prototype.clearCells = function () {
        this.cells.clear();
        while (this.cells.getSize() > 0) {
            this.removeCell(0);
        }
        return this;
    };

    GridControlColumn.prototype.addCell = function (cell) {
        if (!(cell instanceof PMUI.grid.GridControlCell)) {
            throw new Error("addCell(): The parameter must be an instance of PMUI.grid.GridControlCell.");
        }
        this.cells.insert(cell);
        return this;
    };

    GridControlColumn.prototype.setTitle = function (title) {
        this.title = title;
        if (this.html) {
            jQuery(this.contentTitle).empty();
            this.contentTitle.appendChild(document.createTextNode(title));
        }
        return this;
    };

    GridControlColumn.prototype.setGrid = function (grid) {
        if (!(grid instanceof PMUI.grid.ControlGrid)) {
            throw new Error('setGrid(): the grid property should be instance of [PMUI.grid.GridControl]');
        }
        this.grid = grid;
        this.grid.updateDependencies();
        return this;
    };

    GridControlColumn.prototype.getCellDefaultCfg = function () {
        return {
            disabled: this.disabled
        };
    };

    GridControlColumn.prototype.createCell = function () {
        var cell;

        if (!this.CELL instanceof PMUI.grid.GridControlCell) {
            throw new Error("The cell associated to the " + this.type + " is not valid!");
        }
        cell = new this.CELL(this.getCellDefaultCfg());
        this.addCell(cell);

        return cell;
    };

    GridControlColumn.prototype.createHTML = function () {
        var contentTitle,
            sortIcon;

        if (this.html) {
            return this.html;
        }
        GridControlColumn.superclass.prototype.createHTML.call(this);
        contentTitle = PMUI.createHTMLElement('span');
        contentTitle.className = 'pmui-gridpanelColumn-title';
        this.contentTitle = contentTitle;
        this.html.appendChild(contentTitle);
        this.setTitle(this.title);

        return this.html;
    };

    PMUI.extendNamespace('PMUI.grid.GridControlColumn', GridControlColumn);
}());