(function () {
    var DropDownListControlColumn = function (settings) {
        DropDownListControlColumn.superclass.call(this, settings);
        this.options = [];
        DropDownListControlColumn.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.grid.GridControlColumn', DropDownListControlColumn);

    DropDownListControlColumn.prototype.CELL = PMUI.grid.DropDownListControlCell;

    DropDownListControlColumn.prototype.init = function (settings) {
        var defaults = {
            options: []
        };

        jQuery.extend(true, defaults, settings);

        this.setOptions(defaults.options);
    };

    DropDownListControlColumn.prototype.setOptions = function (options) {
        var i,
            cells;

        if (!jQuery.isArray(options)) {
            throw new Error("setOptions(): the parameter must be an array.");
        }
        this.options = options;
        cells = this.cells.asArray();
        for (i = 0; i < cells.length; i += 1) {
            cells[i].setOptions(this.options);
        }
        return this;
    };

    DropDownListControlColumn.prototype.getCellDefaultCfg = function () {
        return jQuery.extend(true, DropDownListControlColumn.superclass.prototype.getCellDefaultCfg.call(this), {
            options: this.options
        });
    };

    PMUI.extendNamespace('PMUI.grid.DropDownListControlColumn', DropDownListControlColumn);
}());