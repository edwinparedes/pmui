(function () {
    var GridControlCell = function (settings) {
        this.controls = [];
        GridControlCell.superclass.call(this, settings);
        /**
         * @property {Boolean} [validAtChange=true]
         * If the validation must be executed every time the field's value changes.
         */
        this.validAtChange = null;
        this.value = null;
        this.onChange = null;
        this.validAtChange = null;
        this.disable = null;
        GridControlCell.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.grid.GridPanelCell', GridControlCell);

    GridControlCell.prototype.init = function (settings) {
        var defaults = {
            onChange: null,
            validAtChange: true,
            disabled: false,
            value: ""
        };

        jQuery.extend(true, defaults, settings);
        this.initControls();

        this.setOnChangeHandler(defaults.onChange)
            .setValidAtChange(defaults.validAtChange)
            .setValue(defaults.value);

        if (defaults.disabled) {
            this.disable();
        } else {
            this.enable();
        }
    };
    /**
     * [setValueToControls description]
     * @param {[type]} value [description]
     */
    GridControlCell.prototype.setValueToControls = function (value) {
        var i;

        for (i = 0; i < this.controls.length; i += 1) {
            this.controls[i].setValue(value);
        }
        return this;
    };
    /**
     * [setValue description]
     * @param {[type]} value [description]
     */
    GridControlCell.prototype.setValue = function (value) {
        if (typeof value === 'number') {
            value = value.toString();
        }
        if (typeof value === 'string') {
            this.value = value;
        } else {
            throw new Error("The setValue() method only accepts string values!");
        }
        this.setValueToControls(value);
        return this;
    };
    /**
     * Turns on/off the validation when the field's value changes.
     * @param {Boolean} validAtChange
     * @chainable
     */
    GridControlCell.prototype.setValidAtChange = function (validAtChange) {
        this.validAtChange = !!validAtChange;
        return this;
    };
    /**
     * Sets the callback function to be called when the field's value changes.
     * @param {Function} handler
     * @chainable
     */
    GridControlCell.prototype.setOnChangeHandler = function (handler) {
        if (typeof handler === 'function' || handler === null) {
            this.onChange = handler;
        }

        return this;
    };

    GridControlCell.prototype.initControls = function () {
        return this;
    };

    GridControlCell.prototype.setValuesToControls = function (values) {
        return this;
    };

    GridControlCell.prototype.setContent = function (content) {
        return this.setValuesToControls(content);
    };

    /**
     * Update the field's value property from the controls
     * @protected
     * @chainable
     */
    GridControlCell.prototype.updateContentFromControls = function () {
        var value = '',
            i;

        for (i = 0; i < this.controls.length; i += 1) {
            value += ' ' + this.controls[i].getValue();
        }

        this.value = this.content = value.substr(1);

        return this;
    };
    /**
     * The onChange event handler constructor.
     * @private
     * @return {Function} The handler.
     */
    GridControlCell.prototype.onChangeHandler = function () {
        var that = this;

        return function () {
            var previousValue = that.content;
            that.updateContentFromControls();
            if (that.validAtChange) {
                that.isValid();
            }

            if (typeof that.onChange === 'function') {
                that.onChange(that.getContent(), previousValue);
            }

            if (that.column) {
                that.column.onChangeHandler(that, that.content, previousValue);
            }
        };
    };
    GridControlCell.prototype.hideMessage = function () {
        this.style.removeClasses(['pmui-error']);
        this.html.removeAttribute("title");
        try {
            jQuery(this.html).tooltip("destroy");
        } catch (e) {
        }
        return this;
    };
    GridControlCell.prototype.showMessage = function (message) {
        this.style.addClasses(['pmui-error']);
        this.html.setAttribute("title", "error");
        jQuery(this.html).tooltip({
            content: message,
            tooltipClass: 'pmui-cell-tooltip-error',
            position: {
                my: "left",
                at: "left bottom"
            }
        });
        jQuery(this.html).tooltip('open');
        return this;
    };
    /**
     * [evalRequired description]
     * @return {[type]} [description]
     */
    GridControlCell.prototype.evalRequired = function () {
        var valid = true, column = this.column;
        if (column) {
            if (column.required && this.value === "") {
                valid = false;
            }
        }
        if (valid) {
            this.hideMessage();
        } else {
            this.showMessage(column.requiredMessage);
        }
        return valid;
    };
    /**
     * [isValid description]
     * @return {Boolean} [description]
     */
    GridControlCell.prototype.isValid = function () {
        var valid = true,
            validator,
            validators;

        valid = valid && this.evalRequired();
        if (!valid) {
            return valid;
        }
        if (this.column) {
            validators = this.column.validators;
            for (validator in validators) {
                if (validators.hasOwnProperty(validator)) {
                    validators[validator].setParent(this);
                    valid = validators[validator].isValid();
                    if (!valid) {
                        return valid;
                    }
                }
            }
        }
        return valid;
    };
    /**
     * Attach the event listeners to the HTML element
     * @private
     * @chainable
     */
    GridControlCell.prototype.defineEvents = function () {
        var i;

        if (this.eventsDefined) {
            return this;
        }
        ;
        for (i = 0; i < this.controls.length; i += 1) {
            this.controls[i].setOnChangeHandler(this.onChangeHandler())
                .defineEvents();
        }
        this.eventsDefined = true;

        return this;
    };
    /**
     * @inheritdoc
     */
    GridControlCell.prototype.enable = function () {
        var i;

        for (i = 0; i < this.controls.length; i += 1) {
            this.controls[i].disable(false);
        }
        this.disabled = false;
        return this;
    };
    /**
     * @inheritdoc
     */
    GridControlCell.prototype.disable = function () {
        var i;

        for (i = 0; i < this.controls.length; i += 1) {
            this.controls[i].disable(true);
        }
        this.disabled = true;
        return this;
    };
    /**
     * [createHTML description]
     * @return {[type]} [description]
     */
    GridControlCell.prototype.createHTML = function () {
        var i;

        if (this.html) {
            return this.html;
        }
        GridControlCell.superclass.prototype.createHTML.call(this);
        for (i = 0; i < this.controls.length; i += 1) {
            this.contentSpan.appendChild(this.controls[i].getHTML());
        }

        this.defineEvents();
        return this.html;
    };

    PMUI.extendNamespace('PMUI.grid.GridControlCell', GridControlCell);
}());