(function () {
    /**
     * @class PMUI.grid.GridPanelCell
     * Class that represent a cell to be used in instances of {@link PMUI.grid.GridPanelRow GridPanelRow}.
     * It's not very useful to instantiate this class to use it alone. Most of cases you won't need to instantiate it,
     * the {@link PMUI.grid.GridPanelRow GridPanelRow} creates its own cells objects automatically.
     * {@link PMUI.grid.GridPanelRow GridPanelRow}.
     * @extends {PMUI.core.Element}
     *
     *    Usage example:
     *
     *        @example
     *        var cell = new PMUI.grid.GridPanelCell({
     *        content: "This is the cell content"
     *        });
     *
     *        document.body.appendChild(cell.getHTML());
     *
     * @constructor
     * Creates a new instance of the GridPanelCell class.
     * @param {Object} settings A JSON object with the config options.
     *
     * @cfg {String|Number|PMUI.core.Element|null} [content=null] The content to be displayed in the cell, in case to
     * be null the cells has no content.
     * @cfg {Number} [columnIndex=null] The column index the cell belongs to.
     * @cfg {PMUI.grid.GridPanelRow} [row=null] The row the cell belongs to.
     * @cfg {PMUI.grid.GridPanelColumn} [column=null] The column the cell belongs to.
     * @cfg {String} [alignment="center"] A string that specifies the alignment for the cell content. The accepted
     * values are: "center", "left", "right".
     * @cfg {Boolean} [disabled=false] If the cell will be disabled or not.
     */
    var GridPanelCell = function (settings) {
        GridPanelCell.superclass.call(this, jQuery.extend(settings, {elementTag: "td"}));
        /**
         * The content for the cell.
         * @type {PMUI.core.Element|String|Number}
         */
        this.content = null;
        /**
         * The position index for the cell, relative to its parent row.
         * @type {Number}
         */
        this.columnIndex = null;
        /**
         * The row the cell belongs to.
         * @type {PMUI.grid.GridPanelRow}
         */
        this.row = null;
        /**
         * The column the cell appertains to.
         * @type {PMUI.grid.GridPanelColumn}
         */
        this.column = null;
        /**
         * The alignment to be applied to the cell content.
         * @type {String}
         */
        this.alignment = null;
        this.contentSpan = null;
        /**
         * If the cell content is disabled or not.
         * @type {Boolean}
         */
        this.disabled = null;
        GridPanelCell.prototype.init.call(this, settings);
    };
    PMUI.inheritFrom("PMUI.core.Element", GridPanelCell);
    /**
     * The object's type.
     * @type {String}
     */
    GridPanelCell.prototype.type = "GridPanelCell";
    /**
     * The class family.
     * @type {String}
     */
    GridPanelCell.prototype.family = "grid";
    /**
     * Initializes the object.
     * @param  {Object} settings A JSON object with the config options.
     * @private
     */
    GridPanelCell.prototype.init = function (settings) {
        var defaults = {
            content: null,
            columnIndex: null,
            row: null,
            column: null,
            alignment: "center",
            disabled: false
        };
        jQuery.extend(true, defaults, settings);

        this.row = defaults.row;
        this.column = defaults.column;
        this.eventsDefined = false;

        if (defaults.disabled) {
            this.disable();
        } else {
            this.enable();
        }

        this.setAlignment(defaults.alignment)
            .setContent(defaults.content)
            .setColumnIndex(defaults.columnIndex);
    };
    /**
     * Sets the alignment for the content in the cell.
     * @param {String} alignment The accepted values are: "center", "left", "right".
     * @chainable
     */
    GridPanelCell.prototype.setAlignment = function (alignment) {
        if (alignment === 'center' || alignment === 'left' || alignment === 'right') {
            this.alignment = alignment;
            if (this.html && this.contentSpan) {
                this.contentSpan.style.textAlign = this.alignment;
                this.style.addProperties({'text-align': this.alignment});
            }
        } else {
            throw new Error('setAlignment(): This method only accepts one of the following values: '
                + '"canter", "left", "right"');
        }
        return this;
    };
    /**
     * Returns the cell's content alignment.
     * @return {String}
     */
    GridPanelCell.prototype.getAlignment = function () {
        return this.alignment;
    };
    /**
     * Sets the content for the cell.
     * @param {String|Number|PMUI.core.Element|null} content The content to be displayed in the cell, in case to
     * be null the cells has no content.
     * @chainable
     */
    GridPanelCell.prototype.setContent = function (content) {
        var type,
            visibleContent;

        this.content = content;

        if (content instanceof PMUI.core.Element) {
            visibleContent = content.getHTML();
        } else if (typeof content === 'number' || typeof content === 'string') {
            visibleContent = document.createTextNode(content);
        } else if (content === null || content === undefined) {
            visibleContent = undefined;
        } else {
            throw new Error("setContent(): invalid parameter, it should be a Number, String or an instance of PMUI.core.Element.");
        }

        if (this.html) {
            jQuery(this.contentSpan).empty();
            if (visibleContent) {
                this.contentSpan.appendChild(visibleContent);
            } else {
                this.contentSpan.innerHTML = '&nbsp;';
            }
            if (this.disabled) {
                this.disable();
            } else {
                this.enable();
            }
        }

        return this;
    };
    /**
     * Returns the column the cell belongs to.
     * @return {PMUI.grid.GridPanelColumn}
     */
    GridPanelCell.prototype.getColumn = function () {
        return this.column;
    };
    /**
     * Returns the row the cell belongs to.
     * @return {PMUI.grid.GridPanelRow}
     */
    GridPanelCell.prototype.getRow = function () {
        return this.row;
    };
    /**
     * Returns the cell's content.
     * @return {String|Number|PMUI.core.Element|null}
     */
    GridPanelCell.prototype.getContent = function () {
        return this.content;
    };
    /**
     * Sets the index of the column the cell belongs to.
     * @param {Number} columnIndex
     */
    GridPanelCell.prototype.setColumnIndex = function (columnIndex) {
        this.columnIndex = columnIndex
        return this;
    };
    /**
     * Returns the index of the column the cell belongs to.
     * @return {Number}
     */
    GridPanelCell.prototype.getColumnIndex = function () {
        return this.columnIndex;
    };
    /**
     * Defines the events for the object.
     * @chainable
     */
    GridPanelCell.prototype.defineEvents = function () {
        this.eventsDefined = true;
        if (this.content instanceof PMUI.core.Element) {
            this.content.defineEvents();
        }
        return this;
    };
    /**
     * Creates the object's html.
     * @return {HTMLElement}
     */
    GridPanelCell.prototype.createHTML = function () {
        var type,
            contentSpan;

        if (this.html) {
            return this.html;
        }

        GridPanelCell.superclass.prototype.createHTML.call(this);
        contentSpan = PMUI.createHTMLElement('span');
        contentSpan.className = 'pmui-gridpanelcell-content';
        this.contentSpan = contentSpan;
        this.html.appendChild(this.contentSpan);
        this.setAlignment(this.alignment)
            .setContent(this.content)
            .setWidth(this.width)
            .defineEvents();
        return this.html;
    };

    GridPanelCell.prototype.setWidth = function (width) {
        if (typeof width === 'number') {
            this.width = width;
        } else if (/^\d+(\.\d+)?px$/.test(width)) {
            this.width = parseInt(width, 10);
        } else if (/^\d+(\.\d+)?%$/.test(width)) {
            this.width = width;
        } else if (/^\d+(\.\d+)?em$/.test(width)) {
            this.width = width;
        } else if (width === 'auto') {
            this.width = width;
        } else {
            throw new Error('setWidth: width param is not a number');
        }

        if (this.html) {
            if (typeof this.width === 'string') {
                this.contentSpan.style.width = 'auto';
            } else {
                this.contentSpan.style.width = this.width + 'px';
            }
        }
        return this;
    };

    GridPanelCell.prototype.applyStyle = function () {
        if (this.html) {
            this.style.applyStyle();

            this.style.addProperties({
                display: this.visible ? this.display : "none",
                position: this.positionMode,
                left: this.x,
                top: this.y,
                width: (typeof this.width === 'string') ? this.width : 'auto',
                height: this.height,
                zIndex: this.zOrder
            });
        }
        return this;
    };
    /**
     * Enables the cell's content.
     * @chainable
     */
    GridPanelCell.prototype.enable = function () {
        this.disabled = false;
        if (this.content && this.content.enable) {
            this.content.enable();
        }
        return this;
    };
    /**
     * Disables the cell's content.
     * @chainable
     */
    GridPanelCell.prototype.disable = function () {
        this.disabled = true;
        if (this.content && this.content.disable) {
            this.content.disable();
        }
        return this;
    };
    PMUI.extendNamespace("PMUI.grid.GridPanelCell", GridPanelCell);

    if (typeof exports !== 'undefined') {
        module.exports = GridPanelCell;
    }

}());