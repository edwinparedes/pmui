(function () {
    var NumberColumn = function (settings) {
        NumberColumn.superclass.call(this, settings);
        this.placeholder = null;
        this.maxLength = null;
        this.mask = null;
        NumberColumn.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.grid.GridControlColumn', NumberColumn);

    NumberColumn.prototype.CELL = PMUI.grid.NumberCell;

    NumberColumn.prototype.init = function (settings) {
        var defaults = {
            placeholder: "",
            maxLength: 524288,
            mask: "##############.###############",
            content: '',
            readOnly: false
        };

        jQuery.extend(true, defaults, settings);

        this.setMaxLength(defaults.maxLength)
            .setPlaceHolder(defaults.placeholder)
            .setReadOnly(defaults.readOnly)
            .setMask(defaults.mask);
    };

    NumberColumn.prototype.setMaxLength = function (input) {
        this.maxLength = input
        return this;
    };

    NumberColumn.prototype.setPlaceHolder = function (input) {
        this.placeholder = input;
        return this;
    };

    NumberColumn.prototype.setReadOnly = function (input) {
        this.readOnly = input;
        return this;
    };

    NumberColumn.prototype.setMask = function (mask) {
        this.mask = mask;
        return this;
    };

    NumberColumn.prototype.getCellDefaultCfg = function () {
        return jQuery.extend(true, NumberColumn.superclass.prototype.getCellDefaultCfg.call(this), {
            maxLength: this.maxLength,
            mask: this.mask,
            placeholder: this.placeholder,
            readOnly: this.readOnly
        });
    };

    PMUI.extendNamespace('PMUI.grid.NumberColumn', NumberColumn);
}());