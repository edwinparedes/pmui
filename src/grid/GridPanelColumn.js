(function () {
    /**
     * @class PMUI.grid.GridPanelColumn
     * Class that represents a column to be used in a {@link PMUI.grid.GridPanel GridPanel}.
     * Usually you don't need to instantiate this class since the {@link PMUI.grid.GridPanel GridPanel} do it on its
     * own, in fact, this class can't be instantiate if a grid parent
     * ({@link PMUI.grid.GridPanel#cfg-grid grid config option}) is not specified.
     * @extends {PMUI.core.Element}
     *
     * We don't include any example for this class 'cause we don't recommend you to do it, let the GridPanelColumn do
     * it. Anyway, if you need add new columns to a GridPanel use its
     * {@link PMUI.grid.GridPanel#method-addColumn addColumn() method}.
     *
     * @constructor
     * Creates a new instance of the GridPanelColumn object.
     * @param {Object} settings A JSON object with the config options.
     *
     * @cfg {String} [title='[untitled]'] The text to display in the column header.
     * @cfg {String} [dataType='string'] The column data type. The accepted values are: "string", "number", "index" or
     * "date".
     * @cfg {Boolean} [sortable=false] If the column will have the sorting function enabled.
     * @cfg {Boolean} [searchable=true] If the column must be filter when a filter criteria is set.
     * @cfg {Function} [onButtonClick=null] A callback function to be called when a column's button is clicked. For
     * info about the callback parameters please read the
     * {@link PMUI.grid.GridPanelColumn#event-onButtonClick onButtonClick event doc}.
     * @cfg {String} [buttonLabel='[button]'] The label for the button on a column for buttons.
     * @cfg {Array} [cells=[]] The cells that belongs to the column.
     * @cfg {PMUI.grid.GridPanel} grid The grid the column belongs to.
     * @cfg {String|Function} [columnData=null]
     * - In case of a String: The data key to be used for the cells in this column.
     * - In case of a Function: A function that will return the content for the cells in column, the parameters
     * received by the function are:
     *    - data {Object}: a JSON object with the data for the current row.
     *    The function will be called in the context of the current {@link PMUI.grid.GridPanelRow row object}.
     * @cfg {Function} [onSort=null] A callback function to be called every time the sorting is executed.
     * @cfg {String} [alignmentCell="center"] A string that specifies the alignmentCell for the content of cells that
     * appertains to this column. The accepted values are: "center", "left", "right".
     * @cfg {Object|PMUI.util.Style|null} [cellStyle=null] The style to be applied to the cells that belongs to the
     * column, if it is set to null (the default value) no custom styles will be applied to the cells.
     * @cfg {Object|PMUI.util.Style|Function|null} [buttonStyle=null] The style for the buttons in the column (only applicable
     * if the column has its {@link #property-dataType dataType property} set to "button"). It can be:
     *
     *  - null then the buttons won't apply any custom style.
     *  - An object literal in that case it can have the properties: cssClasses (an array in which each element is
     *  an string that is a class name) and cssProperties (another object literal that can contain css properties and
     *  its respective values).
     *  - An instance of {@link PMUI.util.Style}.
     *  - A function that must return any of the previous value types. The parameters received by the functions are:
     *    - the {@link PMUI.grid.GridPanelCell GridPanelCell} object in which the style is being applied.
     *    - data {Object}: a JSON object with the data for the current row.
     *    The function will be called in the context of the current {@link PMUI.grid.GridPanelRow row object}.
     * @cfg {Boolean} [disabledButtons=false] If the buttons will be disabled.
     * @cfg {String|Function|null} [buttonTooltip=null] Sets the tooltip message for the buttons (only applicable if
     * the column has its {@link #property-dataType dataType property} set to "button"). It can be a string
     * or null or a function. In the latter case the function must return a string, it will receive two parameters:
     *
     * - The cell, a {@link PMUI.grid.GridPanelCell GridPanelCell} object.
     * - The data that belongs to the current row.
     *
     * The function will be called in the context of the current
     * {@link PMUI.grid.GridPanelCell GridPanelCell cell object}.
     */
    var GridPanelColumn = function (settings) {
        /**
         * The cells that belongs to the column.
         * @type {PMUI.util.ArrayList}
         */
        this.cells = new PMUI.util.ArrayList();
        GridPanelColumn.superclass.call(this, jQuery.extend(settings, {elementTag: "th"}));
        /**
         * The text to be shown in the column header.
         * @type {String}
         */
        this.title = null;
        /**
         * The data type for the cells in this column. It is set by the {@link #cfg-dataType dataType config option} or
         * the {@link #method-setDataType setDataType()} method.
         * @type {String}
         */
        this.dataType = null;
        /**
         * If the column has the sorting functionality enabled.
         * @type {Boolean}
         */
        this.sortable = null;
        /**
         * If the column has the searching functionality enabled.
         * @type {Boolean}
         */
        this.searchable = null;
        /**
         * @event onButtonClick
         * Fired when a button in this column is clicked. The event is called in the column context.
         * @param {PMUI.grid.GridPanelRow} row The row the button belongs to.
         * @param {PMUI.grid.GridPanel} grid The grid the button belongs to.
         */
        this.onButtonClick = null;
        /**
         * The label for the buttons in the column (only applicable when the datatType is set to "button").
         * @type {String|Function}
         */
        this.buttonLabel = null;
        /**
         * If the buttons are disabled or not.
         * @type {Boolean}
         */
        this.disabledButtons = null;
        /**
         * The grid the column belongs to.
         * @type {PMUI.grid.GridPanel}
         */
        this.grid = null;
        /**
         * The data key to be used for the cells in this column.
         * @type {String}
         */
        this.columnData = null;
        /**
         * The column sort order ("ASC" or "DESC").
         * @type {String}
         */
        this.sortOrder = null;
        /**
         * The alignmentCell to be applied to the content of the cells that appertain to this column.
         * @type {String}
         */
        this.alignmentCell = null;
        /**
         * The style object for the cells that belongs to the column.
         * @type {PMUI.util.Style}
         * @private
         */
        this.cellStyle = null;
        /**
         * @property {PMUI.util.Style} buttonStyle The style object to be applied in the buttons that belong to the
         * column. Only applicable when the column's type is 'button'.
         */
        this.buttonStyle = null;
        /**
         * The HTML element in which the cell content will be appended.
         * @type {HTMLElement}
         * @private
         */
        this.contentTitle = null;
        /**
         * The tooltip to be shown for the buttons in the column. Only applicable when the column's type is 'button'.
         * @type {String|Function}
         */
        this.buttonTooltip = null;
        /**
         * The DOM element in which the sorting arrow is shown.
         * @type {HTMLElement}
         * @private
         */
        this.sortIcon = null;
        /**
         * Determines the alignment for the title in the column's header,
         * @type {String}
         */
        this.alignmentTitle = null;
        GridPanelColumn.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom("PMUI.core.Element", GridPanelColumn);

    GridPanelColumn.prototype.type = "GridPanelColumn";
    /**
     * Initialize the object
     * @param  {Object} [settings=null] A JSON object with the config options.
     * @private
     */
    GridPanelColumn.prototype.init = function (settings) {
        var defaults = {
            title: '[untitled]',
            dataType: 'string',
            sortable: false,
            searchable: true,
            onButtonClick: function () {
            },
            buttonLabel: '[button]',
            cells: [],
            grid: null,
            columnData: null,
            onSort: null,
            alignmentCell: "center",
            cellStyle: null,
            buttonStyle: null,
            visible: true,
            disabledButtons: false,
            buttonTooltip: null,
            alignmentTitle: 'center',
            positionMode: 'inherit'
        };

        jQuery.extend(true, defaults, settings);

        this.buttonStyle = new PMUI.util.Style();
        this.cellStyle = new PMUI.util.Style();
        this.onSort = defaults.onSort;
        this.setPositionMode(defaults.positionMode);
        this.setTitle(defaults.title)
            .setButtonTooltip(defaults.buttonTooltip)
            .setCellStyle(defaults.cellStyle)
            .setButtonStyle(defaults.buttonStyle)
            .setAlignmentCell(defaults.alignmentCell)
            .setColumnData(defaults.columnData)
            .setDataType(defaults.dataType)
            .setOnButtonClick(defaults.onButtonClick)
            .setButtonLabel(defaults.buttonLabel)
            .setGrid(defaults.grid)
            .setSortable(defaults.sortable)
            .setCells(defaults.cells)
            .setVisible(defaults.visible)
            .setAlignmentTitle(defaults.alignmentTitle);

        if (defaults.searchable) {
            this.enableSearch();
        } else {
            this.disableSearch();
        }

        if (defaults.disabled) {
            this.disable();
        } else {
            this.enable();
        }
    };
    /**
     * Sets the button tooltip. Only applicable
     * @param {String|Function|null} tooltip The tooltip to be used in the buttons in the column. It can be a string
     * or null or a function. In the latter case the function must return a string, it will receive two parameters:
     *
     * - The cell, a {@link PMUI.grid.GridPanelCell GridPanelCell} object.
     * - The data that belongs to the current row.
     *
     * The function will be called in the context of the current
     * {@link PMUI.grid.GridPanelCell GridPanelCell cell object}.
     */
    GridPanelColumn.prototype.setButtonTooltip = function (tooltip) {
        if (!(typeof tooltip === 'function' || typeof tooltip === 'string' || tooltip === null)) {
            throw new Error('setButtonTooltip(): the parameter must be a function or a string or null.');
        }
        this.buttonTooltip = tooltip;
        return this;
    };
    /**
     * Disables all the buttons in the column, this is applied only if the column's property
     * {@link #property-dataType dataType} is set to "button".
     * @chainable
     */
    GridPanelColumn.prototype.disable = function () {
        var i,
            cells,
            cellsLength;

        this.disabledButtons = true;
        cellsLength = (cells = this.cells.asArray()).length;
        for (i = 0; i < cellsLength; i += 1) {
            cells[i].disable();
        }
        return this;
    };
    /**
     * Enables all the buttons in the column, this is applied only if the column's property
     * {@link #property-dataType dataType} is set to "button".
     * @chainable
     */
    GridPanelColumn.prototype.enable = function () {
        var i,
            cells,
            cellsLength;

        if (this.dataType === 'button') {
            this.disabledButtons = false;
            cellsLength = (cells = this.cells.asArray()).length;
            for (i = 0; i < cellsLength; i += 1) {
                cells[i].enable();
            }
        }
        return this;
    };
    /**
     * [setVisible description]
     * @param {[type]} visible [description]
     */
    GridPanelColumn.prototype.setVisible = function (visible) {
        var cells,
            cellsLength,
            i;

        GridPanelColumn.superclass.prototype.setVisible.call(this, visible);
        if (this.cells) {
            cells = this.cells.asArray();
            cellsLength = cells.length;
            for (i = 0; i < cellsLength; i += 1) {
                cells[i].setVisible(this.visible);
            }
        }
        return this;
    };
    /**
     * [getUsableButtonStyle description]
     * @param  {[type]} cell [description]
     * @return {[type]}      [description]
     */
    GridPanelColumn.prototype.getUsableButtonStyle = function (cell) {
        var theStyle,
            buttonStyle = this.buttonStyle,
            theRow;

        if (typeof buttonStyle !== 'function') {
            return {
                cssClasses: buttonStyle.cssClasses,
                cssProperties: buttonStyle.cssProperties
            };
        } else {
            theRow = cell.row;
            theStyle = buttonStyle.call(theRow, cell, theRow.getData());
            return {
                cssClasses: (theStyle && theStyle.cssClasses) || [],
                cssProperties: (theStyle && theStyle.cssProperties) || {}
            };
        }
    };
    /**
     * Sets the style for the buttons in the column (only applicable if the column has its
     * {@link #property-dataType dataType property} set to "button").
     * @param {PMUI.util.Style|Object|null|Function} cellStyle The style to be applied. It can be:
     *
     *  - null then the buttons won't apply any custom style.
     *  - An object literal in that case it can have the properties: cssClasses (an array in which each element is
     *  an string that is a class name) and cssProperties (another object literal that can contain css properties and
     *  its respective values).
     *  - An instance of {@link PMUI.util.Style}.
     *  - A function that must return any of the previous value types.
     *  @chainable
     */
    GridPanelColumn.prototype.setButtonStyle = function (buttonStyle) {
        var i,
            cells,
            cellsNum,
            theStyle,
            theRow;

        if (buttonStyle !== null && typeof buttonStyle !== 'object' && typeof buttonStyle !== 'function' && !(buttonStyle instanceof PMUI.util.Style)) {
            throw new Error("setbuttonStyle(): The parameter only can be an object, an instance of PMUI.util.Style or null.");
        }
        this.buttonStyle = this.buttonStyle.clear();
        if (buttonStyle instanceof PMUI.util.Style) {
            this.buttonStyle = buttonStyle;
        } else if (typeof buttonStyle === 'function') {
            this.buttonStyle = buttonStyle;
        } else if (buttonStyle) {
            this.buttonStyle.addProperties((buttonStyle && buttonStyle.cssProperties) || {})
                .addClasses((buttonStyle && buttonStyle.cssClasses) || []);
        }
        if (this.dataType === 'button') {
            cells = this.cells.asArray();
            cellsNum = cells.length;
            for (i = 0; i < cellsNum; i += 1) {
                cells[i].getContent().setStyle(this.getUsableButtonStyle(cells[i]));
            }
        }
        return this;
    };
    /**
     * Returns the style for the buttons that belongs to the column.
     * @return {Object} An object literal with the properties: cssProperties and cssClasses
     */
    GridPanelColumn.prototype.getButtonStyle = function () {
        return {
            cssClasses: this.buttonStyle.cssClasses,
            cssProperties: this.buttonStyle.cssProperties
        };
    };
    /**
     * Sets the style for the cells in the column.
     * @param {PMUI.util.Style|Object|null} cellStyle The style to be applied. If it is null then the cells won't apply
     * any custom style.
     */
    GridPanelColumn.prototype.setCellStyle = function (cellStyle) {
        var i,
            cells,
            cellsNum;

        if (cellStyle !== null && typeof cellStyle !== 'object' && !(cellStyle instanceof PMUI.util.Style)) {
            throw new Error("setCellStyle(): The parameter only can be an object, an instance of PMUI.util.Style or null.");
        }
        this.cellStyle = this.cellStyle.clear();
        if (cellStyle instanceof PMUI.util.Style) {
            this.cellStyle = cellStyle;
        } else if (cellStyle) {
            this.cellStyle.addProperties(cellStyle.cssProperties || {})
                .addClasses(cellStyle.cssClasses || []);
        }
        cells = this.cells.asArray();
        cellsNum = cells.length;
        for (i = 0; i < cellsNum; i += 1) {
            cells[i].setStyle({
                cssClasses: this.cellStyle.cssClasses,
                cssProperties: this.cellStyle.cssProperties
            });
        }
        return this;
    };
    /**
     * Returns the style for the cells that belongs to the column.
     * @return {Object} An object literal with the properties: cssProperties and cssClasses
     */
    GridPanelColumn.prototype.getCellStyle = function () {
        return {
            cssProperties: this.cellStyle.cssProperties,
            cssClasses: this.cellStyle.cssClasses
        };
    };
    /**
     * Sets the alignmentCell for the content in the cells that appertain to the column.
     * @param {String} alignmentCell The accepted values are: "center", "left", "right".
     * @chainable
     */
    GridPanelColumn.prototype.setAlignmentCell = function (alignmentCell) {
        var i,
            cells;

        if (alignmentCell === 'center' || alignmentCell === 'left' || alignmentCell === 'right') {
            this.alignmentCell = alignmentCell;
            cells = this.getCells();
            for (i = 0; i < cells.length; i += 1) {
                cells[i].setAlignment(this.alignmentCell);
            }
        } else {
            throw new Error('setAlignment(): This method only accepts one of the following values: '
                + '"center", "left", "right"');
        }
        return this;
    };

    GridPanelColumn.prototype.setAlignmentTitle = function (alignmentTitle) {
        if (alignmentTitle === 'center' || alignmentTitle === 'left' || alignmentTitle === 'right') {
            this.alignmentTitle = alignmentTitle;
            if (this.contentTitle) {
                this.contentTitle.style.textAlign = alignmentTitle;
            }
        } else {
            throw new Error('setAlignment(): This method only accepts one of the following values: '
                + '"center", "left", "right"');
        }
        return this;
    };
    /**
     * Returns the alignment for the content in the cells that appertain to the column.
     * @return {String}
     */
    GridPanelColumn.prototype.getAlignment = function () {
        return this.alignment;
    };
    /**
     * Returns true is the filtering function is enabled, otherwise returns false.
     * @return {Boolean}
     */
    GridPanelColumn.prototype.isSearchable = function () {
        return this.searchable;
    };
    /**
     * Enables the filtering functionality.
     * @chainable
     */
    GridPanelColumn.prototype.enableSearch = function () {
        this.searchable = true;
        return this;
    };
    /**
     * Disables the filtering functionality.
     * @chainable
     */
    GridPanelColumn.prototype.disableSearch = function () {
        this.searchable = false;
        return this;
    };
    /**
     * Returns the data's field name that is used by the row to fill its cell for this column.
     * @return {String}
     */
    GridPanelColumn.prototype.getColumnData = function () {
        return this.columnData;
    };
    /**
     * Sets a function or a string that will represent the data's field name that is used by the row to fill its cell
     * for this column.
     * @param {String|Function} columnData It can be a String or a Function
     * - In case of a String: The data key to be used for the cells in this column.
     * - In case of a Function: A function that will return the content for the cells in column, the parameters receive
     * by the function are:
     *    - data {Object}: a JSON object with the data for the current row.
     *    The function will be called in the context of the current {@link PMUI.grid.GridPanelRow row object}.
     * @chainable
     */
    GridPanelColumn.prototype.setColumnData = function (columnData) {
        if (columnData === null || typeof columnData === 'string' || typeof columnData === 'function') {
            this.columnData = columnData;
        } else {
            throw new Error('setColumnData(): The method only accepts a string or a function or NULL as parameter.');
        }

        return this;
    };
    /**
     * Sets the title to be shown in the column header.
     * @param {String} title
     * @chainable
     */
    GridPanelColumn.prototype.setTitle = function (title) {
        this.title = title;
        if (this.html) {
            jQuery(this.contentTitle).empty();
            this.contentTitle.appendChild(document.createTextNode(title));
            this.contentTitle.appendChild(this.sortIcon);
        }
        return this;
    };
    /**
     * Returns the column's data type.
     * @return {String}
     */
    GridPanelColumn.prototype.getDataType = function () {
        return this.dataType;
    };
    /**
     * Sets the data type for the column.
     * @param {String} dataType The accepted values are:
     *
     * - "number" for cells with numeric values.
     * - "string" for cells with string values.
     * - "index" for cells with the row index.
     * - "date" for cells with date values.
     * - "button" for cells that contain a button.
     * @chainable
     */
    GridPanelColumn.prototype.setDataType = function (dataType) {
        if (dataType === 'string' || dataType === 'number' || dataType === 'index' || dataType === 'date' || dataType === 'button') {
            this.dataType = dataType;
        } else {
            throw new Error('setDataType(): It only accepts "string", "number", "index" or "date".');
        }
        return this;
    };
    /**
     * Enables or disables the sorting functionality.
     * @param {Boolean} sortable
     * @chainable
     */
    GridPanelColumn.prototype.setSortable = function (sortable) {
        if (!(typeof sortable === 'boolean')) {
            throw new Error("setSortable(): property 'sortable' should be a boolean");
        }
        //TODO enable event to listen the clicks on column for sorting
        this.sortable = sortable;
        if (this.html) {
            if (sortable) {
                this.style.addClasses(['pmui-sortable']);
            } else {
                this.style.removeClasses(['pmui-sortable']);
            }
        }
        return this;
    };
    /**
     * Returns the callback function to be executed when a column's button is clicked.
     * @return {Function}
     */
    GridPanelColumn.prototype.getOnButtonClick = function () {
        return this.onButtonClick;
    };
    /**
     * Sets the callback function to be executed when a column's button is clicked.
     * @param {Function} onButtonClick
     * @chainable
     */
    GridPanelColumn.prototype.setOnButtonClick = function (onButtonClick) {
        if (!(typeof onButtonClick == 'function')) {
            throw new Error("setOnButtonClick(): property 'onButtonClick' should be a function");
        }
        this.onButtonClick = onButtonClick;
        //TODO update the listeners for all the buttons in the column
        return this;
    };
    /**
     * Sets the text for the button labels in the column.
     * @param {String|Function} buttonLabel A string or a function that returns the string for the button. Three
     * parameters function will be sent to the function:
     *
     * - The row the button belongs to ({@link PMUI.grid.GridPanelRow GridPanelRow}).
     * - The data from the row (object literal).
     *
     * It is important to mention that the function is called in the context of the column object.
     * @chainable
     */
    GridPanelColumn.prototype.setButtonLabel = function (buttonLabel) {
        if (typeof buttonLabel !== 'string' && typeof buttonLabel !== 'function') {
            throw new Error('setButtonLabel(): the parameter must be a string or a function.');
        }
        this.buttonLabel = buttonLabel;
        //TODO update the label for all the buttons in the column
        return this;
    };
    /**
     * Set the grid the column belongs to.
     * @param {PMUI.grid.Grid} grid
     * @chainable
     */
    GridPanelColumn.prototype.setGrid = function (grid) {
        if (!(grid instanceof PMUI.grid.GridPanel)) {
            throw new Error('setGrid(): the grid property should be instance of [PMUI.grid.GridPanel]');
        }
        this.grid = grid;
        return this;
    };
    /**
     * Adds a cell to the column.
     * @param {PMUI.grid.GridPanelCell} cell
     * @chainable
     */
    GridPanelColumn.prototype.addCell = function (cell) {
        if (!(cell instanceof PMUI.grid.GridPanelCell)) {
            throw new Error("addCell(): this method only accepts instances of PMUI.grid.GridPanelCell.");
        }
        cell.setAlignment(this.alignmentCell);
        cell.setStyle({
            cssProperties: this.cellStyle.getProperties(),
            cssClasses: this.cellStyle.getClasses()
        });
        this.cells.insert(cell);

        return this;
    };
    /**
     * Set the cells for the column.
     * @param {PMUI.util.ArrayList|Array} cells An array in which every element is an instance of the
     * {@link PMUI.grid.GridPanelCell GridPanelCell class}.
     * @chainable
     */
    GridPanelColumn.prototype.setCells = function (cells) {
        var cellsAux = [],
            i;

        this.clearCells();

        if (cells instanceof PMUI.util.ArrayList) {
            cellsAux = cells.items.asArray();
        } else if (jQuery.isArray(cells)) {
            cellsAux = cells;
        } else {
            throw new Error('setCells(): the cells property should be instanceof of PMUI.util.ArrayList or an Array');
        }
        for (i = 0; i < cellsAux.length; i += 1) {
            this.addCell(cellsAux[i]);
        }
        return this;
    };
    /**
     * Return the column's cells.
     * @return {Array} An array in which each element is an instance of the
     * {@link PMUI.grid.GridPanelCell GridPanelCell class}.
     */
    GridPanelColumn.prototype.getCells = function () {
        return this.cells.asArray().slice(0);
    };
    /**
     * Removes a cell from the column.
     * @param  {PMUI.grid.GridPanelCell|Number|String} cell The cell to remove, it can be:
     *
     * - An instance of {@link PMUI.grid.GridPanelCell GridPanelCell}, in this case this cell must belong to the
     * column.
     * - A Number, in this case it is interpreted as the index of the cell to remove.
     * - A String, in this case it is interpreted as the id of the cell to remove.
     * @chainable
     */
    GridPanelColumn.prototype.removeCell = function (cell) {
        var cellToRemove;

        if (cell instanceof PMUI.grid.GridPanelCell) {
            cellToRemove = cell;
        } else if (typeof cell === 'number') {
            cellToRemove = this.cells.get(cell);
        } else if (typeof cell === 'string') {
            cellToRemove = this.cells.find("id", cell);
        }

        if (cellToRemove) {
            cellToRemove.column = null;
            this.cells.remove(cellToRemove);
        }

        return this;
    };
    /**
     * Clears all the cells of the column.
     * @chainable
     */
    GridPanelColumn.prototype.clearCells = function () {
        this.cells.clear();
        while (this.cells.getSize() > 0) {
            this.removeCell(0);
        }
        return this;
    };
    /**
     * Defines the event for the object.
     * @chainable
     */
    GridPanelColumn.prototype.defineEvents = function () {
        var that = this;

        this.removeEvents().eventsDefined = true;
        if (this.html) {
            this.addEvent('click').listen(this.html, function () {
                if (that.sortable) {
                    that.sortOrder = that.sortOrder === 'asc' ? 'desc' : 'asc';
                    if (typeof that.onSort === 'function') {
                        that.onSort(that.sortOrder);
                    }
                }
            });
        }
        return this;
    };
    /**
     * Creates the html for the object.
     * @return {HTMLElement}
     */
    GridPanelColumn.prototype.createHTML = function () {
        var contentTitle,
            sortIcon;

        if (this.html) {
            return this.html;
        }
        GridPanelColumn.superclass.prototype.createHTML.call(this);
        sortIcon = PMUI.createHTMLElement('span');
        sortIcon.className = 'pmui-grid-sort-icon';
        contentTitle = PMUI.createHTMLElement('span');
        contentTitle.className = 'pmui-gridpanelColumn-title';
        this.contentTitle = contentTitle;
        this.sortIcon = sortIcon;
        this.html.appendChild(contentTitle);
        this.setTitle(this.title)
            .setSortable(this.sortable)
            .setWidth(this.width)
            .setAlignmentTitle(this.alignmentTitle);
        return this.html;
    };

    GridPanelColumn.prototype.setWidth = function (width) {
        var finalWidth,
            i,
            cellsLength,
            cells;

        if (typeof width === 'number') {
            this.width = width;
        } else if (/^\d+(\.\d+)?px$/.test(width)) {
            this.width = parseInt(width, 10);
        } else if (/^\d+(\.\d+)?%$/.test(width)) {
            this.width = width;
        } else if (/^\d+(\.\d+)?em$/.test(width)) {
            this.width = width;
        } else if (width === 'auto') {
            this.width = width;
        } else {
            throw new Error('setWidth: width param is not a number');
        }

        if (typeof this.width === 'string') {
            finalWidth = 'auto';
        } else {
            finalWidth = this.width + 'px';
        }

        if (this.html) {
            this.contentTitle.style.minWidth = finalWidth;
        }
        cellsLength = (cells = this.cells.asArray()).length;
        for (i = 0; i < cellsLength; i += 1) {
            cells[i].setWidth(finalWidth);
        }
        return this;
    };

    GridPanelColumn.prototype.applyStyle = function () {
        if (this.html) {
            this.style.applyStyle();

            this.style.addProperties({
                display: this.visible ? this.display : "none",
                position: this.positionMode,
                left: this.x,
                top: this.y,
                width: (typeof this.width === 'string') ? this.width : 'auto',
                height: this.height,
                zIndex: this.zOrder
            });
        }
        return this;
    };

    PMUI.extendNamespace("PMUI.grid.GridPanelColumn", GridPanelColumn);
    if (typeof exports !== 'undefined') {
        module.exports = GridPanelColumn;
    }
}());