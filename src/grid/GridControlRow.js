(function () {
    var GridControlRow = function (settings) {
        GridControlRow.superclass.call(this, settings);
        this.removeButton = null;
    };

    PMUI.inheritFrom('PMUI.grid.GridPanelRow', GridControlRow);

    GridControlRow.prototype.getCell = function (column) {
        var cell = null;
        if (column instanceof PMUI.grid.GridControlColumn) {
            cell = this.cells.find("column", column);
        } else if (typeof column === 'number') {
            cell = this.cells.get(column);
        }
        return cell;
    };

    GridControlRow.prototype.setCells = function () {
        var i,
            columns,
            cellsFactory,
            cell;

        this.clearCells();
        if (this.parent) {
            columns = this.parent.columns.asArray();
            cellsFactory = this.parent.cellsFactory;
            for (i = 0; i < columns.length; i += 1) {
                cell = columns[i].createCell();
                cell.column = columns[i];
                cell.row = this;
                this.cells.insert(cell);
                if (this.html) {
                    this.html.appendChild(cell.getHTML());
                }
                this.data.addAttribute(cell.column.name, cell.getContent());
            }
        }
        return this;
    };

    GridControlRow.prototype.updateData = function (cell) {
        if (this.cells.contains(cell)) {
            this.data.addAttribute(cell.column.name, cell.getContent());
        }
        return this;
    };

    GridControlRow.prototype.defineEvents = function () {
        var that = this;

        if (!this.removeButton) {
            return this;
        }

        jQuery(this.html).on('mouseenter', function () {
            var cell = that.getCell(that.cells.getSize() - 1);
            cell.html.appendChild(that.removeButton);
        }).on('mouseleave', function () {
            jQuery(that.removeButton).detach();
        });
        jQuery(this.removeButton).on('click', '.pmui-gridpanelrow-deleteButton-link', function () {
            if (that.parent) {
                that.parent.removeItem(that);
            }
        });
        return this;
    };

    GridControlRow.prototype.createHTML = function () {
        var deleteButton,
            deleteAnchor;

        if (this.html) {
            return this.html;
        }
        GridControlRow.superclass.prototype.createHTML.call(this);
        deleteButton = PMUI.createHTMLElement('div');
        deleteAnchor = PMUI.createHTMLElement('a');
        deleteButton.className = 'pmui-gridpanelrow-deleteButton';
        deleteAnchor.className = 'pmui-gridpanelrow-deleteButton-link';
        deleteAnchor.href = '#';
        deleteButton.appendChild(deleteAnchor);
        this.removeButton = deleteButton;
        this.defineEvents();
        return this.html;
    };

    GridControlRow.prototype.isValid = function () {
        var i,
            cells = this.cells.asArray(),
            valid = true;

        for (i = 0; i < cells.length; i += 1) {
            valid = valid && cells[i].isValid();
            if (!valid) {
                return valid;
            }
        }
        return valid;
    };

    PMUI.extendNamespace('PMUI.grid.GridControlRow', GridControlRow);
}());