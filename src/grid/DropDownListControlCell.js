(function () {
    var DropDownListControlCell = function (settings) {
        DropDownListControlCell.superclass.call(this, settings);
        DropDownListControlCell.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.grid.GridControlCell', DropDownListControlCell);

    DropDownListControlCell.prototype.init = function (settings) {
        var defaults = {
            options: [],
            value: null
        };

        jQuery.extend(true, defaults, settings);

        this.setOptions(defaults.options);

        if (defaults.value !== null) {
            this.setValue(defaults.value);
        }
    };

    DropDownListControlCell.prototype.initControls = function () {
        this.controls[0] = new PMUI.control.DropDownListControl();
        return this;
    };

    DropDownListControlCell.prototype.clearOptions = function () {
        this.controls[0].clearOptions();
        this.content = this.controls[0].getValue();
        return this;
    };

    DropDownListControlCell.prototype.enableDisableOption = function (disabled, option, group) {
        this.controls[0].enableDisableOption(disabled, option, group);
        return this;
    };

    DropDownListControlCell.prototype.disableOption = function (option, group) {
        return this.controls[0].enableDisableOption(true, option, group);
    };

    DropDownListControlCell.prototype.enableOption = function (option, group) {
        return this.controls[0].enableDisableOption(false, option, group);
    };

    DropDownListControlCell.prototype.removeOption = function (option, group) {
        this.controls[0].removeOption(option, group);
        return this;
    };

    DropDownListControlCell.prototype.addOptionGroup = function (optionGroup) {
        this.controls[0].addOptionGroup(optiongroup);
        return this;
    };

    DropDownListControlCell.prototype.addOption = function (option, group) {
        this.controls[0].addOption(option, group);
        return this;
    };

    DropDownListControlCell.prototype.getSelectedLabel = function () {
        return this.controls[0].getSelectedLabel();
    };

    DropDownListControlCell.prototype.valueExistsInOptions = function (value) {
        return this.controls[0].valueExistsInOptions(value);
    };

    DropDownListControlCell.prototype.getFirstAvailableOption = function () {
        return this.controls[0].getFirstAvailableOption();
    }

    DropDownListControlCell.prototype.setOptions = function (options) {
        this.controls[0].setOptions(options);
        return this;
    };

    DropDownListControlCell.prototype.getOptions = function (includeGroups) {
        return this.controls[0].getOptions(includeGroups);
    };

    DropDownListControlCell.prototype.setValuesToControls = function (values) {
        if (this.controls.length) {
            this.controls[0].setValue(values);
            this.content = this.controls[0].getValue();
        }
        return this;
    };

    DropDownListControlCell.prototype.getContent = function () {
        return this.controls[0].getValue();
    };

    PMUI.extendNamespace('PMUI.grid.DropDownListControlCell', DropDownListControlCell);
}());