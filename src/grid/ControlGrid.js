(function () {
    var ControlGrid = function (settings) {
        ControlGrid.superclass.call(this, jQuery.extend(true, settings, {
            factory: {
                products: {
                    "GridControlRow": PMUI.grid.GridControlRow
                },
                defaultProduct: "GridControlRow"
            }
        }));
        this.columnsFactory = new PMUI.util.Factory();
        this.dependencies = {};
        ControlGrid.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.grid.GridPanel', ControlGrid);

    ControlGrid.prototype.init = function (settings) {
        var defaults = {
            allowedCells: {
                'text': PMUI.grid.TextControlColumn,
                'dropdownlist': PMUI.grid.DropDownListControlColumn,
                'checkboxgroup': PMUI.grid.CheckBoxGroupColumn,
                'number': PMUI.grid.NumberColumn
            }
        };

        jQuery.extend(true, defaults, settings);

        this.setAllowedCells(defaults.allowedCells)
            .setColumns(defaults.columns);
    };

    ControlGrid.prototype.setAllowedCells = function (allowedCells) {
        this.columnsFactory.clearProducts()
            .setProducts(allowedCells);
        return this;
    };

    ControlGrid.prototype.getColumn = function (column) {
        var the_column = null;
        if (typeof column === 'string') {
            the_column = this.columns.find('name', column);
        } else if (typeof column === 'number') {
            the_column = this.columns.get(column);
        }
        return the_column;
    };

    ControlGrid.prototype.setColumns = function (columns) {
        if (this.columnsFactory) {
            ControlGrid.superclass.prototype.setColumns.call(this, columns);
        }
        return this;
    };

    ControlGrid.prototype.removeColumn = function (column) {
        ControlGrid.superclass.prototype.removeColumn.call(this, column);
        return this.updateDependencies();
    };

    ControlGrid.prototype.updateDependencies = function () {
        var dependencies = {},
            i,
            j,
            columns = this.columns.asArray(),
            dependent,
            dependents;

        for (i = 0; i < columns.length; i += 1) {
            dependents = columns[i].dependentCols;
            for (j = 0; j < dependents.length; j += 1) {
                if (this.getColumn(dependents[j])) {
                    if (!dependencies[dependents[j]]) {
                        dependencies[dependents[j]] = [];
                    }
                    dependencies[dependents[j]].push(columns[i]);
                }
            }
        }
        this.dependencies = dependencies;
        return this;
    };

    ControlGrid.prototype.addColumn = function (column) {
        var newColumn;

        if (typeof column === 'object') {
            column.grid = this;
            newColumn = this.columnsFactory.make(column);
        } else {
            throw new Error('addColumn(): invalid column to add');
        }

        if (newColumn) {
            this.columns.insert(newColumn);
            if (this.dom.thead) {
                this.dom.thead.appendChild(newColumn.getHTML());
            }
        }
        this.updateDependencies();
        return this;
    };

    /**
     * Disables one or all the columns.
     * @param {String|Number|PMUI.grid.GridPanelColumn} [column] A criteria to select the column that will be
     * disabled. It supports the following data types:
     *
     * - String, in this case the parameter is used as the column's id.
     * - Number, in this case the parameter is used as the column's index.
     * - An instance of {@link PMUI.grid.GridPanelColumn GridPanelColumn}, in this case the supplied object must be
     * a child of the grid.
     * - [no value], since this parameter is optional, you can skip it. In this case all the columns in the grid will
     * be disabled.
     * @chainable
     */
    ControlGrid.prototype.disableColumn = function (column) {
        var i,
            targetColumn = this.getColumns(column);

        for (i = 0; i < targetColumn.length; i += 1) {
            targetColumn[i].disable();
        }
        return this;
    };
    /**
     * [disableColumn description]
     * @param  {[type]} column [description]
     * @return {[type]}        [description]
     */
    ControlGrid.prototype.enableColumn = function (column) {
        var i,
            targetColumn = this.getColumns(column);

        for (i = 0; i < targetColumn.length; i += 1) {
            targetColumn[i].enable();
        }
        return this;
    };

    ControlGrid.prototype.isValid = function () {
        var i,
            rows,
            valid = true;

        rows = this.getItems();
        for (i = 0; i < rows.length; i += 1) {
            valid = valid && rows[i].isValid();
            if (!valid) {
                return valid;
            }
        }
        return valid;
    };

    ControlGrid.prototype.fireDependency = function () {
    };

    PMUI.extendNamespace('PMUI.grid.ControlGrid', ControlGrid);
}());