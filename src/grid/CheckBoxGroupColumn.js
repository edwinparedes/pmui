(function () {
    var CheckBoxGroupColumn = function (settings) {
        CheckBoxGroupColumn.superclass.call(this, settings);
        this.controlPositioning = null;
        this.maxDirectionOptions = null;
        this.options = [];
        CheckBoxGroupColumn.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.grid.GridControlColumn', CheckBoxGroupColumn);

    CheckBoxGroupColumn.prototype.CELL = PMUI.grid.CheckBoxGroupControlCell;

    CheckBoxGroupColumn.prototype.init = function (settings) {
        var defaults = {
            options: [],
            controlPositioning: "vertical",
            maxDirectionOptions: 1
        };
        jQuery.extend(true, defaults, settings);

        this.setOptions(defaults.options);
        this.setMaxDirectionOptions(defaults.maxDirectionOptions);
        this.setControlPositioning(defaults.controlPositioning);
    };

    CheckBoxGroupColumn.prototype.setOptions = function (options) {
        var i,
            cells;

        if (!jQuery.isArray(options)) {
            throw new Error("setOptions(): the parameter must be an array.");
        }
        this.options = options;
        cells = this.cells.asArray();
        for (i = 0; i < cells.length; i += 1) {
            cells[i].setOptions(this.options);
        }
        return this;
    };
    CheckBoxGroupColumn.prototype.setControlPositioning = function (positioning) {
        this.controlPositioning = positioning;
        return this;
    };

    CheckBoxGroupColumn.prototype.setMaxDirectionOptions = function (maxDirectionOptions) {
        this.maxDirectionOptions = maxDirectionOptions;
        return this;
    };

    CheckBoxGroupColumn.prototype.getCellDefaultCfg = function () {
        return jQuery.extend(true, CheckBoxGroupColumn.superclass.prototype.getCellDefaultCfg.call(this), {
            controlPositioning: this.controlPositioning,
            maxDirectionOptions: this.maxDirectionOptions,
            options: this.options
        });
    };

    PMUI.extendNamespace('PMUI.grid.CheckBoxGroupColumn', CheckBoxGroupColumn);
}());