(function () {
    var TextControlColumn = function (settings) {
        TextControlColumn.superclass.call(this, settings);
        this.maxLength = null;
        this.placeholder = null;
        this.trimOnBlur = null;
        this.readOnly = null;
        TextControlColumn.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.grid.GridControlColumn', TextControlColumn);

    TextControlColumn.prototype.CELL = PMUI.grid.GridTextControlCell;

    TextControlColumn.prototype.init = function (settings) {
        var defaults = {
            maxLength: null,
            placeholder: null,
            trimOnBlur: false,
            readOnly: false
        };

        jQuery.extend(true, defaults, settings);

        this.setMaxLength(defaults.maxLength)
            .setPlaceHolder(defaults.placeholder)
            .setTrimOnBlur(defaults.trimOnBlur)
            .setReadOnly(defaults.readOnly);
    };

    TextControlColumn.prototype.setMaxLength = function (input) {
        var i,
            cells,
            cellsLength;

        if (typeof input !== 'number') {
            throw new Error("setMaxLength(): the parameter must be a number.");
        }
        this.maxLength = input;
        cells = this.cells.asArray();
        cellsLength = cells.length;
        for (i = 0; i < cellsLength; i += 1) {
            cells[i].setMaxLength(this.maxLength);
        }
        return this;
    };
    TextControlColumn.prototype.setPlaceHolder = function (input) {
        var i,
            cells,
            cellsLength;

        if (typeof input !== 'string') {
            throw new Error("setPlaceHolder(): the parameter must be a string.");
        }
        this.placeholder = input;
        cells = this.cells.asArray();
        cellsLength = cells.length;
        for (i = 0; i < cellsLength; i += 1) {
            cells[i].setPlaceHolder(this.placeholder);
        }
        return this;
    };
    TextControlColumn.prototype.setTrimOnBlur = function (input) {
        var i,
            cells,
            cellsLength;

        this.trimOnBlur = !!input;
        cells = this.cells.asArray();
        cellsLength = cells.length;
        for (i = 0; i < cellsLength; i += 1) {
            cells[i].setTrimOnBlur(this.trimOnBlur);
        }
        return this;
    };
    TextControlColumn.prototype.setReadOnly = function (input) {
        var i,
            cells,
            cellsLength;

        this.readOnly = !!input;
        cells = this.cells.asArray();
        cellsLength = cells.length;
        for (i = 0; i < cellsLength; i += 1) {
            cells[i].setReadOnly(this.readOnly);
        }
        return this;
    };
    TextControlColumn.prototype.getCellDefaultCfg = function () {
        return jQuery.extend(true, TextControlColumn.superclass.prototype.getCellDefaultCfg.call(this), {
            maxLength: this.maxLength,
            trimOnBlur: this.trimOnBlur,
            placeholder: this.placeholder,
            readOnly: this.readOnly
        });
    };

    PMUI.extendNamespace('PMUI.grid.TextControlColumn', TextControlColumn);
}());