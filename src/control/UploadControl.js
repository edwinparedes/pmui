(function () {
    var UploadControl = function (settings) {
        UploadControl.superclass.call(this, settings);
        this.accept = null;
        this.multiple = null;
        this.files = null;
        UploadControl.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.control.HTMLControl', UploadControl);

    UploadControl.prototype.type = 'UploadControl';
    UploadControl.prototype.family = 'control';

    UploadControl.prototype.init = function (settings) {
        var defaults;
        defaults = {
            accept: "",
            multiple: false,
            height: '100%'
        };

        jQuery.extend(true, defaults, settings);

        this.setAcceptedFiles(defaults.accept)
            .setMultiple(defaults.multiple)
            .setHeight(defaults.height);
    };

    /**
     * @method setAccepted
     * here we can define the  type  files our send, the valid types are MIME, separation with ","
     * for more information go to http://reference.sitepoint.com/html/mime-types-full
     * @param {String} accept MIME types are valid for our upload
     */
    UploadControl.prototype.setAcceptedFiles = function (accept) {
        if (!(typeof accept === "string")) {
            throw new Error('setAcceptedFiles(): The parameter type is invalid, should be type string');
        }
        this.accept = '';
        this.accept = accept;
        if (this.html) {
            this.html.accept = accept;
        }
        return this;
    };

    UploadControl.prototype.defineEvents = function () {
        var that = this;
        UploadControl.superclass.prototype.defineEvents.call(this);
        this.html.addEventListener("click", function (e) {
            e.stopPropagation();
        });
        return this;
    };

    UploadControl.prototype.setMultiple = function (value) {
        if (!(typeof value === 'boolean')) {
            throw new Error('setMultiple(): The parameter type is invalid, should be type string');
        }
        this.multiple = value;
        if (this.html) {
            this.html.multiple = value;
        }
        return this;
    };

    UploadControl.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }
        UploadControl.superclass.prototype.createHTML.call(this);
        this.html.type = 'file';
        this.setMultiple(this.multiple);
        this.setAcceptedFiles(this.accept);

        return this.html;
    };

    PMUI.extendNamespace('PMUI.control.UploadControl', UploadControl);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = UploadControl;
    }
}());