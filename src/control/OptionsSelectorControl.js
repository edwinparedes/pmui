(function () {
    /**
     * @class PMUI.control.OptionsSelectorControl
     * @extends PMUI.control.HTMLControl
     * Class to handle the Select HTML form element.
     *
     * Example usage:
     *
     *      @example
     *               OptionsSelectorControl = new PMUI.control.OptionsSelectorControl({
     *                   items: [
     *                       {
     *                           text: "First option"
     *                       },
     *                       {
     *                           text: "Second option",
     *                           selected: true
     *                       },
     *                       {
     *                           text: "Third option"
     *                       }
     *                   ],
     *                   listeners: {
     *                       select: function (item, event) {
     *                           console.log("selected", item);
     *                       }
     *                  }
     *               });
     *               document.body.appendChild(OptionsSelectorControl.getHTML());
     *               OptionsSelectorControl.defineEvents();
     *
     * @cfg {Array} items Defines the data items as an array objects.
     *
     *              items: [
     *                  {
     *                      ...
     *                  },
     *                  {
     *                      text: "Item1",
     *                      selected: true
     *                  },
     *                  {
     *                      ...
     *                  }
     *              ]
     *
     * @cfg {Boolean} multipleSelection Defines wether the component will support the behavior of the check group.
     *
     *
     */
    var OptionsSelectorControl = function (settings) {
        OptionsSelectorControl.superclass.call(this, settings);
        /**
         * @property {Boolean} [multipleSelection=false] If the property is enabled, the items can be
         * selected like checkGroup otherwise is like to radioGroup
         */
        this.multipleSelection = null;
        /**
         * @property {String} [orientation=null]
         *
         */
        this.orientation = null;
        /**
         * @property {Object} [container=null]
         * @private
         */
        this.container = null;
        /**
         * @property {Object} [listeners] A config object containing one or more handlers to be added
         * to this object during initialization. By default the 'select' handler is enabled
         * @readOnly
         */
        this.listeners = {
            /**
             * @event select
             * Fires when the component has been selected. Returning the current object and the event.
             * @return {Object} {@link PMUI.control.OptionsSelectorItemControl OptionsSelectorItemControl}
             * @return {Object} event
             */
            select: function () {
            }
        };
        /**
         * @property {@link PMUI.util.ArrayList ArrayList}
         *
         */
        this.itemsSelected = null;

        this.items = new PMUI.util.ArrayList();
        OptionsSelectorControl.prototype.init.call(this, settings);
    };
    PMUI.inheritFrom('PMUI.control.HTMLControl', OptionsSelectorControl);
    OptionsSelectorControl.prototype.type = "control";
    OptionsSelectorControl.prototype.family = "control";

    OptionsSelectorControl.prototype.init = function (settings) {
        var defaults = {
            multipleSelection: false,
            orientation: 'horizontal',
            items: [],
            iconClass: '',
            listeners: {
                select: function () {
                }
            }
        };
        jQuery.extend(true, defaults, settings);
        this.setMultipleSelection(defaults.multipleSelection)
            .setOrientation(defaults.orientation)
            .setChildren(defaults.items)
            .setIconClass(defaults.iconClass)
            .setListeners(defaults.listeners);

        this.itemsSelected = new PMUI.util.ArrayList();

    };
    /**
     * Sets the value whether the {@link PMUI.control.OptionsSelectorItemControl items} can be selected
     * at the same time
     * @param {Boolean} status
     */
    OptionsSelectorControl.prototype.setMultipleSelection = function (status) {
        this.multipleSelection = (typeof status === 'boolean') ? status : false;
        return this;
    };
    /**
     * Sets the orientation for every {@link PMUI.control.OptionsSelectorItemControl element}
     * There are two possibilities:
     * - horizontal, the elements will appear horizontally
     * - vertical, the elements will appear vertically
     * @param {String} orientation
     */
    OptionsSelectorControl.prototype.setOrientation = function (orientation) {
        var i,
            items,
            options = {
                horizontal: true,
                vertical: true
            },
            layout;
        this.orientation = (options[orientation]) ? orientation : 'horizontal';
        if (this.html) {
            items = this.items.asArray();
            for (i = 0; i < items.length; i += 1) {
                if (this.orientation === "vertical") {
                    items[i].html.setAttribute("style", "display:block;");
                    this.html.setAttribute("style", "display:inline-table;");
                } else {
                    items[i].html.removeAttribute("style");
                    this.html.removeAttribute("style");
                }
            }
        }
        return this;
    };
    /**
     * Sets the {@link PMUI.control.OptionsSelectorItemControl elements} child inside of
     * {@link PMUI.control.OptionsSelectorControl container}
     * @param {Array} items Represents to children
     */
    OptionsSelectorControl.prototype.setChildren = function (items) {
        var i;
        if (items instanceof Array) {
            this.items.clear();
            for (i = 0; i < items.length; i += 1) {
                this.addItem(items[i]);
            }
        }
        return this;
    };
    /**
     * SetValue method
     * @param {String} values Defines the current values of the items
     */
    OptionsSelectorControl.prototype.setValue = function (values) {
        var options,
            items,
            i,
            separator;

        if (values) {
            if (values.length > 0) {
                //options = JSON.parse(values);
                separator = this.field.separator;
                options = values.split(separator);
                items = this.items.asArray();
                for (i = 0; i < items.length; i += 1) {
                    if (jQuery.inArray(items[i].getValue(), options) !== -1) {
                        items[i].selected = false;
                        items[i].selectItem();
                    }
                }

                if (this.multipleSelection === false && options.length > 1) {
                    throw  new Error("Is not possible sets more than one when the multipleSelection property is false.")
                }
            }
        }

        return this;
    };
    /**
     * Sets the class name for the icon that appears in the header
     * @param {String} class name
     */
    OptionsSelectorControl.prototype.setIconClass = function (className) {
        this.iconClass = (typeof className === 'string') ? className : '';
        return this;
    };
    /**
     * Gets items selected
     * @return {Object} this
     */
    OptionsSelectorControl.prototype.getItemsSelected = function () {
        return this.itemsSelected.asArray()
    };
    /**
     * Sets listeners to object
     * @param {Object} listeners
     */
    OptionsSelectorControl.prototype.setListeners = function (listeners) {
        if (typeof listeners === 'object') {
            this.listeners = listeners;
        }
        return this;
    };
    /**
     * Adds new items to the component
     * @param {Object|{@link PMUI.control.OptionsSelectorItemControl OptionsSelectorItemControl}} item Represents the item
     * for the parent container.
     * @return {PMUI.control.OptionsSelectorControl} this
     */
    OptionsSelectorControl.prototype.addItem = function (item) {
        var factory,
            itemToBeAdded;
        factory = new PMUI.behavior.BehaviorFactory({
            products: {
                "optionsSelectorItem": PMUI.control.OptionsSelectorItemControl
            },
            defaultProduct: "optionsSelectorItem"
        });
        if (typeof item === "object") {
            itemToBeAdded = factory.make(item);
        }

        itemToBeAdded.parent = this;
        this.items.insert(itemToBeAdded);
        if (this.html) {
            this.html.appendChild(itemToBeAdded.getHTML());
            itemToBeAdded.defineEvents();
        }
        return this;
    };
    /**
     * Removes and item
     * @param  {[type]} item [description]
     * @return {[type]}      [description]
     */
    OptionsSelectorControl.prototype.removeOption = function (position) {
        var items = this.items.asArray(),
            item;
        if (items[position]) {
            item = items[position];
            this.items.remove(item);
            this.html.removeChild(item.html);
        }
        return item;
    };
    /**
     * [disableOption description]
     * @param  {[type]} position [description]
     * @param  {[type]} disabled [description]
     * @return {[type]}          [description]
     */
    OptionsSelectorControl.prototype.disableOption = function (position, disable) {
        var items = this.items.asArray(),
            item;

        if (items[position]) {
            item = items[position];
            item.setDisable(disable)
        }

        return item;
    };
    OptionsSelectorControl.prototype.getOptions = function () {
        return this.items.asArray();
    };
    OptionsSelectorControl.prototype.clearOptions = function () {
        var items = this.items.asArray(),
            i;
        for (i = 0; i < items.length; i += 1) {
            this.html.removeChild(items[i].html);
        }

        this.items.clear();
        return this;
    };

    /**
     * Creates the HTML elements
     * @return {HTMLElement} Returns the HTMLElement of the current component
     */
    OptionsSelectorControl.prototype.createHTML = function () {
        var container,
            items,
            i;
        container = PMUI.createHTMLElement("span");
        container.className = "pmui-field-control-container";
        if (this.orientation === "vertical") {
            container.setAttribute("style", "display:inline-table;");
        }

        items = this.items.asArray();
        if (typeof items.length === 'number') {
            for (i = 0; i < items.length; i += 1) {
                container.appendChild(items[i].getHTML());
            }
        }
        container.id = this.id;
        this.html = container;
        return this.html;
    };
    /**
     * Defines and active all the events related to the object
     */
    OptionsSelectorControl.prototype.defineEvents = function () {
        var that = this,
            items,
            i;

        this.eventsDefined = true;
        if (that.items.getSize() > 0) {
            items = that.items.asArray();
            for (i = 0; i < items.length; i += 1) {
                items[i].defineEvents();
            }
        }
        return this;
    };


    PMUI.extendNamespace("PMUI.control.OptionsSelectorControl", OptionsSelectorControl);
    if (typeof exports !== "undefined") {
        module.exports = OptionsSelectorControl;
    }

}());