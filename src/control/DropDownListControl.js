(function () {
    /**
     * @class PMUI.control.DropDownListControl
     * @extends PMUI.control.HTMLControl
     * Class to handle the Select HTML form element.
     *
     * Example usage:
     *
     *      @example
     *      var control;
     *      $(function() {
     *          var settings = {
     *              name: "myList",
     *              options: [
     *                  {
     *                      label: "one",
     *                      value: 1
     *                  },
     *                  {
     *                      label: "two",
     *                      value: 2,
     *                      disabled: true
     *                  },
     *                  {
     *                      label: "three",
     *                      value: 3,
     *                      selected: true
     *                  }, 
     *                  {
     *                      label: "Letters",
     *                      options: [
     *                          {
     *                              value: "A"
     *                          },
     *                          {
     *                              value: "B"
     *                          }
     *                      ]
     *                  },
     *                  {
     *                      label: "months",
     *                      disabled: true,
     *                      options: [
     *                          {
     *                              value: "january"
     *                          },
     *                          {
     *                              value: "february"
     *                          }
     *                      ]
     *                  }
     *              ],
     *              value: 2
     *          };
     *          control = new PMUI.control.DropDownListControl(settings);
     *          document.body.appendChild(control.getHTML());
     *      });
     *
     * @constructor
     * Creates a new instance of the DropDownListControl.
     * @param {Object} [settings=null] An JSON object with the config options.
     */
    /**
     * @cfg {Array} [options=[]]
     * An array with all the options to be contained by the control.
     *
     * Each element in the array is a JSON object, this JSON object can represent an option group
     * or an option item.
     *
     * In case to represent an option item it can contain the next properties:
     *
     * - value {String} (required): the value for the option.
     * - label {String} (optional): the label for the option, if isn't specified the value is used instead.
     * - selected {Boolean} (optional): if the option is selected. #Note. If the configuration object has the
     * "value" propery set then this "selected" property will be
     * - disabled {Boolean} (optional): if the option is disabled or not.
     *
     * On the other hand, in case to represent an option group, it can contain the next properties:
     *
     * - label {String} (required): The name for the option group.
     * - disabled {Boolean} (optional): If the group is disabled or not.
     * - options {Array} (required): An array in which each element is a JSON object representing an option item,
     * so every item must have the structure explained above (for represent option items). #Note. This propery makes
     * the difference between an option and a option group. If the "options" property is not specified or if it isn't
     * an array then it will treated like a option item.
     * @cfg {String|Number} [value=null] The value of the option that is wanted to be selected. It must be one of the values
     * of the list options, otherwise it will be set to "".
     */
    var DropDownListControl = function (settings) {
        DropDownListControl.superclass.call(this, settings);
        /**
         * @property {Array} [options] An array with all the options/option groups form the control.
         * @private
         */
        this.options = [];
        /**
         * @property {String} [elementTag='input'] The tag for the HTML element to be created.
         * @private
         */
        this.elementTag = 'select';
        DropDownListControl.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.control.HTMLControl', DropDownListControl);

    DropDownListControl.prototype.type = "DropDownListControl";
    /**
     * Initializes the object.
     * @param  {Oject} [settings=null] A JSON opbject with the config options.
     * @private
     */
    DropDownListControl.prototype.init = function (settings) {
        var defaults = {
            options: [],
            value: null,
            height: 30
        };

        jQuery.extend(true, defaults, settings);

        this.setOptions(defaults.options);
        this.setHeight(defaults.height);

        if (defaults.value !== null) {
            this.setValue(defaults.value);
        }
    };
    /**
     * Clear all the options from the control.
     * @chainable
     */
    DropDownListControl.prototype.clearOptions = function () {
        this.options = [];
        if (this.html) {
            jQuery(this.html).empty();
        }
        this.value = "";
        return this;
    };
    /**
     * Enables or disables one or more options/option groups.
     * @param  {Boolean} disabled If the function will disable (use true) or enable (use false)
     * the options/option groups.
     * @param  {String|Number|Object} option It can be a string, a number or a JSON object.
     *
     * - In case to be a String, it will be enabled/disabled the options that match the string in its value and the
     * option groups that match the string in its label. In this case more than one single item can be
     * enabled/disabled.
     * - In case to be a Number, it will be enabled/disabled the option/option group which index position matches the
     * number. Obviously, in this case only one item will be enabled/disabled.
     * - In case to be an object you can specify if the change will be applied only to options or option groups,
     * it should have two properties:
     *     - criteria {String}: The value (in case of options) or the label (in case of option groups) the items needs
     *     to match for apply the changes.
     *     - applyTo {String} (optional), it can take the following values:
     *         - "groups", the change will be applied only to the option groups.
     *         - "options", the change will be applied only to the options (direct child of the object).
     *         - [any other string value], the default value, it indicates that the change will be applied to both
     *         options/option groups that matches the criteria in its value/label respectly.
     *
     * @param  {String} [group] It it is specified must be an String making reference to an existing option group label.
     * Using this parameter, the elements to be match by the second parameter will be search only in the option groups
     * that match this parameter in its label.
     * @chainable
     * @private
     */
    DropDownListControl.prototype.enableDisableOption = function (disabled, option, group) {
        var dataGroupTarget = this,
            htmlGroupTarget = jQuery(this.html),
            i,
            j,
            suboptions,
            objectDefaults = {
                applyTo: 'all'
            };
        disabled = !!disabled;

        if (group) {
            for (i = 0; i < this.options.length; i += 1) {
                if (this.options[i].isGroup && this.options[i].label === group) {
                    dataGroupTarget = this.options[i];
                    break;
                }
            }
            if (dataGroupTarget === this.options) {
                throw new Error('disableOption(): the group "' + group + '" wasn\'t found.');
            }
            htmlGroupTarget = jQuery(this.html).find('>optgroup[label="' + group + '"]');
        }

        if (typeof option === 'number') {
            dataGroupTarget.options[option].disabled = disabled;
            htmlGroupTarget.find(">*").eq(option).attr("disabled", disabled);
        } else if (typeof option === 'string') {
            for (i = 0; i < dataGroupTarget.options.length; i += 1) {
                if (!dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].value === option) {
                    dataGroupTarget.options[i].disabled = disabled;
                } else if (dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].label === option) {
                    dataGroupTarget.options[i].disabled = disabled;
                    suboptions = dataGroupTarget.options[i].options;
                    for (j = 0; j < suboptions.length; j += 1) {
                        if (suboptions[j].value === option) {
                            suboptions[j].disabled = true;
                        }
                    }
                }
            }
            jQuery(htmlGroupTarget).find('option[value="' + option + '"]').add('optgroup[label="' + option + '"]')
                .attr("disabled", disabled);
        } else if (typeof option === 'object') {
            jQuery.extend(true, objectDefaults, option);
            if (objectDefaults.applyTo === 'groups') {
                for (i = 0; i < dataGroupTarget.options.length; i += 1) {
                    if (dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].label === option.criteria) {
                        dataGroupTarget.options[i].disabled = disabled;
                    }
                }
                jQuery(htmlGroupTarget).find('optgroup[label="' + option.criteria + '"]').attr("disabled", disabled);
            } else if (objectDefaults.applyTo === 'options') {
                for (i = 0; i < dataGroupTarget.options.length; i += 1) {
                    if (!dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].value === option.criteria) {
                        dataGroupTarget.options[i].disabled = disabled;
                    }
                }
                jQuery(htmlGroupTarget).find('option[value="' + option.criteria + '"]')
                    .attr("disabled", disabled.criteria);
            } else {
                option = option.criteria;
                for (i = 0; i < dataGroupTarget.options.length; i += 1) {
                    if (!dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].value === option) {
                        dataGroupTarget.options[i].disabled = disabled;
                    } else if (dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].label === option) {
                        dataGroupTarget.options[i].disabled = disabled;
                        suboptions = dataGroupTarget.options[i].options;
                        for (j = 0; j < suboptions.length; j += 1) {
                            if (suboptions[j].value === option) {
                                suboptions[j].disabled = true;
                            }
                        }
                    }
                }
                jQuery(htmlGroupTarget).find('option[value="' + option + '"]').add('optgroup[label="' + option + '"]')
                    .attr("disabled", disabled);
            }
        } else {
            throw new Error('disableOption(): the first parameter must be a Number or a String.');
        }

        return this;
    };
    /**
     * Disables one or more options/option groups.
     * @param  {String|Number|Object} option It can be a string, a number or a JSON object.
     *
     * - In case to be a String, it will be disabled the options that match the string in its value and the option
     * groups which match the string in its label. In this case more than one single item can be disabled.
     * - In case to be a Number, it will be disabled the option/option group which index position matches the number.
     * Obviously, in this case only one item will be disabled.
     * - In case to be an object you can specify if the change will be applied only to options or option groups,
     * it should have two properties:
     *     - criteria {String}: The value (in case of options) or the label (in case of option groups) the items needs
     *     to match for apply the changes.
     *     - applyTo {String} (optional), it can take the following values:
     *         - "groups", the change will be applied only to the option groups.
     *         - "options", the change will be applied only to the options (direct child of the object).
     *         - [any other string value], the default value, it indicates that the change will be applied to both
     *         options/option groups that matches the criteria in its value/label respectly.
     *
     * @param  {String} [group] It it is specified must be an String making reference to an existing option group label.
     * Using this parameter, the elements to be match by the first parameter will be search only in the option groups
     * that match this parameter in its label.
     * @chainable
     */
    DropDownListControl.prototype.disableOption = function (option, group) {
        return this.enableDisableOption(true, option, group);
    };
    /**
     * Enables one or more options/option groups.
     * @param  {String|Number|Object} option It can be a string, a number or a JSON object.
     *
     * - In case to be a String, it will be enabled the options that match the string in its value and the option
     * groups which match the string in its label. In this case more than one single item can be enabled.
     * - In case to be a Number, it will be enabled the option/option group which index position matches the number.
     * Obviously, in this case only one item will be enabled.
     * - In case to be an object you can specify if the change will be applied only to options or option groups,
     * it should have two properties:
     *     - criteria {String}: The value (in case of options) or the label (in case of option groups) the items needs
     *     to match for apply the changes.
     *     - applyTo {String} (optional), it can take the following values:
     *         - "groups", the change will be applied only to the option groups.
     *         - "options", the change will be applied only to the options (direct child of the object).
     *         - [any other string value], the default value, it indicates that the change will be applied to both
     *         options/option groups that matches the criteria in its value/label respectly.
     *
     * @param  {String} [group] It it is specified must be an String making reference to an existing option group label.
     * Using this parameter, the elements to be match by the first parameter will be search only in the option groups
     * that match this parameter in its label.
     * @chainable
     */
    DropDownListControl.prototype.enableOption = function (option, group) {
        return this.enableDisableOption(false, option, group);
    };
    /**
     * Removes one or more option/option groups.
     * @param  {String|Number|Object} option It can be a string, a number or a JSON object.
     *
     * - In case to be a String, it will be removed the options that match the string in its value and the option
     * groups which match the string in its label. In this case more than one single item can be removed.
     * - In case to be a Number, it will be removed the option/option group which index position matches the number.
     * Obviously, in this case only one item will be removed.
     * - In case to be an object you can specify if the change will be applied only to options or option groups,
     * it should have two properties:
     *     - criteria {String}: The value (in case of options) or the label (in case of option groups) the items needs
     *     to match for apply the changes.
     *     - applyTo {String} (optional), it can take the following values:
     *         - "groups", the change will be applied only to the option groups.
     *         - "options", the change will be applied only to the options (direct child of the object).
     *         - [any other string value], the default value, it indicates that the change will be applied to both
     *         options/option groups that matches the criteria in its value/label respectly.
     *
     * @param  {String} [group] It it is specified must be an String making reference to an existing option group label.
     * Using this parameter, the elements to be match by the first parameter will be search only in the option groups
     * that match this parameter in its label.
     *
     * ##Note. Removing an option group implies removing all its child options.
     * @chainable
     */
    DropDownListControl.prototype.removeOption = function (option, group) {
        var dataGroupTarget = this, htmlGroupTarget = jQuery(this.html), i, j, suboptions,
            objectDefaults = {
                applyTo: 'all'
            };

        if (group) {
            for (i = 0; i < this.options.length; i += 1) {
                if (this.options[i].isGroup && this.options[i].label === group) {
                    dataGroupTarget = this.options[i];
                    break;
                }
            }
            if (dataGroupTarget === this.options) {
                throw new Error('disableOption(): the group "' + group + '" wasn\'t found.');
            }
            htmlGroupTarget = jQuery(this.html).find('>optgroup[label="' + group + '"]');
        }

        if (typeof option === 'number') {
            dataGroupTarget.options.splice(option, 1);
            htmlGroupTarget.find(">*").eq(option).remove();
        } else if (typeof option === 'string') {
            for (i = 0; i < dataGroupTarget.options.length; i += 1) {
                if (!dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].value === option) {
                    dataGroupTarget.options.splice(i, 1);
                    i -= 1;
                } else if (dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].label === option) {
                    suboptions = dataGroupTarget.options[i].options;
                    for (j = 0; j < suboptions.length; j += 1) {
                        if (suboptions[j].value === option) {
                            suboptions.splice(j, 1);
                            j -= 1;
                        }
                    }
                    dataGroupTarget.options.splice(i, 1);
                    i -= 1;
                }
            }
            jQuery(htmlGroupTarget).find('option[value="' + option + '"]').add('optgroup[label="' + option + '"]')
                .remove();
        } else if (typeof option === 'object') {
            jQuery.extend(true, objectDefaults, option);
            if (objectDefaults.applyTo === 'groups') {
                for (i = 0; i < dataGroupTarget.options.length; i += 1) {
                    if (dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].label === option.criteria) {
                        dataGroupTarget.options.splice(i, 1);
                        i -= 1;
                    }
                }
                jQuery(htmlGroupTarget).find('optgroup[label="' + option.criteria + '"]').remove();
            } else if (objectDefaults.applyTo === 'options') {
                for (i = 0; i < dataGroupTarget.options.length; i += 1) {
                    if (!dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].value === option.criteria) {
                        dataGroupTarget.options.splice(i, 1);
                        i -= 1;
                    }
                }
                jQuery(htmlGroupTarget).find('option[value="' + option.criteria + '"]').remove();
            } else {
                option = option.criteria;
                for (i = 0; i < dataGroupTarget.options.length; i += 1) {
                    if ((!dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].value === option) ||
                        (dataGroupTarget.options[i].isGroup && dataGroupTarget.options[i].label === option)) {
                        dataGroupTarget.options.splice(i, 1);
                        i -= 1;
                    }
                }
                jQuery(htmlGroupTarget).find('option[value="' + option + '"]').add('optgroup[label="' + option + '"]')
                    .remove();
            }
        } else {
            throw new Error('disableOption(): the first parameter must be a Number or a String.');
        }

        return this;
    };
    /**
     * Adds a new option group to the control
     * @param {Object} optionGroup A JSON object with the following properties:
     *
     * - label {String} (required): the label for the option group.
     * - disabled {Boolean}(optional): if the option group will be disabled or not.
     * it defaults to false.
     * - options {Array} (optional): An array of JSON object, each one represents an option and
     * should have the same structure than the "option" paremeter for the
     * {@link PMUI.control.DropDownListControl#addOption addOption() method}.
     * @chainable
     */
    DropDownListControl.prototype.addOptionGroup = function (optionGroup) {
        var newOptionGroup = {},
            optionGroupHTML,
            i;

        if (!optionGroup.label) {
            throw new Error("addOptionGroup(): a label for the new option group is required!");
        }

        newOptionGroup.label = optionGroup.label;
        newOptionGroup.disabled = !!optionGroup.disabled;
        newOptionGroup.isGroup = true;
        newOptionGroup.options = [];

        this.options.push(newOptionGroup);

        if (this.html) {
            optionGroupHTML = PMUI.createHTMLElement('optgroup');
            optionGroupHTML.label = newOptionGroup.label;
            optionGroupHTML.disabled = newOptionGroup.disabled;
            this.html.appendChild(optionGroupHTML);
        }
        if (!jQuery.isArray(optionGroup.options)) {
            optionGroup.options = [];
        }
        for (i = 0; i < optionGroup.options.length; i += 1) {
            this.addOption(optionGroup.options[i], newOptionGroup.label);
        }

        return this;
    };
    /**
     * Adds a new option to the control or to an option group.
     * @param {Object} option An object with ther settings for the new option.
     * this object can have the following properties:
     *
     * - value {String} (required): the value for the option.
     * - label {String} (optional): the label for the option, if isn't specified the value is used instead.
     * - selected {Boolean} (optional): if the option is selected. #Note. If the configuration object has the
     * "value" propery set then this "selected" property will be
     * - disabled {Boolean} (optional): if the option is disabled or not.
     *
     * @param {String} group The name of the option group in which the new option will be added. If it doesn't exist
     * it will be created.
     *
     * @chainable
     */
    DropDownListControl.prototype.addOption = function (option, group) {
        var newOption = {},
            optionHTML,
            i,
            groupHTML,
            flag = false;

        newOption.value = option.value !== null && option.value !== undefined && option.value.toString ? option.value : (option.label || "");
        newOption.value = newOption.value.toString();
        newOption.label = option.label || newOption.value;
        newOption.label = newOption.label.toString();
        newOption.disabled = !!option.disabled;
        newOption.isGroup = false;

        if (!group) {
            this.options.push(newOption);
        } else {
            for (i = 0; i < this.options.length; i += 1) {
                if (this.options[i].isGroup && this.options[i].label === group) {
                    this.options[i].options.push(newOption);
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                this.addOptionGroup({
                    label: group
                });
                this.options[this.options.length - 1].options.push(newOption);
            }
        }

        if (this.html) {
            optionHTML = PMUI.createHTMLElement('option');
            optionHTML.value = newOption.value;
            optionHTML.selected = !!option.selected;
            optionHTML.label = newOption.label;
            optionHTML.disabled = newOption.disabled;
            optionHTML.textContent = optionHTML.label;

            if (group) {
                groupHTML = jQuery(this.html).find('optgroup[label="' + group + '"]');
                if (groupHTML.length) {
                    groupHTML.get(0).appendChild(optionHTML);
                } else {
                    throw new Error("addOption(): the optiongroup \"" + group + "\" wasn't found");
                }
            } else {
                jQuery(this.html).append(optionHTML);
            }
        }

        if (option.selected) {
            this.value = newOption.value;
        }
        if (this.getOptions().length == 1) {
            this.value = newOption.value;
        }
        return this;
    };
    /**
     * Returns the label from the option currently selected.
     * @return {String}
     */
    DropDownListControl.prototype.getSelectedLabel = function () {
        var i;
        if (this.html) {
            return jQuery(this.html).find('option:selected').attr('label');
        }
        for (i = 0; i < this.options.length; i += 1) {
            if (this.options[i].value === this.value) {
                return this.options[i].label;
            }
        }

        return "";
    };
    /**
     * Determines if a value exists in any of the list options.
     * @param  {String|Number} value The value to be searched
     * @return {Boolean} It returns true if the value was found in any of the list options, otherwise it returns false.
     */
    DropDownListControl.prototype.valueExistsInOptions = function (value) {
        var i,
            j,
            options,
            optionsLength,
            subOptions,
            subOptionsLength;

        optionsLength = (options = this.options || []).length;
        for (i = 0; i < optionsLength; i += 1) {
            if (options[i].isGroup) {
                subOptionsLength = (subOptions = options[i].options || []).length;
                for (j = 0; j < subOptionsLength; j += 1) {
                    if (subOptions[j].value == value) {
                        return true;
                    }
                }
            } else if (options[i].value == value) {
                return true;
            }
        }
        return false;
    };
    /**
     * Returns the first available option in the list.
     * @return {Object|null} It returns a object literal with the label and value properties from the found option.
     * @private
     */
    DropDownListControl.prototype.getFirstAvailableOption = function () {
        var i,
            j,
            options,
            optionsLength,
            subOptions,
            subOptionsLength;

        optionsLength = (options = this.options || []).length;
        for (i = 0; i < optionsLength; i += 1) {
            if (options[i].isGroup) {
                return (options[i].options.length && options[i].options[0]) || null;
            } else {
                return options[i];
            }
        }
        return null;
    }
    /**
     * Sets the options/option groups for the control.
     * @param {Array} options An array with the same structure that the
     * {@link PMUI.control.DropDownListControl#cfg-options "options"} property in the
     * Config options section.
     * @chainable
     */
    DropDownListControl.prototype.setOptions = function (options) {
        var i,
            valueExists,
            firstOption;
        if (jQuery.isArray(options)) {
            this.clearOptions();
            for (i = 0; i < options.length; i += 1) {
                if (jQuery.isArray(options[i].options)) {
                    this.addOptionGroup(options[i]);
                } else {
                    this.addOption(options[i]);
                }
            }
            if (!this.valueExistsInOptions(this.value)) {
                firstOption = this.getFirstAvailableOption();
                this.value = (firstOption && firstOption.value) || "";
            }
        }

        return this;
    };
    /**
     * Returns the options/option groups from the field
     * @param  {Boolean} [includeGroups=false] If it's evaluated as true then it will include
     * the option groups with its child elements, otherwise it will return only the option items.
     * @return {Array}
     *
     * example
     *    list.getOptions(false);
     *     [La Paz][Cochabamba][Santa Cruz][Buenos Aires][Santa Fe][Cordoba][Santiago][.][.][Mexico D.F]
     *
     *    list.getOptions(true)
     *      [BOLIVIA
     *          [La Paz][Cochabamba][SantaCruz]
     *      ]
     *      [ARGENTINA
     *          [Buenos Aires][Santa Fe][Cordoba]
     *       ]
     *      [CHILE
     *         [x][y][z]
     *      ]
     *      [New York]
     *      [Mexico D.F.]
     */
    DropDownListControl.prototype.getOptions = function (includeGroups) {
        var options = [],
            i,
            j;
        if (includeGroups) {
            return this.options.slice(0);
        }
        for (i = 0; i < this.options.length; i += 1) {
            if (!this.options[i].isGroup) {
                options.push(this.options[i]);
            } else {
                for (j = 0; j < this.options[i].options.length; j += 1) {
                    options.push(this.options[i].options[j]);
                }
            }
        }

        return options;
    };
    /**
     * Defines the events for the control
     * @chainable
     */
    DropDownListControl.prototype.defineEvents = function () {
        var that = this;
        DropDownListControl.superclass.superclass.prototype.defineEvents.call(this);
        if (this.html) {
            this.addEvent('change').listen(this.html, function () {
                that.onChangeHandler();
            });
        }
        return this;
    };
    /**
     * Sets the selected option in the dropdown.
     * @param {String|Number} value The value of the option that is wanted to be selected. It must be one of the values
     * of the list options, otherwise it will be set to "".
     */
    DropDownListControl.prototype.setValue = function (value) {
        var firstOption;
        if (!this.valueExistsInOptions(value)) {
            firstOption = this.getFirstAvailableOption();
            value = (firstOption && firstOption.value) || "";
        }
        return DropDownListControl.superclass.prototype.setValue.call(this, value);
    };
    /**
     * Creates the HTML element for the control.
     * @return {HTMLElement}
     */
    DropDownListControl.prototype.createHTML = function () {
        var value;
        if (this.html) {
            return this.html;
        }
        value = this.value;
        DropDownListControl.superclass.prototype.createHTML.call(this);
        this.setOptions(this.options.slice(0))
            .setValue(value);

        return this.html;
    };

    PMUI.extendNamespace('PMUI.control.DropDownListControl', DropDownListControl);
}());