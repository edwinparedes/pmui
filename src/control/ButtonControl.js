(function () {
    /**
     * @class PMUI.control.ButtonControl
     * @extends PMUI.control.Control
     *
     *  this button is a control, the config options is not equal to button of UI
     *  the use of this control is for a field
     */
    var ButtonControl = function (settings) {
        ButtonControl.superclass.call(this, settings);
        /**
         * this a PMUI.ui.Button object
         * @type {PMUI.ui.Button}
         */
        this.button = null;
        ButtonControl.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.control.Control', ButtonControl);

    ButtonControl.prototype.type = "ButtonControl";

    ButtonControl.prototype.init = function (settings) {
        var defaults = {
            handler: null
        };
        jQuery.extend(true, defaults, settings);
        this.button = new PMUI.ui.Button();
        this.setHandler(defaults.handler);
    };
    /**
     * diabled the buttonControl
     * @param {Boolean} value if true disable button, default value false
     * @chainable
     */
    ButtonControl.prototype.setDisabled = function (value) {
        if (this.button) {
            this.button.setDisabled(value);
        }

        return this;
    }
    /**
     * disable Button
     * @chainable
     */
    ButtonControl.prototype.disable = function () {
        if (this.button) {
            this.button.disable();
        }

        return this;
    };
    /**
     * ebable button
     * @chainable
     */
    ButtonControl.prototype.enable = function () {
        this.button.enable();
        return this;
    };
    /**
     * set bordet for Button
     * @chainable
     */
    ButtonControl.prototype.setBorder = function (border) {
        this.button.setBorder(border);
        return this;
    };
    /**
     * this click is established with jQuery, simulate the behavior click
     * @chainable
     */
    ButtonControl.prototype.click = function () {
        this.button.click();
        return this;
    };
    /**
     * set handler for button
     * @param {Function} handle the event to trigger
     * @chainable
     */
    ButtonControl.prototype.setHandler = function (handler) {
        this.button.setHandler(handler);
        return this;
    };
    /**
     * remove  one event of the events
     * @param  {String} type It is an event that is stored in an object event
     * @chainable
     */
    ButtonControl.prototype.removeEvent = function (type) {
        this.button.removeEvent(type);
        return this;
    };
    /**
     * removes all events
     * @return {[type]}
     */
    ButtonControl.prototype.removeEvents = function () {
        this.button.removeEvents();
        return this;
    };
    /**
     * fix the text for the button
     * @param {String} text
     */
    ButtonControl.prototype.setText = function (text) {
        this.button.setText(text);
        return this;
    };
    ButtonControl.prototype.setIcon = function (iconClass) {
        this.button.setIcon(iconClass);
        return this;
    };
    ButtonControl.prototype.setAliasButton = function (alias) {
        this.button.setAliasButton(alias);
        return this;
    };
    ButtonControl.prototype.setParent = function (parent) {
        this.button.setParent(parent);
        return this;
    };
    ButtonControl.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }

        this.html = this.button.getHTML();

        return this.html;
    };
    ButtonControl.prototype.defineEvents = function () {
        this.button.defineEvents();
        return this;
    };
    ButtonControl.prototype.setHeight = function (height) {
        if (this.button) {
            this.button.setHeight(height);
        }
        return this;
    };
    ButtonControl.prototype.setIconPosition = function (position) {
        this.setIconPosition(position);
        return this;
    };
    /**
     * fix a message for  button
     * @param {String} messageTooltip
     */
    ButtonControl.prototype.setTooltipMessage = function (messageTooltip) {
        this.button.setTooltipMessage(messageTooltip);
        return this;
    };

    PMUI.extendNamespace('PMUI.control.ButtonControl', ButtonControl);

    if (typeof exports !== "undefined") {
        module.exports = ButtonControl;
    }
}());