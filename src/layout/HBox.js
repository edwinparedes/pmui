(function () {
    /**
     * @class  PMUI.layout.HBox
     * @extends PMUI.layout.Layout
     * Class created to handle Horizontal Box layout changes
     *
     * @constructor
     * Creates a new isntance of the object
     * @param {Object} options Constructor object
     */
    var HBox = function (options) {
        HBox.superclass.call(this, options);
        HBox.prototype.init.call(this, options);
    };

    PMUI.inheritFrom('PMUI.layout.Layout', HBox);

    /**
     * Defines the object's type
     * @type {String}
     */
    HBox.prototype.type = "HBox";

    /**
     * @private
     * Initializes the object with default values
     * @param  {Object} options
     */
    HBox.prototype.init = function (options) {
        var defaults = {};
        jQuery.extend(true, defaults, options);
    };

    /**
     * Applies the layout to the current element
     */
    HBox.prototype.applyLayout = function () {
        // get the object of this layout
        var owner = this.belongsTo,
            items = owner.items,
            totalProportion = 0,
            usableWidth = owner.getUsableWidth(),
            usableHeight = owner.getUsableHeight(),
            i,
            width,
            item;

        var ownerParent = owner;
        // search the width in anysome parent
        if (owner != null) {
            if (owner.getWidth() != "auto" && owner.parent != null) {
                ownerParent = owner.parent;
            }
            if (owner.html) {
                owner.html.style.width = ownerParent.getUsableWidth();
            }
        }
        // compute totalProportion
        for (i = 0; i < items.getSize(); i += 1) {
            item = items.get(i);
            totalProportion += item.proportion;
        }

        xPosition = 0;
        for (i = 0; i < items.getSize(); i += 1) {
            item = items.get(i);
            if (item.html) {
                item.setDisplay("inline-block");
            }
            width = usableWidth * (item.proportion / totalProportion);
            item.setWidth(width);
            item.setHeight(usableHeight);
        }
        return this;
    };

    PMUI.extendNamespace('PMUI.layout.HBox', HBox);

    if (typeof exports !== 'undefined') {
        module.exports = HBox;
    }

}());