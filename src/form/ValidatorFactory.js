(function () {
    /**
     * @class  PMUI.form.ValidatorFactory
     * @extend PMUI.util.Factory
     * Extends the factory class to produce field validators.
     *
     * Its default products are:
     *
     * - {@link PMUI.form.TextLengthValidator TextLengthValidator} objects: using "textLength".
     * - {@link PMUI.form.RegExpValidator RegExpValidator} objects: using "regexp".
     *
     * @constructor
     * Creates a new instance od the class
     */
    var ValidatorFactory = function () {
        ValidatorFactory.superclass.call(this);
        ValidatorFactory.prototype.init.call(this);
    };

    PMUI.inheritFrom('PMUI.util.Factory', ValidatorFactory);

    ValidatorFactory.prototype.init = function () {
        var defaults = {
            products: {
                "textLength": PMUI.form.TextLengthValidator,
                "regexp": PMUI.form.RegExpValidator
            },
            defaultProduct: "textLength"
        };
        this.setProducts(defaults.products)
            .setDefaultProduct(defaults.defaultProduct);
    };

    PMUI.extendNamespace('PMUI.form.ValidatorFactory', ValidatorFactory);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ValidatorFactory;
    }

}());