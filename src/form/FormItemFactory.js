(function () {
    /**
     * @class  PMUI.form.FormItemFactory
     * @extend PMUI.util.Factory
     * Extends the factory class to produce objects to be included in containers for forms.
     *
     * Its default products are:
     *
     * - {@link PMUI.form.FormPanel FormPanel} objects: using "panel".
     * - {@link PMUI.field.TextField TextField} objects: using "text".
     * - {@link PMUI.field.DropDownListField DropDownListField} objects: using "dropdown".
     * - {@link PMUI.field.RadioButtonGroupField RadioButtonGroupField} objects: using "radio".
     * - {@link PMUI.field.CheckBoxGroupField CheckBoxGroupField} objects: using "checkbox".
     * - {@link PMUI.field.TextAreaField TextAreaField} objects: using "textarea".
     * - {@link PMUI.field.DateTimeField DateTimeField} objects: using "datetime".
     *
     * @constructor
     * Creates a new instance od the class
     */
    var FormItemFactory = function () {
        FormItemFactory.superclass.call(this);
        FormItemFactory.prototype.init.call(this);
    };

    PMUI.inheritFrom('PMUI.util.Factory', FormItemFactory);

    FormItemFactory.prototype.init = function () {
        var defaults = {
            products: {
                "field": PMUI.form.Field,
                "panel": PMUI.form.FormPanel,
                "text": PMUI.field.TextField,
                "password": PMUI.field.PasswordField,
                "dropdown": PMUI.field.DropDownListField,
                "radio": PMUI.field.RadioButtonGroupField,
                "checkbox": PMUI.field.CheckBoxGroupField,
                "textarea": PMUI.field.TextAreaField,
                "datetime": PMUI.field.DateTimeField,
                "optionsSelector": PMUI.field.OptionsSelectorField,
                "upload": PMUI.field.UploadField,
                "buttonField": PMUI.field.ButtonField,
                "annotation": PMUI.field.TextAnnotationField
            },
            defaultProduct: "panel"
        };
        this.setProducts(defaults.products)
            .setDefaultProduct(defaults.defaultProduct);
    };

    PMUI.extendNamespace('PMUI.form.FormItemFactory', FormItemFactory);

}());