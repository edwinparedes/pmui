(function () {
    /**
     * @class PMUI.form.RegExpValidator
     * @extends PMUI.form.Validator
     * A text validator based in regular expressions.
     *
     * Usually this object won't be instatiate directly, it will be instantiate through a
     * {@link PMUI.form.Field Field} object just like is shown in the
     * {@link PMUI.field.TextField TextField documentation}. We recommend to use it this way.
     *
     * The other way to instantiate an object from this class:
     *
     *      var myValidator,
     *          field,
     *          callback = function() {
     *              this.isValid();
     *          };
     *
     *      field = new PMUI.field.TextField({
     *          onChange: callback
     *      });
     *
     *      myValidator = new RegExpValidator({
     *          criteria: /^\d+$/,
     *          errorMessage: "You must introduce only number digits."
     *      });
     *
     *      field.addValidator(myValidator);
     *
     *      document.body.appendChild(field.getHTML());
     *
     * @cfg {Object|RegExp} criteria The criteria config option can be:
     *
     * - a JSON object: in this case it must have two porperties:
     *     - pattern (String): the pattern string to be use as the regular expression
     (don't forget to escape special characters)
     *     - modifiers (String): this is optional, this is the modifier for the regular expression.
     * - a RegExp object
     */
    var RegExpValidator = function (options) {
        RegExpValidator.superclass.call(this, options);
        RegExpValidator.prototype.init.call(this, options);
    };

    PMUI.inheritFrom('PMUI.form.Validator', RegExpValidator);

    RegExpValidator.prototype.type = "RegExpValidator";

    RegExpValidator.prototype.init = function (options) {
        var defaults = {
            errorMessage: "The text pattern doesn't match"
        };

        $.extend(true, defaults, options);

        this.setErrorMessage(defaults.errorMessage);
    };
    /**
     * Execute the validation.
     * @chainable
     * @private
     */
    RegExpValidator.prototype.validate = function () {
        var res = false,
            regExp;

        if (this.parent) {
            if (this.criteria instanceof RegExp) {
                if ((this.parent.valueType == 'integer' || this.parent.valueType == 'float' || this.parent.valueType == 'number') && !this.parent.value) {
                    this.valid = this.criteria.test(0);
                } else {
                    this.valid = this.criteria.test(this.parent.value);
                }
            } else if (typeof this.criteria === 'string') {
                regExp = new RegExp(this.criteria.pattern, this.criteria.modifiers);
                this.valid = regExp.text(this.parent.value);
            }
        } else {
            this.valid = false;
        }

        return this;
    };

    PMUI.extendNamespace('PMUI.form.RegExpValidator', RegExpValidator);

    if (typeof exports !== "undefined") {
        module.exports = RegExpValidator;
    }
}());