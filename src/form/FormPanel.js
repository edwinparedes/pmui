(function () {
    /**
     * @class PMUI.form.FormPanel
     * @extends PMUI.core.Panel
     * Class to handle form containers for {@link PMUI.form.Field Field objects},
     * {@link PMUI.form.FormPanel FormPanel objects}, and {@link PMUI.form.TabPanel TabPanel objects}.
     *
     *      @example
     *      var p;
     *      $(function() {
     *          p = new PMUI.form.FormPanel({
     *              width: 600, 
     *              height: 130,
     *              fieldset: true,
     *              legend: "my fieldset panel",
     *              items: [
     *                  {
     *                      pmType: "text",
     *                      label: "Name",
     *                      id: "123",
     *                      value: "",
     *                      placeholder: "insert your name",
     *                      name: "name"
     *                  },{
     *                      pmType: "text",
     *                      label: "Last name",
     *                      value: "",
     *                      placeholder: "your lastname here asshole!",
     *                      name: "lastname"
     *                  }, {
     *                      pmType: "panel",
     *                      layout: 'hbox',
     *                      items: [
     *                          {
     *                              pmType: "text",
     *                              label: "E-mail",
     *                              value: "",
     *                              name: "email"
     *                          },{
     *                              pmType: "text",
     *                              label: "Phone",
     *                              value: "555",
     *                              name: "phone"
     *                          }
     *                      ]
     *                  }
     *              ],
     *              layout: "vbox"
     *          });
     *          document.body.appendChild(p.getHTML());
     *          console.log("haha");
     *      });
     *
     * @cfg {PMUI.form.Form} [form=null] The {@link PMUI.form.Form Form} the object belongs to.
     * @cfg {Array} [items=[]] The array with the items to be contained by the object.
     *      //example
     *      {
     *         ......
     *         items: [
     *             {
     *                 pmType: "text",
     *                 label: "Name",
     *                 id: "123",
     *                 value: "",
     *                 placeholder: "insert your name",
     *                 name: "name"
     *             }, {
     *                 pmType: "text",
     *                 label: "Last name",
     *                 value: "",
     *                 placeholder: "your lastname here asshole!",
     *                 name: "lastname"
     *             }, {
     *                 pmType: "panel",
     *                 layout: 'hbox',
     *                 items: [
     *                     {
     *                         pmType: "text",
     *                         label: "E-mail",
     *                         value: "",
     *                         name: "email"
     *                     },{
     *                         pmType: "text",
     *                         label: "Phone",
     *                         value: "555",
     *                         name: "phone"
     *                     }
     *                 ]
     *             }
     *         ],
     *         ......
     *     });
     * @cfg {Boolean} [fieldset=false] If the panel will apply the fieldset behavior.
     * @cfg {String} [legend=""] The text for the legend to show in case the object applies the fieldset behavior.
     * (only applicable if the the object will apply the fieldset behavior).
     * @cfg {Number} [legendSize=12] The size for the text in the fieldset's legend.
     */
    var FormPanel = function (settings) {
        /**
         * @property {PMUI.util.ArrayList} fields
         * Object that contains all the object's direct children that are {@link PMUI.form.Field Field} objects.
         * @private
         */
        this.fields = new PMUI.util.ArrayList();
        /**
         * @property {PMUI.util.ArrayList} formPanels
         * Object that contains all the object's direct children that are {@link PMUI.form.FormPanel FormPanel}
         objects.
         * @private
         */
        this.formPanels = new PMUI.util.ArrayList();
        /**
         * @property {PMUI.util.ArrayList} tabPanels
         * Object that contains all the object's direct children that are {@link PMUI.form.TabPanel TabPanel} objects.
         * @private
         */
        this.tabPanels = new PMUI.util.ArrayList();

        FormPanel.superclass.call(this, settings);
        /**
         * @property {Boolean} [fieldset]
         * If the panel has the fieldset behavior.
         * @readonly
         */
        this.fieldset = null;
        /**
         * @property {PMUI.form.Form} [form=null] The {@link PMUI.form.Form Form} the object belongs to.
         */
        this.form = null;
        /**
         * @property {String} [legend=""] The text for the legend to show in case of the fieldset behavior
         will be applied.
         */
        this.legend = null;
        /**
         * @property {String} [legendElement=""] The HTML element for containt the legend to show in case of the
         fieldset behavior will be applied.
         */
        this.legendElement = null;
        /**
         * @property {Number} [fieldsetLegendSize] The size for the text showed in the fieldset legend.
         * @readonly
         */
        this.legendSize = null;
        this.onChange = null;
        this._onAvailabilityChange = null;

        FormPanel.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Panel', FormPanel);

    FormPanel.prototype.type = 'FormPanel';

    FormPanel.prototype.init = function (settings) {
        var defaults = {
            form: null,
            fieldset: false,
            legend: "",
            padding: (settings && settings.fieldset) ? "5px 10px" : "10px",
            borderWidth: (settings && settings.fieldset) ? 1 : "",
            legendSize: 12,
            onChange: null,
            onAvailabilityChange: null
        };

        jQuery.extend(true, defaults, settings);

        this.setPadding(defaults.padding)
            .setBorderWidth(defaults.borderWidth)
            .setForm(defaults.form)
            .setOnAvailabilityChange(defaults.onAvailabilityChange);

        this.form = defaults.form;
        this.fieldset = defaults.fieldset;
        this.legend = defaults.legend;
        this.legendSize = defaults.legendSize;
        this.onChange = defaults.onChange;

        if (this.fieldset) {
            this.setElementTag("fieldset");
        }
    };
    /**
     * Sets the function to be called when the availability on any contained item changes.
     * @param handler
     */
    FormPanel.prototype.setOnAvailabilityChange = function (handler) {
        if (!(typeof handler === 'function' || handler === null)) {
            throw new Error("onAvailabilityChange(): The parameter must be a function or NULL.");
        }
        this._onAvailabilityChange = handler;
        return this;
    };
    /**
     * Returns a field from the form panel.
     * @param  {String} name The name of the field to find.
     * @return {PMUI.form.Field|null}
     */
    FormPanel.prototype.getField = function (name) {
        var i,
            items = this.getItems(),
            field = null;

        for (i = 0; i < items.length; i += 1) {
            if (items[i] instanceof PMUI.form.Field) {
                if (items[i].getName() === name) {
                    return items[i];
                }
            } else {
                field = items[i].getField(name);
                if (field) {
                    return field;
                }
            }
        }
        return field;
    };
    /**
     * Returns an array of items depending on the parameters the method receives.
     * @param  {String} [filter=undefined]
     * A string which specifiest the bunch of items that will be returned.
     * It defaults to {undefined}, that means that only the direct child items will be in the returned array.
     *
     * Alternatively this param can take one of the following values:
     *
     * - 'fields': it will return only the child {@link PMUI.form.Field Field} items.
     * - 'formPanels': it will return only the child {@link PMUI.form.FormPanel FormPanel} items.
     * - 'tabPanels': it will return only the child {@link PMUI.form.TabPanel TabPanel} items.
     *
     * @param  {Boolean} [includeChildren=false]
     *
     * If the value is evaluated as false only the direct child items will be returned,
     * otherwise additionaly will be added the items for all child items.
     *
     * Note: This parameter only has effect when the [filter] parameter is provided.
     * @return {Array}
     */
    FormPanel.prototype.getItems = function (filter, includeChildren) {
        var res = [],
            items,
            size,
            i,
            targetArray,
            the_class;

        switch (filter) {
            case 'fields':
                the_class = PMUI.form.Field;
                targetArray = this.fields;
                break;
            case 'formPanels':
                the_class = PMUI.form.FormPanel;
                targetArray = this.formPanels;
                break;
            case 'tabPanels':
                the_class = PMUI.form.TabPanel;
                targetArray = this.tabPanels;
                break;
            default:
                return FormPanel.superclass.prototype.getItems.call(this);
        }

        if (includeChildren) {
            if (the_class) {
                items = this.items.asArray();
                size = items.length;
                for (i = 0; i < size; i += 1) {
                    if (items[i] instanceof the_class) {
                        res.push(items[i]);
                    } else {
                        res = res.concat(items[i].getItems(filter, true));
                    }
                }
            } else {
                throw new Error('getItems(): The valid values for the "filter" parameter are: "fields",' +
                    '"formPanels" or "tabPanels", received: ' + filter);
            }
        } else {
            res = targetArray.asArray().slice(0);
        }
        return res;
    };
    /**
     * Sets the form the form panel belongs to.
     * @param {PMUI.form.Form} form
     * @chainable
     */
    FormPanel.prototype.setForm = function (form) {
        if (form instanceof PMUI.form.Form) {
            this.form = form;
        }

        return this;
    };
    /**
     * Sets the {@link PMUI.util.Factory Factory} object for the FormPanel
     * @param {PMUI.util.Factory|Object} factory It can be a {@link PMUI.util.Factory Factory} object,
     or a JSON object.
     * If it is a JSON object then a {@link PMUI.form.FormItemFactory FormItemFactory}
     * is created using the JSON object as the config options.
     * @chainable
     * @private
     */
    FormPanel.prototype.setFactory = function (factory) {
        if (factory instanceof PMUI.util.Factory) {
            this.factory = factory;
        } else {
            this.factory = new PMUI.form.FormItemFactory(factory);
        }
        return this;
    };
    /**
     * Clear all the items.
     * @chainable
     */
    FormPanel.prototype.clearItems = function () {
        FormPanel.superclass.prototype.clearItems.call(this);
        if (this.fields) {
            this.fields.clear();
            this.formPanels.clear();
            this.tabPanels.clear();
        }
        return this;
    };
    /**
     * Sets the text for the legend to be displayed just in case of the fieldset behavior is beign applied.
     * @param {String} legend
     * @chainable
     */
    FormPanel.prototype.setLegend = function (legend) {
        if (typeof legend === 'string') {
            this.legend = legend;
            if (this.legendElement) {
                this.legendElement.textContent = legend;
            }
        } else {
            throw new Error("setLegend(): this method accepts string values as only parameter.");
        }

        return this;
    };
    /**
     * Creates the HTML element for the FormPanel.
     * @chainable
     */
    FormPanel.prototype.createHTML = function () {
        var legendElement,
            html;

        if (this.html) {
            return this.html;
        }

        FormPanel.superclass.prototype.createHTML.call(this);

        if (this.fieldset) {
            legendElement = PMUI.createHTMLElement('legend');
            legendElement.textContent = this.legend;
            legendElement.className = 'pmui-formpanel-legend';
            legendElement.style.fontSize = this.legendSize + "px";
            this.legendElement = legendElement;
            jQuery(this.html).prepend(legendElement);
        }
        return this.html;
    };
    /**
     * Returns the object's usable height.
     * @return {Number|String}
     */
    FormPanel.prototype.getUsableHeight = function () {
        var height = FormPanel.superclass.prototype.getUsableHeight.call(this);
        if (isNaN(height)) {
            return height;
        }
        if (this.fieldset) {
            height -= this.legendSize;
        }

        return height;
    };
    /**
     * Sets the internal handler function for the onChange Event
     * @return {Function} The handler function.
     * @private
     */
    FormPanel.prototype.onChangeHandler = function () {
        var that = this;
        return function (a, b, c) {
            var target,
                newValue,
                previousValue;

            if (this instanceof PMUI.form.Field) {
                target = this;
                newValue = a;
                previousValue = b;
            } else {
                target = a;
                newValue = b;
                previousValue = c;
            }
            if (typeof that.onChange === 'function') {
                that.onChange(target, newValue, previousValue);
            }
            if (this instanceof PMUI.form.Field && this.form) {
                (this.form.onChangeHandler())(target, newValue, previousValue);
            }
        };
    };
    FormPanel.prototype._getOnAvailabilityChangeHandler = function () {
        var that = this;
        return function (panel, enabled) {
            if (typeof that._onAvailabilityChange === 'function') {
                that._onAvailabilityChange(panel, enabled);
            }
        };
    };
    /**
     * Add an item to the object.
     * @param {Object|PMUI.form.Field|PMUI.form.FormPanel|PMUI.form.TabPanel} item
     * The item parameter can be:
     *
     * - A JSON object, in that case, it must have at least a "pmType" property
     * which can have any of the values specifed in the {@link PMUI.form.FormItemFactory FormItemFactory 
     documentation}.
     * - A {@link PMUI.form.Field Field} object.
     * - A {@link PMUI.form.FormPanel FormPanel} object.
     * - A {@link PMUI.form.TabPanel TabPanel} object.
     */
    FormPanel.prototype.addItem = function (item, index) {
        var itemToBeAdded;

        if (this.factory) {
            if (typeof item === 'object' && !item.layout) {
                item.layout = (this.layout && this.layout.type.toLowerCase()) || "vbox";
            }
            if (this.factory.isValidClass(item) || this.factory.isValidName(item.pmType)) {
                itemToBeAdded = this.factory.make(item);
            } else {
                throw new Error('Invalid item to add.');
            }
        }
        if (itemToBeAdded) {
            itemToBeAdded.setForm(this.form);
            itemToBeAdded.setDisplay("inline-block");
            if (itemToBeAdded instanceof PMUI.form.FormPanel) {
                itemToBeAdded.setOnAvailabilityChange(this._getOnAvailabilityChangeHandler());
                this.formPanels.insert(itemToBeAdded);
            } else if (itemToBeAdded instanceof PMUI.form.Field) {
                this.fields.insert(itemToBeAdded);
                if (this.form) {
                    this.form.data.addItem(itemToBeAdded.data);
                }
            }
            FormPanel.superclass.prototype.addItem.call(this, itemToBeAdded, index);
        }

        return this;
    };
    /**
     * Removes an item from the form panel.
     * @param  {String|Number|PMUI.core.Element} item
     * It can be:
     * - a String: in this case this string must be the id of the element to be removed.
     * - a Number: in this case this number must be the index of the element to be removed.
     * - a {@link PMUI.core.Element Element} object: in this case it will be removed
     * (if it is contained by the current form panel).
     * @chainable
     */
    FormPanel.prototype.removeItem = function (item) {
        var itemToRemove;

        if (item instanceof PMUI.core.Element) {
            itemToRemove = item;
        } else {
            if (typeof item === 'string') {
                itemToRemove = this.items.find("id", item.id);
            } else if (typeof item === 'number') {
                itemToRemove = this.items.get(item);
            }
        }
        if (itemToRemove) {
            if (itemToRemove instanceof PMUI.form.Field) {
                if (this.form) {
                    this.form.data.removeItem(itemToRemove.data);
                }
            }
            FormPanel.superclass.prototype.removeItem.call(this, itemToRemove);
        }

        return this;
    };
    /**
     * Returns true if the validation passes otherwise it returns false.
     * @return {Boolean}
     */
    FormPanel.prototype.isValid = function () {
        var items = this.items.asArray(),
            i,
            valid = true;

        for (i = 0; i < items.length; i += 1) {
            if (items[i].disabled) {
                continue;
            }
            valid = valid && items[i].isValid();
            if (!valid) {
                if (items[i] instanceof PMUI.form.Field) {
                    items[i].setFocus();
                }
                return valid;
            }
        }

        return valid;
    };

    FormPanel.prototype.disable = function () {
        FormPanel.superclass.prototype.disable.call(this);
        if (typeof this._onAvailabilityChange === 'function') {
            this._onAvailabilityChange(this, false);
        }
        return this;
    };

    FormPanel.prototype.enable = function () {
        FormPanel.superclass.prototype.enable.call(this);
        if (typeof this._onAvailabilityChange === 'function') {
            this._onAvailabilityChange(this, true);
        }
        return this;
    };

    PMUI.extendNamespace('PMUI.form.FormPanel', FormPanel);

    if (typeof exports !== "undefined") {
        module.exports = FormPanel;
    }
}());