(function () {
    /**
     * @class PMUI.form.Form
     * @extends PMUI.core.Panel
     *
     *  Usage example:
     *
     *      @example
     *      var f;
     *      $(function() {
     *          f = new PMUI.form.Form({
     *              onSubmit: function() {
     *                  console.log("submitting...");
     *              },
     *              onChange: function(field, newValue, previousValue) {
     *                  console.log("The field " + field.getName() + " has changed from \"" + previousValue + "\" to \"" + newValue + "\"");
     *              },
     *              items: [
     *                  {
     *                      pmType: "text",
     *                      label: "Name",
     *                      id: "123",
     *                      value: "",
     *                      placeholder: "insert your name",
     *                      name: "name",
     *                      helper: "Introduce your name",
     *                      required : true,
     *                      controlsWidth: 200
     *                  }, 
     *                  {
     *                      pmType: "datetime",
     *                      visible : false,
     *                      label: "birth date",
     *                      name: "birthdate"
     *                  },
     *                  {
     *                      pmType: "dropdown",
     *                      label: "age",
     *                      options: [
     *                          {
     *                              label: "",
     *                              value: ""
     *                          },
     *                          {
     *                              label: "from 0 to 7 years old",
     *                              value: "0-7"
     *                          },
     *                          {
     *                              label: "from 8 to 13 years old",
     *                              value: "8-13"
     *                          },
     *                          {
     *                              label: "from 14 to 19 years old",
     *                              value: "14-19"
     *                          },
     *                          {
     *                              label: "from 20 to 30 years old",
     *                              value: "20-30"
     *                          },
     *                          {
     *                              label: "from 31 to 45 years old",
     *                              value: "31-45"
     *                          },
     *                          {
     *                              label: "older than 45",
     *                              value: "46+"
     *                          }
     *                      ],
     *                      name: "age",
     *                      helper: "Select one of the options",
     *                      required: true,
     *                      onChange: function() {
     *                          console.log("The value has been changed");
     *                      },
     *                      controlsWidth: 100
     *                  },
     *                  {
     *                      pmType: "radio",
     *                      label: "Gender",
     *                      value: "m",
     *                      name: "gender",
     *                      required: true,
     *                      options: [
     *                          {
     *                              label: "Male",
     *                              value: "m"
     *                          },
     *                          {
     *                              label: "Female",
     *                              value: "f"
     *                          }
     *                      ]
     *                  }, 
     *                  {
     *                      pmType: "text",
     *                      label: "E-mail",
     *                      value: "",
     *                      name: "email",
     *                      helper: "you email here",
     *                      required: true,
     *                      validators: [
     *                          {
     *                              pmType: "regexp",
     *                              criteria: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
     *                              errorMessage: "Please enter a valid email address"
     *                          }
     *                      ]
     *                  }, 
     *                  {
     *                      pmType: "checkbox",
     *                      label: "Things you like to do",
     *                      value: "[\"music\"]",
     *                      name: "hobbies",
     *                      helper: "Check up to 3 options",
     *                      options: [
     *                          {
     *                              label: "Music",
     *                              value: "music"
     *                          },
     *                          {
     *                              label: "Programming",
     *                              value: "programming"
     *                          },
     *                          {
     *                              label: "Bike ridding",
     *                              value: "bike"
     *                          },
     *                          {
     *                              label: "Gastronomy",
     *                              value: "gastronomy"
     *                          },
     *                          {
     *                              label: "Movies",
     *                              value: "movies"
     *                          }
     *                      ],
     *                      validators: [],
     *                      required: true
     *                  },
     *                  {
     *                      pmType: "panel",
     *                      fieldset: true,
     *                      legend: "Another Info",
     *                      layout: "vbox",
     *                      items: [
     *                          {
     *                              pmType: "panel",
     *                              layout: 'hbox',
     *                              items: [
     *                                  {
     *                                      pmType: "text",
     *                                      label: "Country",
     *                                      value: "Bolivia",
     *                                      name: "country"
     *                                  },
     *                                  {
     *                                      pmType: "text",
     *                                      label: "City",
     *                                      name: "city"
     *                                  }
     *                              ]
     *                          },
     *                          {
     *                              pmType: "panel",
     *                              layout: 'hbox',
     *                              items: [
     *                                  {
     *                                      pmType: "text",
     *                                      label: "Address",
     *                                      name: "address",
     *                                      placeholder: "Your address here"
     *                                  },
     *                                  {
     *                                      pmType: "text",
     *                                      label: "Zip Code",
     *                                      name: "zip"
     *                                  }
     *                              ]
     *                          }, 
     *                          {
     *                              pmType: "panel",
     *                              layout: 'vbox',
     *                              items: [
     *                                  {
     *                                      pmType: "textarea",
     *                                      label: "About you",
     *                                      name: "about"
     *                                  }
     *                              ]
     *                          }
     *                      ]
     *                  }
     *              ],
     *              footerItems: [
     *                  {
     *                      pmType: "button",
     *                      text: "Submit",
     *                      handler: function() {
     *                          f.submit();
     *                          console.log("submitting form...");
     *                      }
     *                  }, {
     *                      pmType: "button",
     *                      text: "Reset",
     *                      handler: function() {
     *                          f.reset();
     *                      }
     *                  }
     *              ]
     *          });
     *          document.body.appendChild(f.getHTML());
     *      });
     *
     * The example above will generate a form with 8 fields:
     * Name, Last Name, E-mail, Phone, Country, City, Address, Zip Code.
     * The first four ones are required and each one has a helper.
     * The email and Phone fields also have a validator for control the input.
     *
     * The latest four ones are contained by a fieldset.
     *
     * @cfg {String} title The title for the form.
     * @cfg {name} [name=<the object's id>] The name for the form.
     * @cfg {String} [encType="application/x-www-form-urlencoded"] The value for the form's encType attribute .
     * @cfg {Array} [items=[]] An array in which every element can be
     * a JSON Object or a supported object by Form:
     *
     * - In case to be a JSON object, it should includes a "pmType" property with a valid value.
     * The valid values for the pmType property are specified in the
     * {@link PMUI.form.FormItemFactory FormItemFactory documentation}.
     *
     * - In case of objects, use an object that the {@link PMUI.form.FormItemFactory FormItemFactory} can produce.
     *
     * Example:
     *     var form, items;
     *
     *     items = [
     *          {
     *              pmType: "text",
     *              label: "Name",
     *              id: "123",
     *              value: "",
     *              placeholder: "insert your name",
     *              name: "name",
     *              helper: "Introduce your name",
     *              required: true
     *          },
     *          {
     *              pmType: "text",
     *              label: "Last name",
     *              value: "",
     *              placeholder: "your lastname here asshole!",
     *              name: "lastname",
     *              helper: "Introduce your lastname",
     *              required: true
     *          }
     *      ];
     *
     *     form = new PMUI.form.Form({
     *         name: "My form",
     *         items: items
     *     });
     *
     * Depending on the type of element you add, it can contain a nested items property to define its child items:
     *
     *      f = new PMUI.form.Form({
     *          items: [
     *              .......
     *              {
     *                  pmType: "panel",
     *                  fieldset: true,
     *                  legend: "Another Info",
     *                  layout: "vbox",
     *                  items: [
     *                      {
     *                          pmType: "panel",
     *                          layout: 'hbox',
     *                          items: [
     *                              {
     *                                  pmType: "text",
     *                                  label: "Country",
     *                                  value: "Bolivia",
     *                                  name: "country"
     *                              },
     *                              {
     *                                  pmType: "text",
     *                                  label: "City",
     *                                  name: "city"
     *                              }
     *                          ]
     *                      },
     *                      {
     *                          pmType: "panel",
     *                          layout: 'hbox',
     *                          items: [
     *                              {
     *                                  pmType: "text",
     *                                  label: "Address",
     *                                  name: "address",
     *                                  placeholder: "Your address here"
     *                              },
     *                              {
     *                                  pmType: "text",
     *                                  label: "Zip Code",
     *                                  name: "zip"
     *                              }
     *                          ]
     *                      }
     *                  ]
     *              }
     *          ]
     *      });
     *
     * As you can see in the example above, only the container type objects can have an "item" property.
     *
     * @cfg {Number} [fontSize=12] The size to be use for the font in the form.
     * @cfg {Number} [width=600] The width for the form.
     * In case of using a String you only can use 'auto' or 'inherit' or ##px or ##% or ##em when ## is a number.
     * @cfg {Number} [height=400] The height for the form.
     * In case of using a String you only can use 'auto' or 'inherit' or ##px or ##% or ##em when ## is a number.
     * @cfg {Boolean} [visibleHeader=true] If the header is visible or not.
     * @cfg {String} [layout='vbox'] The layout type to apply to the form.
     * @cfg {Array} [buttons=[]] An array in which each element is JSON Object or a
     * {@link Form.ui.Button Button object}
     * @deprecated This config option will be removed soon, please use the {@link #cfg-footerItems footerItems} config
     * option instead.
     * @cfg {Function} [onChange=null] A callback function to be called everytime a form's field changes.
     * This callback function will be called in the current object context and will receive three parameters:
     *
     * - The field that changed.
     * - The new field's value.
     * - The previous field's value.
     *
     *      var f = new PMUI.form.Form({
     *          onChange: function(field, newValue, previousValue) {
     *              console.log("Action in the form named \"" + this.getName() + "\":");
     *              console.log("The field " + field.getName() + " has changed from \"" + previousValue + 
     "\" to \"" + newValue + "\"");
     *          },
     *          ......
     *      };
     *
     * @cfg {Function} [onSubmit=null] A callback function to be called when the form is submitted.
     * This callback function will be called in the form object context.
     *
     *      var f = new PMUI.form.Form({
     *          onSubmit: function() {
     *              console.log("Submitting the form named \"" + this.getName() + "\"...");
     *          },
     *          ......
     *      };
     * @cfg {Array} [footerItems=[]] Sets the elements in the window footer, this elements can be instances of Button
     * and/or instances of Label. The value for this config option must be an array in which each element can be:
     *
     * - An object literal, in this case the object literal must have the property "pmType" with its value set to
     * "button" (if you want the element be a {@link PMUI.ui.Button Button}) or "label" (if you want the element be a
     * {@link PMUI.ui.TextLabel Label}). Optionally you can add the respective config options for each case.
     *
     * - A PMUI object, in this case it must be an instance of {@link PMUI.ui.Button Button} or an instance of
     * {@link PMUI.ui.TextLabel Label}.
     * @cfg {Function} [onLoad=null] The callback function to be executed when the form is ready to use. for info about
     * the parameters received by the callback please read the {@link #event-onLoad onLoad event} documentation.
     * @cfg {Srting} [buttonPanelPosition='bottom'] The position for the form's footer. It can be 'bottom' or 'top'.
     */
    var Form = function (settings) {
        Form.superclass.call(this, settings);
        /**
         * The name for the form
         * @type {String}
         * @readonly
         */
        this.name = null;
        /**
         * The form's encType property
         * @type {String}
         * @readonly
         */
        this.encType = null;
        /**
         * The text for the form's title.
         * @type {String}
         * @readonly
         */
        this.title = null;
        /**
         * The size for the font in the form.
         * @type {Number}
         * @readonly
         */
        this.fontSize = null;
        /**
         * If the header is visible or not.
         * @type {Boolean}
         * @readonly
         */
        this.visibleHeader = null;
        /**
         * The callback function to be called when a form's field changes.
         * @type {Function}
         */
        this.onChange = null;
        /**
         * The callback functionto be called when the form is submitted.
         * @type {Function}
         */
        this.onSubmit = null;
        /**
         * The form's data object.
         * @type {PMUI.data.DataSet}
         * @private
         */
        this.data = null;
        /**
         * The DOM object that plays the role of title container in the form.
         * @type {HTMLElement}
         * @private
         */
        this.header = null;
        /**
         * The form footer.
         * @type {PMUI.panel.ButtonPanel}
         * @private
         */
        this.footer = null;
        /**
         * The height for the footer area in the Form
         * @type {Number}
         * @readonly
         */
        this.footerHeight = null;
        /**
         * @event onLoad
         * Fired when the form is ready to use.
         * @param {PMUI.form.Form} form The form.
         */
        this.onLoad = null;
        /**
         * If the form is dirty.
         * @type {Boolean}
         */
        this.dirty = null;
        this._requiredFieldNotification = null;
        /**
         * The position for the form's footer.
         * @type {String}
         */
        this.buttonPanelPosition = null;
        this.dependencies = {};
        this.alignmentButtons = null;
        Form.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Panel', Form);

    Form.prototype.type = 'PMForm';

    Form.prototype.family = 'PMForm';

    Form.prototype.init = function (settings) {
        var defaults = {
            title: "Untitled form",
            name: this.id,
            encType: "application/x-www-form-urlencoded",
            items: [],
            fontSize: 12,
            width: 600,
            height: 400,
            visibleHeader: true,
            layout: 'vbox',
            buttons: [],
            footerItems: [],
            buttonPanelPosition: 'bottom',
            onChange: null,
            onSubmit: null,
            onLoad: null,
            alignmentButtons: 'right'
        };

        $.extend(true, defaults, settings);

        this.panel = new PMUI.form.FormPanel({
            form: this,
            width: defaults.width,
            layout: defaults.layout,
            style: {
                cssProperties: {
                    "overflow": "auto"
                }
            },
            onChange: this.onChangeHandler()
        });

        this.footer = new PMUI.panel.ButtonPanel({
            style: {
                cssClasses: ['pmui-form-footer']
            }
        });

        this.data = new PMUI.data.DataSet();

        this.buttons = new PMUI.util.ArrayList();

        this.visibleHeader = defaults.visibleHeader;
        this.onChange = defaults.onChange;
        this.onSubmit = defaults.onSubmit;

        this.setLayout(defaults.layout)
            .setFontSize(defaults.fontSize)
            .setWidth(defaults.width)
            .setTitle(defaults.title)
            .setName(defaults.name)
            .setEncType(defaults.encType)
            .setItems(defaults.items)
            .setOnLoadHandler(defaults.onLoad)
            .setButtonPanelPosition(defaults.buttonPanelPosition)
            .setAlignmentButtons(defaults.alignmentButtons);

        //TODO tell the guys about the deprecated methods
        if (defaults.footerItems && defaults.footerItems.length) {
            this.setFooterItems(defaults.footerItems);
        } else {
            this.setButtons(defaults.buttons);
        }
        if (typeof this.onLoad === 'function') {
            this.onLoad(this);
        }
    };

    Form.prototype.updateDependencies = function () {
        var dependencies = {},
            i,
            j,
            fields = this.getFields(),
            dependent,
            dependents;

        for (i = 0; i < fields.length; i += 1) {
            dependents = fields[i].dependentFields;
            for (j = 0; j < dependents.length; j += 1) {
                if (this.getField(dependents[j])) {
                    if (!dependencies[dependents[j]]) {
                        dependencies[dependents[j]] = [];
                    }
                    dependencies[dependents[j]].push(fields[i]);
                }
            }
        }
        this.dependencies = dependencies;
        return this;
    };


    /**
     * Sets the position for the form's footer.
     * @param {String} position A string with the text 'bottom' or 'top', this sets the position for the form's footer.
     */
    Form.prototype.setButtonPanelPosition = function (position) {
        if (position === 'top' || position === 'bottom') {
            this.buttonPanelPosition = position;
            if (this.html) {
                if (position === 'top') {
                    this.html.insertBefore(this.footer.html, this.panel.html);
                } else {
                    this.html.appendChild(this.footer.html);
                }

            }
        }
        return this;
    };
    /**
     * Sets the callback to be executed when the {@link #event-onLoad onLoad event} is fired.
     * @param {Function|null} handler The callback function or the null constant, in the latter case no callback
     * function will be executed.
     * @chainable
     */
    Form.prototype.setOnLoadHandler = function (handler) {
        if (!(typeof handler === 'function' || handler === null)) {
            throw new Error('setOnLoadHandler(): the parameter must be a function or null.');
        }
        this.onLoad = handler;
        return this;
    }
    /**
     * Removes an iten from the form's footer.
     * @param  {PMUI.core.Element|String|Number} item It can be a string (id of the child to remove),
     a number (index of the child to remove) or a {Element} object.
     * @chainable
     */
    Form.prototype.removeFooterItem = function (item) {
        this.footer.removeItem(item);
        return this;
    };
    /**
     * Removes all the items in the form's footer.
     * @chainable
     */
    Form.prototype.clearFooterItems = function () {
        this.footer.clearItems();
        return this;
    };
    /**
     * Adds an item into the forms footer.
     * @param {Object|PMUI.ui.Button|PMUI.ui.Button} item It can be:
     *
     * - An object literal, in this case it can have the config options for create a {@link PMUI.ui.Button Button} or a
     * {@link PMUI.ui.TextLabel Label}, additionally it must include the respective pmType ('button' for Button and 'label'
     * for Label).
     *
     * -A PMUI object, in this case it must be an instance of {@link PMUI.ui.Button Button} or an instance of {@link
        * PMUI.ui.TextLabel Label}.
     */
    Form.prototype.addFooterItem = function (item) {
        this.footer.addItem(item);
        this.showFooter();
        return this;
    };
    /**
     * Sets the items to be shown in the form's footer.
     * @param {Array} items An array in which each element can be:
     *
     * - An object literal, in this case it can have the config options for create a {@link PMUI.ui.Button Button} or a
     * {@link PMUI.ui.TextLabel Label}, additionally it must include the respective pmType ('button' for Button and 'label'
     * for Label).
     *
     * -A PMUI object, in this case it must be an instance of {@link PMUI.ui.Button Button} or an instance of {@link
        * PMUI.ui.TextLabel Label}.
     */
    Form.prototype.setFooterItems = function (items) {
        var i;

        if (!jQuery.isArray(items)) {
            throw new Error('setFooterItems(): The parameter must be an array.');
        }
        this.clearFooterItems();
        this.hideFooter();
        for (i = 0; i < items.length; i += 1) {
            this.addFooterItem(items[i]);
        }

        return this;
    };
    /**
     * The method which sets the internal callback function for the onChange event.
     * @return {Function}
     * @private
     */
    Form.prototype.onChangeHandler = function () {
        var that = this;
        return function (field, newValue, previousValue) {
            if (field.initialValue === newValue) {
                that.dirty = false;
            } else {
                that.dirty = true;
            }
            if (typeof that.onChange === 'function') {
                that.onChange(field, newValue, previousValue);
            }
        };
    };
    /**
     * Sets the form to dirty/clean.
     * @param {Boolean} dirty True to set the form to dirty, False to set it to clean.
     * @chainable
     */
    Form.prototype.setDirty = function (dirty) {
        this.dirty = !!dirty;
        return this;
    };
    /**
     * Check if the form is dirty
     * @return {Boolean} Returns true if the for is dirty, otherwise it returns false.
     */
    Form.prototype.isDirty = function () {
        return this.dirty;
    };
    /**
     * Removes any button previously added to the form.
     * @param  {String|Number|PMUI.ui.Button} button
     * This param can be:
     *  - String: in that case the string must be the id of the button to remove.
     *  - Number: in that case the number is evaluated as the index of the button to remove.
     *  - Button: the button to remove.
     * @chainable
     * @deprecated This method will be removed soon, please use the {@link #method-removeFooterItem removeFooterItem()}
     * method instead.
     */
    Form.prototype.removeButton = function (button) {
        return this.removeFooterItem(button);
    };
    /**
     * Remove all the buttons from the form.
     * @chainable
     * @deprecated This method will be removed soon, please use the {@link #method-clearFooterItems clearFooterItems()}
     * method instead.
     */
    Form.prototype.clearButtons = function () {
        return this.clearFooterItems();
    };
    /**
     * Adds a button into the form. The button will be added to the form's footer.
     * @param {Object|PMUI.ui.Button} button
     * It can be a:
     *
     * - {@link PMUI.ui.Button Button} object.
     * - JSON Object: use the same JSON structure for create {@link PMUI.ui.Button Button} objects.
     *
     * @chainable
     * @deprecated This method will be removed soon, please use the {@link #method-addFooterItem addFooterItem()}
     * method instead.
     */
    Form.prototype.addButton = function (button) {
        return this.addFooterItem(button);
    };
    /**
     * Sets the buttons for the form. They will be added to the footer.
     * @param {Array} buttons An array in which each element is a JSON object or a {@link PMUI.ui.Button Button}.
     * @chainable
     * @deprecated This method will be removed soon, please use the {@link #method-setFooterItems setFooterItems()}
     * method instead.
     */
    Form.prototype.setButtons = function (buttons) {
        return this.setFooterItems(buttons);
    };
    /**
     * Updates the dimensions for the form's areas.
     * @chainable
     */
    Form.prototype.updateDimensions = function () {
        var headerHeight = 0,
            footerHeight = 0,
            bodyHeight;

        if (!this.panel) {
            return this;
        }
        if (this.getHeight() !== 'auto') {
            if (this.visibleHeader && this.title) {
                headerHeight = 2 * this.fontSize;
            }
            if (this.footer.getItems().length) {
                footerHeight = (1.7 * this.fontSize) + 12;
            }
            bodyHeight = this.getHeight() - headerHeight - footerHeight;
        }
        return this;
    };
    /**
     * Sets the form's height.
     * @param {Number|String} height it can be a number or a string.
     * In case of using a String you only can use 'auto' or 'inherit' or ##px or ##% or ##em when ## is a number
     * @chainable
     */
    Form.prototype.setHeight = function (height) {
        Form.superclass.prototype.setHeight.call(this, height);
        this.updateDimensions();
        return this;
    };
    /**
     * Sets the form's width.
     * @param {Number|String} width it can be a number or a string.
     * In case of using a String you only can use 'auto' or 'inherit' or ##px or ##% or ##em when ## is a number
     * @chainable
     */
    Form.prototype.setWidth = function (width) {
        Form.superclass.prototype.setWidth.call(this, width);
        this.updateDimensions();
        return this;
    };
    /**
     * Sets the size for the font toi be used in the form.
     * @param {Number} fontSize
     */
    Form.prototype.setFontSize = function (fontSize) {
        if (typeof fontSize === 'number') {
            this.fontSize = fontSize;
            this.style.addProperties({
                "font-size": fontSize + "px"
            });
            this.updateDimensions();
        } else {
            throw new Error("setFontSize(): this method only accepts a number as parameter.");
        }

        return this;
    };
    /**
     * Returns a field from the form.
     * @param  {String} name The name of the field to find.
     * @return {PMUI.form.Field|null}
     */
    Form.prototype.getField = function (name) {
        return this.panel.getField(name);
    };
    /**
     * Returns all the fields contained by the form.
     * @return {Array} An array in which each element is a {PMUI.form.Field}.
     */
    Form.prototype.getFields = function () {
        return this.panel.getItems("fields", true);
    };
    /**
     * Returns the items from the form's body.
     * @return {Array} An array with all the form's items.
     */
    Form.prototype.getItems = function () {
        return this.panel.getItems();
    };
    /**
     * Sets the layout mode for the form's root panel.
     * @param {String} layout it can take one of the following values:
     *
     * - "vbox" for vertical positioning.
     * - "hbox" for horizontal positioning.
     * - "box" without layout.
     */
    Form.prototype.setLayout = function (layout) {
        var factory;

        if (this.panel) {
            factory = new PMUI.layout.LayoutFactory();
            this.panel.layout = factory.make(layout);
            this.panel.layout.setContainer(this.panel);
            if (this.html) {
                this.panel.layout.applyLayout();
            }
        }
        return this;
    };
    /**
     * Adds an item to the form's root panel. It only creates one of
     * the {@link PMUI.form.FormItemFactory FormItemFactory} supported products.
     * @param {String|Object|PMUI.form.FormPanel|PMUI.form.Field} item
     * It can accept one of the following data types:
     *
     * - a String: one of the supported pmTypes by the {@link PMUI.form.FormItemFactory FormItemFactory} object.
     * - a JSON Object: use the JSON structure the object's constructor needs, additionally add the pmType property,
     * you can use any of the pmTypes supported by the {@link PMUI.form.FormItemFactory FormItemFactory}.
     * - a {@link PMUI.form.FormPanel FormPanel} object.
     * - a{@link PMUI.form.Field Field} object
     */
    Form.prototype.addItem = function (item) {
        if (this.panel) {
            this.panel.addItem(item);
            this.updateDependencies();
        }
        return this;
    };
    /**
     * Set items for the form's root panel.
     * @param {Array} items An array in which each element can be one of the accepted parameters
     * in the {@link PMUI.form.Form#addItem addItem() method}.
     * @chainable
     */
    Form.prototype.setItems = function (items) {
        if (this.panel) {
            this.panel.setItems(items);
            this.updateDependencies();
        }
        return this;
    };
    /**
     * Returns all the form's direct items.
     * @param {String|Number} id If the parameter is a string then
     * it will be take as the id for the element to find and return,
     * but if the element is a Number it will return the object with that
     * index position.
     * @returns {Object}
     */
    Form.prototype.getItem = function (i) {
        return Form.superclass.prototype.getItem.call(this.panel, i);
    };
    /**
     * Sets the title text for the form.
     * @param {String} title
     * @chainable
     */
    Form.prototype.setTitle = function (title) {
        if (typeof title === 'string') {
            this.title = title;
            if (this.header) {
                $(this.header).empty();
                if (title) {
                    $(this.header).append('<h2 class="pmui-form-title"></h2>').find('h2').text(title);
                } else {
                    $(this.header).append('<h2 class="pmui-form-title"></h2>').find('h2').html("&nbsp;");
                }
                this.updateDimensions();
            }
        }

        return this;
    };
    /**
     * Sets the name for the form
     * @param {String} name
     * @chainable
     */
    Form.prototype.setName = function (name) {
        this.name = name;
        if (this.html) {
            this.html.name = name;
        }

        return this;
    };
    /**
     * Sets the enctype property for the form
     * @param {String} encType
     * @chainable
     */
    Form.prototype.setEncType = function (encType) {
        this.encType = encType;
        if (this.html) {
            this.html.setAttribute("encType", encType);
        }

        return this;
    };
    /**
     * Shows the form header
     * @chainable
     */
    Form.prototype.showHeader = function () {
        this.visibleHeader = true;
        this.header.style.display = "";
        this.updateDimensions();

        return this;
    };
    /**
     * Hides the form's header
     * @chainable
     */
    Form.prototype.hideHeader = function () {
        this.visibleHeader = false;
        this.header.style.display = "none";
        this.updateDimensions();

        return this;
    };
    /**
     * Returns true if all the form's fields assert the their validations otherwise returns false.
     * @return {Boolean}
     */
    Form.prototype.isValid = function () {
        var res = this.panel.isValid();

        if (res) {
            this.hideRequiredFieldNotification();
        } else {
            this.showRequiredFieldNotification();
        }

        return res;
    };
    /**
     * Submits the form.
     * @chainable
     */
    Form.prototype.submit = function () {
        if (this.isValid()) {
            if (typeof this.onSubmit === 'function') {
                this.onSubmit(this.getData());
            }
            //TODO: call send() method from proxy
        }

        return this;
    };
    /**
     * Resets the form to their initial values.
     * @chainable
     */
    Form.prototype.reset = function () {
        var fields,
            i;

        fields = this.getFields();
        for (i = 0; i < fields.length; i += 1) {
            fields[i].reset();
        }
        if (this.dirty) {
            this.dirty = false;
        }
        return this;
    };
    /**
     * Returns the form's data.
     * @param {String} [format='object'] The format in which the data will be returned, it can take three values:
     *
     * - 'xml', the data will be returned in xml format.
     * - 'json', the data will be returned in a json-formated string.
     * - 'object', the data will be returned in a object literal format.
     * - otherwise or nothing, the data will be returned in a object literal format but only containing the name and values from
     * fields.
     * @return {Object|String} The data type of the returned item will depend on the input parameter, please read the
     * method description for more info.
     */
    Form.prototype.getData = function (format) {
        var fields,
            i,
            res,
            data = new PMUI.data.DataSet();

        fields = this.getFields();
        if (format !== 'xml' && format !== 'json' && format !== 'object') {
            res = {};
            for (i = 0; i < fields.length; i += 1) {
                if (!fields[i].disabled) {
                    res[fields[i].getName()] = fields[i].getValue();
                }
            }
        } else {
            for (i = 0; i < fields.length; i += 1) {
                if (!fields[i].disabled) {
                    data.addItem(fields[i].data);
                }
            }
            if (format === 'xml') {
                return data.getXML();
            } else if (format === 'json') {
                return data.getJSON();
            } else if (format === 'object') {
                return data.getData();
            }
        }
        return res;
    };
    Form.prototype.hideRequiredFieldNotification = function () {
        if (this._requiredFieldNotification) {
            this._requiredFieldNotification.style.display = 'none';
        }
        return this;
    };

    Form.prototype.showRequiredFieldNotification = function () {
        if (this._requiredFieldNotification) {
            this._requiredFieldNotification.style.display = '';
        }
        return this;
    };
    /**
     * Define the event listeners for the form.
     * @chainable
     */
    Form.prototype.defineEvents = function () {
        var that = this;

        if (this.html && !this.eventsDefined) {
            this.panel.defineEvents();
            this.footer.defineEvents();
            this.addEvent('submit').listen(this.html, function (e) {
                e.preventDefault();
                e.stopPropagation();
            });
            this.panel.setOnAvailabilityChange(function () {
                that.isValid();
            });
            this.eventsDefined = true;
        }
        return this;
    };
    /**
     * Creates the HTML for the form.
     * @return {HTMLElement}
     */
    Form.prototype.createHTML = function () {
        var html,
            header,
            notificationArea,
            notification,
            notificationText = "Fields marked with asterisk (%%ASTERISK%%) are required.".translate()
                .replace(/%%ASTERISK%%/g, '<span style="color: #e84c3d">*</span>');

        if (this.html) {
            return this.html;
        }
        html = PMUI.createHTMLElement('form');
        html.id = this.id;
        html.className = 'pmui-form';

        header = PMUI.createHTMLElement('div');
        header.className = 'pmui-form-header';

        notification = new PMUI.field.TextAnnotationField({
            id: "requiredMessage",
            name: "Message",
            textType: PMUI.field.TextAnnotationField.TEXT_TYPES.HTML,
            text: notificationText,
            text_Align: "center"
        });
        notificationArea = PMUI.createHTMLElement('div');
        notificationArea.style.display = 'none';
        notificationArea.appendChild(notification.getHTML());
        this._requiredFieldNotification = notificationArea;

        this.header = header;

        html.appendChild(header);
        html.appendChild(this.panel.getHTML());
        html.appendChild(notificationArea);
        html.appendChild(this.footer.getHTML());
        this.html = html;

        this.setTitle(this.title)
            .setName(this.name)
            .setEncType(this.encType);

        if (this.visibleHeader) {
            this.showHeader();
        } else {
            this.hideHeader();
        }

        this.panel.setPadding('20px 10px 20px 10px');

        this.style.applyStyle();
        if (this.layout) {
            this.layout.applyLayout();
        }
        if (this.getFields().length) {
            this.setFocus(0);
        }
        this.setButtonPanelPosition(this.buttonPanelPosition);

        this.defineEvents();

        return this.html;
    };
    /**
     * clean all form fields
     * @chainable
     */
    Form.prototype.clearItems = function () {
        this.panel.clearItems();
        return this;
    };
    /**
     * clear all form fields
     * @chainable
     */
    Form.prototype.removeItem = function (item) {
        var i = 0;
        Form.superclass.prototype.removeItem.call(this, item);
        this.panel.removeItem(item);
        return this;
    };
    /**
     * hides the footer where the buttons are located
     * @chainable
     */
    Form.prototype.hideFooter = function () {
        this.footer.setVisible(false);
        return this;
    };
    /**
     * shows the footer where the buttons are located
     * @chainable
     */
    Form.prototype.showFooter = function () {
        this.footer.setVisible(true);
        return this;
    };
    /**
     * @method setFocus
     * set the focus on a form field
     * @chainable
     */
    Form.prototype.setFocus = function () {
        var j,
            fields = this.getFields();

        for (j = 0; j < fields.length; j += 1) {
            if ((!fields[j].isReadOnly || !fields[j].isReadOnly()) && !fields[j].disabled && fields[j].isVisible()) {
                fields[j].setFocus();
                break;
            }
        }
        return this;
    }
    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = Form;
    }
    Form.prototype.setAlignmentButtons = function (value) {
        if (typeof value == "string") {
            this.alignmentButtons = value;
            this.footer.setAlignment(value);
            return this;
        }
        throw new Error('setAlignmentButtons(), the value is no type valid')
    };

    PMUI.extendNamespace('PMUI.form.Form', Form);
}());