(function () {
    /**
     * @class PMUI.ui.InfoMessageWindow
     * A window to show an info message.
     * @extends {PMUI.ui.MessageWindow}
     *
     * Usage example:
     *
     *      @example
     *      var message_window = new PMUI.ui.InfoMessageWindow({
     *          message: 'This is an info message.'
     *      });
     *      message_window.open();
     *
     * @constructor
     * Creates a new instance of the class InfoMessageWindow
     * @param {Object} [settings={}] An object with the config options.
     *
     * @cfg {String} [title='Information'] The message to show in the message window.
     */
    var InfoMessageWindow = function (settings) {
        InfoMessageWindow.superclass.call(this, settings);
        InfoMessageWindow.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.ui.MessageWindow', InfoMessageWindow);
    /**
     * Initializes the object.
     * @param  {Object} settings The object with the config options.
     * @private
     */
    InfoMessageWindow.prototype.init = function (settings) {
        var defaults = {
            title: 'Info'
        };

        jQuery.extend(true, defaults, settings);

        this.setTitle(defaults.title);
    };
    /**
     * Creates the message window HTML.
     * @return {HTMLElement}
     */
    InfoMessageWindow.prototype.createHTML = function () {
        InfoMessageWindow.superclass.prototype.createHTML.call(this);

        jQuery(this.dom.icon).addClass('pmui-messagewindow-icon-info');

        return this.html;
    };

    PMUI.extendNamespace('PMUI.ui.InfoMessageWindow', InfoMessageWindow);

    if (typeof exports !== "undefined") {
        module.exports = InfoMessageWindow;
    }
}());