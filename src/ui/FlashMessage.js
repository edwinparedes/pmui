(function () {
    /**
     * @class PMUI.ui.FlashMessage
     * A message to display for a while.
     * @extends {PMUI.core.Element}
     *
     * Usage example:
     *
     *      @example
     *      var message = new PMUI.ui.FlashMessage({
     *          message: "Hi!, this is a flash message!",
     *          duration: 5000,
     *          severity: 'info'
     *      });
     *
     *      message.show();
     *
     * @constructor
     * Creates a new instance of the class.
     * @param {Object} [settings] The config options.
     *
     * @cfg {String|Array} [message=""] The object's message. It can be a single string or an array of strings. In the
     * llatter case the mesage will be showed in a list format.
     * @cfg {Number} [duration=3000] The time in milliseconds the message will be displayed.
     * @cfg {HTMLElement|PMUI.ui.Element} [appendTo=document.body] The html element/PMUI's object the flash message
     * will be displayed.
     * @cfg {String} [display="absolute"] The display mode for the object's html.
     * @cfg {String} [position="absolute"] The position mode for the object's html.
     * @cfg {String} [severity="info"] The severity for the message. Valid values: 'info', 'success', 'error'.
     */
    var FlashMessage = function (settings) {
        FlashMessage.superclass.call(this, settings);
        /**
         * The object's message. Set by the {@link #cfg-message message} config option and the
         * {@link #method-setMessage setMessage()} method.
         * @type {String|Array}
         * @readonly
         */
        this.message = null;
        /**
         * The duration in milliseconds to show the message. Set by the {@link #cfg-duration duration} config option
         * and the {@link #method-setDuration setDuration()} method.
         * @type {Number}
         * @readonly
         */
        this.duration = null;
        /**
         * The html element/PMUI's object the message will be displayed in. Set by the {@link #cfg-appendTo appendTo}
         * config option and the {@link #method-setAppendTo setAppendTo()} method.
         * @type {HTMLElement|PMUI.core.Element}
         */
        this.appendTo = null;
        /**
         * The message's severity. Set by the {@link #cfg-severity severity} config option and the
         * {@link #method-setSeverity setSeverity()} method.
         * @type {String}
         * @readonly
         */
        this.severity = null;
        FlashMessage.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Element', FlashMessage);
    /**
     * The object's type.
     * @type {String}
     */
    FlashMessage.prototype.type = 'FlashMessage';
    /**
     * The initializer method.
     * @param  {Object} [settings] The config options
     * @private
     */
    FlashMessage.prototype.init = function (settings) {
        var defaults = {
            message: "",
            duration: 3000,
            appendTo: document.body,
            display: 'inline-block',
            positionMode: 'fixed',
            severity: 'info'
        };

        jQuery.extend(true, defaults, settings);
        this.setMessage(defaults.message)
            .setDuration(defaults.duration)
            .setAppendTo(defaults.appendTo)
            .setPositionMode(defaults.positionMode)
            .setDisplay(defaults.display)
            .setSeverity(defaults.severity);
    };
    /**
     * Sets the severity for the message.
     * @param {String} severity Valid values: "info", "success" or "error".
     * @chainable
     */
    FlashMessage.prototype.setSeverity = function (severity) {
        if (!(severity === 'success' || severity === 'error' || severity === 'info')) {
            throw new Error('setSeverity(): the parameter must be "success" or "error" or "info"');
        }
        this.severity = severity;
        this.style.removeClasses(['pmui-info', 'pmui-error', 'pmui-success']).addClasses(['pmui-' + severity]);

        return this;
    };
    /**
     * Sets the html element/PMUI's object in which the message will be displayed.
     * @param {HTMLElement|PMUI.core.Element} appendTo
     * @chainable
     */
    FlashMessage.prototype.setAppendTo = function (appendTo) {
        if (!(PMUI.isHTMLElement(appendTo) || appendTo instanceof PMUI.core.Element)) {
            throw new Error("setAppendTo(): The parameter must be a HTML element or an instance of PMUI.ui.Element.");
        }
        this.appendTo = appendTo;
        return this;
    };
    /**
     * Sets the duration for display the message.
     * @param {Number} duration The duration in milliseconds.
     * @chainable
     */
    FlashMessage.prototype.setDuration = function (duration) {
        if (typeof duration !== 'number') {
            throw new Error('setDuration(): The parameter must be a number.');
        }
        this.duration = duration;
        return this;
    };
    /**
     * Sets the message for the object.
     * @param {String|Array} message It can be a string or an array of strings, in the latter case the message will be
     * showed in a list format.
     * @chainable
     */
    FlashMessage.prototype.setMessage = function (message) {
        var ul,
            li,
            i;

        if (typeof message !== 'string' && !jQuery.isArray(message)) {
            throw new Error('setMessage(): The parameter must be a message.');
        }
        this.message = (typeof message === 'string') ? jQuery.trim(message) : message;
        if (this.html) {
            jQuery(this.html).empty();
            if (typeof message === 'string') {
                this.html.textContent = message;
            } else {
                ul = PMUI.createHTMLElement('ul');
                ul.className = 'pmui-flashmessage-list';
                ul.style.listStyleType = 'none';
                for (i = 0; i < message.length; i += 1) {
                    li = PMUI.createHTMLElement('li');
                    li.textContent = message[i];
                    ul.appendChild(li);
                }
                this.html.appendChild(ul);
            }

        }
        return this;
    };
    /**
     * Shows the message in the html element/PMUI's object specified by the {@link #cfg-appendTo appendTo} config
     * option or the {@link #method-setAppendTo setAppendTo()} method.
     * @chainable
     */
    FlashMessage.prototype.show = function () {
        var targetHTML = this.appendTo,
            html = this.html,
            top = 50,
            w,
            pw;

        if (!PMUI.isHTMLElement(targetHTML)) {
            targetHTML = targetHTML.html;
        }
        if (targetHTML) {
            if (!html) {
                html = this.getHTML();
            }
            jQuery(html).fadeTo(1, 0).get(0).style.top = top + "px";
            document.body.appendChild(html);
            w = jQuery(html).outerWidth();
            targetHTML.appendChild(html);
            this.style.addProperties({
                left: '50%',
                'margin-left': w / -2
            });
            jQuery(html).finish().css({
                'top': '50px'
            }).fadeTo(1, 0).animate({
                top: "-=" + top,
                opacity: 1
            }, 400, 'swing').delay(this.duration).animate({
                top: "+=" + top,
                opacity: 0,
                zIndex: '0'
            });
        }
        return this;
    };
    /**
     * @inheritDoc
     */
    FlashMessage.prototype.createHTML = function () {
        FlashMessage.superclass.prototype.createHTML.call(this);
        this.setMessage(this.message);
        return this.html;
    };

    PMUI.extendNamespace('PMUI.ui.FlashMessage', FlashMessage);

    if (typeof exports !== 'undefined') {
        module.exports = FlashMessage;
    }
}());