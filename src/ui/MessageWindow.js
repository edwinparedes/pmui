(function () {
    var MessageWindow = function (settings) {
        MessageWindow.superclass.call(this, settings);
        /**
         * The message for the message window.
         * @type {String}
         * @readonly
         */
        this.message = null;
        /**
         * An object for store the necessary dom elements to build the message window HTML.
         * @type {Object}
         * @private
         */
        this.dom = {};
        this.iconClass = null;
        this.windowMessageType = null;
        MessageWindow.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.ui.Window', MessageWindow);


    MessageWindow.prototype.type = 'WindowMessage';

    MessageWindow.prototype.family = 'ui';

    MessageWindow.prototype.init = function (settings) {
        var defaults = {
            message: '[not-message]',
            iconClass: '',
            windowMessageType: 'default',
            title: '',
            footerAlign: "right"
        };

        jQuery.extend(true, defaults, settings);

        this.setFooterAlign(defaults.footerAlign);
        this.setMessage(defaults.message);
        this.setWindowMessageType(defaults.windowMessageType);
        this.setTitle(defaults.title);

    };

    MessageWindow.prototype.setMessage = function (message) {
        if (typeof message !== 'string') {
            throw new Error('setMessage(): the parameter must be a string.');
        }

        this.message = jQuery.trim(message);

        if (this.dom.messageContainer) {
            this.dom.messageContainer.textContent = message;
        }

        return this;
    };
    MessageWindow.prototype.getMessage = function () {
        return this.message;
    };

    MessageWindow.prototype.setWindowMessageType = function (type) {
        var classMessage = "";

        if (typeof type !== "string") {
            throw  new Error("settype(): the type value is not valid");
        }
        this.windowMessageType = type;
        this.style.removeClasses(["pmui-windowmessage-default", "pmui-windowmessage-error", "pmui-windowmessage-warning", "pmui-windowmessage-success"]);
        classMessage = 'pmui-windowmessage-' + this.windowMessageType;
        switch (this.windowMessageType) {
            case 'error':
                this.style.addClasses([classMessage]);
                break;
            case 'warning':
                this.style.addClasses([classMessage]);
                break;
            case 'success':
                this.style.addClasses([classMessage]);
                break;
            case 'default':
                this.style.addClasses([classMessage]);
                break;
        }
        return this;
    };

    MessageWindow.prototype.getWindowMessageType = function () {
        return this.windowMessageType;
    };

    MessageWindow.prototype.addItem = function () {
        return this;
    };

    MessageWindow.prototype.createHTML = function () {
        var table,
            row,
            cellIcon,
            cellLabel,
            icon,
            p,
            textContent;

        MessageWindow.superclass.prototype.createHTML.call(this);

        table = PMUI.createHTMLElement('table');
        table.className = 'pmui-messagewindow-container';
        row = PMUI.createHTMLElement('tr');
        cellIcon = PMUI.createHTMLElement('td');
        cellLabel = PMUI.createHTMLElement('td');
        cellLabel.style.textAlign = "center";
        cellLabel.className = "pmui-content-label"
        icon = PMUI.createHTMLElement('span');
        icon.className = 'pmui-messagewindow-icon';
        textContent = PMUI.createHTMLElement('span');
        textContent.className = 'pmui-messagewindow-message';

        cellIcon.appendChild(icon);
        row.appendChild(cellIcon);


        cellLabel.appendChild(textContent);
        row.appendChild(cellLabel);
        table.appendChild(row);

        this.dom.icon = icon;
        this.dom.messageContainer = textContent;

        this.body.appendChild(table);

        this.setMessage(this.message);
        this.body.style.padding = "15px";
        cellLabel.style.width = (typeof this.width == 'number') ? this.width * (0.84) + "px" : "70%";
        this.body.style.minHeight = "inherit";
        return this.html;
    };

    MessageWindow.prototype.updateDimensionsAndPosition = function () {
        var bodyHeight,
            footerHeight,
            headerHeight,
            windowHeight = this.height,
            windowWidth;

        if (!this.footer || !this.html || !this.isOpen) {
            return this;
        }
        if (this.footerHeight === 'auto') {
            this.footer.setHeight('auto');
        } else {
            this.footer.setHeight(this.footerHeight);
        }

        if (windowHeight === 'auto') {
            this.body.style.height = 'auto';
            this.body.style.minHeight = 'inherit';
        } else {
            if (/^\d+(\.\d+)?em$/.test(windowHeight)) {
                windowHeight = PMUI.emToPx(parseInt(windowHeight, 10), this.modalObject);
            } else if (/^\d+(\.\d+)?%$/.test(windowHeight)) {
                windowHeight = jQuery(this.html).outerHeight();
            }
            footerHeight = this.visibleFooter ? jQuery(this.footer.getHTML()).outerHeight() : 0;
            headerHeight = jQuery(this.header).outerHeight();
            bodyHeight = windowHeight - footerHeight - headerHeight;
            if (bodyHeight <= 0) {
                bodyHeight = 0;
            }
            this.body.style.minHeight = '';
            this.body.style.height = bodyHeight + "px";
        }

        windowWidth = jQuery(this.header).width();
        windowWidth = windowWidth - (this.visibleCloseButton ? jQuery(this.closeButton.getHTML()).outerWidth() : 0);
        this.dom.titleContainer.style.width = windowWidth < 0 ? 0 : windowWidth + 'px';

        windowWidth = jQuery(this.html).outerWidth();
        windowHeight = jQuery(this.html).outerHeight();

        this.addCSSProperties({
            left: '50%',
            "margin-left": (windowWidth / -2) + "px",
            top: '50%',
            "margin-top": (windowHeight / -2) + "px"
        });
        return this;
    }

    PMUI.extendNamespace('PMUI.ui.MessageWindow', MessageWindow);

    if (typeof exports !== "undefined") {
        module.exports = MessageWindow;
    }
}());
