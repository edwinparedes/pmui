(function () {
    /**
     * @class PMUI.ui.ConfirmMessageWindow
     * A window to show a question message. It haas built-in Yes and No buttons, but they can be overrode by some
     * custom ones.
     * @extends {PMUI.ui.MessageWindow}
     *
     * Usage example:
     *
     *      @example
     *      var message_window = new PMUI.ui.ConfirmMessageWindow({
     *          message: 'Do you want to display an alert?',
     *              onYes: function() {
     *                  alert("this is an alert");
     *                  message_window.close();
     *              }, 
     *              onNo: function() {
     *                  message_window.close();
     *              }
     *      });
     *      message_window.open();
     *
     * @constructor
     * Creates a new instance of the class ConfirmMessageWindow
     * @param {Object} [settings={}] An object with the config options.
     *
     * @cfg {String} [title='Error'] The message to show in the message window.
     * @cfg {String} [yesText='Yes'] The text for the window's built-in Yes button.
     * @cfg {String} [noText='No'] The text for the window's built-in No button.
     * @cfg {Function|null} [onYes=null] The handler to be executed when the built-in Yes button is clicked.
     * @cfg {Function|null} [onNo=null] The handler to be executed when the built-in No button is clicked.
     * @cfg footerItems
     * The items to be shown in the message window's footer, the accepted values for this are the same that the ones
     * for the Window's footerItems config option.
     *
     * By default this is an empty array, if you modify this then the built-in Yes and No buttons won't be used,
     * therefore the yesText, noText config options and onYes, onNo handlers won't be used either.
     */
    var ConfirmMessageWindow = function (settings) {
        ConfirmMessageWindow.superclass.call(this, settings);
        ConfirmMessageWindow.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.ui.MessageWindow', ConfirmMessageWindow);
    /**
     * Initializes the object.
     * @param  {Object} settings The object with the config options.
     * @private
     */
    ConfirmMessageWindow.prototype.init = function (settings) {
        var yesButton = new PMUI.ui.Button(),
            noButton = new PMUI.ui.Button(),
            defaults = {
                title: 'Confirm',
                yesText: 'Yes',
                noText: 'No',
                onYes: null,
                onNo: null,
                footerItems: []
            };

        jQuery.extend(true, defaults, settings);

        this.setTitle(defaults.title);

        if (!defaults.footerItems || !defaults.footerItems.length) {
            defaults.footerItems = [
                yesButton.setHandler(defaults.onYes).setText(defaults.yesText),
                noButton.setHandler(defaults.onNo).setText(defaults.noText)
            ];
        }
        this.setFooterItems(defaults.footerItems);
    };
    /**
     * @inheritdoc
     */
    ConfirmMessageWindow.prototype.createHTML = function () {
        ConfirmMessageWindow.superclass.prototype.createHTML.call(this);

        jQuery(this.dom.icon).addClass('pmui-messagewindow-icon-confirm');

        return this.html;
    };

    PMUI.extendNamespace('PMUI.ui.ConfirmMessageWindow', ConfirmMessageWindow);

    if (typeof exports !== "undefined") {
        module.exports = ConfirmMessageWindow;
    }
}());