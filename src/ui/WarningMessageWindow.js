(function () {
    /**
     * @class PMUI.ui.WarningMessageWindow
     * A window to show a warning message.
     * @extends {PMUI.ui.MessageWindow}
     *
     * Usage example:
     *
     *      @example
     *      var message_window = new PMUI.ui.WarningMessageWindow({
     *          message: 'This is a warning message.'
     *      });
     *      message_window.open();
     *
     * @constructor
     * Creates a new instance of the class WarningMessageWindow
     * @param {Object} [settings={}] An object with the config options.
     *
     * @cfg {String} [title='Warning'] The message to show in the message window.
     */
    var WarningMessageWindow = function (settings) {
        WarningMessageWindow.superclass.call(this, settings);
        WarningMessageWindow.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.ui.MessageWindow', WarningMessageWindow);
    /**
     * Initializes the object.
     * @param  {Object} settings The object with the config options.
     * @private
     */
    WarningMessageWindow.prototype.init = function (settings) {
        var defaults = {
            title: 'Warning'
        };

        jQuery.extend(true, defaults, settings);

        this.setTitle(defaults.title);
    };
    /**
     * Creates the message window ĤTML.
     * @return {HTMLElement}
     */
    WarningMessageWindow.prototype.createHTML = function () {
        WarningMessageWindow.superclass.prototype.createHTML.call(this);

        jQuery(this.dom.icon).addClass('pmui-messagewindow-icon-warning');

        return this.html;
    };

    PMUI.extendNamespace('PMUI.ui.WarningMessageWindow', WarningMessageWindow);

    if (typeof exports !== "undefined") {
        module.exports = WarningMessageWindow;
    }
}());