(function () {
    /**
     * @class PMUI.ui.ErrorMessageWindow
     * A window to show a error message.
     * @extends {PMUI.ui.MessageWindow}
     *
     * Usage example:
     *
     *      @example
     *      var message_window = new PMUI.ui.ErrorMessageWindow({
     *          message: 'This is an error message.'
     *      });
     *      message_window.open();
     *
     * @constructor
     * Creates a new instance of the class ErrorMessageWindow
     * @param {Object} [settings={}] An object with the config options.
     *
     * @cfg {String} [title='Error'] The message to show in the message window.
     */
    var ErrorMessageWindow = function (settings) {
        ErrorMessageWindow.superclass.call(this, settings);
        ErrorMessageWindow.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.ui.MessageWindow', ErrorMessageWindow);
    /**
     * Initializes the object.
     * @param  {Object} settings The object with the config options.
     * @private
     */
    ErrorMessageWindow.prototype.init = function (settings) {
        var defaults = {
            title: 'Error'
        };

        jQuery.extend(true, defaults, settings);

        this.setTitle(defaults.title);
    };
    /**
     * Creates the message window HTML.
     * @return {HTMLElement}
     */
    ErrorMessageWindow.prototype.createHTML = function () {
        ErrorMessageWindow.superclass.prototype.createHTML.call(this);

        jQuery(this.dom.icon).addClass('pmui-messagewindow-icon-error');

        return this.html;
    };

    PMUI.extendNamespace('PMUI.ui.ErrorMessageWindow', ErrorMessageWindow);

    if (typeof exports !== "undefined") {
        module.exports = ErrorMessageWindow;
    }
}());