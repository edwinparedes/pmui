(function () {
    /**
     * @class PMUI.ui.TextLabel
     * A text label that can contain a plain text or an HTML formatted text.
     * @extends {PMUI.core.Element}
     *
     * Usage example:
     *
     *      @example
     *      var plainText, htmlText;
     *      plainText = new PMUI.ui.TextLabel({
     *          textMode: 'plain',
     *          text: 'Hello! this is a label with a plain text in it',
     *          style: {
     *              cssProperties: {
     *                  background: "red"
     *              }
     *          }
     *      });
     *      htmlText = new PMUI.ui.TextLabel({
     *          textMode: 'html',
     *          text: '<h1>Hello!</h1> this ia a label with a <b> HTML</b> formatted text'
     *      });
     *      document.body.appendChild(plainText.getHTML());
     *      document.body.appendChild(htmlText.getHTML());
     *
     * @constructor
     * Creates a new instance of the class.
     * @param {Object} [settings={}] An object literal with the config options to config the object.
     *
     * @cfg {String} [textMode='plain'] The rendering mode for the text in the label. Possible values:
     *
     * - "plain": for plain text rendering.
     * - "html": for html text rendering. Only the <b>, <i>, <h1> to <h6> tags are allowed, any other tags will be
     * removed.
     * @cfg {String} [elementTag='span'] The HTML tag to be used as the text label.
     * @cfg {String} [text=""] The label's text.
     */
    var TextLabel = function (settings) {
        TextLabel.superclass.call(this, settings);
        /**
         * The text rendering mode. Set by the {@link #cfg-textMode textMode config option} and the
         * {@link #method-setTextMode setTextMode() method}.
         * @type {String}
         * @readonly
         */
        this.textMode = null;
        /**
         * The current displayed label's text. Set by the {@link #cfg-text text config option} and the
         * {@link #method-setText setText() method}.
         * @type {String}
         * @readonly
         */
        this.text = null;
        /**
         * The original set text.
         * @type {String}
         * @private
         */
        this.rawText = null;
        TextLabel.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Element', TextLabel);
    /**
     * The object's type.
     * @type {String}
     */
    TextLabel.prototype.type = 'TextLabel';
    /**
     * Initializes the object.
     * @param  {Object} [settings={}] An object literal with the config options the new object will be initialized
     * with.
     * @chainable
     * @private
     */
    TextLabel.prototype.init = function (settings) {
        var defaults = {
            textMode: 'plain',
            elementTag: 'span',
            text: ''
        };

        jQuery.extend(true, defaults, settings);

        this.setElementTag(defaults.elementTag)
            .setTextMode(defaults.textMode)
            .setText(defaults.text);

        return this;
    };
    /**
     * Sets the label's text.
     * @param {String} text
     */
    TextLabel.prototype.setText = function (text) {
        var tags;

        if (typeof text !== 'string') {
            throw new Error('setText(): the parameter must be a string.');
        }
        this.text = this.rawText = text;
        if (this.textMode === 'html') {
            allowed = ("<b><i><h1><h2><h3><h4><h5><h6>".match(/<[a-z][a-z0-9]*>/g) || []).join('');
            tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
                commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
            this.text = text.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
                return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
            });
        }

        if (this.html) {
            if (this.textMode === 'html') {
                this.html.innerHTML = this.text;
            } else {
                this.html.textContent = this.text;
            }
        }
        return this;
    };
    /**
     * Sets the text rendering mode.
     * @param {String} textMode Possible values:
     *
     * - "plain": for plain text rendering.
     * - "html": for html text rendering. Only the <b>, <i>, <h1> to <h6> tags are allowed, any other tags will be
     * removed.
     */
    TextLabel.prototype.setTextMode = function (textMode) {
        if (textMode !== 'plain' && textMode !== 'html') {
            throw Error("setTextMode(): The parameter must be \"plain\" or \"html\".");
        }
        this.textMode = textMode;
        if (typeof this.rawText === 'string') {
            this.setText(this.rawText);
        }
        return this;
    };
    /**
     * Creates the label's HTML.
     * @return {HTMLElement}
     */
    TextLabel.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }
        TextLabel.superclass.prototype.createHTML.call(this);
        this.setText(this.rawText);
        return this.html;
    };

    PMUI.extendNamespace('PMUI.ui.TextLabel', TextLabel);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = TextLabel;
    }
}());