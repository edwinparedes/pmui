(function () {
    /**
     * @class PMUI.util.Style
     * Class that represent the style of a an object.
     *
     *      // i.e
     *      // Let's assume that 'shape' is a CustomShape
     *      var style = new Style({
     *          cssClasses: [
     *              'sprite-class', 'marker-class', ...
     *          ],
     *          cssProperties: {
     *              border: 1px solid black,
     *              background-color: grey,
     *              ...
     *          },
     *          belongsTo: shape
     *      })
     *
     * @constructor Creates a new instance of this class
     * @param {Object} options
     * @cfg {Array} [cssClasses=[]] the classes that `this.belongsTo` has
     * @cfg {Object} [cssProperties={}] the css properties that `this.belongsTo` has
     * @cfg {Object} [belongsTo=null] a pointer to the owner of this instance
     */
    var Style = function (options) {

        /**
         * JSON Object used to map each of the css properties of the object,
         * this object has the same syntax as the object passed to jQuery.css()
         *      cssProperties: {
         *          background-color: [value],
         *          border: [value],
         *          ...
         *      }
         * @property {Object}
         */
        this.cssProperties = null;

        /**
         * Array of all the classes of this object
         *      cssClasses = [
         *          'class_1',
         *          'class_2',
         *          ...
         *      ]
         * @property {Array}
         */
        this.cssClasses = null;

        /**
         * Pointer to the object to whom this style belongs to
         * @property {Object}
         */
        this.belongsTo = null;


        Style.prototype.initObject.call(this, options);
    };


    /**
     * The type of this class
     * @property {String}
     */
    Style.prototype.type = "Style";

    /**
     * Constant for the max z-index
     * @property {number} [MAX_ZINDEX=100]
     */
    Style.MAX_ZINDEX = 100;

    /**
     * Instance initializer which uses options to extend the config options to
     * initialize the instance
     * @private
     * @param {Object} options
     */
    Style.prototype.initObject = function (options) {
        var defaults = {
            cssClasses: [],
            cssProperties: {},
            belongsTo: null
        };

        jQuery.extend(true, defaults, options);
        this.cssClasses = defaults.cssClasses;
        this.cssProperties = defaults.cssProperties;
        this.belongsTo = defaults.belongsTo;
    };

    /**
     * Applies cssProperties and cssClasses to `this.belongsTo`
     * @chainable
     */
    Style.prototype.applyStyle = function () {
        var i,
            t,
            class_i;

        if (!this.belongsTo.html) {
            throw new Error("applyStyle(): can't apply style to an" +
                " object with no html.");
        }

        // apply the cssProperties
        jQuery(this.belongsTo.html).css(this.cssProperties);

        //adding default classes
        t = this.belongsTo.type.toLowerCase();
        if (this.cssClasses.indexOf('pmui-' + t) === -1) {
            this.cssClasses.unshift('pmui-' + t);
        }
        if (this.cssClasses.indexOf('pmui') === -1) {
            this.cssClasses.unshift('pmui');
        }

        // apply saved classes
        for (i = 0; i < this.cssClasses.length; i += 1) {
            class_i = this.cssClasses[i];
            if (!$(this.belongsTo.html).hasClass(class_i)) {
                jQuery(this.belongsTo.html).addClass(class_i);
            }
        }
        return this;
    };
    Style.prototype.unapplyStyle = function () {
        var t,
            property;

        if (!this.belongsTo.html) {
            throw new Error("unapplyStyle(): can't unapply style to an object with no html.");
        }
        t = this.belongsTo.type.toLowerCase();
        jQuery(this.belongsTo.html).removeClass("pmui-" + t);
        for (property in this.cssProperties) {
            jQuery(this.belongsTo.html).css(property, "");
        }
    };

    /**
     * Extends the property `cssProperties` with a new object and also applies those new properties
     * @param {Object} properties
     * @chainable
     */
    Style.prototype.addProperties = function (properties) {
        jQuery.extend(true, this.cssProperties, properties);
        jQuery(this.belongsTo && this.belongsTo.html).css(properties);
        return this;
    };

    /**
     * Gets a property from `this.cssProperties` using jQuery or `window.getComputedStyle()`
     * @param {String} property
     * @return {String}
     */
    Style.prototype.getProperty = function (property) {
        return this.cssProperties[property] ||
            jQuery(this.belongsTo.html).css(property) ||
            (this.belongsTo.html && window.getComputedStyle(this.belongsTo.html, null)
                .getPropertyValue(property)) || "";
    };
    /**
     * Returns all the style's css properties set explicitly.
     * @return {Object} An object literal with the properties.
     */
    Style.prototype.getProperties = function () {
        return this.cssProperties;
    };
    /**
     * Removes ´properties´ from the ´this.cssProperties´, also disables those properties from
     * the HTMLElement
     * @param {Array} properties An array in which each element is th name of the cssProperty to be removed.
     * @chainable
     */
    Style.prototype.removeProperties = function (properties) {
        var property,
            i;

        for (i = 0; i < properties.length; i += 1) {
            property = properties[i];
            if (this.cssProperties.hasOwnProperty(property)) { // JS Code Convention
                jQuery(this.belongsTo.html).css(property, "");   // reset inline style
                delete this.cssProperties[property];
            }
        }
        return this;
    };
    /**
     * Removes all properties from the object.
     * @chainable
     */
    Style.prototype.removeAllProperties = function () {
        var key;

        if (this.belongsTo) {
            for (key in this.cssProperties) {
                jQuery(this.belongsTo.html).css(key, "");
            }
        }
        this.cssProperties = {};
        return this;
    };
    /**
     * Adds new classes to ´this.cssClasses´ array
     * @param {Array} cssClasses
     * @chainable
     */
    Style.prototype.addClasses = function (cssClasses) {
        var i,
            cssClass;

        if (cssClasses && cssClasses instanceof Array) {
            for (i = 0; i < cssClasses.length; i += 1) {
                cssClass = cssClasses[i];
                if (typeof cssClass === "string") {
                    if (this.cssClasses.indexOf(cssClass) === -1) {
                        this.cssClasses.push(cssClass);
                        jQuery(this.belongsTo && this.belongsTo.html).addClass(cssClass);
                    }
                } else {
                    throw new Error("addClasses(): array element is not of type string");
                }
            }
        } else {
            throw new Error("addClasses(): parameter must be of type Array");
        }
        return this;
    };

    /**
     * Removes classes from ´this.cssClasses´ array, also removes those classes from
     * the HTMLElement
     * @param {Array} cssClasses
     * @chainable
     */
    Style.prototype.removeClasses = function (cssClasses) {
        var i,
            index,
            cssClass;

        if (cssClasses && cssClasses instanceof Array) {
            for (i = 0; i < cssClasses.length; i += 1) {
                cssClass = cssClasses[i];
                if (typeof cssClass === "string") {
                    index = this.cssClasses.indexOf(cssClass);
                    if (index !== -1) {
                        jQuery(this.belongsTo.html).removeClass(this.cssClasses[index]);
                        this.cssClasses.splice(index, 1);
                    }
                } else {
                    throw new Error("removeClasses(): array element is not of " +
                        "type string");
                }
            }
        } else {
            throw new Error("removeClasses(): parameter must be of type Array");
        }
        return this;
    };
    /**
     * Removes all the classes from ´this.cssClasses´ array
     * @param {Array} cssClasses
     * @chainable
     */
    Style.prototype.removeAllClasses = function () {
        while (this.cssClasses.length) {
            jQuery(this.belongsTo && this.belongsTo.html).removeClass(this.cssClasses.pop());
        }
        return this;
    };

    /**
     * Checks if the class is a class stored in ´this.cssClasses´
     * @param cssClass
     * @return {boolean}
     */
    Style.prototype.containsClass = function (cssClass) {
        return this.cssClasses.indexOf(cssClass) !== -1;
    };

    /**
     * Returns an array with all the classes of ´this.belongsTo´
     * @return {Array}
     */
    Style.prototype.getClasses = function () {
        return this.cssClasses.slice(0);
    };

    /**
     * Clears all the css properties and classes.
     * @chainable
     */
    Style.prototype.clear = function () {
        return this.removeAllClasses().removeAllProperties();
    }

    /**
     * Serializes this instance
     * @return {Object}
     * @return {Array} return.cssClasses
     */
    Style.prototype.stringify = function () {
        return {
            cssClasses: this.cssClasses
        };
    };

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = Style;
    }

    PMUI.extendNamespace('PMUI.util.Style', Style);

}());