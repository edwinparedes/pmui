(function () {
    /**
     * @class  PMUI.field.DateTimeField
     * @extends {PMUI.form.Field}
     * This field is to manipulate the [PMI.control.DateTimeControl]
     *
     * example:
     *
     *      @example
     *      var dateTimePicker;
     *          $(function() {
     *              dateTimePicker = new PMUI.field.DateTimeField(                   
     *                  {   
     *                      label:'Calendar',
     *                      helper: 'This is calendar Gregorian',
     *                      value: new Date(), 
     *                      datetime : true,
     *                      dateFormat: 'M dd yy',
     *                      minDate: -90,
     *                      maxDate: "+1y -1m -4d",
     *                      firstDay: 1,
     *                      months: {
     *                          "january": {
     *                              name: "Enero",
     *                              shortname: "Ene"
     *                          },
     *                          "february": {
     *                              name: "Febrero",
     *                              shortname: "Feb"
     *                          },
     *                          "march": {
     *                              name: "Marzo",
     *                              shortname: "Mar"
     *                          },
     *                          "april": {
     *                              name: "Abril",
     *                              shortname: "Abr"
     *                          },
     *                          "may": "May",
     *                          "june": "Junio",
     *                          "july": "July",
     *                          "august": "Agosto",
     *                          "september": "Septiembre",
     *                          "october": "Octubre",
     *                          "november": "Noviembre",
     *                          "december": "Diciembre"
     *                      },
     *                      days: {
     *                          "sunday": {
     *                              name: "Domingo",
     *                              shortname: "Do"
     *                          },
     *                          "monday": {
     *                              name: "Lunes",
     *                              shortname: "Lu"
     *                          },
     *                          "tuesday": {
     *                              name: "Martes",
     *                              shortname: "Ma"
     *                          },
     *                          "wednesday": {
     *                              name: "Miércoles",
     *                              shortname: "Mi"
     *                          },
     *                          "thursday": {
     *                              name: "Jueves",
     *                              shortname: "Ju"
     *                          },
     *                          "friday": "Viernes",
     *                          "saturday": "Sábado"
     *                      }
     *                  }
     *              );
     *              document.body.appendChild(dateTimePicker.getHTML());
     *              dateTimePicker.defineEvents();
     *          });
     *
     * @constructor Creates an instance of the class DateTimeField.
     * @param {Object} options Initialization options.
     * @cfg {Boolean} [datetime=false] If the [PMUI.control.DateTimeControl] You can see more about the configuration
     * in {@link PMUI.control.DateTimeControl#cfg-datetime datetime}
     * @cfg {String} [dateFormat="yy-mm-dd H:i:s"|"yy-mm-dd"],necessary to set the date format property of the control
     * [PMUI.control.DateTimeControl]
     * You can see more about the configuration in  {@link PMUI.control.DateTimeControl#cfg-dateFormat dateFormat}
     * @cfg {Object} [months={"january": "January", "february": "February", "march": "March", "april": "April", 
     * "may": "May", "june": "June", "july": "July", "august": "August", "september": "September", 
     * "october": "October", "november": "November", "december": "December"}], A JSON object to set the names and
     * shortnames for every month in year.
     * You can see more about the configuration in {@link PMUI.control.DateTimeControl#cfg-months months}
     * @cfg {Object} [days={"sunday": "Sunday","monday": "Monday","tuesday": "Tuesday","wednesday": "Wednesday",
     * "thursday": "Thursday","friday": "Friday","saturday": "Saturday"}], A JSON object to set the name and shortname
     * for every day of week.
     * You can see more about the configuration in {@link PMUI.control.DateTimeControl#cfg-days days}
     * @cfg {String|Number} [minDate=-365] A value which sets the min selectable date for the calendar. You can see
     * more about the configuration in
     * {@link PMUI.control.DateTimeControl#cfg-minDate minDate}
     * @cfg {String|Number} [maxDate=365] A value which sets the max selectable date for the calendar.
     * @cfg {Number} [firstDay=0] Sets the first day of week. You can use numbers from 0 to 6, 0 means Sunday, 1 means
     * Monday and so on.
     * @cfg {String} [returnFormat="UTC"] Specifies the date format that will be used for the returning date.
     * The valid values are:
     *
     * - "UTC", returns a date in the format: yyyy-mm-ddTHH:ii:ss-HH:mm (i.e. 2013-08-31T00:08:00+04:00).
     * - "@" or "timestamp", returns a date in timestamp format.
     * - [any other valid format string], will return the date using the string as the dateformat. This string can
     * contain any of the wilcards specified in the
     * {@link PMUI.control.DateTimeControl#cfg-dateFormat dateFormat config option}.
     */
    var DateTimeField = function (settings) {
        DateTimeField.superclass.call(this, settings);
        /**
         * The default date format to be used in the returning date. Set by the {@link #cfg-returnFormat} config option
         * and the {@link #method-setReturnFormat setReturnFormat()}  method.
         * @type {String}
         */
        this.returnFormat = null;
        DateTimeField.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.form.Field', DateTimeField);

    DateTimeField.prototype.type = "DateTimeField";

    DateTimeField.prototype.init = function (settings) {
        var defaults = {
            datetime: false,
            dateFormat: settings && settings.datetime ? 'yy-mm-dd H:i:s' : 'yy-mm-dd',
            months: {
                "january": "January",
                "february": "February",
                "march": "March",
                "april": "April",
                "may": "May",
                "june": "June",
                "july": "July",
                "august": "August",
                "september": "September",
                "october": "October",
                "november": "November",
                "december": "December"
            },
            days: {
                "sunday": "Sunday",
                "monday": "Monday",
                "tuesday": "Tuesday",
                "wednesday": "Wednesday",
                "thursday": "Thursday",
                "friday": "Friday",
                "saturday": "Saturday"
            },
            minDate: -365,
            maxDate: 365,
            firstDay: 0,
            returnFormat: 'UTC'
        };

        jQuery.extend(true, defaults, settings);

        this.setDateFormat(defaults.dateFormat)
            .setMonths(defaults.months)
            .setDays(defaults.days)
            .setMinDate(defaults.minDate)
            .setMaxDate(defaults.maxDate)
            .setFirstDay(defaults.firstDay)
            .setReturnFormat(defaults.returnFormat)
            .setValue(defaults.value)
            .visibleDateTime(defaults.datetime);
    };
    /**
     * Enables/disabled the calendar's [PMUI.control.DateTimeControl] time supporting.
     * @param  {Boolean} visible If it's true, then the time supporting is enabled, otherwise it's disabled.
     * @chainable
     */
    DateTimeField.prototype.visibleDateTime = function (visible) {
        this.controls[0].visibleDateTime(visible);
        return this;
    };
    /**
     * Returns the [PMUI.control.DateTimeControl] maximum selectable date.
     * @param {String} [format="UTC"] The format to applied to the returning date.
     * @return {String} The maximum selectable date in string format.
     */
    DateTimeField.prototype.getMaxDate = function (format) {
        return this.controls[0].getMaxDate(format);
    };
    /**
     * Returns the [PMUI.control.DateTimeControl] minimun selectable date.
     * @param {String} [format="UTC"] The format to applied to the returning date.
     * @return {String} The maximum selectable date in string format.
     */
    DateTimeField.prototype.getMinDate = function (format) {
        return this.controls[0].getMinDate(format);
    };
    /**
     * Sets the date formatr to be used when you get the value from the object.
     * @param {String} format The valid values are:
     *
     * - "UTC", returns a date in the format: yyyy-mm-ddTHH:ii:ss-HH:mm (i.e. 2013-08-31T00:08:00+04:00).
     * - "@" or "timestamp", returns a date in timestamp format.
     * - [any other valid format string], will return the date using the string as the dateformat. This string can
     * contain any of the wilcards specified in the
     * {@link PMUI.control.DateTimeControl#cfg-dateFormat dateFormat config option}.
     * @chainable
     */
    DateTimeField.prototype.setReturnFormat = function (format) {
        if (typeof format === 'string') {
            this.returnFormat = format;
        } else {
            throw new Error("setReturnFormat(): the parameter must be a string.");
        }

        return this;
    };
    /**
     * Returns the day index of the first day of week.
     * @return {Number} A number refering a day: 0 for Sunday, 1 for Monday and so on.
     */
    DateTimeField.prototype.getFirstDay = function () {
        return this.controls[0].getFirstDay();
    };
    /**
     * Sets the value to the field's controls.
     * @protected
     * @param {String} value
     * @param {String} utc
     * @chainable
     */
    DateTimeField.prototype.setValueToControls = function (value, utc) {
        this.controls[0].setValue(value, utc);
        return this;
    };
    /**
     * Sets the field's value.
     * @param {String} value
     * @param {String} utc
     */
    DateTimeField.prototype.setValue = function (value, utc) {
        value = value || "";
        if (value instanceof Date || typeof value === 'string' || typeof value === 'number') {
            if (this.controls[0]) {
                this.setValueToControls(value, utc);
                this.value = this.controls[0].getValue(this.returnFormat);
                this.data.setValue(this.value);
            }
        } else {
            throw new Error("The setValue() method only accepts string values!");
        }
        return this;
    };
    /**
     * Sets the first day of week.
     * @param {Number} day Use 0 for Sunday, 1 for Monday, 2 for Tuesday and so on!.
     */
    DateTimeField.prototype.setFirstDay = function (day) {
        this.controls[0].setFirstDay(day);
        return this;
    };
    /**
     * Sets the format for the date to be displayed in the control's textbox control.
     * You can see more about here [PMUI.control.DateTimeControl] in method
     * {@link PMUI.control.DateTimeControl#setDateFormat setDateFormat}
     **/
    DateTimeField.prototype.setDateFormat = function (dateFormat) {
        this.controls[0].setDateFormat(dateFormat);
        return this;
    };
    /**
     * Sets the minimum date the [PMUI.control.DateTimeControl] can accept as a selectable one.
     * You can see more about here [PMUI.control.DateTimeControl] in method
     * {@link PMUI.control.DateTimeControl#setMinDate setMinDate}
     */
    DateTimeField.prototype.setMinDate = function (date) {
        this.controls[0].setMinDate(date);
        return this;
    };
    /**
     * Sets the max date the [PMUI.control.DateTimeControl] can accept as a selectable one.
     * You can see more about here in the class [PMUI.control.DateTimeControl] in method
     {@link PMUI.control.DateTimeControl#setMaxDate setMaxDate}
     */
    DateTimeField.prototype.setMaxDate = function (date) {
        this.controls[0].setMaxDate(date);
        return this;
    };
    /**
     * Sets the months the [PMUI.control.DateTimeControl] names/shortnames to be used by the calendar.
     * @param {Object} months An object with the same structure that the [PMUI.control.DateTimeControl]
     * method {@link PMUI.control.DateTimeControl#setMonths setMonths}.
     * @chainable
     */
    DateTimeField.prototype.setMonths = function (months) {
        this.controls[0].setMonths(months);
        return this;
    };
    /**
     * Sets the [PMUI.control.DateTimeControl] name/shortnames for the days to be used in the calendar.
     * @param {Object} days A JSON object with the same structure than the [PMUI.control.DateTimeControl]
     * method {@link PMUI.control.DateTimeControl#setDays setDays}
     */
    DateTimeField.prototype.setDays = function (days) {
        this.controls[0].setDays(days);
        return this;
    };
    /**
     * Returns the formated date.
     * @return {String}
     */
    DateTimeField.prototype.getFormatedDate = function () {
        return this.controls[0].getFormatedDate();
    };
    /**
     * Returns the field's value.
     * @param  {String} [format] Specifies the date format that will be used for the returning date. If it isn't
     * specified, the {@link #property-returnFormat returnFormat} will be used instead.
     *
     * The valid values are:
     *
     * - "UTC", returns a date in the format: yyyy-mm-ddTHH:ii:ss-HH:mm (i.e. 2013-08-31T00:08:00+04:00).
     * - "@" or "timestamp", returns a date in timestamp format.
     * - [any other valid format string], will return the date using the string as the dateformat. This string can
     * contain any of the wilcards specified in the
     * {@link PMUI.control.DateTimeControl#cfg-dateFormat dateFormat config option}.
     * @return {String}
     */
    DateTimeField.prototype.getValue = function (format) {
        return this.controls[0].getValue(format || this.returnFormat);
    };
    /**
     * Sets the controls for the field.
     * Since this is an abstract method, it must be implemented in its non-abstract subclasses
     * @abstract
     * @private
     */
    DateTimeField.prototype.setControls = function () {
        if (this.controls.length) {
            return this;
        }
        this.controls.push(new PMUI.control.DateTimeControl());
        return this;
    };
    /**
     * Updates the current value from the controls which are part of the field
     * @chainable
     */
    DateTimeField.prototype.updateValueFromControls = function () {
        this.value = this.controls[0].getValue(this.returnFormat);
        this.data.setValue(this.value);
        return this;
    };

    PMUI.extendNamespace('PMUI.field.DateTimeField', DateTimeField);

    if (typeof exports !== "undefined") {
        module.exports = DateTimeField;
    }
}());