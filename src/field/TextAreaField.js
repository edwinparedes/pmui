(function () {
    /**
     * @class PMUI.field.TextAreaField
     * @extends PMUI.field.TextField
     * Class to handle a {@link PMUI.control.TextAreaControl}.
     *
     * Usage example:
     *
     *      @example
     *           var a;
     *           $(function() {
     *                a = new PMUI.field.TextAreaField({
     *                     id:'12345',
     *                     label: "coment",
     *                     cols: 300,   
     *                     rows: 200,
     *                     name: 'coments',
     *                     placeholder: 'make your comment here...........',
     *                     required: true,
     *                     labelposition:'left',
     *                     labelVerticalPosition:'top',
     *                     tooltipPosition:'bottom',
     *                     helper: "Introduce a text (200 chars. max.)",
     *                     validators: [
     *                         {
     *                             pmType: 'textLength',
     *                             criteria: {
     *                                 maxLength: 200
     *                             }
     *                         }
     *                     ]
     *                 });
     *                 document.body.appendChild(a.getHTML());
     *             });
     *
     *
     * TextAreaField, is a field for very long text strings
     * to generate the above code on TextAreaField requires, one id, label, name, placeholder, required,
     labelposition,
     * labelVerticalPosition, tooltipPosition, helper.
     *
     * In case to modified label position or tooltip position it can contain the next properties:
     * @cfg {'string'} [labelVerticalPosition='top'] in case that the position of the label left or right we
     fixed this
     * property in three cases ('top', 'center', 'bottom')
     * @cfg {'string'} [tooltipPosition='top'] this property can set the tooltip in two cases ('top' or 'bottom')
     for TextAreaField
     */

    var TextAreaField = function (settings) {

        TextAreaField.superclass.call(this, settings);
        /**
         * @property {string} [labelVerticalPosition='top'], in case that the position of the label left
         or right we fixed this
         * property in three cases ('top', 'center', 'bottom')
         * @readonly
         */
        this.labelVerticalPosition = null;
        /**
         * @property {string} [tooltipPosition='top'] this property can set the tooltip in two cases ('top' or
         'bottom').
         * @readonly
         */
        this.tooltipPosition = null;
        this.cols = null;
        this.rows = null;

        TextAreaField.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.field.TextField', TextAreaField);

    TextAreaField.prototype.type = "TextAreaField";

    TextAreaField.prototype.init = function (settings) {
        var defaults = {
            labelVerticalPosition: 'top',
            tooltipPosition: 'top',
            cols: 'auto',
            rows: 'auto'
        };

        jQuery.extend(true, defaults, settings);

        this.setLabelVerticalPosition(defaults.labelVerticalPosition);
        this.setTooltipPosition(defaults.tooltipPosition);
        this.setRows(defaults.rows);
        this.setCols(defaults.cols);

    };

    /**
     * Sets the labelVerticalPosition for the Label of the TextAreaField.
     * input values ​​(top, center and bottom) only affect the field if the label of TextAreaField is in
     * left or right position.
     * @param {String} labelVerticalPosition
     * @chainable
     */

    TextAreaField.prototype.setLabelVerticalPosition = function (value) {
        var x,
            result;

        if (value === 'top' || value === 'bottom' || value === 'center') {
            this.labelVerticalPosition = value;

            if (this.controls[0] && this.html) {
                this.dom.labelTextContainer.style.position = 'relative';
                if (this.labelPosition === 'left' || this.labelPosition === 'right') {
                    if (this.labelVerticalPosition === 'top') {
                        this.dom.labelTextContainer.style.top = '0px';
                        this.dom.labelTextContainer.style.verticalAlign = 'top';
                    }
                    if (this.labelVerticalPosition === 'bottom') {
                        this.dom.labelTextContainer.style.top = '0px';

                        this.dom.labelTextContainer.style.verticalAlign = 'bottom';
                    }
                    if (this.labelVerticalPosition === 'center') {

                        this.dom.labelTextContainer.style.verticalAlign = 'top';
                        result = ($(this.controls[0].getHTML()).outerHeight() / 2) -
                            ($(this.dom.labelTextContainer).outerHeight() / 2);

                        this.dom.labelTextContainer.style.top = (result) + 'px';

                    }
                }
            }
        }
        else {
            throw new Error("The value is not 'top' or 'bottom' please enter these values");
        }

        return this;
    };
    /**
     * Sets the label position
     * input values(top, bottom, left, right)
     * @param {String} labelPosition
     * @chainable
     */
    TextAreaField.prototype.setLabelPosition = function (position) {
        if (this.html && (position === 'top' || position === 'bottom')) {
            this.dom.labelTextContainer.style.top = '0px';
        }
        TextAreaField.superclass.prototype.setLabelPosition.call(this, position);

        return this;
    };
    /**
     * Sets the tooltipPosition for the Label of the TextAreaField.
     * input values ​​(top and bottom) only affect the field if the label of TextAreaField
     * @param {String} tooltipPosition
     * @chainable
     */
    TextAreaField.prototype.setTooltipPosition = function (value) {

        if (value === 'top' || value === 'bottom') {
            this.tooltipPosition = value;

            if (this.controls[0] && this.html) {


                if (this.tooltipPosition === 'top') {
                    this.helper.icon.style.addProperties({'vertical-align': 'top'});
                }
                if (this.tooltipPosition === 'bottom') {

                    this.helper.icon.style.addProperties({'vertical-align': 'bottom'});
                }
            }
        }
        else {
            throw new Error("The value is not 'top' or 'bottom' please enter these values");
        }

        return this;
    };
    /**
     * Sets the  setHeight for the {PMUI.controls.TextAreaControl}.
     * @param {String|number} [PMUI.controls.TextAreaControl]
     * @chainable
     */
    TextAreaField.prototype.setControlHeight = function (height) {
        this.controls[0].setHeight(height);
        return this;
    };
    /**
     * Sets the  setWidth for the {PMUI.controls.TextAreaControl}.
     * @param {String|number} [PMUI.controls.TextAreaControl]
     * @chainable
     */
    TextAreaField.prototype.setControlWidth = function (width) {
        this.controls[0].setWidth(width);
        return this;
    };

    /**
     * Sets the text for the TextAreaField label.
     * @param {String} label
     */

    TextAreaField.prototype.setLabel = function (label) {
        if (typeof label === 'string') {
            this.label = label;
        } else {
            throw new Error("The setLabel() method only accepts string values!");
        }
        if (this.dom.fieldTextLabel) {
            this.dom.fieldTextLabel.textContent = this.label;
        }
        if (this.dom.fieldRequired) {
            this.dom.fieldRequired.textContent = '*';
            this.setRequired(this.required);
        }
        if (this.dom.fieldColon) {
            this.dom.fieldColon.textContent = ":";
        }
        return this;
    };
    /**
     * Sets the control for the TextField
     * @chainable
     * @private
     */
    TextAreaField.prototype.setControls = function () {
        if (this.controls.length) {
            return this;
        }

        this.controls.push(new PMUI.control.TextAreaControl());

        return this;
    };

    /**
     * Disables the field
     * @param {Boolean} Disable, if the field, then control is activated, if the control is enabled
     * @chainable
     */
    TextAreaField.prototype.disableField = function () {
        this.controls[0].disable(true);
        return this;
    };
    /**
     * Disables/enables the control
     * @param {Boolean} Enable, if the field, then control is activated, if the control is diabled
     * @chainable
     */
    TextAreaField.prototype.enableField = function () {
        this.controls[0].disable(false);
        return this;
    };
    /**
     * Creates the HTML element for the textAreaField
     * @return {HTMLElement}
     */
    TextAreaField.prototype.createHTML = function () {
        TextAreaField.superclass.prototype.createHTML.call(this);
        this.setLabelVerticalPosition(this.labelVerticalPosition);
        this.setTooltipPosition(this.tooltipPosition);
        return this.html;
    };
    /**
     * assign hight as rows to textArea,
     * @param {number|String} rows will be assigned as high in PX, the entered values ​​can
     * be numbers, 'number + px', 'auto' or percentage
     * @chainable
     */
    TextAreaField.prototype.setRows = function (rows) {
        this.controls[0].setHeight(rows);
        return this;
    };
    /**
     * assign width as Cols to textArea,
     * @param {number|String} Cols will be assigned as high in PX, the entered values ​​can
     * be numbers, 'number + px', 'auto' or percentage
     * @chainable
     */
    TextAreaField.prototype.setCols = function (cols) {
        this.controls[0].setWidth(cols);
        return this;
    };

    PMUI.extendNamespace('PMUI.field.TextAreaField', TextAreaField);

    if (typeof exports !== "undefined") {
        module.exports = TextAreaField;
    }
}());