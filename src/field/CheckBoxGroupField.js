(function () {
    /**
     * @class  PMUI.field.CheckBoxGroupField
     * Field where the values ​​can be selected from a group of options.
     * @extends PMUI.form.Field
     *
     *  Usage example:
     *
     *      @example
     *             var a;
     *             $(function() {
     *                 a = new PMUI.field.CheckBoxGroupField({
     *                     label: "development tools",
     *                     controlPositioning: 'vertical',
     *                     maxDirectionOptions: 2,
     *                     required: true,
     *                     value: '1',
     *                     helper: "what lanaguages you know?. Select others besides javascript",
     *                     options : [
     *                         {  label: 'Java',
     *                            disabled: false,
     *                            value: 'java',
     *                            selected :true
     *                         },
     *                         { label: 'JavaScript',
     *                            disabled: true,
     *                            value: 'javaScript',
     *                            selected :true
     *                         },
     *                         { label: 'Python',
     *                            disabled: false,
     *                            value: 'python'
     *                         },
     *                          {  label: 'C#',
     *                            disabled: true,
     *                            value: 'visualbasic'
     *                         }
     *                     ],
     *                      onChange: function(newVal, oldVal) {
     *                         console.log("The value for the field  \"" + this.getLabel() + 
     *                         "\": has change from \"" + oldVal + "\" to \"" + newVal + "\"");
     *                     },
     *                 });
     *                 document.body.appendChild(a.getHTML());
     *             });
     *
     *
     * The example above will generate a field with 4 CheckBox options.
     *
     * @constructor Creates an instance of the class CheckBoxGroupField.
     * @param {Object} options Initialization options.
     * @cfg {Array} [options=[]] An array in which every element is a JSON object with the same
     structure required in the
     * {@link PMUI.field.CheckBoxGroupField#addOption addOption() method}.
     * @cfg {String} [controlPositioning="vertical"] A string that determines the direction for the options to be added
     * in the field. This string can have one of the following values:
     *
     * - "horizontal" (default), in this case all the options will be included  in horizontal order.
     * - "vertical", in this case all the options will be included in vertical order.
     * @cfg {Number} [maxDirectionOptions=1] The maximum number of options to be added in the current direction
     * (set by the {@link PMUI.control.CheckBoxGroupField#cfg-controlPositioning controlPositioning config option}).
     *
     * @cfg {String} [value='[]'] The value that determines which options will be checked. the String must have an
     * array format in which each element is the value of the options that are wanted to be checked.
     */
    var CheckBoxGroupField = function (settings) {
        CheckBoxGroupField.superclass.call(this, settings);
        /**
         * @property {String} controlPositioning The direction for the options to be added in the field.
         * @readonly
         */
        this.controlPositioning = null;
        /**
         * @property {Number} maxDirectionOptions The max number of options that can be in the current options
         * direction.
         * @readonly
         */
        this.maxDirectionOptions = null;
        /**
         * The status if the controls to be saved when the field is switch between enabled/disabled.
         * @type {Object}
         * @private
         */
        this.auxControlStates = {};

        this.controlTable = {};
        CheckBoxGroupField.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.form.Field', CheckBoxGroupField);

    CheckBoxGroupField.prototype.type = "CheckBoxGroupField";

    CheckBoxGroupField.prototype.init = function (settings) {
        var defaults = {
            options: [],
            controlPositioning: "vertical",
            maxDirectionOptions: 1,
            value: '[]'
        };

        jQuery.extend(true, defaults, settings);
        this.setOptions(defaults.options)
            .setMaxDirectionOptions(defaults.maxDirectionOptions)
            .setControlPositioning(defaults.controlPositioning);
    };
    /**
     * Enables the field. Notice that the controls that initially were disabled will continue being disabled.
     * @chainable
     */
    CheckBoxGroupField.prototype.enable = function () {
        var key,
            i,
            controlsLength = this.controls.length,
            controls = this.controls;
        CheckBoxGroupField.superclass.prototype.enable.call(this);
        this.disabled = false;
        for (i = 0; i < controlsLength; i += 1) {
            controls[i].disable(this.auxControlStates[controls[i].id]);
        }
        return this;
    };
    /**
     * Sets the max number of controls that can be in the current direction (the direction is set by the
     * CheckBoxGroupField's
     * {@link PMUI.field.CheckBoxGroupField#setControlPositioning setControlPositioning() method}).
     * @param {Number} max It should be an integer, otherwise it will be floor rounded. If the value is equal or minor
     * than 0 it means there's no limit.
     */
    CheckBoxGroupField.prototype.setMaxDirectionOptions = function (max) {
        if (typeof max === 'number') {
            this.maxDirectionOptions = Math.floor(max);
            if (this.html) {
                this.setControlPositioning(this.controlPositioning);
            }
        } else {
            throw new Error("setMaxDirectionOptions(): it only accepts number values.");
        }

        return this;
    };
    /**
     * It overrides the Field's {@link PMUI.form.Field#setControlPositioning setControlPositioning() method}.
     * This new implementation sets the direction for the control addition: horizontal or vertical.
     * @param {String} positioning It can be "horizontal" or "vertical".
     */
    CheckBoxGroupField.prototype.setControlPositioning = function (positioning) {
        var errorMessage = "The setControlPositioning() method only accepts \"horizontal\" or \"vertical\" as value.",
            table,
            tbody,
            cell,
            row,
            i,
            column,
            rowIndex;
        if (typeof positioning === 'string') {
            if (!(positioning === 'horizontal' || positioning === 'vertical')) {
                return this;
            }
            this.controlPositioning = positioning;
            if (this.html && this.controls) {
                for (i = 0; i < this.controls.length; i += 1) {
                    jQuery(this.controls[i].getHTML()).detach();
                }
                $(this.dom.controlContainer).empty();
                table = PMUI.createHTMLElement("table");
                table.className = 'pmui-field-control-table';
                tbody = PMUI.createHTMLElement("tbody");
                if (positioning === 'horizontal') {
                    row = PMUI.createHTMLElement("tr");
                    for (i = 0; i < this.controls.length; i += 1) {
                        cell = PMUI.createHTMLElement('td');
                        this.controls[i].getHTML();
                        this.controls[i].control.tabIndex = i;
                        cell.appendChild(this.controls[i].getHTML());
                        row.appendChild(cell);
                        if (this.maxDirectionOptions > 0 && (i + 1) % this.maxDirectionOptions === 0) {
                            tbody.appendChild(row);
                            row = PMUI.createHTMLElement("tr");
                        }
                    }
                    tbody.appendChild(row);
                } else {
                    column = 0;
                    for (i = 0; i < this.controls.length; i += 1) {
                        cell = PMUI.createHTMLElement('td');
                        this.controls[i].getHTML();
                        this.controls[i].control.tabIndex = i;
                        cell.appendChild(this.controls[i].getHTML());
                        rowIndex = this.maxDirectionOptions === 0 ? i : i % this.maxDirectionOptions;

                        row = jQuery(tbody).find('tr').eq(rowIndex).get(0);
                        if (!row) {
                            row = PMUI.createHTMLElement('tr');
                            tbody.appendChild(row);
                        }
                        row.appendChild(cell);
                    }
                }
                this.controlTable = table;
                table.appendChild(tbody);
                this.dom.controlContainer.appendChild(table);
                this.dom.controlContainer.appendChild(this.helper.getHTML());
            }
        }

        return this;
    };
    /**
     * Sets all the options for the control for checkbox button.
     * @param {Array} options An array in which every element is a JSON object with the
     same structure required in the
     * {@link PMUI.field.CheckBoxGroupField#addOption addOption() method}.
     * @chainable
     */
    CheckBoxGroupField.prototype.setOptions = function (options) {
        var i = 0;

        this.value = [];
        this.value = JSON.stringify(this.value);

        if (jQuery.isArray(options)) {
            for (i = 0; i < options.length; i += 1) {
                this.addOption(options[i]);
            }
        } else {
            throw new Error("setOptions(): the supplied argument must be an array.");
        }

        return this;
    };
    /**
     * Adds a new option to the CheckBoxGroupField.
     * @param {Object} option A JSON object, which can have the same properties that the config options
     * for the {@link PMUI.control.SelectableControl SelectableControl class} except by "mode",
     it will always be override by the value "checkBox".
     * @chainable
     */
    CheckBoxGroupField.prototype.addOption = function (option) {
        var newOption;

        newOption = new PMUI.control.SelectableControl(jQuery.extend(true, option, {
            mode: 'checkbox',
            name: this.controls.length + 1,
            selected: option.selected
        }));

        if (newOption.isSelected()) {
            this.value = JSON.parse(this.value);
            this.value.push(newOption.getValue());
            this.value = JSON.stringify(this.value);
        }
        if (this.eventsDefined) {
            newOption.setOnChangeHandler(this.onChangeHandler()).getHTML();
            newOption.setOnBeforeChange(this.onBeforeChangeHandler());
            newOption.defineEvents();
        }
        this.auxControlStates[newOption.id] = newOption.disabled;
        this.controls.push(newOption);
        this.setControlPositioning(this.controlPositioning);

        return this;
    };
    /**
     * Removes one or more options (checkBox Button).
     * @param  {String|Number|PMUI.control.SelectableControl} option It can be:
     *
     *  - A number, in that case the parameter is used as the index of the item to be removed.
     *  - A String, in that case the parameter is used as the item's value,
     *  that means that everyone item that has that value will be removed.
     *  - An instance of {@link PMUI.control.SelectableControl SelectableControl} which must be a control of the field.
     * @chainable
     */
    CheckBoxGroupField.prototype.removeOption = function (item) {
        var itemToRemove,
            i;
        if (item instanceof PMUI.control.SelectableControl) {
            for (i = 0; i < this.controls.length; i += 1) {
                if (this.controls[i] === item) {
                    itemToRemove = i;
                    break;
                }
            }
        } else if (typeof item === 'string') {
            for (i = 0; i < this.controls.length; i += 1) {
                if (this.controls[i].id === item) {
                    itemToRemove = this.controls[i];
                    break;
                }
            }
        } else {
            itemToRemove = item;
        }
        if (typeof itemToRemove === 'number') {
            itemToRemove = this.controls[itemToRemove];
            delete this.auxControlStates[itemToRemove.id];
            jQuery(itemToRemove.html).detach();
            this.controls.splice(itemToRemove, 1);
            this.setControlPositioning(this.controlPositioning);
        }
        return this;
    };
    /**
     * It clears all the options from the field.
     * @chainable
     */
    CheckBoxGroupField.prototype.clearOptions = function () {
        while (this.controls.length) {
            this.removeOption(0);
        }

        return this;
    };
    /**
     * Update the field's value property from the controls
     * the parameter value is a clean array;
     * @protected
     * @chainable
     */
    CheckBoxGroupField.prototype.updateValueFromControls = function () {
        var value = [], i;

        for (i = 0; i < this.controls.length; i += 1) {
            if (this.controls[i].isSelected()) {
                value.push(this.controls[i].getValue());
            }
        }

        this.value = JSON.stringify(value);
        this.data.setValue(this.value);

        return this;
    };
    /**
     * Sets the value to the field's controls.
     * @protected
     * @param {String} value
     * @chainable
     */
    CheckBoxGroupField.prototype.setValueToControls = function (value) {
        var i, j, values, controls, controlsLength;

        try {
            value = value.replace(/'/g, "\"");
            values = JSON.parse(value);
        } catch (e) {
            values = '[]';
        }

        controlsLength = (controls = this.controls.slice(0)).length;
        for (i = 0; i < controlsLength; i += 1) {
            controls[i].deselect();
        }
        for (i = 0; i < values.length; i += 1) {
            for (j = 0; j < controlsLength; j += 1) {
                if (controls[j].getValue() === values[i]) {
                    controls[j].select();
                    controls.splice(j, 1);
                    controlsLength -= 1;
                    j -= 1;
                }
            }
        }
        return this;
    };
    /**
     * Evaluates the required validation.
     * @return {Boolean} Returns true if the validation passes otherwise returns false.
     */
    CheckBoxGroupField.prototype.evalRequired = function () {
        var valid = true, value;
        value = JSON.parse(this.getValue());
        if (this.required && value.length === 0) {
            this.showMessage(this.requiredMessage, "error");
            valid = false;
        } else {
            this.hideMessage();
        }
        return valid;
    };

    CheckBoxGroupField.prototype.isValid = function () {
        var valid = true,
            validator;

        valid = valid && this.evalRequired();
        if (!valid) {
            $(this.controlTable).addClass('error');
            return valid;
        }
        $(this.controlTable).removeClass('error');
        for (validator in this.validators) {
            if (this.validators.hasOwnProperty(validator)) {
                valid = valid && this.validators[validator].isValid();
                if (!valid) {
                    this.message.setText(this.validators[validator].errorMessage);
                    return valid;
                }
            }
        }
        return valid;
    };
    /**
     * @inheritdoc
     */
    CheckBoxGroupField.prototype.getValue = function (format) {
        var castFormat = format || this.valueType,
            controls = this.controls,
            i,
            res = [];

        for (i = 0; i < controls.length; i += 1) {
            if (controls[i].isSelected()) {
                res.push(PMUI.castValue(controls[i].getValue(), castFormat));
            }
        }
        return JSON.stringify(res);
    };
    /**
     * Disable the option
     * @param {number|string} disable If the value is part of group CheckBox
     * @chainable
     */
    CheckBoxGroupField.prototype.disableOption = function (value) {
        var i;
        for (i = 0; i < this.controls.length; i += 1) {
            if (this.controls[i].value == value || this.controls[i].label == value || value == i) {
                this.controls[i].disable(true);
                return this;
            }
        }
        throw new Error('the value send is not part of group CheckBox');
    };
    /**
     * Enable the option
     * @param {number|string} disable If the value is part of group CheckBox
     * Enables a field's option.
     */
    CheckBoxGroupField.prototype.enableOption = function (value) {
        var i;
        for (i = 0; i < this.controls.length; i += 1) {
            if (this.controls[i].value == value || this.controls[i].label == value || value == i) {
                this.controls[i].disable(false);
                return this;
            }
        }
        throw new Error('the value send is not part of group CheckBox');
    };

    CheckBoxGroupField.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }
        CheckBoxGroupField.superclass.prototype.createHTML.call(this)
        if (this.disabled) {
            this.disable();
        }
        return this.html;
    };

    CheckBoxGroupField.prototype.checkedOption = function (option) {
        var i;
        for (i = 0; i < this.controls.length; i += 1) {
            if (option === i || option === this.controls[i].label || option === this.controls[i].value) {
                if (!this.controls[i].selected) {
                    this.controls[i].select();
                } else {
                    this.controls[i].deselect();
                }
                return this;
            }
        }
        return this;
    };

    CheckBoxGroupField.prototype.defineEvents = function () {
        var that = this;
        CheckBoxGroupField.superclass.prototype.defineEvents.call(this);
        this.addEvent('click').listen(this.dom.labelTextContainer, function (e) {
            if (that.controls.length) {
                $(that.controls[0].html).find('input')[0].focus();
            }
        });
        return this;
    };
    /**
     * @method getOptions
     * @ obtains under each option and configuration
     * @chainable
     */
    CheckBoxGroupField.prototype.getOptions = function () {
        var i,
            options = [],
            option;

        for (i = 0; i < this.controls.length; i += 1) {
            option = {};
            option["label"] = this.controls[i].label;
            option["value"] = this.controls[i].value;
            option["disabled"] = this.controls[i].disabled;
            option["selected"] = this.controls[i].selected;
            options.push(option);
        }
        return options;
    };

    CheckBoxGroupField.prototype.getValueFromControls = function () {
        var value = [],
            i;

        for (i = 0; i < this.controls.length; i += 1) {
            if (this.controls[i].isSelected()) {
                value.push(this.controls[i].getValue());
            }
        }

        this.value = JSON.stringify(value);
        this.data.setValue(this.value);
        return this.value;
    };
    PMUI.extendNamespace('PMUI.field.CheckBoxGroupField', CheckBoxGroupField);

    if (typeof exports !== "undefined") {
        module.exports = CheckBoxGroupField;
    }
}());