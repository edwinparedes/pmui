(function () {
    /**
     * @class PMUI.field.DropDownListField
     * @extends PMUI.form.Field
     * Class to handle a {@link PMUI.control.DropDownListControl} field.
     *
     * Usage example:
     *
     *      @example
     *              var settings, list;
     *              $(function(){
     *                  settings = {
     *                   id : '123',
     *                  label : "Pais",
     *                  name : 'List Select',
     *                  required: true,
     *                  helper: "selected one opcion of the list",
     *                  options: [                                
     *                  {
     *                   label: "BOLIVIA",
     *                        options: [
     *                        {
     *                              label: "La Paz",
     *                              value: 1,
     *                              disabled : true
     *                          },
     *                          {
     *                              label: "Cochabamba",
     *                              value: 2
     *                          },
     *                          {
     *                              label: "SantaCruz",
     *                              value: 3
     *                          },
     *                      ]
     *                   },
     *                   {
     *                        label: "ARGENTINA",
     *                      options: [
     *                          {
     *                              label: "Buenos Aires",
     *                              value: 4
     *                          },
     *                          {
     *                              label: "Santa Fe",
     *                              value: 5
     *                          },
     *                          {
     *                                  value: "Cordoba"
     *                          }
     *                      ]
     *                   },
     *                   {
     *                        label: "CHILE",
     *                      options: [
     *                          {
     *                              label: "Arica",
     *                              value: 6
     *                          },
     *                          {
     *                              label: "Santiago",
     *                              value: 7
     *                          },
     *                          {
     *                                  value: "Concepcion"
     *                          }
     *                      ]
     *                   },
     *                   {
     *                        label: "New York",
     *                        value: 8
     *                   },
     *                   {
     *                        label: "Mexico D.F.",
     *                        value: 9
     *                   }
     *               ]
     *           }
     *           list = new PMUI.field.DropDownListField(settings);
     *           document.body.appendChild(list.getHTML());
     *         });
     *
     *
     * DropDownListField typo is a selection field, enclose the values â€‹â€‹that we can choose from a list
     * of options. The attributes that accompany the opening tag are:
     * The code above will generate a DropDownListField, required one id, name, requiere,
     * helper, text  for list the options, one opcion array  with the list options
     *
     * @constructor Creates an instance of the class DropDownListField.
     * @param {Object} options Initialization options.
     * @cfg {Array} [options=[]] An array with all the options to be contained by the
     {PMUI.control.DropDownListControl}.
     *
     * Each element in the array is a JSON object, this JSON object can represent an option group
     * or an option item.
     *
     * In case to represent an option item it can contain the next properties:
     *
     * - value {String} (required): the value for the option.
     * - label {String} (optional): the label for the option, if isn't specified the value is used instead.
     * - selected {Boolean} (optional): if the option is selected. #Note. If the configuration object has the
     * "value" propery set then this "selected" property will be
     * - disabled {Boolean} (optional): if the option is disabled or not.
     *
     * On the other hand, in case to represent an option group, it can contain the next properties:
     *
     * - label {String} (required): The name for the option group.
     * - disabled {Boolean} (optional): If the group is disabled or not.
     * - options {Array} (required): An array in which each element is a JSON object representing an option item,
     * so every item must have the structure explained above (for represent option items). #Note. This propery makes
     * the difference between an option and a option group. If the "options" property is not specified or if it isn't
     * an array then it will treated like a option item.
     * @cfg {String|Number} [listWidth='auto'] This value sets the width of the list
     */


    var DropDownListField = function (settings) {

        DropDownListField.superclass.call(this, settings);

        /**
         * @property {Boolean} [disabled=false] If the field is disabled or not.
         * @readonly
         */

        this.listWidth = null;
        //this.defaultValue = null;

        DropDownListField.prototype.init.call(this, settings);

    };

    PMUI.inheritFrom("PMUI.form.Field", DropDownListField);

    DropDownListField.prototype.type = "DropDownListField";

    DropDownListField.prototype.init = function (settings) {
        var defaults = {
            options: [],
            listWidth: 'auto',
            value: null
        };

        jQuery.extend(true, defaults, settings);

        this.setOptions(defaults.options);
        this.setListWidth(defaults.listWidth);
        this.updateValueFromControls();

        if (defaults.value !== null) {
            this.setValue(defaults.value);
        }
    };
    /**
     * Sets the options/option groups for the control.
     * @param {Array} options An array with the same structure that the
     * {@link PMUI.field.DropDownListField#cfg-options "options"} property in the
     * Config settings section.
     */
    DropDownListField.prototype.setOptions = function (options) {
        var i;
        if (jQuery.isArray(options)) {
            this.controls[0].setOptions(options);
        }
        if (!this.value && options) {
            for (i = 0; i < options.length; i += 1) {
                if (options[i].selected) {
                    this.initialValue = options[i].value || options[i].label || "";
                    break;
                }
            }
        }
        this.value = this.controls[0].getValue();
        return this;
    };
    /**
     * Returns the options/option groups from the field
     * @param  {Boolean} [includeGroups=false] If it's evaluated as true then it will include
     * the option groups with its child elements, otherwise it will return only the option items.
     * @return {Array}
     *
     *         example
     *
     *          list.getOptions(false)
     *         [La Paz][Cochabamba][Santa Cruz][Buenos Aires][Santa Fe][Cordoba][Santiago][.][.][Mexico D.F]
     *
     *        list.getOptions(true)
     *          [BOLIVIA
     *              [La Paz][Cochabamba][SantaCruz]
     *          ]
     *          [ARGENTINA
     *              [Buenos Aires][Santa Fe][Cordoba]
     *           ]
     *          [CHILE
     *             [x][y][z]
     *          ]
     *          [New York]
     *          [Mexico D.F.]
     *
     * @return {Array}
     */
    DropDownListField.prototype.getOptions = function (includeGroups) {
        return this.controls[0].getOptions(includeGroups);
    };
    /**
     * Clear all the options from the control.
     * @chainable
     */
    DropDownListField.prototype.clearOptions = function () {
        this.controls[0].clearOptions();
        this.value = this.controls[0].value;
        return this;
    };

    /**
     * Disables one or more options/option groups.
     * @param  {String|Number|Object} option It can be a string, a number or a JSON object.
     *
     * - In case to be a String, it will be disabled the options that match the string in its value and the option
     * groups which match the string in its label. In this case more than one single item can be disabled.
     * - In case to be a Number, it will be disabled the option/option group which index position matches the number.
     * Obviously, in this case only one item will be disabled.
     * - In case to be an object you can specify if the change will be applied only to options or option groups,
     * it should have two properties:
     *     - criteria {String}: The value (in case of options) or the label (in case of option groups) the items
     needs
     *     to match for apply the changes.
     *     - applyTo {String} (optional), it can take the following values:
     *         - "groups", the change will be applied only to the option groups.
     *         - "options", the change will be applied only to the options (direct child of the object).
     *         - [any other string value], the default value, it indicates that the change will be applied to both
     *         options/option groups that matches the criteria in its value/label respectly.
     *
     * @param  {String} [group] It it is specified must be an String making reference to an existing option group
     label.
     * Using this parameter, the elements to be match by the first parameter will be search only in the option groups
     * that match this parameter in its label.
     *
     *         example
     *              list =    [a
     *                             [a0]
     *                             [a1]
     *                        ]
     *                        [b
     *                             [b0]
     *                             [b1]
     *                        ]
     *                        [c]
     *   //disabling sending a number
     *              list.disableOption(0); -->disabled group a
     *              list.disableOption(4); -->disabled option b[b0]
     *
     *    //disabling sending a string
     *             list.disableOption('a') --> disabled group a
     *             list.disableOption('b0') --> disabled option b[b0]

     *   //disabling sending a objects
     list.disableOption({criteria:'a', applyTo:'groups'}) --> disabled group a
     *
     * @chainable
     */

    DropDownListField.prototype.disableOption = function (option, group) {
        this.controls[0].disableOption(option, group);
        return this;
    };

    /**
     * Enables one or more options/option groups.
     * @param  {String|Number|Object} option It can be a string, a number or a JSON object.
     *
     * - In case to be a String, it will be enabled the options that match the string in its value and the option
     * groups which match the string in its label. In this case more than one single item can be enabled.
     * - In case to be a Number, it will be enabled the option/option group which index position matches the number.
     * Obviously, in this case only one item will be enabled.
     * - In case to be an object you can specify if the change will be applied only to options or option groups,
     * it should have two properties:
     *     - criteria {String}: The value (in case of options) or the label (in case of option groups) the items needs
     *     to match for apply the changes.
     *     - applyTo {String} (optional), it can take the following values:
     *         - "groups", the change will be applied only to the option groups.
     *         - "options", the change will be applied only to the options (direct child of the object).
     *         - [any other string value], the default value, it indicates that the change will be applied to both
     *         options/option groups that matches the criteria in its value/label respectly.
     *
     * @param  {String} [group] It it is specified must be an String making reference to an existing option group
     label.
     * Using this parameter, the elements to be match by the first parameter will be search only in the option groups
     * that match this parameter in its label.
     * @chainable
     */

    DropDownListField.prototype.enableOption = function (option, group) {
        this.controls[0].enableOption(option, group);
        return this;
    };

    /**
     * Removes one or more option/option groups.
     * @param  {String|Number|Object} option It can be a string, a number or a JSON object.
     *
     * - In case to be a String, it will be removed the options that match the string in its value and the option
     * groups which match the string in its label. In this case more than one single item can be removed.
     * - In case to be a Number, it will be removed the option/option group which index position matches the number.
     * Obviously, in this case only one item will be removed.
     * - In case to be an object you can specify if the change will be applied only to options or option groups,
     * it should have two properties:
     *     - criteria {String}: The value (in case of options) or the label (in case of option groups) the items
     needs
     *     to match for apply the changes.
     *     - applyTo {String} (optional), it can take the following values:
     *         - "groups", the change will be applied only to the option groups.
     *         - "options", the change will be applied only to the options (direct child of the object).
     *         - [any other string value], the default value, it indicates that the change will be applied to both
     *         options/option groups that matches the criteria in its value/label respectly.
     *
     * @param  {String} [group] It it is specified must be an String making reference to an existing option group
     label.
     * Using this parameter, the elements to be match by the first parameter will be search only in the option groups
     * that match this parameter in its label.
     *
     * ##Note. Removing an option group implies removing all its child options.
     * @chainable
     */
    DropDownListField.prototype.removeOption = function (option, group) {
        this.controls[0].removeOption(option, group);
        return this;
    };
    /**
     * Adds a new option group to the Field
     * @param {Object} optionGroup A JSON object with the following properties:
     *
     * - label {String} (required): the label for the option group.
     * - disabled {Boolean}(optional): if the option group will be disabled or not.
     * it defaults to false.
     * - options {Array} (optional): An array of JSON object, each one represents an option and
     * should have the same structure than the "option" paremeter for the
     * {@link PMUI.field.DropDownListField#addOption addOption() method}.
     */
    DropDownListField.prototype.addOptionGroup = function (optionGroup) {
        this.controls[0].addOptionGroup(optionGroup);
        return this;
    };

    /**
     * Adds a new option to the list of the DropDownListField or to an option group.
     * @param {Object} option An object with ther settings for the new option.
     * this object can have the following properties:
     *
     * - value {String} (required): the value for the option.
     * - label {String} (optional): the label for the option, if isn't specified the value is used instead.
     * - selected {Boolean} (optional): if the option is selected. #Note. If the configuration object has the
     * "value" propery set then this "selected" property will be
     * - disabled {Boolean} (optional): if the option is disabled or not.
     *
     * @param {String} group The name of the option group in which the new option will be added. If it doesn't exist
     * it will be created.
     *
     *         usage
     *         // add option a groupÃ§
     *
     *         list.addOption({value:'5', label:'Pando', selected: true, disabled:true },'BOLIVIA')
     *
     *         //or add option
     *
     *         list.addOption({value:'paris', label:'Paris'})
     *
     * @chainable
     */
    DropDownListField.prototype.addOption = function (option, group) {
        this.controls[0].addOption(option, group);
        if (this.getOptions().length == 1) {
            this.value = this.controls[0].value;
        }
        return this;
    };

    /**
     * Returns the label from the option currently selected.
     * @return {String}
     */
    DropDownListField.prototype.getSelectedLabel = function () {
        return this.controls[0].getSelectedLabel();
    };
    /**
     * Sets the control for the DropDownListField
     * @chainable
     * @private
     */
    DropDownListField.prototype.setControls = function () {
        if (this.controls.length) {
            return this;
        }
        this.controls.push(new PMUI.control.DropDownListControl());

        return this;
    };
    /**
     * Set the width for the HTML DropDownListControl(select)
     * @param {Number|String} width height it can be a number or a string.
     In case of using a String you only can use 'auto' or 'inherit' or ##px or ##% or ##em when ## is a number
     * @chainable
     */

    DropDownListField.prototype.setListWidth = function (width) {
        this.listWidth = width;
        this.controls[0].setWidth(this.listWidth);
        return this;
    };


    DropDownListField.prototype.getListWidth = function (width) {
        return this.listWidth;
    };

    DropDownListField.prototype.setValue = function (value) {
        var val;
        if (this.controls[0]) {
            val = this.controls[0].setValue(value).getValue();
        }
        DropDownListField.superclass.prototype.setValue.call(this, val || value);
        return this;
    };

    PMUI.extendNamespace('PMUI.field.DropDownListField', DropDownListField);

    if (typeof exports !== "undefined") {
        module.exports = DropDownListField;
    }
}());