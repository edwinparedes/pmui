(function () {
    /**
     * @class PMUI.field.OptionsSelectorField
     * @extends PMUI.form.Field
     *
     * How use the class.
     * The component represents basically two types of known controls, those are the radio group and check group fields.
     * By default the component has the functionality to the radio group field with a better look and feel.
     *
     * The example below shows the parameters (config options) for build an field of options selector (radio group).
     *
     *     @example
     *               OptionsSelectorControl = new PMUI.field.OptionsSelectorField({
     *                   label: "radiogroup",
     *                   items: [
     *                       {
     *                           text: "First option"
     *                       },
     *                       {
     *                           text: "Second option",
     *                           selected: true
     *                       },
     *                       {
     *                           text: "Third option"
     *                       }
     *                   ],
     *                   listeners: {
     *                       select: function (item, event) {
     *                           console.log("selected", item);
     *                       }
     *                  }
     *               });
     *               document.body.appendChild(OptionsSelectorControl.getHTML());
     *               OptionsSelectorControl.defineEvents();
     *
     *
     * The otherwise is posible create a check group field applying the following sentence on the JSON config:
     *
     *          {   ...
     *              multipleSelection: true
     *              ...
     *          }
     *
     * With the above parameter the component will have the behavior of the check group field.
     *
     *     @example
     *               checkboxGroup = new PMUI.field.OptionsSelectorField({
     *                   label: "radiogroup",
     *                   multipleSelection: true,
     *                   items: [
     *                       {
     *                           text: "First option"
     *                       },
     *                       {
     *                           text: "Second option",
     *                           selected: true
     *                       },
     *                       {
     *                           text: "Third option"
     *                       }
     *                   ],
     *                   listeners: {
     *                       select: function (item, event) {
     *                           console.log("selected", item);
     *                       }
     *                  }
     *               });
     *               document.body.appendChild(checkboxGroup.getHTML());
     *               checkboxGroup.defineEvents();
     *
     *
     * @cfg {Array} items Defines the data items as an array objects.
     *
     *              items: [
     *                  {   
     *                      ... 
     *                  },
     *                  {
     *                      text: "Item1",
     *                      selected: true
     *                  },
     *                  {   
     *                      ... 
     *                  }
     *              ]
     *
     * @cfg {Boolean} [multipleSelection=false] Defines wether the component will support the behavior of the check group.
     * @cfg {String} [orientation="horizontal"] Defines the orientation of the component, it means if the orientation is vertical or
     * horizontal
     *
     *
     */
    var OptionsSelectorField = function (settings) {
        OptionsSelectorField.superclass.call(this, settings);
        /**
         * @property {Boolean} [disabled=false] If the field is disabled or not.
         * @readonly
         */

        this.listWidth = null;
        /**
         * @property {String} controlPositioning The direction for the options to be added in the field.
         * @readonly
         */
        this.controlPositioning = null;
        /**
         * @property {String} [separator=","]
         * Defines the separator between every item selected on the field
         */
        this.separator = null;
        OptionsSelectorField.prototype.init.call(this, settings);

    };

    PMUI.inheritFrom("PMUI.form.Field", OptionsSelectorField);

    OptionsSelectorField.prototype.type = "OptionsSelectorField";

    OptionsSelectorField.prototype.init = function (settings) {
        var defaults = {
            options: [],
            listWidth: 'auto',
            separator: ",",
            controlPositioning: "vertical"
        };

        jQuery.extend(true, defaults, settings);

        this.setConfigControl(defaults);
        this.setSeparator(defaults.separator);
        this.setListWidth(defaults.listWidth);
        this.setControlPositioning(defaults.controlPositioning);
    };

    /**
     * Set if the HTML element is visible or not.
     * @param {Boolean} visible
     * @chainable
     */
    OptionsSelectorField.prototype.setVisible = function (visible) {
        var j,
            k;

        visible = !!visible;
        this.visible = visible;

        if (this.controls) {
            for (j = 0; j < this.controls.length; j += 1) {
                this.controls[j].visible = visible;
            }
        }

        if (this.html) {
            this.style.addProperties({"display": this.visible ? this.display : "none"});
            if (this.controls) {
                for (k = 0; k < this.controls.length; k += 1) {
                    this.controls[k].style.addProperties({"display": this.visible ? this.display : "none"});
                }
            }

        }

        return this;
    };
    /**
     * Sets the separator for the item when use the {@link PMUI.field.OptionsSelectorField#setValue setvalue} method
     * or the {@link PMUI.field.OptionsSelectorField#getValue getvalue} method
     * @param {String} type
     * It is a string that represent the separator. For example: '#', '|', ',', ...
     *
     */
    OptionsSelectorField.prototype.setSeparator = function (type) {
        if (typeof type === "string") {
            this.separator = type;
        }
        return this;
    };

    /**
     * Sets the options/option groups for the control.
     * @param {Array} options An array with the same structure that the
     * {@link PMUI.field.OptionsSelectorField#cfg-options "options"} property in the
     * Config settings section.
     */
    OptionsSelectorField.prototype.setOptions = function (options) {
        var i = 0,
            c,
            j,
            that = this,
            controls,
            k;

        if (jQuery.isArray(options)) {
            for (j = 0; j < that.controls.length; j += 1) {
                that.dom.controlContainer.removeChild(that.controls[j].html);
            }
            ;
            that.setConfigControl({
                items: options
            });
            controls = this.controls.slice();
            for (k = 0; k < controls.length; k += 1) {
                if (controls[k] !== null) {
                    if (that.dom.controlContainer) {
                        that.dom.controlContainer.appendChild(controls[k].getHTML());
                        controls[k].defineEvents();
                        controls[k] = null;
                    }
                }
            }
        } else {
            throw new Error("setOptions(): the supplied argument must be an array.");
        }

        return this;
    };
    OptionsSelectorField.prototype.setConfigControl = function (options) {
        var newControl;
        newControl = new PMUI.control.OptionsSelectorControl(options);
        newControl.setField(this);
        this.controls = [];
        this.controls.push(newControl);
        return this;
    };
    OptionsSelectorField.prototype.setControlPositioning = function (positioning) {
        var that = this,
            k;
        if (typeof positioning === 'string') {
            this.controlPositioning = positioning;
            controls = this.controls.slice();
            for (k = 0; k < controls.length; k += 1) {
                if (controls[k] !== null) {
                    if (that.dom.controlContainer) {
                        that.dom.controlContainer.appendChild(controls[k].getHTML());
                        controls[k] = null;
                    }
                }
            }
        }

        return this;
    };
    OptionsSelectorField.prototype.setOnChangeHandler = function () {
        return this;
    };
    /**
     * Sets the value for the field
     * If the parameter is simple string like to
     *
     *     "myoption" || "1"
     *
     * the value will be assigned to all the items.
     *
     * Otherwise the parameter must have the follow structure
     *
     *      "1,2,3" || "first,second,third, ...]"
     *
     * @param {String} value
     */
    OptionsSelectorField.prototype.setValue = function (value) {
        value = value.toString();
        if (typeof value === 'string') {
            controls = this.controls[0];

            this.value = value;
            this.data.setValue(value);
            this.setValueToControls(value);
        } else {
            throw new Error("The setValue() method only accepts string values!");
        }
        return this;
    };
    /**
     * Gets the value for the item
     * @return {[type]} [description]
     */
    OptionsSelectorField.prototype.getValue = function () {
        var controls,
            values,
            i;

        controls = this.controls[0];
        items = this.getItemsSelected();
        if (controls.multipleSelection) {
            values = [];
            for (i = 0; i < items.length; i += 1) {
                values.push(items[i].getValue());
            }
            values.sort();
            values = values.join(this.separator);
        } else {
            values = items[0].getValue();
        }
        this.value = values
        return this.value;
    };
    /**
     * Adds a new item to current options of the OptionsSelector field
     * @param {Boolean} option This is a basic {@link PMUI.control.OptionsSelectorItemControl element} for the field.
     */
    OptionsSelectorField.prototype.addOption = function (option) {
        var newOption,
            control;

        control = this.controls[0];
        if (control.html) {
            control.addItem(jQuery.extend(true, option, {name: this.controls.length + 1}));
        }

        return this;
    };
    /**
     * Gets the options or items of the field
     * @return {Array} options Items of the field
     */
    OptionsSelectorField.prototype.getOptions = function (includeGroups) {
        return this.controls[0].getOptions(includeGroups);
    };
    /**
     * Clear all the options from the control.
     * @chainable
     */
    OptionsSelectorField.prototype.clearOptions = function () {
        this.controls[0].clearOptions();
        return this;
    };
    /**
     * [disableOption description]
     * @param  {[type]} position [description]
     * @return {[type]}          [description]
     */
    OptionsSelectorField.prototype.disableOption = function (position) {
        this.controls[0].disableOption(position, true);
        return this;
    };
    /**
     * [enableOption description]
     * @param  {[type]} position [description]
     * @return {[type]}          [description]
     */
    OptionsSelectorField.prototype.enableOption = function (position) {
        this.controls[0].disableOption(position, false);
        return this;
    };
    /**
     * Removes an option of the field
     * @param  {Number} position Represent the index of the options array
     *
     *  For example if the options array is:
     *
     *      options = [a, b, c, d]
     *
     * And the option 'b' has to be removed, the method will be:
     *
     *      obj.removeOption(2);
     *
     * @return {Object} this
     */
    OptionsSelectorField.prototype.removeOption = function (position) {
        this.controls[0].removeOption(position);
        return this;
    };
    OptionsSelectorField.prototype.getSelectedLabel = function () {
        this.controls[0].getSelectedLabel();
        return this;
    };
    /**
     * Disables the field, the label and the control of the field are disabled
     * @return {Object} this
     */
    OptionsSelectorField.prototype.disable = function () {
        var items,
            i;
        this.disabled = true;
        OptionsSelectorField.superclass.prototype.disable.call(this);
        if (jQuery.isArray(this.controls)) {
            if (this.controls[0]) {
                control = this.controls[0];
                for (i = 0; i < control.items.getSize(); i += 1) {
                    control.disableOption(i, true);
                }
            }
        }
        return this;
    };
    /**
     * This method enable the field if the label and the control of the item are disabled.
     * @return {Object} this
     */
    OptionsSelectorField.prototype.enable = function () {
        var control,
            i;
        this.disabled = false;
        OptionsSelectorField.superclass.prototype.enable.call(this);
        if (jQuery.isArray(this.controls)) {
            if (this.controls[0]) {
                control = this.controls[0];
                for (i = 0; i < control.items.getSize(); i += 1) {
                    control.disableOption(i, false);
                }
            }

        }
        return this;
    };

    OptionsSelectorField.prototype.setListWidth = function (width) {
        var items,
            i;

        if (typeof this.width === "number") {
            this.listWidth = width;
            if (jQuery.isArray(this.controls)) {
                items = this.controls;
                for (i = 0; i < items.length; i += 1) {
                    items[i].setWidth(this.listWidth);
                }
            }
        }
        return this;
    };
    OptionsSelectorField.prototype.getListWidth = function (width) {
        return this.listWidth;
    };
    /**
     * Gets all items selected on the OptionsSelector
     * @return {Array} items Represent all the items selected on the Field
     */
    OptionsSelectorField.prototype.getItemsSelected = function () {
        var items,
            i,
            selected = [];

        if (jQuery.isArray(this.controls)) {
            items = this.controls;
            for (i = 0; i < items.length; i += 1) {
                selected = jQuery.merge(selected, items[i].getItemsSelected());
            }
        }
        return selected;
    };
    /**
     * Sets the orientation for every {@link PMUI.control.OptionsSelectorItemControl element}
     * There are two possibilities:
     *
     * - horizontal, the elements will be aligned horizontally
     * - vertical, the elements will be aligned vertically
     *
     * @param {String} orientation
     * @return {Object} this
     */
    OptionsSelectorField.prototype.setOrientation = function (orientation) {
        var items,
            i;

        if (typeof orientation === "string" && jQuery.isArray(this.controls) === true) {
            items = this.controls;
            for (i = 0; i < items.length; i += 1) {
                items[i].setOrientation(orientation);
            }
        }
        return this;
    };
    OptionsSelectorField.prototype.setLabel = function (label) {

        if (typeof label === 'string') {
            this.label = label;
        } else {
            throw new Error("The setLabel() method only accepts string values!");
        }
        if (this.dom.labelTextContainer) {
            if (label === '[field]' || label === '') {
                this.label = '';
                this.dom.labelTextContainer.textContent = this.label;

            } else {
                this.dom.labelTextContainer.textContent = this.label + (this.colonVisible ? ':' : '');
            }
        }
        return this;
    };

    PMUI.extendNamespace('PMUI.field.OptionsSelectorField', OptionsSelectorField);

    if (typeof exports !== "undefined") {
        module.exports = OptionsSelectorField;
    }

}());