(function () {
    var UploadField = function (settings) {
        UploadField.superclass.call(this, settings);
        UploadField.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.form.Field', UploadField);

    UploadField.prototype.type = 'UploadField';
    UploadField.prototype.family = 'field';

    UploadField.prototype.init = function (settings) {
        var defaults;
        defaults = {
            accept: "",
            multiple: false
        };

        jQuery.extend(true, defaults, settings);

        this.setAcceptedFiles(defaults.accept)
            .setMultipleFiles(defaults.multiple);
    };

    /**
     * @method setAccepted
     * here we can define the  type  files our send, the valid types are MIME, separation with ","
     * for more information go to http://reference.sitepoint.com/html/mime-types-full
     * @param {String} accept MIME types are valid for our upload
     */
    UploadField.prototype.setAcceptedFiles = function (accept) {
        this.controls[0].setAcceptedFiles(accept);
        return this;
    };

    UploadField.prototype.setMultipleFiles = function (value) {
        this.controls[0].setMultiple(value);
        return this;
    };

    UploadField.prototype.setControls = function () {
        if (this.controls.length) {
            return this;
        }
        this.controls.push(new PMUI.control.UploadControl());
        return this;
    };

    PMUI.extendNamespace('PMUI.field.UploadField', UploadField);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = UploadField;
    }
}());