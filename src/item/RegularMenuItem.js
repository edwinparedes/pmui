(function () {
    var RegularMenuItem = function (settings) {
        this.childMenu = null;
        this.factory = null;
    };

    PMUI.inheritFrom('PMUI.item.MenuItem', RegularMenuItem);

    RegularMenuItem.prototype.init = function (settings) {
        var defaults = {
            items: []
        };

        jQuery.extend(true, defaults, settings);
        this.childMenu = new PMUI.ui.Menu();
        this.setItems(defaults.items);
    };

    RegularMenuItem.prototype.setItems = function (items) {
    };

});