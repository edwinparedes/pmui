(function () {
    /**
     * @class PMUI.core.Container
     * Handles HTML elements that have a container behavior (they can be contain {Element} objects)
     * @abstract
     * @extend PMUI.core.Element
     *  * Usage example (only for subclasses since this is an abstract class):
     *
     *      @example
     *       //Remember, this is an abstract class so it shouldn't be instantiate,
     *       //anyway we are instantiating it just for this example
     *       c = new PMUI.core.Container({
     *            x: 10,
     *            y: 20,
     *            positionMode: "absolute",
     *            width: 300,
     *            height: 100,
     *            style: {
     *                cssProperties: {
     *                    "background-color": "yellow"
     *                },
     *                cssClasses: [".square"]
     *            },
     *            items : [
     *               new PMUI.core.Element({
     *                    y : 0,
     *                    width: 100,
     *                    height: 100,
     *                    style: {
     *                        cssProperties: {
     *                            "background-color": "red"
     *                        },
     *                        cssClasses: [".square"]
     *                    }
     *               }),
     *               new PMUI.core.Element({
     *                    y : 100,
     *                    width: 100,
     *                    height: 100,
     *                    style: {
     *                        cssProperties: {
     *                            "background-color": "yellow"
     *                        },
     *                        cssClasses: [".square"]
     *                    }
     *               })
     *            ]
     *        });
     *        document.body.appendChild(c.getHTML());
     *
     * @constructor
     * Creates a new instance
     * @param {Object} settings
     *
     * @cfg {Array} [items=[]] An array with the items to be added to tyhe container. If the
     * {@link PMUI.core.Container#cfg-dataItems dataItems config option} is  specified then this config option will be
     * ignored.
     *
     * Each array's item can be:
     *
     *  - A JSON object with the config options required by an {@link PMUI.core.Element Element object}.
     *  - An instance of the {@link PMUI.core.Element Element class}.
     *
     * @cfg {String} [behavior='nobehavior'] A string that specifies the behavior for the container and its items.
     * The accepted values are the same ones that the specified ones for the
     * {@link PMUI.core.Container#method-setBehavior setBehavior() method}.
     *
     * @cfg {Array} [dataItems=null] An array in which each element is the data for the items that will be contained by
     * the container. If this config option is specfied the {@link PMUI.core.Container#cfg-items items config option}
     * will be ignored.
     *
     * @cfg {Function} [onDrop=null] The callback function to be executed everytime a draggable item is dropped on the
     * container. This callback can return a boolean value or not return anything. If it returns "false" then the drop
     * action will be cancelled. For info about the parameters sent to the callback function please read the
     * {@link PMUI.core.Container#event-onDrop onDrop} event documentation.
     *
     * @cfg {Function} [onSort=null] The callback function to be executed everytime a container's item order changes.
     * For info about the parameters sent to the callback function please read the
     * {@link PMUI.core.Container#event-onSort onSort} event documentation.
     *
     * @cfg {String} [sortableItems=null] Specifies which items inside the element should be sortable in case a
     * sort behavior is applied. It must be a jQuery selector.
     *
     * @cfg {Boolean} [disabled=false] If the interactive functions like the drag, drop, sort behaviors will be
     * disabled or not.
     *
     * @cfg {Function|null} [onBeforeDrop=null] The callback to be executed before a draggable item that was dropped
     * on the container is added.
     *
     * @cfg {Function|null} [onDropOut=null] The callback to be executed everytime a draggable item is drop out from
     * the container to another one. To know the parameters sent to the callback function read the documentation for
     * the {@link #event-onDropOut onDropOut} event.
     *
     * @cfg {Function|null} [onDragStart=null] The callback function to be executed everytime any of its draggable items
     * starts to be dragged. For info about the parameters sent to the callback function read the documentation for
     * the {@link #event-onDropOut onDropOut} event.
     *
     * @cfg {Function|null} [onAdd=null] The callback function be executed eveytime the {@link #event-onAdd onAdd event}.
     */
    var Container = function (settings) {
        Container.superclass.call(this, settings);
        /**
         * @property {PMUI.util.ArrayList} items
         * An ArrayList object that contains all the child Items
         */
        this.items = null;
        /**
         * @property {PMUI.util.Factory} factory
         * X
         */
        this.factory = null;
        /**
         * @property {PMUI.behavior.ContainerItemBehavior} behaviorObject The container's behavior object.
         * @private
         */
        this.behaviorObject = null;
        /**
         * @property {String} behavior The behavior's pmType being used by the object.
         * @readonly
         */
        this.behavior = null;
        /**
         * @property {HTMLElement} containmentArea The HTML area in which the items HTML will be append
         * @readonly
         */
        this.containmentArea = null;
        /**
         * @event onDragStart
         * Fired when the dragging is starting on any ot its items.
         * @param {PMUI.core.Container} container The container from which the draggable is starting to be dragged.
         * @param {PMUI.core.Element} draggableItem The item thais starting to be dragged.
         */
        this.onDragStart = null;
        /**
         * @event onDrop
         * Fired everytime a draggable item is dropped on the container.
         * @param {PMUI.core.Container} container The container on which the draggable item was dropped.
         * @param {PMUI.core.Element} draggableItem The item that was dropped on the container.
         * @param {Number} index The order index in which the new element was dropped.
         */
        this.onDrop = null;
        /**
         * @event onDropOut
         * Fired when a draggable item is drop out from the container to another one.
         * @param {PMUI.core.Element} draggable The draggable PMUI object involved in the action.
         * @param {PMUI.core.Container} origin The container the draggable item was drag from.
         * @param {PMUI.core.Container} destiny The container the draggable was dropped on.
         */
        this.onDropOut = null;
        /**
         * @event
         * Fired before an item is added to the container by dropping.
         * @param {PMUI.core.Element} draggableItem The container.
         * @param {PMUI.core.Element} draggableItem The item that was dropped and is ready to be appended to the
         * container.
         * @param {Number} index The new element's order index in which it will be added.
         */
        this.onBeforeDrop = null;
        /**
         * @event onSort
         * Fired everytime a container's item order changes.
         * @param {PMUI.core.Container} container The container on which the sort occur.
         * @param {PMUI.core.Element} orderedElement The element which was ordered.
         * @param {Number} index The new element's order index.
         */
        this.onSort = null;
        /**
         * Specifies which items inside the element should be sortable in case a drag/sort behavior is applied.
         * @type {String}
         */
        this.sortableItems = null;
        /**
         * If the container is disabled or not. Set by the {@link #cfg-disabled disabled} config option and the
         * {@link #method-disable disable()} method.
         * @type {Boolean}
         * @readonly
         */
        this.disabled = null;
        /**
         * If the container's behavior is disabled or not. Set by the {@link #cfg-disabledBehavio disabledBehavior} config option and the
         * {@link #method-disableBehavior disableBehavior()} method.
         * @type {Boolean}
         */
        this.disabledBehavior = null;
        /**
         * A flag that indicates if actually a massive action is being performed.
         * @private
         * @type {Boolean}
         */
        this.massiveAction = false;
        /**
         * @event onAdd
         * Fired everytime an item is added to the object.
         * @param {PMUI.core.Container} container The container in which the item was added to.
         * @param {PMUI.core.Element} item The item that was added.
         * @param {Number} index The index in which the item was added.
         */
        this.onAdd = null;
        /**
         * @property {String} dragDropBehaviorScope A draggable/droppable container with the same scope value as
         * another droppable/draggable container's item will be accepted by the behavior.
         */
        this.dragDropBehaviorScope = null;

        this.onSortStart = null;
        this.onStopDrag = null;
        /**
         * Restricts dragging/sorting from starting unless the mousedown occurs on the specified element(s). Only
         * elements that descend from the draggable element are permitted.
         * @type {String}
         */
        this.dragHandler = null;
        Container.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Element', Container);

    Container.prototype.type = 'Container';
    /**
     * Initializes the object.
     * @param  {Object} settings A JSON object with the config options.
     * @private
     */
    Container.prototype.init = function (settings) {
        var defaults = {
            items: [],
            behavior: 'nobehavior',
            dataItems: null,
            onDragStart: null,
            onBeforeDrop: null,
            onDrop: null,
            onDropOut: null,
            onSort: null,
            sortableItems: '> *',
            disabled: false,
            onAdd: null,
            onSortStart: null,
            onStopDrag: null,
            dragDropBehaviorScope: 'pmui-containeritem-behavior',
            dragHandler: null
        };

        jQuery.extend(true, defaults, settings);

        this.items = new PMUI.util.ArrayList();

        this.setFactory(defaults.factory)
            .setOnAddHandler(defaults.onAdd);

        if (jQuery.isArray(defaults.dataItems)) {
            this.setDataItems(defaults.dataItems);
        } else {
            this.setItems(defaults.items);
        }

        this.setSortableItems(defaults.sortableItems)
            .setDragHandler(defaults.dragHandler)
            .setDragDropBehaviorScope(defaults.dragDropBehaviorScope)
            .setBehavior(defaults.behavior)
            .setOnDragStartHandler(defaults.onDragStart)
            .setOnBeforeDropHandler(defaults.onBeforeDrop)
            .setOnDropHandler(defaults.onDrop)
            .setOnDropOutHandler(defaults.onDropOut)
            .setOnSortHandler(defaults.onSort);
        this.setOnSortStartHandler(defaults.onSortStart);
        this.setOnStopDragHandler(defaults.onStopDrag);
        if (this.disabled) {
            this.disable();
        } else {
            this.enable();
        }
    };
    /**
     * If specified, restricts dragging/sorting from starting unless the mousedown occurs on the specified element(s).
     * Only elements that descend from the draggable element are permitted.
     * @param {String} handler The selector for the handler.
     * @chainable
     */
    Container.prototype.setDragHandler = function (handler) {
        if (!(typeof handler === 'string' || handler === null)) {
            throw new Error("The parameter must be a string or null");
        }
        this.dragHandler = handler;
        this.setBehavior(this.behavior);
        return this;
    };
    /**
     * Sets the {@link #property-dragDropBehaviorScope dragDropBehaviorScope} property.
     * @param {String} scope Any string that represents a scope.
     * @chainable
     */
    Container.prototype.setDragDropBehaviorScope = function (scope) {
        if (typeof scope !== 'string') {
            throw new Error("setDragDropBehaviorScope(): The parameter must be a string.");
        }
        this.dragDropBehaviorScope = scope;
        this.setBehavior(this.behavior);
        return this;
    };
    /**
     * Sets the handler to be executed everytime the {@link #event-onAdd onAdd} event is fired.
     * @param {Function|null} handler The handler to execute, it also can be null.
     * @chainable
     */
    Container.prototype.setOnAddHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error('setOnAddHandler(): The parameter ust be a function or null.');
        }
        this.onAdd = handler;
        return this;
    };
    /**
     * Sets the callback function to be executed everytime any of its draggable items stop to be dragged.
     * @param {Function|null} handler It can be a function or null.
     */
    Container.prototype.setOnStopDragHandler = function (handler) {
        if (handler !== null && typeof handler !== 'function') {
            throw new Error('setOnStopDragHandler(): the parameter must be a function or null.');
        }
        this.onStopDrag = handler;
        return this;
    };
    /**
     * Sets the callback function to be executed everytime any of its draggable items starts to be dragged. For info
     * about the parameters sent to the callback function read the documentation for
     * the {@link #event-onDropOut onDropOut} event.
     * @param {Function|null} handler It can be a function or null.
     */
    Container.prototype.setOnDragStartHandler = function (handler) {
        if (!(typeof handler === 'function' || handler === null)) {
            throw new Error('setOnDragStartHandler(): the parameter must be a function or null.');
        }
        this.onDragStart = handler;
        return this;
    };
    /**
     * Sets the callback function to be executed when a draggable item is dragged from the container and dropped on
     * another container. This callback can return a boolean value or not return anything. If it returns "false" then
     * the drop action will be cancelled. For info about the parameters sent to the callback function please read the
     * {@link PMUI.core.Container#event-onDrop onDrop} event documentation.
     * @param {Function|null} handler
     * @chainable
     */
    Container.prototype.setOnDropOutHandler = function (handler) {
        if (!(typeof handler === 'function' || handler === null)) {
            throw new Error('setOnDropOutHandler(): the parameter must be a function or null.');
        }
        this.onDropOut = handler;
        return this;
    };
    /**
     * Disables the container's behavior.
     * @chainable
     */
    Container.prototype.disableBehavior = function () {
        this.disabledBehavior = true;
        this.behaviorObject.disable();
        return this;
    };
    /**
     * Enables the container's behavior.
     * @chainable
     */
    Container.prototype.enableBehavior = function () {
        this.disabledBehavior = false;
        this.behaviorObject.enable();
        this.disabled = false;
        this.style.removeClasses(['pmui-disabled']);
        return this;
    };
    /**
     * Disables interactive functions like the drag, drop, sort behaviors.
     * @chainable
     */
    Container.prototype.disable = function () {
        this.disabled = true;
        this.style.addClasses(['pmui-disabled']);
        return this.disableBehavior();
    };
    /**
     * Enables interactive functions like the drag, drop, sort behaviors.
     * @chainable
     */
    Container.prototype.enable = function () {
        this.disabled = false;
        this.style.removeClasses(['pmui-disabled']);
        return this.enableBehavior();
    };
    /**
     * Specifies which items inside the element should be sortable in case a drag/sort behavior is applied.
     * @param {String} selector A jQuery selector.
     * @chainable
     */
    Container.prototype.setSortableItems = function (selector) {
        this.sortableItems = selector;
        return this.setBehavior(this.behavior);
    };
    /**
     * Sets the callback function to be executed everytime a container's item order changes.
     * @param {Function} handler It can be a function or null. For info about the parameters sent to the callback
     * function please read the {@link PMUI.core.Container#event-onSort onSort} event documentation.
     */
    Container.prototype.setOnSortHandler = function (handler) {
        if (handler !== null && typeof handler !== 'function') {
            throw new Error('setOnSortHandler(): the parameter must be a function or null.');
        }
        this.onSort = handler;
        return this;
    };

    Container.prototype.setOnSortStartHandler = function (handler) {
        if (handler !== null && typeof handler !== 'function') {
            throw new Error('setOnSortStartHandler(): the parameter must be a function or null.');
        }
        this.onSortStart = handler;
        return this;
    };
    /**
     * Sets the callback function to be executed before a draggable item is added to the container by dropping.
     * @param {Function} handler It can be a function or null. For info about the parameters sent to the callback
     * function please read the {@link PMUI.core.Container#event-onBeforeDrop onBeforeDrop} event documentation.
     */
    Container.prototype.setOnBeforeDropHandler = function (handler) {
        if (handler !== null && typeof handler !== 'function') {
            throw new Error('setOnDropHandler(): the parameter must be a function or null.');
        }
        this.onBeforeDrop = handler;
        return this;
    };
    /**
     * Sets the callback function to be executed everytime a draggable item is dropped on the container.
     * @param {Function} handler It can be a function or null. For info about the parameters sent to the callback
     * function please read the {@link PMUI.core.Container#event-onDrop onDrop} event documentation.
     */
    Container.prototype.setOnDropHandler = function (handler) {
        if (handler !== null && typeof handler !== 'function') {
            throw new Error('setOnDropHandler(): the parameter must be a function or null.');
        }
        this.onDrop = handler;
        return this;
    };
    /**
     * Adds a new item but using raw data to create it.
     * @param {Object} dataItem A JSON object with the data to create the new object.
     * @param {Number} [index] The position index in which the data item will be inserted.
     */
    Container.prototype.addDataItem = function (dataItem, index) {
        var args,
            i;
        if (typeof dataItem !== 'object') {
            throw new Error('addDataItem(): The parameter must be an object.');
        }
        args = [{
            data: dataItem
        }];
        for (i = 1; i < arguments.length; i += 1) {
            args.push(arguments[i]);
        }
        this.addItem.apply(this, args);
        return this;
    };
    /**
     * Sets the data toi be used to create every item to be contained by the object.
     * @param {Array} dataItems An array in which each element is an object with the data for each of the items.
     */
    Container.prototype.setDataItems = function (dataItems) {
        var i;
        if (jQuery.isArray(dataItems)) {
            this.clearItems();
            this.massiveAction = true;
            for (i = 0; i < dataItems.length; i += 1) {
                this.addDataItem(dataItems[i]);
            }
            this.massiveAction = false;
            this.paintItems();
        }

        return this;
    };
    /**
     * Returns the html element in which the items are appended.
     * @return {HTMLElement}
     */
    Container.prototype.getContainmentArea = function () {
        this.getHTML();
        return this.containmentArea;
    };
    /**
     * Returns all the container's items the behavior will be applied on.
     * @return {Array} An array of items.
     * @private
     */
    Container.prototype.getBehavioralItems = function () {
        return this.getItems();
    };
    /**
     * Enables/disables one, two, all of the following behaviors:
     *
     * - "drag", allows to drag items from the object, also them can be dropped on another containers that have enables
     * the "drop" behavior.
     * - "drop", allows to drop draggable items from another container on the object.
     * - "sort", allows reorder the items in the object. Combined with "drop" behavior makes the object capable to
     * receive objects (drop) in a specific position.
     * @param {String} behavior The behavior to be set, the accepted values are:
     *
     * - "drag", enables the dragging functionality.
     * - "dragclone", enables the clone-dragging functionality.
     * - "drop", enables the dropping functionality.
     * - "sort", enabled the sorting functionality.
     * - "dragdrop", enables both dragging and dropping functionality.
     * - "dragsort", enables both dragging and sorting functionality.
     * - "dropsort", enables both dropping and sorting functionality.
     * - "dragdropsort", enables the three behaviors: dragging, dropping and sorting.
     * - "nobehavior", disables all the behaviors.
     * @chainable
     */
    Container.prototype.setBehavior = function (behavior, index) {
        var obj,
            behaviorObject,
            factory;

        if (typeof behavior === 'string') {
            factory = new PMUI.behavior.ContainerItemBehaviorFactory();
            obj = {
                pmType: behavior,
                targetObject: this,
                scope: this.dragDropBehaviorScope,
                handle: this.dragHandler
            };
            if (this.behaviorObject) {
                this.behaviorObject.detachBehavior();
            }
            behaviorObject = factory.make(obj);
            if (behaviorObject) {
                this.behaviorObject = behaviorObject;
                this.behavior = behavior;
            }
            if (this.html) {
                if (index !== undefined) {
                    this.behaviorObject.attachBehavior(index);
                } else {
                    this.behaviorObject.attachBehavior();
                }
            }
        }
        return this;
    };

    Container.prototype.addingBehaviorsElement = function (behavior, index) {
        var obj,
            behaviorObject,
            factory;

        if (typeof behavior === 'string') {
            factory = new PMUI.behavior.ContainerItemBehaviorFactory();
            obj = {
                pmType: behavior,
                targetObject: this,
                scope: this.dragDropBehaviorScope,
                handle: this.dragHandler
            };
            if (this.behaviorObject) {
                this.behaviorObject.detachBehavior();
            }
            behaviorObject = factory.make(obj);
            if (behaviorObject) {
                this.behaviorObject = behaviorObject;
                this.behavior = behavior;
            }
            if (this.html) {
                if (index !== undefined) {
                    this.behaviorObject.attachBehavior(index);
                } else {
                    this.behaviorObject.attachBehavior();
                }
            }
        }
        return this;
    };
    /**
     * Sets the "factory" property with a {PMUI.util.Factory} object
     * @param {Object} factory
     * @chainable
     * @private
     */
    Container.prototype.setFactory = function (factory) {
        if (factory instanceof PMUI.util.Factory) {
            this.factory = factory;
        } else {
            this.factory = new PMUI.util.Factory(factory);
        }
        return this;
    };

    /**
     * Removes a child element from the object
     * @param  {PMUI.core.Element|String|Number} item It can be a string (id of the child to remove),
     a number (index of the child to remove) or a {Element} object.
     * @chainable
     */
    Container.prototype.removeItem = function (item) {
        var itemToRemove,
            parent;
        if (item instanceof PMUI.core.Element) {
            itemToRemove = item;
        } else {
            if (typeof item === 'string') {
                itemToRemove = this.items.find("id", item.id);
            } else if (typeof item === 'number') {
                itemToRemove = this.items.get(item);
            }
        }
        if (itemToRemove) {
            parent = itemToRemove.parent;
            if (parent) {
                parent.items.remove(itemToRemove);
            } else {
                this.items.remove(itemToRemove);
            }
            itemToRemove.parent = null;
            if (this.html) {
                jQuery(itemToRemove.html).detach();
            }
        }
        if (!this.items.getSize()) {
            this.style.addClasses(['pmui-empty']);
        }
        if (this.massiveAction) {
            return this;
        }
        return this;
    };
    /**
     * Removes all the child items
     * @chainable
     */
    Container.prototype.clearItems = function () {
        this.massiveAction = true;
        while (this.items.getSize() > 0) {
            this.removeItem(0);
        }
        this.massiveAction = false;
        return this;
    };
    /**
     * Returns true if the item, used as the unique method parameter, is a direct child of the current Container object,
     otherwise returns false
     * @param  {PMUI.core.Element}  item The target child object
     * @return {Boolean}
     */
    Container.prototype.isDirectParentOf = function (item) {
        return this.items.indexOf(item) >= 0;
    };
    /**
     * Returns true if the supplied argument can be accepted as an item of the container.
     * @param  {PMUI.core.Element} obj The element to be evaluated.
     * @return {Boolean}
     */
    Container.prototype.canContain = function (obj) {
        return obj instanceof PMUI.core.Element ? this.factory.isValidClass(obj) : false;
    };
    /**
     * Paint a container's item (puts its HTML into the container's HTML).
     * @param  {PMUI.core.Element} item The item to add.
     * @chainable
     * @private
     */
    Container.prototype.paintItem = function (item, index) {
        var referenceItem;
        if (this.html) {
            if (index !== null && index !== undefined) {
                referenceItem = this.items.get(index + 1);
                if (referenceItem) {
                    this.containmentArea.insertBefore(item.getHTML(), referenceItem.getHTML());
                } else {
                    this.containmentArea.appendChild(item.getHTML());
                }
            } else {
                this.containmentArea.appendChild(item.getHTML());
            }
            if (this.eventsDefined) {
                item.defineEvents();
            }
            index = this.items.indexOf(item);
            this.setBehavior(this.behavior, index);
        }
        return this;
    };
    /**
     * Paint all the container items.
     * @chainable
     * @private
     */
    Container.prototype.paintItems = function () {
        var items,
            i;
        if (this.containmentArea) {
            items = this.items.asArray()
            for (i = 0; i < items.length; i += 1) {
                this.paintItem(items[i]);
            }
        }
        return this;
    };
    /**
     * Adds an child item to the object.
     * @param {PMUI.core.Element|Object} item It can be one of the following data types:
     * - {PMUI.core.Element} the object to add.
     * - {Object} a JSON object with the settings for the Container to be added.
     * @param {Number} [index] An index in which the item will be added.
     * @chainable
     */
    Container.prototype.addItem = function (item, index) {
        var itemToBeAdded;
        if (this.factory) {
            itemToBeAdded = this.factory.make(item);
        }
        if (itemToBeAdded && !this.isDirectParentOf(itemToBeAdded)) {
            if (index !== null && index !== undefined) {
                this.items.insertAt(itemToBeAdded, index);
            } else {
                this.items.insert(itemToBeAdded);
            }
            itemToBeAdded.parent = this;
            if (!this.massiveAction) {
                this.paintItem(itemToBeAdded, index);
            }
            if (typeof this.onAdd === 'function') {
                index = typeof index === 'number' ? index : this.items.getSize() - 1;
                this.onAdd(this, this.getItem(index), index);
            }
        }
        return this;
    };
    /**
     * Clear all the object's current child items and add new ones
     * @param {Array} items An array where each element can be a {Element} object or a JSON object
     where is specified the setting for the child item to be added
     * @chainable
     */
    Container.prototype.setItems = function (items) {
        var i;
        if (jQuery.isArray(items)) {
            this.clearItems();
            this.massiveAction = false;
            for (i = 0; i < items.length; i += 1) {
                this.addItem(items[i]);
            }
        }
        return this;
    };
    /**
     * Returns an array with all the child elements
     * @return {Array}
     */
    Container.prototype.getItems = function () {
        return this.items.asArray();
    };
    /**
     * Returns one single item based on the id or index position
     * @param  {String|Number} id If the parameter is a string then
     it will be take as the id for the element to find and return,
     but if the element is a Number it returns the object iwth that
     index position
     * @return {PMUI.core.Element}
     */
    Container.prototype.getItem = function (i) {
        var t;
        if (typeof i === 'number') {
            t = this.items.get(i);
        } else if (typeof i === 'string') {
            t = this.items.find('id', i);
        } else if (this.items.indexOf(i) >= 0) {
            t = i;
        }
        return t;
    };
    /**
     * Returns the index for the specified item.
     * @param  {PMUI.core.Element|String} item It can be:
     *
     * - {@link PMUI.core.Element}
     * - String, it will be interpreted as the items's id.
     * @return {Number}
     */
    Container.prototype.getItemIndex = function (item) {
        var evaluatedItem;

        if (item instanceof PMUI.core.Element) {
            evaluatedItem = item;
        } else if (typeof item === 'number') {
            evaluatedItem = this.items.get(item);
        } else if (typeof item === 'string') {
            evaluatedItem = this.items.find("id", item);
        }

        return this.items.indexOf(evaluatedItem);
    };
    /**
     * Creates the object's HTML element
     * @return {HTMLElement}
     */
    Container.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }
        Container.superclass.prototype.createHTML.call(this);
        this.containmentArea = this.html;
        this.paintItems();
        this.style.applyStyle();
        this.setBehavior(this.behavior);

        return this.html;
    };
    /**
     * Moves an object's item from a position to another.
     * @param  {PMUI.core.Element} item The item to move.
     * @param  {Number} index The index position in which the item will be moved.
     * @chainable
     */
    Container.prototype.moveItem = function (item, index) {
        var items = this.items,
            currentIndex,
            referenceObject;

        item = this.getItem(item);

        if (item instanceof PMUI.core.Element && this.isDirectParentOf(item)) {
            currentIndex = items.indexOf(item);
            items = items.asArray();
            referenceObject = this.items.get(index + (currentIndex < index ? 1 : 0));
            item = items.splice(currentIndex, 1)[0];
            this.items.remove(item);
            this.items.insertAt(item, index);
            if (this.html) {
                if (index === this.items.getSize() - 1) {
                    this.containmentArea.appendChild(item.getHTML());
                } else {
                    this.containmentArea.insertBefore(item.getHTML(), referenceObject.getHTML());
                }
            }
        }
        return this;
    };
    /**
     * Returns the function to be executed when an accepted draggable is dragged over the droppable area.
     * @return {Function}
     */
    Container.prototype.onDragOver = function () {
        return function () {
        };
    };
    /**
     * Returns the function to be executed when the items order has been changed.
     * @return {Function}
     * @private
     */
    Container.prototype.onSortingChange = function () {
        var that = this;
        return function (e, ui) {
            var itemHTML = ui.item.get(0),
                item = PMUI.getPMUIObject(itemHTML),
                newIndex;
            newIndex = jQuery(that.getContainmentArea()).find('>*').index(itemHTML);

            that.moveItem(item, newIndex);
            if (typeof that.onSort === 'function') {
                that.onSort(that, item, newIndex);
            }
        };
    };
    /**
     * Handler to be executed everytime the mouse enter a position over any container's draggable/sortable item.
     * @param {PMUI.core.Element} draggable The draggable PMUI object.
     * @chainable
     */
    Container.prototype.onDraggableMouseOver = function (draggable) {
        return this;
    };
    /**
     * Handler to be executed everytime the mouse leaves a position over any container's draggable/sortable item.
     * @param {PMUI.core.Element} draggable The draggable PMUI object.
     * @chainable
     */
    Container.prototype.onDraggableMouseOut = function (draggable) {
        return this;
    };
    /**
     * Executes children events defined
     * @return {PMUI.core.Container}
     * @chainable
     */
    Container.prototype.defineEvents = function () {
        var j,
            children,
            that = this;
        Container.superclass.prototype.defineEvents.call(this);
        if (that.items.getSize() > 0) {
            children = that.getItems();
            for (j = 0; j < children.length; j += 1) {
                children[j].defineEvents();
            }
        }
        return this;
    };

    PMUI.extendNamespace('PMUI.core.Container', Container);

    // Publish to NodeJS environment
    if (typeof exports !== 'undefined') {
        module.exports = Container;
    }

}());