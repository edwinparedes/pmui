(function () {
    /**
     * @class PMUI.core.Panel
     * Handles panels to be inserted into containers, it is composed by three main elements: header, body and footer
     * @abstract
     * @extends {PMUI.core.Container}
     *
     *       //Remember, this is an abstract class so it shouldn't be instantiate,
     *       //anyway we are instantiating it just for this example
     *        var p1, p2, panel;
     *        p1 = new PMUI.core.Panel({
     *            height : 140,
     *            width : 90,
     *            display : 'inline-block',
     *            style : {
     *                cssProperties : {
     *                    'background-color':'red'
     *                }
     *            }
     *        });
     *        p2 = new PMUI.core.Panel({
     *            height : 140,
     *            width : 90,
     *            display : 'inline-block',
     *            style : {
     *                cssProperties : {
     *                    'background-color':'yellow'
     *                }
     *            }
     *        });
     *        panel = new PMUI.core.Panel({
     *            height : 150,
     *            width : 200,
     *            display : 'block',
     *            items : [
     *                p1,
     *                p2
     *            ],
     *            layout : 'vbox',
     *            padding : "5px",
     *            borderWidth : "1px",
     *            border : true
     *        });
     *        document.body.appendChild(panel.getHTML());
     *
     * @constructor
     * Creates a new instacne of the object
     * @param {Object} settings
     *
     * @cfg {Object|PMUI.core.Panel|null} [panel=null] The panel to be appended into the current Panel. It can be an
     * object literal with the {@link PMUI.core.Panel config options for Panel} or an instance of
     * {@link PMUI.core.Panel Panel} or null.
     * @cfg {PMUI.core.Container} [parent=null] The panel's parent element.
     * @cfg {String|PMUI.layout.Layout} [layout='box'] The layout to be applied to the panel, the layout determines the
     * render position for the panel's items. The possible layouts are:
     *
     * - box: this applies no layout to the panel. To set the layout to box if you use a string as value for the config
     * option use "box", in the other case, if you use an object, use an instance of {@link PMUI.layout.Box Box}.
     * - hbox: this applies a layout that renders the panel's items side by side using the items's
     * {@link PMUI.core.Element#property-propotion proportion property} to calculate the width for each item. To set
     * this layout to the panel, if you are using a string as the value for the config option, use "hbox", in the other
     * case, if you are using an object use an instance of {@link PMUI.layout.HBox HBox}.
     * - vbox: this applies a layout that renders the panel's items in vertical position using the items's
     * {@link PMUI.core.Element#property-propotion proportion property} to calculate the height for each item. To set
     * this layout to the panel, if you are using a string as the value for the config option, use "vbox", in the other
     * case, if you are using an object use an instance of {@link PMUI.layout.VBox VBox}.
     * @cfg {String|Number} [padding=""] Sets the css padding property for the panel.  It can be a number or and string
     * with the format ##px or an empty string. If you use an empty string as the parameter, the padding will be set
     * dinamically from the used css file.
     * @cfg {String|Number} [borderWidth=""] Sets the border width for the panel. It can be a number or a string with
     * the format '##px' or an empty string. If you use an empty string as the parameter, the border width will be set
     * dinamically from the used css file.
     */
    var Panel = function (settings) {
        Panel.superclass.call(this, settings);
        /**
         * The child {Panel} object
         * @type {PMUI.core.Panel}
         */
        this.panel = null;
        /**
         * The Panel's parent object
         * @type {PMUI.core.Container}
         */
        this.parent = null;
        /**
         * A {Layout} object which handles the position layout for the object's direct child elements
         * @property {PMUI.layout.Layout}
         */
        this.layout = null;
        /**
         * @property {String} [topPadding=""]
         * The value for the css padding-top property to be applied to the object's html.
         * @readonly
         */
        this.topPadding = null;
        /**
         * @property {String} [leftPadding=""]
         * The value for the css padding-left property to be applied to the object's html.
         * @readonly
         */
        this.leftPadding = null;
        /**
         * @property {String} [bottomPadding=""]
         * The value for the css padding-bottom property to be applied to the object's html.
         * @readonly
         */
        this.bottomPadding = null;
        /**
         * @property {String} [rightPadding=""]
         * The value for the css padding-right property to be applied to the object's html.
         * @readonly
         */
        this.rightPadding = null;
        /**
         * @property {String} [borderWidth=""] A string with the format ##px (where ## is a number).
         * It determines the panel's border width.
         * @readonly
         */
        this.borderWidth = null;
        /**
         * @property {Boolean} [border=false] If the border is being shown.
         * @readonly
         */
        this.border = null;
        Panel.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Container', Panel);

    /**
     * Defines the object's type
     * @type {String}
     */
    Panel.prototype.type = 'Panel';

    /**
     * @private
     * Initializes the object with default values
     * @param  {Object} options
     */
    Panel.prototype.init = function (settings) {
        var defaults = {
            panel: null,
            parent: null,
            layout: 'box',
            padding: "",
            borderWidth: ""
        };

        jQuery.extend(true, defaults, settings);

        this.setPanel(defaults.panel)
            .setParent(defaults.parent)
            .setLayout(defaults.layout)
            .setPadding(defaults.padding)
            .setBorderWidth(defaults.borderWidth);

    };
    /**
     * Applies the css classes and ptoperties to the element's html which the object is related
     * @chainable
     */
    Panel.prototype.applyStyle = function () {
        if (this.html) {
            this.style.applyStyle();

            this.style.addProperties({
                display: this.visible ? this.display : "none",
                position: this.positionMode,
                left: this.x,
                top: this.y,
                width: this.width,
                height: this.height,
                zIndex: this.zOrder,
                "padding-top": this.topPadding,
                "padding-right": this.rightPadding,
                "padding-bottom": this.bottomPadding,
                "padding-left": this.leftPadding,
                "border-width": this.borderWidth,
                "border-style": this.borderWidth !== "" ? "solid" : "",
                "box-sizing": "border-box"
            });
        }
        return this;
    };
    /**
     * Sets the border width for the panel
     * @param {String|Number} width It can be a number or a string with the format '##px' or an empty string.
     * If you use an empty string as the parameter, the border width will be set dinamically from the used css file.
     */
    Panel.prototype.setBorderWidth = function (width) {
        var error = false;
        if (typeof width === 'number') {
            this.borderWidth = width + "px";
        } else if (typeof width === 'string') {
            width = jQuery.trim(width);
            if (/^\d+(\.\d+)?px$/.test(width) || width === "") {
                this.borderWidth = width;
            } else {
                error = true;
            }
        } else {
            error = false;
        }

        if (error) {
            throw new Error("setBorderWidth(): invalid parameter.");
        } else {
            this.applyStyle();
            if (this.layout) {
                this.layout.applyLayout();
            }
        }

        return this;
    };
    /**
     * Returns the border width from the object
     * @return {String}
     */
    Panel.prototype.getBorderWidth = function () {
        return this.borderWidth || this.style.getProperty("border-width");
    };
    /**
     * Sets the padding values for the panel.
     * @param {String|Number} padding It can be a number or and string with the format ##px or an empty string.
     * If you use an empty string as the parameter, the padding will be set dinamically from the used css file.
     */
    Panel.prototype.setPadding = function (padding) {
        var error = false;
        if (typeof padding === 'string') {
            padding = jQuery.trim(padding);
            padding = padding.split(/\s+/).join(" ");
            if (/^(\d+(\.\d+)?(px|%)\s)+$/.test(padding + " ")) {
                padding = padding.match(/\d+(\.\d+)?(px|%)/g);
                switch (padding.length) {
                    case 4:
                        this.topPadding = padding[0];
                        this.rightPadding = padding[1];
                        this.bottomPadding = padding[2];
                        this.leftPadding = padding[3];
                        break;
                    case 3:
                        this.topPadding = padding[0];
                        this.rightPadding = this.leftPadding = padding[1];
                        this.bottomPadding = padding[2];
                        break;
                    case 2:
                        this.topPadding = this.bottomPadding = padding[0];
                        this.rightPadding = this.leftPadding = padding[1];
                        break;
                    case 1:
                        this.topPadding = this.rightPadding = this.bottomPadding = this.leftPadding = padding[0];
                        break;
                }
            } else if (padding === "") {
                this.topPadding = this.rightPadding = this.bottomPadding = this.leftPadding = "";
            } else {
                error = true;
            }
        } else if (typeof padding === 'number') {
            this.topPadding = this.rightPadding = this.bottomPadding = this.leftPadding = padding + "px";
        } else {
            error = true;
        }

        if (error) {
            throw new Error("setPadding(): Invalid parameter.");
        } else {
            this.applyStyle();
            if (this.layout) {
                this.layout.applyLayout();
            }
        }

        return this;
    };
    /**
     * Returns an array with the padding values in the following order: [top, right, bottom, left].
     * @return {Array}
     */
    Panel.prototype.getPadding = function () {
        return [
            this.topPadding || this.style.getProperty("padding-top"),
            this.rightPadding || this.style.getProperty("padding-right"),
            this.bottomPadding || this.style.getProperty("padding-bottom"),
            this.leftPadding || this.style.getProperty("padding-left")
        ];
    };
    /**
     * Sets the panel object
     * @param {Object} panel Panel object
     */
    Panel.prototype.setPanel = function (panel) {
        if (panel) {
            if (panel instanceof Panel) {
                this.panel = panel;
            } else if (typeof panel === 'object') {
                this.panel = new Panel(panel);
            }

            if (this.html) {
                jQuery(this.html).empty().append(panel.getHTML());
            }
        }

        return this;
    };
    /**
     * Returns the object's html usable width.
     * @return {Number|String} It can return a Number or an String with a css property like "auto".
     */
    Panel.prototype.getUsableWidth = function () {
        var padding = this.getPadding(), border = parseInt(this.getBorderWidth(), 10) * 2;
        if (isNaN(this.getWidth())) {
            return this.getWidth();
        }
        return this.getWidth() - (parseInt(padding[1], 10) || 0) - (parseInt(padding[3], 10) || 0) - (border || 0);
    };
    /**
     * Returns the object's html usable height
     * @return {Number|String} It can return a Number or an String with a css property like "auto".
     */
    Panel.prototype.getUsableHeight = function () {
        var padding = this.getPadding(), border = parseInt(this.getBorderWidth(), 10) * 2;
        if (isNaN(this.getHeight())) {
            return this.getHeight();
        }
        return this.getHeight() - (parseInt(padding[0], 10) || 0) - (parseInt(padding[2], 10) || 0) - (border || 0);
    };
    /**
     * Sets the parent object
     * @param {Object} parent
     */
    Panel.prototype.setParent = function (parent) {
        this.parent = parent;
        return this;
    };

    /**
     * Sets the layout for the panel.
     * @param {Object|String} layout Layout object
     */
    Panel.prototype.setLayout = function (layout) {
        var factory = new PMUI.layout.LayoutFactory();
        if (!(typeof layout === "string")) {
            throw new Error("setLayout(): Property values sent an invalid, expected 'box' , 'vbox' or 'hbox'");
        }
        this.layout = factory.make(layout);
        this.layout.setContainer(this);
        if (this.html) {
            this.layout.applyLayout();
        }
        return this;
    };

    /**
     * Add an item to the panel.
     * @param {PMUI.core.Element|Object} item.
     * @param {Number} [index] The index position in which the item will be appended.
     * It can be a valid JSON object or an object that inherits from {@link PMUI.core.Element Element}.
     * @chainable
     */
    Panel.prototype.addItem = function (item, index) {
        Panel.superclass.prototype.addItem.call(this, item, index);
        if (this.layout) {
            this.layout.applyLayout();
        }

        return this;
    };
    /**
     * Sets the width for the Panel's HTML element
     * @param {Number|String} width It can be a number or a string.
     * In case of using a String you only can use 'auto' or 'inherit' or ##px or ##% or ##em when ## is a number
     * @chainable
     */
    Panel.prototype.setWidth = function (width) {
        Panel.superclass.prototype.setWidth.call(this, width);
        if (this.layout) {
            this.layout.applyLayout();
        }

        return this;
    };
    /**
     * Sets the height for the Panel's HTML element
     * @param {Number|String} height It can be a number or a string.
     * In case of using a String you only can use 'auto' or 'inherit' or ##px or ##% or ##em when ## is a number
     * @chainable
     */
    Panel.prototype.setHeight = function (height) {
        Panel.superclass.prototype.setHeight.call(this, height);
        if (this.layout) {
            this.layout.applyLayout();
        }
        return this;
    };
    /**
     * Returns the object pointed to the parent
     * @return {Object}
     */
    Panel.prototype.getParent = function () {
        return this.parent;
    };
    /**
     * Creates the HTML element for the Panel
     * @return {HTMLElement}
     */
    Panel.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }

        Panel.superclass.prototype.createHTML.call(this);
        this.applyStyle();
        if (this.layout) {
            this.layout.applyLayout();
        }

        return this.html;
    };

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = Panel;
    }

    PMUI.extendNamespace('PMUI.core.Panel', Panel);
}());