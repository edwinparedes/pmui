(function() {
    /**
     * @class PMUI.behavior.ContainerItemDragDropBehavior
     * @extends PMUI.behavior.ContainerItemBehavior
     * Class that encapsulates the drop behavior for a {@link PMUI.core.Container Container} object and the drag 
     * behavior for its items. The object that applies this behavior, will be able to:
     * 
     * - Accept draggable items from other Containers which ones are applying dragging behaviors 
     * ({@link PMUI.behavior.ContainerItemDragBehavior drag}, 
     * {@link PMUI.behavior.ContainerItemDragDropBehavior dragdrop}, 
     * {@link PMUI.behavior.ContainerItemDragSortBehavior dragsort}, 
     * {@link PMUI.behavior.ContainerItemDragDropSortBehavior dragdropsort}). 
     * - The object's items will be able to be dragged to another Containers that are applying dropping behaviors
     * ({@link PMUI.behavior.ContainerItemDropBehavior drop}, 
     * {@link PMUI.behavior.ContainerItemDragDropBehavior dragdrop}, 
     * {@link PMUI.behavior.ContainerItemDropSortBehavior dropsort}, 
     * {@link PMUI.behavior.ContainerItemDragDropSortBehavior dragdropsort}).
     * 
     * Since this class is created through the 
     * {@link PMUI.behavior.ContainerItemBehaviorFactory ContainerItemBehaviorFactory} it shouldn't be instantiated in 
     * most cases (it is instantiated internally by the object of the Container class).
     * If you want to apply this behavior to a {@link PMUI.core.Container Container} object, just call its 
     * {@link PMUI.core.Container#method-setBehavior setBehavior()} method with the string parameter "dragdrop" or set it 
     * at instantiation time using its {@link PMUI.core.Container#cfg-behavior behavior} config option.
     *
     * Usage example:
     *
     *      @example
     *      var tree = new PMUI.panel.TreePanel({
     *          items: [
     *              {
     *                  label: "draggable item #1"
     *              }, {
     *                  label: "draggable item #2"
     *              }, {
     *                  label: "draggable item #3"
     *              }, {
     *                  label: "draggable item #4"
     *              }, {
     *                  label: "draggable item #5"
     *              }, {
     *                  label: "draggable item #6"
     *              }
     *          ]
     *      }), tree2 = new PMUI.panel.TreePanel({
     *          style: {
     *              cssClasses: ["droppable-list"]
     *          },
     *          items: [
     *              {
     *                  label: "draggable item #7"
     *              }, {
     *                  label: "draggable item #8"
     *              }, {
     *                  label: "draggable item #9"
     *              }, {
     *                  label: "draggable item #10"
     *              }, {
     *                  label: "draggable item #11"
     *              }, {
     *                  label: "draggable item #12"
     *              }
     *          ]
     *      });
     *
     *      tree.setBehavior("dragdrop");
     *      tree2.setBehavior("dragdrop");
     *      document.body.appendChild(tree.getHTML());
     *      //Add a separator
     *      document.body.appendChild(document.createElement('hr'));
     *      document.body.appendChild(tree2.getHTML());
     */
    var ContainerItemDragDropBehavior = function(settings) {
        ContainerItemDragDropBehavior.superclass.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.behavior.ContainerItemBehavior', ContainerItemDragDropBehavior);
    /**
     * @inheritdoc
     */
    ContainerItemDragDropBehavior.prototype.updateBehaviorAvailability = function() {
        var d = this.disabled ? 'disable' : 'enable';
        this.targetHTML.droppable(d);
        jQuery(this.getTargetObjectItems()).draggable(d);
        if(this.disabled) {
            this.detachOnDraggableMouseEvents();
        } else {
            this.attachOnDraggableMouseEvents();
        }
        return this;
    };
    /**
     * Returns the html from the items of the {@link #property-targetObject targetObject} 
     * @return {Object} A jQuery object with all the target object' items html.
     */
    ContainerItemDragDropBehavior.prototype.getTargetObjectItems = function() {
        var $html = $(null),
            i,
            item,
            items,
            targetObject = this.targetObject;
        items = targetObject.getBehavioralItems();
        for(i = 0; i < items.length; i += 1) {
            item = items[i].getHTML();
            $html = $html.add(item);
        }
        return $html;
    };
    /**
     * @inheritdoc
     */
    ContainerItemDragDropBehavior.prototype.onDropOut = function() {
        var aux;
        ContainerItemDragDropBehavior.superclass.prototype.onDropOut.apply(this, arguments);
        //The next lines avoid an uncontrolable error generated on the jquery ui's stop callback
        //when it try to access certain data that will be undefined because of the destruction of the
        //drag behavior when it is added to the new container in which it was dropped on.
        //What it does is add some data in the jquery data of the object's html.
        aux = jQuery(this.draggedObject.html).data('ui-draggable');
        if(!aux) {
            aux = {};
            jQuery(this.draggedObject.html).data('ui-draggable', aux);
        }
        if(!aux.sortables) {
            aux.sortables = [];
        }
        if(!aux.options) {
            aux.options = {};
        }
        return this;
    };
    /**
     * Defines the handler to be executed when an accepted draggable is dragged over the 
     * {@link #property-targetObject targetObject}.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemDragDropBehavior.prototype.onOver = function() {
        var that = this;
        return function(e, ui) {
            (that.targetObject.onDragOver()());
        };
    };
    /**
     * Defines the handler to be executed when an accepted draggable is dragged out of the 
     * {@link #property-targetObject targetObject}.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemDragDropBehavior.prototype.onOut = function() {
        var that = this;
        return function(e, ui) {

        };
    };
    /**
     * Attaches the behavior to the {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDragDropBehavior.prototype.attachBehavior = function() {
        var $html,
            i,
            items,
            item,
            targetObject = this.targetObject;
        if(!this.behaviorAttached) {
            $html = this.getTargetObjectItems();

            $html.draggable({
                appendTo: document.body,
                connectToSortable: '.pmui-containeritembehavior-sort',
                helper: 'clone',
                revert: "invalid",
                drag: this.onDrag(),
                start: this.onStart(),
                stop: this.onStop(),
                scope: this.scope,
                handle: this.handle || false
            });
            this.targetHTML = jQuery(this.targetObject.getContainmentArea());
            this.targetHTML.droppable({
                drop: this.onDrop(),
                out: this.onOut(),
                over: this.onOver(),
                tolerance: 'pointer',
                scope: this.scope
            });
            this.attachOnDraggableMouseEvents();
        }
        return ContainerItemDragDropBehavior.superclass.prototype.attachBehavior.call(this);
    };
    /**
     * Detaches the behavior from the 
     * {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDragDropBehavior.prototype.detachBehavior = function() {
        if(this.behaviorAttached) {
            try {
                this.getTargetObjectItems().draggable('destroy');
                this.targetHTML.droppable('destroy');
                this.detachOnDraggableMouseEvents();
            } catch(e) {}
        }
        return ContainerItemDragDropBehavior.superclass.prototype.detachBehavior.call(this);
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemDragDropBehavior', ContainerItemDragDropBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemDragDropBehavior;
    }
}());