(function() {
    /**
     * @class PMUI.behavior.ContainerItemSortBehavior
     * @extends PMUI.behavior.ContainerItemDragBehavior
     * Class that encapsulates the sort behavior for {@link PMUI.core.Container Container} objects. This behavior 
     * enables the {@link #property-targetObject targetObject}'s' items to be sortable.
     * 
     * Since this class is created through the 
     * {@link PMUI.behavior.ContainerItemBehaviorFactory ContainerItemBehaviorFactory} it shouldn't be instantiated in 
     * most cases (it is instantiated internally by the object of the Container class).
     * If you want to apply this behavior to a {@link PMUI.core.Container Container} object, just call its 
     * {@link PMUI.core.Container#method-setBehavior setBehavior()} method with the string parameter "sort" or set it 
     * at instantiation time using its {@link PMUI.core.Container#cfg-behavior behavior} config option.
     *
     * Usage example:
     *
     *      @example
     *      var tree = new PMUI.panel.TreePanel({
     *          items: [
     *              {
     *                  label: "sortable item #1"
     *              }, {
     *                  label: "sortable item #2"
     *              }, {
     *                  label: "sortable item #3"
     *              }, {
     *                  label: "sortable item #4"
     *              }, {
     *                  label: "sortable item #5"
     *              }, {
     *                  label: "sortable item #6"
     *              }
     *          ]
     *      });
     *
     *      tree.setBehavior("sort");
     *      document.body.appendChild(tree.getHTML());
     */
    var ContainerItemSortBehavior = function(settings) {
        ContainerItemSortBehavior.superclass.call(this, settings);
        this.alignment = null;
        ContainerItemSortBehavior.prototype.init.call(this,settings);
    };

    PMUI.inheritFrom('PMUI.behavior.ContainerItemDragBehavior', ContainerItemSortBehavior);/**
     * @inheritdoc
     */

    ContainerItemSortBehavior.prototype.init = function (settings) {
        var defaults = {
            alignment : 'vertical'
        };

        jQuery.extend(true, defaults, settings);
        this.setAlignment(defaults.alignment);
    };

    ContainerItemSortBehavior.prototype.setAlignment = function (alignment) {
        if (!(typeof alignment === "string" && (alignment === "vertical" || alignment === "horizontal"))){
            throw new Error  ("ContainerItemSortBehavior.setAlignment(): the parameter is no valid, must be a vertical or horizontal value"); 
        }
        this.alignment = alignment;
        return this;
    };

    ContainerItemSortBehavior.prototype.updateBehaviorAvailability = function() {
        this.targetHTML.sortable(this.disabled ? 'disable' : 'enable');
        return this;
    };
    /**
     * Defines the handler to be executed when the
     * {@link #property-targetObject targetObject}'s item sorting starts.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemSortBehavior.prototype.onStart = function() {
        var that = this;
        return function(e, ui) {
            var targetObject,
                o;
            that.cancelledDrop = null;
            o = PMUI.getPMUIObject(ui.item.get(0));
            
            if(that.targetObject.isDirectParentOf(o)) {
                targetObject = that.targetObject;
                
                if ( that.alignment == "vertical" ) {
                    ui.helper.get(0).style.width = $(e.target).width() + "px";
                }
                that.sortIObject = o;
                if(typeof targetObject.onSortStart === 'function') {
                    targetObject.onSortStart(targetObject, that.sortIObject);
                }
            }
        };
    };
    /**
     * Defines the handler to be executed when the
     * {@link #property-targetObject targetObject}'s item sorting stops.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemSortBehavior.prototype.onStop = function() {
        var that = this;
        return function() {};
    };
    /**
     * Defines the handler to be executed when the user stopped sorting and the 
     * {@link #property-targetObject targetObject}'s items positions have 
     * changed.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemSortBehavior.prototype.onUpdate = function() {
        return this.targetObject.onSortingChange();
    };
    /**
     * Attaches the behavior to the {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemSortBehavior.prototype.attachBehavior = function() {
        var $html,
            i,
            items,
            item,
            targetObject = this.targetObject;
        if(!this.behaviorAttached) {
            this.targetHTML = jQuery(this.targetObject.getContainmentArea());
            this.targetHTML.sortable({
                containment: 'parent',
                sort: this.onDrag(),
                start: this.onStart(),
                stop: this.onStop(),
                update: this.onUpdate(),
                handle: this.handle || false
            });
        }
        return ContainerItemSortBehavior.superclass.superclass.prototype.attachBehavior.call(this);
    };
    /**
     * Detaches the behavior from the 
     * {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemSortBehavior.prototype.detachBehavior = function() {
        if(this.behaviorAttached) {
            try {
                targetHTML.sortable('destroy');
            } catch(e) {}
        }
        return ContainerItemSortBehavior.superclass.superclass.prototype.detachBehavior.call(this);
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemSortBehavior', ContainerItemSortBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemSortBehavior;
    }
}());