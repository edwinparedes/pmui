(function() {
    /**
     * @class PMUI.behavior.ContainerItemDragDropSortBehavior
     * @extends PMUI.behavior.ContainerItemDragDropBehavior
     * Class that encapsulates the drop behavior for a {@link PMUI.core.Container Container} object and the drag and 
     * sort behaviors for its items. The object that applies this behavior, will be able to:
     * 
     * - Accept draggable items from other Containers which ones are applying dragging behaviors 
     * ({@link PMUI.behavior.ContainerItemDragBehavior drag}, 
     * {@link PMUI.behavior.ContainerItemDragDropBehavior dragdrop}, 
     * {@link PMUI.behavior.ContainerItemDragSortBehavior dragsort}, 
     * {@link PMUI.behavior.ContainerItemDragDropSortBehavior dragdropsort}).
     * 
     * - The object's items will be able to be dragged to another Containers that are applying dropping behaviors
     * ({@link PMUI.behavior.ContainerItemDropBehavior drop}, 
     * {@link PMUI.behavior.ContainerItemDragDropBehavior dragdrop}, 
     * {@link PMUI.behavior.ContainerItemDropSortBehavior dropsort}, 
     * {@link PMUI.behavior.ContainerItemDragDropSortBehavior dragdropsort}).
     *
     * - The object's items will be enabled for sorting.
     * 
     * Since this class is created through the 
     * {@link PMUI.behavior.ContainerItemBehaviorFactory ContainerItemBehaviorFactory} it shouldn't be instantiated in 
     * most cases (it is instantiated internally by the object of the Container class).
     * If you want to apply this behavior to a {@link PMUI.core.Container Container} object, just call its 
     * {@link PMUI.core.Container#method-setBehavior setBehavior()} method with the string parameter "dragdropsort" or set it 
     * at instantiation time using its {@link PMUI.core.Container#cfg-behavior behavior} config option.
     *
     * Usage example:
     *
     *      @example
     *      var tree = new PMUI.panel.TreePanel({
     *          items: [
     *              {
     *                  label: "draggable item #1"
     *              }, {
     *                  label: "draggable item #2"
     *              }, {
     *                  label: "draggable item #3"
     *              }, {
     *                  label: "draggable item #4"
     *              }, {
     *                  label: "draggable item #5"
     *              }, {
     *                  label: "draggable item #6"
     *              }
     *          ]
     *      }), tree2 = new PMUI.panel.TreePanel({
     *          style: {
     *              cssClasses: ["droppable-list"]
     *          },
     *          items: [
     *              {
     *                  label: "draggable item #7"
     *              }, {
     *                  label: "draggable item #8"
     *              }, {
     *                  label: "draggable item #9"
     *              }, {
     *                  label: "draggable item #10"
     *              }, {
     *                  label: "draggable item #11"
     *              }, {
     *                  label: "draggable item #12"
     *              }
     *          ]
     *      });
     *
     *      tree.setBehavior("dragdropsort");
     *      tree2.setBehavior("dragdropsort");
     *      document.body.appendChild(tree.getHTML());
     *      //Add a separator
     *      document.body.appendChild(document.createElement('hr'));
     *      document.body.appendChild(tree2.getHTML());
     */
    var ContainerItemDragDropSortBehavior = function(settings) {
        ContainerItemDragDropSortBehavior.superclass.call(this, settings);
        this.dropped = null;
        this.cancelledDrop = null;
    };

    PMUI.inheritFrom('PMUI.behavior.ContainerItemDragDropBehavior', ContainerItemDragDropSortBehavior);
    /**
     * @inheritdoc
     */
    ContainerItemDragDropSortBehavior.prototype.updateBehaviorAvailability = function() {
        this.targetHTML.sortable(this.disabled ? 'disable' : 'enable');
        if(this.disabled) {
            this.detachOnDraggableMouseEvents();
        } else {
            this.attachOnDraggableMouseEvents();
        }
        return this;
    };
    /**
     * Defines the handler to be executed when the user stopped sorting and the 
     * {@link #property-targetObject targetObject}'s items positions have 
     * changed.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemDragDropSortBehavior.prototype.onUpdate = function() {
        var that = this;
        return function(e, ui) {
            var parentHTML = that.targetObject.getHTML(),
                item = PMUI.getPMUIObject(ui.item.get(0));
            if(that.dropped) {
                that.dropped = false;
                return;
            }

            if(!item) {
                return;
            }

            if(jQuery(parentHTML).has(item.getHTML()).length) {
                (that.targetObject.onSortingChange()(e, ui));
            }
        };
    };
    /**
     * @inheritdoc
     */
    ContainerItemDragDropSortBehavior.prototype.onStart = function() {
        var that = this;
        return function(e, ui) {
            var targetObject, o;
            that.cancelledDrop = null;
            //Since the jQuery sortable's start callback is executed when the sorting starts and not neccesarily when
            //one of its own items starts dragging we need to do the next validation:
            //check if the item dragged belongs to the sortable html. In that case we fire the onDragStart callback.
            o = PMUI.getPMUIObject(ui.item.get(0));
            if(that.targetObject.isDirectParentOf(o)) {
                targetObject = that.targetObject;
                ui.helper.get(0).style.width = $(e.target).width() + "px";
                ui.item.hide();
                that.draggedObject = o;
                if(typeof targetObject.onDragStart === 'function') {
                    targetObject.onDragStart(targetObject, that.draggedObject);
                }
            }
        };
    };
    /**
     * Defines the handler to be executed when an accepted draggable is dropped on the 
     * {@link #property-targetObject targetObject}.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemDragDropSortBehavior.prototype.onDrop = function() {
        var that = this;
        return function(e, ui) {
            var targetObject = that.targetObject,
                droppable = $(this),
                item = PMUI.getPMUIObject(ui.item.get(0)),
                data,
                index = !targetObject.getItems().length ? 0 
                    : droppable.find('>*').index(item.getHTML()),
                performDrop;

            if(index < 0) {
                index = droppable.find('>*').index(droppable.find('.ui-draggable'));
            }
            if(typeof targetObject.onBeforeDrop === 'function') {
                targetObject.onBeforeDrop(targetObject, item, index);
            }
            if(typeof targetObject.onDrop === 'function') {
                performDrop = targetObject.onDrop(targetObject, item, index);
            }
            if(performDrop || performDrop === undefined) {
                item.getParent().behaviorObject.onDropOut(item, targetObject, index);
                that.dropped = true;
                if(!targetObject.canContain(item)) {
                    $(e.target).find('.ui-draggable').detach();   
                }
                that.cancelledDrop = false;
            } else {
                //if the dropped object came from a container with draggable or sortable behavior
                if(ui.helper) {
                    //from draggable
                    ui.sender.draggable('cancel');
                } else {
                    //from sortable
                    ui.sender.sortable('cancel');
                }
                that.cancelledDrop = true;
            }
        };
    };
    /**
     * @inheritdoc
     */
    ContainerItemDragDropSortBehavior.prototype.onStop = function() {
        var that = this;
        return function(e, ui) {
            if(that.cancelledDrop) {
                ui.item.remove();
            }
            that.cancelledDrop = null;
        };
    };
    /**
     * Attaches the behavior to the {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDragDropSortBehavior.prototype.attachBehavior = function() {
        var $html,
            i,
            items,
            item,
            targetObject = this.targetObject;
        if(!this.behaviorAttached) {
            this.targetHTML = jQuery(this.targetObject.getContainmentArea());
            this.targetHTML.addClass('pmui-containeritembehavior-sort').sortable({
                appendTo: document.body,
                connectWith: '.pmui-containeritembehavior-sort',
                helper: 'clone',
                sort: this.onDrag(),
                start: this.onStart(),
                stop: this.onStop(),
                receive: this.onDrop(),
                out: this.onOut(),
                over: this.onOver(),
                update: this.onUpdate(),
                items: this.sortableItems,
                tolerance: 'pointer',
                placeholder: this.placeholderClass,
                handle: this.handle || false,
                zIndex: 1000
            });
            this.attachOnDraggableMouseEvents();
        }
        return ContainerItemDragDropSortBehavior.superclass.superclass.prototype.attachBehavior.call(this);
    };
    /**
     * Detaches the behavior from the 
     * {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDragDropSortBehavior.prototype.detachBehavior = function() {
        if(this.behaviorAttached) {
            try {
                this.targetHTML.removeClass('pmui-containeritembehavior-sort').sortable('destroy');
                this.detachOnDraggableMouseEvents();
            } catch(e) {}
        }
        return ContainerItemDragDropSortBehavior.superclass.prototype.detachBehavior.call(this);
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemDragDropSortBehavior', ContainerItemDragDropSortBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemDragDropSortBehavior;
    }
}());