(function () {
    var DataControlDragBehavior = function (settings) {
        DataControlDragBehavior.superclass.call(this, settings);
        this.targetObject = null;
        this.scope = null;
        this.helper = null;
        DataControlDragBehavior.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.behavior.DragBehavior', DataControlDragBehavior);

    DataControlDragBehavior.prototype.init = function (settings) {
        var defaults = {
            scope: 'pmui-datacontrol-dragdrop-scope',
            helper: 'original'
        };

        jQuery.extend(true, defaults, settings);

        //TODO: uncomment the following validation when the GridPanel's rows are handled through objects.
        //by now it will accept html elements as the targetObject option.
        /*if(!defaults.targetObject || typeof defaults.targetObject !== 'object') {
         throw new Error("The target object is null or invalid.");
         }*/

        if (!defaults.targetObject) {
            throw new Error("The target object is null or invalid.");
        }
        this.targetObject = defaults.targetObject;
        this.helper = defaults.helper;
        this.scope = defaults.scope;
    };

    DataControlDragBehavior.prototype.onReceiveHandler = function () {
        var targetObject = this.targetObject;
        return function (e, ui) {
            var row,
                index,
                newIndex,
                diff,
                globalIndex,
                grid,
                auxArray;
            row = ui.item.data("pmui-datacontrol-data");
            grid = row.parent;
            //remove the item from its previous grid
            index = grid.getRowIndex(row);
            grid.items.asArray().splice(index, 1);
            grid.goToPage(grid.currentPage);
            //append the object to the new grid
            newIndex = $(targetObject.dom.tbody).find('tr').index(ui.item);
            $(ui.item).detach();
            targetObject.addItem(row, newIndex);
            row.setCells();
        };
    };

    DataControlDragBehavior.prototype.onDropHandler = function () {
        return function (e, ui) {
            if (ui.item.parent().get(0) === this) {
                var row,
                    index,
                    newIndex,
                    diff,
                    globalIndex,
                    grid,
                    auxArray;
                row = ui.item.data("pmui-datacontrol-data");
                grid = row.parent;
                index = row.parent.indexInPage(row);
                newIndex = $(grid.dom.tbody).find('tr').index(ui.item);
                diff = newIndex - index;
                newIndex = index + diff;
                globalIndex = grid.getRowIndex(row);
                row = grid.items.asArray().splice(globalIndex, 1)[0];
                newIndex = globalIndex + diff;
                grid.items.asArray().splice(newIndex, 0, row);
            }
        };
    };

    DataControlDragBehavior.prototype.attachDragBehavior = function () {
        var $html,
            i,
            items,
            item,
            index,
            row,
            object = this.targetObject;
        items = this.targetObject.getItems();
        for (i = 0; i < items.length; i += 1) {
            item = $(items[i].getHTML()).data("pmui-datacontrol-data", (function (j) {
                return object.items.get(j);
            }(i)));
            if (!$html) {
                $html = jQuery(item);
            } else {
                $html = $html.add(item);
            }
        }

        try {
            $(this.targetObject.dom.tbody).sortable("destroy");
        } catch (e) {
        }

        $(this.targetObject.dom.tbody).sortable({
            connectWith: '.pmui-gridpanel-tbody',
            update: this.onDropHandler(),
            receive: this.onReceiveHandler()
        });

        return this;
    };

    PMUI.extendNamespace('PMUI.behavior.DataControlDragBehavior', DataControlDragBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = DataControlDragBehavior;
    }
}());