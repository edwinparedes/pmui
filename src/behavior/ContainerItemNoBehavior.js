(function() {
    /** 
     * @class PMUI.behavior.ContainerItemNoBehavior
     * @extends PMUI.behavior.ContainerItemBehavior
     */
    var ContainerItemNoBehavior = function(settings) {
        ContainerItemNoBehavior.superclass.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.behavior.ContainerItemBehavior', ContainerItemNoBehavior);
    /** [attachBehavior description] */
    ContainerItemNoBehavior.prototype.attachBehavior = function() {
        var $html,
            i,
            items,
            item,
            targetObject = this.targetObject;
        if(!this.behaviorAttached) {
            items = targetObject.getItems();
            for(i = 0; i < items.length; i += 1) {
                item = items[i].getHTML();
                if(!$html) {
                    $html = jQuery(item);
                } else {
                    $html = $html.add(item);
                }
            }

            try {
                $html.draggable('destroy');
            } catch(e) {}

            try {
                $(targetObject.getContainmentArea()).droppable('destroy');
            } catch(e) {}

            try {
                $(targetObject.getContainmentArea()).sortable('destroy');
            } catch(e) {}
        }
        return ContainerItemNoBehavior.superclass.prototype.attachBehavior.call(this);
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemNoBehavior', ContainerItemNoBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemNoBehavior;
    }
}());