(function() {
    /**
     * @class PMUI.behavior.ContainerItemDropBehavior
     * @extends PMUI.behavior.ContainerItemBehavior
     * Class that encapsulates the drop behavior for {@link PMUI.core.Container Container} objects. The object that 
     * applies this behavior will be able to accept draggable items from other Containers which ones are applying 
     * dragging behaviors ({@link PMUI.behavior.ContainerItemDragBehavior drag}, 
     * {@link PMUI.behavior.ContainerItemDragDropBehavior dragdrop}, 
     * {@link PMUI.behavior.ContainerItemDragSortBehavior dragsort}, 
     * {@link PMUI.behavior.ContainerItemDragDropSortBehavior dragdropsort}). .
     * 
     * Since this class is created through the 
     * {@link PMUI.behavior.ContainerItemBehaviorFactory ContainerItemBehaviorFactory} it shouldn't be instantiated in 
     * most cases (it is instantiated internally by the object of the Container class).
     * If you want to apply this behavior to a {@link PMUI.core.Container Container} object, just call its 
     * {@link PMUI.core.Container#method-setBehavior setBehavior()} method with the string parameter "drop" or set it 
     * at instantiation time using its {@link PMUI.core.Container#cfg-behavior behavior} config option.
     *
     * Usage example:
     *
     *      @example
     *      var tree = new PMUI.panel.TreePanel({
     *          items: [
     *              {
     *                  label: "draggable item #1"
     *              }, {
     *                  label: "draggable item #2"
     *              }, {
     *                  label: "draggable item #3"
     *              }, {
     *                  label: "draggable item #4"
     *              }, {
     *                  label: "draggable item #5"
     *              }, {
     *                  label: "draggable item #6"
     *              }
     *          ]
     *      }), tree2 = new PMUI.panel.TreePanel({
     *          style: {
     *              cssClasses: ["droppable-list"]
     *          },
     *          items: [
     *              {
     *                  label: 'Drop Here!'
     *              }
     *          ]
     *      });
     *
     *      tree.setBehavior("drag");
     *      tree2.setBehavior("drop");
     *      document.body.appendChild(tree.getHTML());
     *      //Add a separator
     *      document.body.appendChild(document.createElement('hr'));
     *      document.body.appendChild(tree2.getHTML());
     */
    var ContainerItemDropBehavior = function(settings) {
        ContainerItemDropBehavior.superclass.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.behavior.ContainerItemBehavior', ContainerItemDropBehavior);
    /**
     * @inheritdoc
     */
    ContainerItemDropBehavior.prototype.updateBehaviorAvailability = function() {
        this.targetHTML.droppable(this.disabled ? 'disable' : 'enable');
        return this;
    };
    /**
     * Defines the handler to be executed when an accepted draggable is dragged over the 
     * {@link #property-targetObject targetObject}.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemDropBehavior.prototype.onOver = function() {
        var that = this;
        return function(e, ui) {
            (that.targetObject.onDragOver()());
        };
    };
    /**
     * Defines the handler to be executed when an accepted draggable is dragged out of the 
     * {@link PMUI.behavior.ContainerItemDragBehavior#property-targetObject targetObject}.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemDropBehavior.prototype.onOut = function() {
        var that = this;
        return function(e, ui) {

        };
    };
    /**
     * Attaches the behavior to the {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDropBehavior.prototype.attachBehavior = function() {
        var $html,
            i,
            items,
            item,
            targetObject = this.targetObject;
        if(!this.behaviorAttached) {
            this.targetHTML = $(this.targetObject.getContainmentArea());
            this.targetHTML.droppable({
                scope: this.scope,
                drop: this.onDrop(),
                out: this.onOut(),
                over: this.onOver(),
                hoverClass: this.hoverClass,
                tolerance: 'pointer'
            });
        }
        return ContainerItemDropBehavior.superclass.prototype.attachBehavior.call(this);
    };
    /**
     * Detaches the behavior from the 
     * {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDropBehavior.prototype.detachBehavior = function() {
        if(this.behaviorAttached) {
            try {
                this.targetHTML.droppable('destroy');
            } catch(e) {}
        }
        return ContainerItemDropBehavior.superclass.prototype.detachBehavior.call(this);
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemDropBehavior', ContainerItemDropBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemDropBehavior;
    }
}());