(function() {
    var DataControlNoDropBehavior = function(settings) {
        DataControlNoDropBehavior.superclass.call(this, settings);
        this.targetObject = null;        
        DataControlNoDropBehavior.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.behavior.NoDropBehavior', DataControlNoDropBehavior);

    DataControlNoDropBehavior.prototype.init = function(settings) {
        if(!settings.targetObject) {
            throw new Error("The target object wasn't specified.");
        }

        this.targetObject = settings.targetObject;
    };

    DataControlNoDropBehavior.prototype.attachDropBehavior = function() {
        var html = this.targetObject.getHTML();

        try {
            $(html).droppable('destroy');
        } catch(e) {}

        return this;
    };

    PMUI.extendNamespace('PMUI.behavior.DataControlNoDropBehavior', DataControlNoDropBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = DataControlNoDropBehavior;
    }
}());