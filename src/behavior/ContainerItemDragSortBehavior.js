(function() {
    /**
     * @class PMUI.behavior.ContainerItemDragSortBehavior
     * @extends PMUI.behavior.ContainerItemDragBehavior
     * Class that encapsulates the drag and sort behaviors for a {@link PMUI.core.Container Container} object items. 
     * The items of the object that applies this behavior, will be able to:
     * 
     * - Be dragged to another Containers that are applying dropping behaviors
     * ({@link PMUI.behavior.ContainerItemDropBehavior drop}, 
     * {@link PMUI.behavior.ContainerItemDragDropBehavior dragdrop}, 
     * {@link PMUI.behavior.ContainerItemDropSortBehavior dropsort}, 
     * {@link PMUI.behavior.ContainerItemDragDropSortBehavior dragdropsort}). 
     * - Be sortable.
     * 
     * Since this class is created through the 
     * {@link PMUI.behavior.ContainerItemBehaviorFactory ContainerItemBehaviorFactory} it shouldn't be instantiated in 
     * most cases (it is instantiated internally by the object of the Container class).
     * If you want to apply this behavior to a {@link PMUI.core.Container Container} object, just call its 
     * {@link PMUI.core.Container#method-setBehavior setBehavior()} method with the string parameter "dragsort" or set it 
     * at instantiation time using its {@link PMUI.core.Container#cfg-behavior behavior} config option.
     *
     * Usage example:
     *
     *      @example
     *      var tree = new PMUI.panel.TreePanel({
     *          items: [
     *              {
     *                  label: "draggable item #1"
     *              }, {
     *                  label: "draggable item #2"
     *              }, {
     *                  label: "draggable item #3"
     *              }, {
     *                  label: "draggable item #4"
     *              }, {
     *                  label: "draggable item #5"
     *              }, {
     *                  label: "draggable item #6"
     *              }
     *          ]
     *      }), tree2 = new PMUI.panel.TreePanel({
     *          style: {
     *              cssClasses: ["droppable-list"]
     *          },
     *          items: [
     *              {
     *                  label: 'Drop Here!'
     *              }
     *          ]
     *      });
     *
     *      tree.setBehavior("dragsort");
     *      tree2.setBehavior("drop");
     *      document.body.appendChild(tree.getHTML());
     *      //Add a separator
     *      document.body.appendChild(document.createElement('hr'));
     *      document.body.appendChild(tree2.getHTML());
     */
    var ContainerItemDragSortBehavior = function(settings) {
        ContainerItemDragSortBehavior.superclass.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.behavior.ContainerItemDragBehavior', ContainerItemDragSortBehavior);
    /**
     * @inheritdoc
     */
    ContainerItemDragSortBehavior.prototype.updateBehaviorAvailability = function() {
        this.targetHTML.sortable(this.disabled ? 'disable' : 'enable');
        if(this.disabled) {
            this.detachOnDraggableMouseEvents();
        } else {
            this.attachOnDraggableMouseEvents();
        }
        return this;
    };
    /**
     * @inheritdoc
     */
    ContainerItemDragSortBehavior.prototype.onStart = function() {
        var that = this;
        return function(e, ui) {
            var targetObject, o;
            //Since the jQuery sortable's start callback is executed when the sorting starts and not neccesarily when
            //one of its own items starts dragging we need to do the next validation:
            //check if the item dragged belongs to the sortable html. In that case we fire the onDragStart callback.
            o = PMUI.getPMUIObject(ui.item.get(0));
            if(that.targetObject.isDirectParentOf(o)) {
                targetObject = that.targetObject;
                ui.helper.get(0).style.width = $(e.target).width() + "px";
                ui.item.hide();
                that.draggedObject = o;
                if(typeof targetObject.onDragStart === 'function') {
                    targetObject.onDragStart(targetObject, that.draggedObject);
                }
            }
        };
    };
    /**
     * Defines the handler to be executed when the user stopped sorting and the 
     * {@link #property-targetObject targetObject}'s items positions have 
     * changed.
     * @return {Function} The handler.
     * @private
     */
    ContainerItemDragSortBehavior.prototype.onUpdate = function() {
        var that = this;
        return function(e, ui) {
            var parentHTML = that.targetObject.getHTML(),
                item = PMUI.getPMUIObject(ui.item.get(0));

            if(item && jQuery(parentHTML).has(item.getHTML()).length) {
                (that.targetObject.onSortingChange()(e, ui));
            }
        };
    };
    /**
     * Attaches the behavior to the {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDragSortBehavior.prototype.attachBehavior = function() {
        var $html,
            i,
            items,
            item,
            targetObject = this.targetObject;
        if(!this.behaviorAttached) {
            this.targetHTML = jQuery(targetObject.getContainmentArea());
            this.targetHTML.sortable({
                appendTo: document.body,
                connectWith: '.pmui-containeritembehavior-sort',
                helper: 'clone',
                revert: "invalid",
                start: this.onStart(),
                update: this.onUpdate(),
                items: this.sortableItems,
                placeholder: this.placeholderClass,
                handle: this.handle || false,
                zIndex: 1000
            });
            this.attachOnDraggableMouseEvents();
        }
        return ContainerItemDragSortBehavior.superclass.superclass.prototype.attachBehavior.call(this);
    };
    /**
     * Detaches the behavior from the 
     * {@link #property-targetObject targetObject}.
     * @chainable
     */
    ContainerItemDragSortBehavior.prototype.detachBehavior = function() {
        if(this.behaviorAttached) {
            try {
                this.targetHTML.sortable('destroy');
            } catch(e) {}
        }
        return ContainerItemDragSortBehavior.superclass.prototype.detachBehavior.call(this);
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemDragSortBehavior', ContainerItemDragSortBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemDragSortBehavior;
    }
}());