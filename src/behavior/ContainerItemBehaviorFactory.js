(function() {
    /**
     * @class PMUI.behavior.ContainerItemBehaviorFactory
     * A factory that produces the behavior objects for {@link PMUI.core.Container Container} objects.
     *
     * It accepts the following pmTypes: 
     * 
     * - using "nobehavior" you get a {@link PMUI.behavior.ContainerItemNoBehavior ContainerItemNoBehavior object}.
     * - using "drag" you get a {@link PMUI.behavior.ContainerItemDragBehavior ContainerItemDragBehavior object}.
     * - using "drop" you get a {@link PMUI.behavior.ContainerItemDropBehavior ContainerItemDropBehavior object}.
     * - using "sort" you get a {@link PMUI.behavior.ContainerItemSortBehavior ContainerItemSortBehavior object}.
     * - using "dragdrop" you get a {@link PMUI.behavior.ContainerItemDragDropBehavior ContainerItemDragDropBehavior object}.
     * - using "dragsort" you get a {@link PMUI.behavior.ContainerItemDragSortBehavior ContainerItemDragSortBehavior object}.
     * - using "dropsort" you get a {@link PMUI.behavior.ContainerItemDropSortBehavior ContainerItemDropSortBehavior object}.
     * - using "dragdropsort" you get a {@link PMUI.behavior.ContainerItemDragDropSortBehavior ContainerItemDragDropSortBehavior object}.
     *
     * The default pmType is 'nobehavior'.
     * @extends {PMUI.util.Factory}
     */
    var ContainerItemBehaviorFactory = function(settings) {
        ContainerItemBehaviorFactory.superclass.call(this, settings);
        ContainerItemBehaviorFactory.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.util.Factory', ContainerItemBehaviorFactory);
    /**
     * Initializes the new instance.
     * @param  {Object} settings An object with the config options.
     * @private
     */
    ContainerItemBehaviorFactory.prototype.init = function(settings) {
        var defaults = {
            products: {
                'nobehavior': PMUI.behavior.ContainerItemNoBehavior,
                'drag': PMUI.behavior.ContainerItemDragBehavior,
                'dragclone': PMUI.behavior.ContainerItemDragCloneBehavior,
                'drop': PMUI.behavior.ContainerItemDropBehavior,
                'sort': PMUI.behavior.ContainerItemSortBehavior,
                'dragdrop': PMUI.behavior.ContainerItemDragDropBehavior,
                'dragsort': PMUI.behavior.ContainerItemDragSortBehavior,
                'dropsort': PMUI.behavior.ContainerItemDropSortBehavior,
                'dragdropsort': PMUI.behavior.ContainerItemDragDropSortBehavior
            },
            defaultProduct: 'nobehavior'
        };

        jQuery.extend(true, defaults, settings);

        this.setProducts(defaults.products)
            .setDefaultProduct(defaults.defaultProduct);
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemBehaviorFactory', ContainerItemBehaviorFactory);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemBehaviorFactory;
    }
}());