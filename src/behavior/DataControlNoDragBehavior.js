(function () {
    var DataControlNoDragBehavior = function (settings) {
        DataControlNoDragBehavior.superclass.call(this, settings);
        this.targetObject = null;
        DataControlNoDragBehavior.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.behavior.NoDragBehavior', DataControlNoDragBehavior);

    DataControlNoDragBehavior.prototype.init = function (settings) {
        //TODO: uncomment the following validation when the GridPanel's rows are handled through objects.
        //by now it will accept html elements as the targetObject option.
        /*if(!defaults.targetObject || typeof defaults.targetObject !== 'object') {
         throw new Error("The target object is null or invalid.");
         }*/
        this.targetObject = settings.targetObject;
    };

    DataControlNoDragBehavior.prototype.attachDragBehavior = function () {
        var $html,
            i,
            items,
            item;

        if (this.targetObject instanceof PMUI.panel.GridPanel) {
            html = $(this.targetObject.getHTML()).find('tr');
        } else {
            items = this.targetObject.getItems();
            for (i = 0; i < items.length; i += 1) {
                item = $(items[i].getHTML()).data("'pmui-datacontrol-data'", items[i]);
                if (!$html) {
                    $html = jQuery(item);
                } else {
                    $html = $html.add(item);
                }
            }
        }
        try {
            $(html).draggable("destroy");
        } catch (e) {
        }

        return this;
    };

    PMUI.extendNamespace('PMUI.behavior.DataControlNoDragBehavior', DataControlNoDragBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = DataControlNoDragBehavior;
    }
}());