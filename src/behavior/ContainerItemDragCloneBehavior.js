(function() {
    /**
     * @class PMUI.behavior.ContainerItemDragCloneBehavior
     * @extends PMUI.behavior.ContainerItemDragBehavior
     * Class that encapsulates the drag behavior for the items of a {@link PMUI.core.Container Container}. The object 
     * that applies this behavior will be able to create a clone item to be dragged to another Containers that are 
     * applying dropping behaviors
     * ({@link PMUI.behavior.ContainerItemDropBehavior drop}, 
     * {@link PMUI.behavior.ContainerItemDragDropBehavior dragdrop}, 
     * {@link PMUI.behavior.ContainerItemDropSortBehavior dropsort}, 
     * {@link PMUI.behavior.ContainerItemDragDropSortBehavior dragdropsort}).
     * 
     * Since this class is created through the 
     * {@link PMUI.behavior.ContainerItemBehaviorFactory ContainerItemBehaviorFactory} it shouldn't be instantiated in 
     * most cases (it is instantiated internally by the object of the Container class).
     * If you want to apply this behavior to a {@link PMUI.core.Container Container} object, just call its 
     * {@link PMUI.core.Container#method-setBehavior setBehavior()} method with the string parameter "dragclone" or set it 
     * at instantiation time using its {@link PMUI.core.Container#cfg-behavior behavior} config option.
     *
     * Usage example:
     *
     *      @example
     *      var tree = new PMUI.panel.TreePanel({
     *          items: [
     *              {
     *                  label: "draggable item #1"
     *              }, {
     *                  label: "draggable item #2"
     *              }, {
     *                  label: "draggable item #3"
     *              }, {
     *                  label: "draggable item #4"
     *              }, {
     *                  label: "draggable item #5"
     *              }, {
     *                  label: "draggable item #6"
     *              }
     *          ]
     *      }), tree2 = new PMUI.panel.TreePanel({
     *          style: {
     *              cssClasses: ["droppable-list"]
     *          },
     *          items: [
     *              {
     *                  label: 'Drop Here!'
     *              }
     *          ]
     *      });
     *
     *      tree.setBehavior("dragclone");
     *      tree2.setBehavior("drop");
     *      document.body.appendChild(tree.getHTML());
     *      //Add a separator
     *      document.body.appendChild(document.createElement('hr'));
     *      document.body.appendChild(tree2.getHTML());
     */
    var ContainerItemDragCloneBehavior = function(settings) {
        ContainerItemDragCloneBehavior.superclass.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.behavior.ContainerItemDragBehavior', ContainerItemDragCloneBehavior);

    /**
     * @inheritdoc
     */
    ContainerItemDragCloneBehavior.prototype.onStart = function() {
        var that = this;
        return function(e, ui) {
            var targetObject = that.targetObject;
            ui.helper.get(0).style.width = $(e.target).width() + "px";
            that.draggedObject = PMUI.getPMUIObject(this);
            if(typeof targetObject.onDragStart === 'function') {
                targetObject.onDragStart(targetObject, that.draggedObject);
            }
        };
    };
    /**
     * @inheritodc
     */
    ContainerItemDragCloneBehavior.prototype.onDropOut = function(item, destiny, index) {
        var targetObject = this.targetObject;
        data = item.getData();
        destiny.addDataItem(data, index);
        if(typeof targetObject.onDropOut === 'function') {
            targetObject.onDropOut(item, targetObject, destiny);
        }
        return this;
    };

    PMUI.extendNamespace('PMUI.behavior.ContainerItemDragCloneBehavior', ContainerItemDragCloneBehavior);

    // Declarations created to instantiate in NodeJS environment
    if (typeof exports !== "undefined") {
        module.exports = ContainerItemDragCloneBehavior;
    }
}());