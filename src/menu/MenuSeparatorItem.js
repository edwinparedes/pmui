(function () {
    var MenuSeparatorItem = function (settings) {
        MenuSeparatorItem.superclass.call(this, jQuery.extend(true, settings, {
            elementTag: 'div'
        }));
        MenuSeparatorItem.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.menu.MenuItem', MenuSeparatorItem);

    MenuSeparatorItem.prototype.type = 'MenuSeparatorItem';

    MenuSeparatorItem.prototype.isLeaf = function () {
        return true;
    };

    PMUI.extendNamespace('PMUI.menu.MenuSeparatorItem', MenuSeparatorItem);
}());