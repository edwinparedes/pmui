(function () {
    var MenuOption = function (settings) {
        MenuOption.superclass.call(this, settings);
        this.onClick = null;
        this.text = null;
        this.hideOnClick = null;
        this.disabled = null;
        MenuOption.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.menu.MenuItem', MenuOption);

    MenuOption.prototype.type = 'MenuOption';

    MenuOption.prototype.init = function (settings) {
        var defaults = {
            onClick: null,
            elementTag: 'li',
            text: '[option]',
            hideOnClick: true,
            disabled: false
        };

        jQuery.extend(true, defaults, settings);
        this.setElementTag(defaults.elementTag)
            .setText(defaults.text)
            .setOnClickHandler(defaults.onClick)
            .hideOnClick = !!defaults.hideOnClick;

        if (defaults.disabled) {
            this.disable();
        } else {
            this.enable();
        }
    };

    MenuOption.prototype.enable = function () {
        this.disabled = false;
        this.style.removeClasses(['pmui-disabled']);
        return this;
    };

    MenuOption.prototype.disable = function () {
        this.disabled = true;
        this.style.addClasses(['pmui-disabled']);
        return this;
    };

    MenuOption.prototype.setText = function (text) {
        if (typeof text !== 'string') {
            throw new Error('setText(): the parameter must be a srting.');
        }
        this.text = text;
        if (this.dom.textContainer) {
            this.dom.textContainer.textContent = text;
        }
        return this;
    };

    MenuOption.prototype.setOnClickHandler = function (onClick) {
        if (!(onClick === null || typeof onClick === 'function')) {
            throw new Error('setOnClickHandler(): The parameter must be a function or null.');
        }
        this.onClick = onClick;
        return this;
    };

    MenuOption.prototype.onClickHandler = function () {
        var that = this;

        return function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (!that.disabled) {
                if (typeof that.onClick === 'function') {
                    that.onClick(that);
                }
                if (that.hideOnClick) {
                    that.parent.hide();
                }
            }
        };
    };

    MenuOption.prototype.remove = function () {
        this.parent.removeItem(this);
        return this;
    };

    MenuOption.prototype.defineEventListeners = function () {
        this.removeEvents();
        this.addEvent('click').listen(this.dom.title, this.onClickHandler());
        return this;
    };

    MenuOption.prototype.createHTML = function () {
        var link,
            textContainer,
            iconContainer;

        if (this.html) {
            return this.html;
        }
        MenuOption.superclass.prototype.createHTML.call(this);

        link = PMUI.createHTMLElement('a');
        link.href = '#';
        link.className = 'pmui-menuoption-title';

        textContainer = PMUI.createHTMLElement('span');
        textContainer.className = 'pmui-menuoption-text';

        iconContainer = PMUI.createHTMLElement('i');
        iconContainer.className = 'pmui-menuoption-text-icon';

        this.dom.title = link;
        this.dom.textContainer = textContainer;
        this.dom.iconContainer = iconContainer;

        link.appendChild(iconContainer);
        link.appendChild(textContainer);
        this.html.appendChild(link);

        this.setText(this.text);

        this.defineEventListeners();

        return this.html;
    };

    PMUI.extendNamespace('PMUI.menu.MenuOption', MenuOption);
}());