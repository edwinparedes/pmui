(function () {
    var MenuItemFactory = function (settings) {
        MenuItemFactory.superclass.call(this, settings);
        MenuItemFactory.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.util.Factory', MenuItemFactory);

    MenuItemFactory.prototype.type = "MenuItemFactory";

    MenuItemFactory.prototype.init = function (settings) {
        var defaults = {
            products: {
                "menuRegularOption": PMUI.menu.MenuRegularOption,
                "menuSeparatorItem": PMUI.menu.MenuSeparatorItem
            },
            defaultProduct: "menuRegularOption"
        };

        this.setDefaultProduct(defaults.defaultProduct)
            .setProducts(defaults.products);
    };

    PMUI.extendNamespace('PMUI.menu.MenuItemFactory', MenuItemFactory);
}());