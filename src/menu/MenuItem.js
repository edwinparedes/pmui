(function () {
    var MenuItem = function (settings) {
        MenuItem.superclass.call(this, settings);
        this.dom = {};
        this.parent = null;
        MenuItem.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Element', MenuItem);

    MenuItem.prototype.type = 'MenuItem';

    MenuItem.prototype.init = function (settings) {
        var defaults = {
            parent: null
        }

        jQuery.extend(true, defaults, settings);

        this.setParent(defaults.parent);
    };

    MenuItem.prototype.setParent = function (parent) {
        if (!(parent === null || parent instanceof PMUI.menu.Menu)) {
            throw new Error('setParent(): The parameter must be an instance of PMUI.ui.Menu or null.');
        }
        this.parent = parent;
        return this;
    };

    MenuItem.prototype.getParent = function () {
        return this.parent;
    };

    MenuItem.prototype.getRootMenu = function () {
        var parent = this.parent;

        if (this.parent) {
            return this.parent.getRootMenu();
        }
        return parent;
    };

    MenuItem.prototype.isLeaf = function () {
        throw new Error("isLeaf() is being called from an abstract class.");
    };

    MenuItem.prototype.getMenu = function () {
        return this.parent;
    };

    MenuItem.prototype.setContextMenu = function () {
        return this;
    };

    MenuItem.prototype.getMenuTargetElement = function () {
        var rootMenu = this.getRootMenu();

        if (rootMenu) {
            return rootMenu.getTargetElement();
        }
        return rootMenu
    };

    MenuItem.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }
        MenuItem.superclass.prototype.createHTML.call(this);
        return this.html;
    };

    PMUI.extendNamespace('PMUI.menu.MenuItem', MenuItem);

}());