//***********************other class******************************************************//
describe('PMUI.draw.SegmentMoveHandler', function () {
	var segmentMove, segment, rectangle;
	beforeEach(function () {
	 	segment = new PMUI.draw.Segment();
	 	rectangle = new PMUI.draw.Rectangle();
	 	segmentMoveHandler = new PMUI.draw.SegmentMoveHandler({
               width: 8,
               height: 8,
               parent: segment,
               orientation: 0, 
               representation: rectangle,
               color: new PMUI.util.Color(255, 0, 0)  
           });
	});
	afterEach(function () {
	});
	describe('method "paint"', function () {
		it("should Paints this resize handler by calling it's parent's paint and setting the visibility of this resize handler", function () {
            var v = segmentMoveHandler.visible;
            segmentMoveHandler.createHTML();
            segmentMoveHandler.paint();
            expect(segmentMoveHandler.visible == v).toBeTruthy();
		});
	})	
});