//***********************other class******************************************************//
describe('PMUI.core.MenuItemElement', function () {
	var a, b;
	beforeEach(function () {
		a = new PMUI.core.MenuItemElement(
											{
												text: "item1",
            									className: "pmui-menu-item-element",
            									classNameDisabled: "pmui-menu-item-element-disabled",
            									toolTip: null,
            									disabled: false,
            									focused: false,
            									icon: 'pmui-menu-icon-empty'
											}
			);
		document.body.appendChild(a.getHTML());
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
		b = null;
	});
	describe('method "setText"', function () {
		it('should set the menuItemElement text', function () {
			expect(a.text === "item1").toBeTruthy();
			a.setText("new_text");
			expect(a.text === "new_text").toBeTruthy();
		});
	});
	describe('method "setClassName"', function () {
		it('should set the className', function () {
			expect(a.className === "pmui-menu-item-element").toBeTruthy();
			a.setClassName("pmui-menu-item-element1");
			expect(a.className === "pmui-menu-item-element1").toBeTruthy();
		});
	});
	describe('method "setFloating"', function () {
		it('should set the floating property', function () {
			expect(a.floating).toBeTruthy();
			a.setFloating(false);
			expect(a.floating).toBeFalsy();
			expect(a.positionMode).toEqual("absolute");
			expect(a.visible).toBeFalsy();
		});
	});
	describe('method "setListeners"', function () {
		it('should set listeners for the menuItemElement', function () {
			a.setListeners({click: function(){}});
			expect(typeof a.listeners === "object").toBeTruthy();
			expect(typeof a.listeners.click === "function").toBeTruthy();
		});
	});
	describe('method "setClassNameDisabled"', function () {
		it('should set className property', function () {
			expect(a.classNameDisabled === "pmui-menu-item-element-disabled").toBeTruthy();
			a.setClassNameDisabled("pmui-menu-item-element-disabled_new");
			expect(a.classNameDisabled === "pmui-menu-item-element-disabled_new").toBeTruthy();
		});
	});
	describe('method "setParent"', function () {
		it('should set the parent for the menuItemElement', function () {
			expect(a.parent).toBeNull();
			a.setParent(new PMUI.core.Item());
			expect(a.parent).not.toBeNull();
			expect(typeof a.parent === 'object').toBeTruthy();
		});
	});
	describe('method "setIcon"', function () {
		it('should set the className porperty', function () {
			expect(a.icon === 'pmui-menu-icon-empty').toBeTruthy();
			a.setIcon('pmui-menu-icon-full');
			expect(a.icon === 'pmui-menu-icon-full').toBeTruthy();
		});
	});
	describe('method "setDisabled"', function () {
		it('should set the disabled property, it must be a boolean value', function () {
			expect(a.disabled).toBeFalsy();
			a.setDisabled(true);
			expect(a.disabled).toBeTruthy();
		});
	});
	describe('method "setFocused"', function () {
		it('should set focused porperty, it must be a boolean vaule', function () {
			expect(a.focused).toBeFalsy();
			a.setFocused(true);
			expect(a.focused).toBeTruthy();
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});
