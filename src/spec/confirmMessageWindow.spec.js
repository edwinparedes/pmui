//***********************other class******************************************************//
describe('PMUI.ui.ConfirmMessageWindow', function () {
	var a, x = 1, y = 1;
	beforeEach(function () {
		a = new PMUI.ui.ConfirmMessageWindow(
											{
                								title: 'ConfirmMessage',
                								yesText: 'Yes',
                								noText: 'No',
                								onYes: function () {x = x + 1},
                								onNo: function () {y = y + 1}
            								}
				);
		a.open();
	});
	afterEach(function () {
		a = null;
	});
	describe('class behavior', function () {
		it('[US-58,e]should be able to show a message and two handlers for yes and no options', function () {
			expect(a.title === "ConfirmMessage").toBeTruthy();
			expect($(a.html).find('div[class="pmui-window-header"]').text()).toContain("ConfirmMessage");
			$(a.html).find('span[class="pmui-button-label"]').slice(1).trigger('click');
			expect(x === 2).toBeTruthy();
			expect(y === 2).toBeTruthy();
			$(a.html).find('span[class="pmui-button-label"]').slice(1).trigger('click');
			expect(x === 3).toBeTruthy();
			expect(y === 3).toBeTruthy();
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});