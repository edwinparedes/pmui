//***********************other class******************************************************//
describe('PMUI.ui.Button', function () {
    var a, b;
    beforeEach(function () {
        a = new PMUI.ui.Button({
                    handler : function () {        
                    },
                    text : 'closed',
                    iconClass:"pmui-gridpanel-pager_previous",
                    iconPosition : "left",
                    messageTooltip : 'this button to close the window'
        });
    });
    describe('class behavior', function () {
        it('[US-16,d]should create a new button object with default properties', function () {
            expect(a.icon === "pmui-gridpanel-pager_previous").toBeTruthy();
            expect(a.aliasButton === null).toBeTruthy();
            expect(a.height === "auto").toBeTruthy();
            expect(a.text === "closed").toBeTruthy();
        });
        it('[US-16,f]should create a new HTML element', function () {
            var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
        });
        it('[US-16,e]should set a new text for a button object', function () {
            expect(a.text === "closed").toBeTruthy();
            a.setText("new_value_button");
            expect(a.text === "new_value_button").toBeTruthy();
        });
        it('[US-16,e]should set a new icon different from null to a button object', function () {
            expect(a.icon === "pmui-gridpanel-pager_previous").toBeTruthy();
            a.setIcon("icon1");
            expect(a.icon === "icon1").toBeTruthy();
        });
        it('[US-16,e]should set a new alias different from null to a button object', function () {
            expect(a.aliasButton === null).toBeTruthy();
            a.setAliasButton("button1");
            expect(a.aliasButton === "button1").toBeTruthy();
        });
    });/*
    describe('method "show"', function () {
        it('should show the button already created', function () {
            a = new PMUI.ui.Button();
            b = a.show();
            expect(b).toBeDefined();
            expect(b.tagName).toBeDefined();
            expect(b.nodeType).toBeDefined();
            expect(b.nodeType).toEqual(document.ELEMENT_NODE);
        });

    });*/
    describe('method "setButtonClass"', function () {
        it('should set the button class', function () {
            expect(typeof a.buttonClass === "string").toBeTruthy();
            expect(a.buttonClass === "").toBeTruthy();
            a.setButtonClass("any_class");
            expect(a.buttonClass === "any_class").toBeTruthy();
        });
    });
    describe('method "setHandler"', function () {
        it('should set a handler for a button', function () {
            var c = new PMUI.ui.Button({
                    text: "button",
                    style: {
                        cssClasses: [
                        'mafe-toolbar-button-sprite',
                        'mafe-trigger'
                        ]
                    },
                });

            document.body.appendChild(c.getHTML());
            c.setHandler(function(){c.setText("button2")});
            $(c).trigger('click');
            expect(c.text === "button2");
            $(c.getHTML()).remove();
            c = null;
        });
    });
    describe('method "setParent"', function () {
        it('should set the parent for the button', function () {
            var f = new PMUI.form.Form();
            document.body.appendChild(f.getHTML());
            html = f.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
            a.setParent(f.html);
            expect(a.parent).toBeDefined();
            expect(a.parent.tagName).toBeDefined();
            expect(a.parent.nodeType).toBeDefined();
            expect(a.parent.nodeType).toEqual(document.ELEMENT_NODE);
        });
    });
    describe('method "setHeight"', function () {
        it('should set height for the button', function () {
            document.body.appendChild(a.getHTML());
            a.setHeight(50);
            expect(a.height === "50px").toBeTruthy();
            expect(a.html.style.lineHeight === "50px").toBeTruthy();
        });
    });
    describe('method "setIconPosition"', function () {
        it('should set the icon position, it must be a string("left" or "right")', function () {
            a = new PMUI.ui.Button({
                    handler : function () {
                        alert('hola mundo');     
                    },
                    text : 'button',
                    iconClass:"pmui-gridpanel-pager_previous",
                    messageTooltip : 'this button to close the window'
                });
            document.body.appendChild(a.getHTML());
            expect(function () {a.setIconPosition("left");}).not.toThrow();
            expect(a.iconPosition === "left").toBeTruthy();
            expect(function () {a.setIconPosition("right");}).not.toThrow();
            expect(a.iconPosition === "right").toBeTruthy();
            expect(function () {a.setIconPosition("top");}).toThrow();
            $(a.getHTML()).remove();
            a = null;
        });
    });
    describe('method "setMessageTooltip"', function () {
        it('should set the tooltip message for the button', function () {
            a = new PMUI.ui.Button({
                    handler : function () {
                        alert('hola mundo');     
                    },
                    text : 'button',
                    iconClass:"pmui-gridpanel-pager_previous",
                    messageTooltip : 'this button to close the window'
                });
            document.body.appendChild(a.getHTML());
            a.setMessageTooltip("this is a button");
            expect(a.messageTooltip === "this is a button").toBeTruthy();
        });
    });
});

