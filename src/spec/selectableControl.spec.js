//***********************other class******************************************************//
describe('PMUI.control.SelectableControl', function () {
	var a, b;
	beforeEach(function () {
		a = new PMUI.control.SelectableControl({
			mode: "checkbox",
			label: "option1",
			selected: false,
			value: "opt1",
			name: "idopt" 
		});
		document.body.appendChild(a.getHTML());
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
	});
	describe('class behavior', function () {
		it('[US-26,a]should be able to set the selectable control type(radio/checkbox)', function () {
			expect(a.mode === "checkbox").toBeTruthy();
			expect($(a.getHTML()).find('input[type="checkbox"]').length).not.toEqual(0);
			b = new PMUI.control.SelectableControl({
				mode: "radio",
				label: "option1",
				selected: false	
			});
			document.body.appendChild(b.getHTML());
			expect(b.mode === "radio").toBeTruthy();
			expect($(b.getHTML()).find('input[type="radio"]').length).not.toEqual(0);
		});
		it('[US-26,b]should be able to set the value and the name', function () {
			expect(a.value === "opt1").toBeTruthy();
			expect($(a.getHTML()).find('input[value="opt1"]').length).not.toEqual(0);
			expect(a.name === "idopt").toBeTruthy();
			expect($(a.getHTML()).find('input[name="idopt"]').length).not.toEqual(0);
			a.setValue("new_opt");
			expect(a.value === 'new_opt').toBeTruthy();
			expect($(a.getHTML()).find('input[value="new_opt"]').length).not.toEqual(0);
			a.setName("new_id");
			expect(a.name === "new_id").toBeTruthy();
			expect($(a.getHTML()).find('input[name="new_id"]').length).not.toEqual(0);
		});
		it('[US-26,c]should set the checked property', function () {
			expect(a.selected).toBeFalsy();
			a.selected = true;
			expect(a.selected).toBeTruthy();
		});
		it('[US-26,d]should set the disabled property', function () {
			expect(a.disabled).toBeFalsy();
			a.disable(true);
			expect(a.disabled).toBeTruthy();
		});
	});
	describe('method "setLabel"', function () {
		it('should set the label for the SelectableControl', function () {
			expect(a.label === "option1").toBeTruthy();
			a.setLabel("new_label");
			expect(a.label === "new_label").toBeTruthy();
		});
	});
	describe('method "select"', function () {
		it('should set selected property to the SelectableControl', function () {
			expect(a.selected).toBeFalsy();
			a.select();
			expect(a.selected).toBeTruthy();
		});
	});
	describe('method "deselect"', function () {
		it('should set deselected property to the SelectableControl', function () {
			expect(a.selected).toBeFalsy();
			a.select();
			expect(a.selected).toBeTruthy();
			a.deselect();
			expect(a.selected).toBeFalsy();
		});
	});
	describe('method "selected"', function () {
		it('should get the value of selected property from the SelectableControl', function () {
			expect(a.selected).toBeFalsy();
			a.select();
			expect(a.selected).toBeTruthy();
			a.deselect();
			expect(a.selected).toBeFalsy();
		});
	});
	/*describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});*/
});