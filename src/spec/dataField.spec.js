//***********************other class******************************************************//
describe('PMUI.data.DataField', function () {
	var f, g, i;
	describe('class behavior', function () {
		it('[US-23,b]Should create a new DataField object and set the type of value', function () {
			f = new PMUI.data.DataField();
			expect(f.typeField === "string").toBeTruthy();
			f.setType("number");
			expect(f.typeField == "number").toBeTruthy();
			expect(f.custom).toBeFalsy();
			f.setType("date");
			expect(f.typeField == "date").toBeTruthy();
			expect(f.custom).toBeFalsy();
			f.setType("boolean");
			expect(f.typeField == "boolean").toBeTruthy();
			expect(f.custom).toBeFalsy();
			f.setType("object");
			expect(f.typeField == "object").toBeTruthy();
			expect(f.custom).toBeFalsy();
			f.setType("custom_type");
			expect(f.typeField == "custom_type").toBeTruthy();
			expect(f.custom).toBeTruthy();
		});
		it('[US-23,c]Should create a new DataField object and get its value', function () {
			i = new PMUI.data.DataItem();
			i.set("id","val1");
			f = new PMUI.data.DataField(i);
			f.setValue("value_for_field");
			expect(f.getRecord().key === "id").toBeTruthy();
			expect(f.getRecord().value === "value_for_field").toBeTruthy();
			expect(f.getRecord().type === "DataItem").toBeTruthy();
		});
	});
});