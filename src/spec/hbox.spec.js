	//***********************other class******************************************************//
describe('PMUI.layout.HBox', function () {
	var a, b, c, p, q, r, t;
	beforeEach(function () {
		a = new PMUI.grid.GridPanel(
                        {
                            draggable: true,
                            pageSize:10,
                            columns:[
                                {
                                    title:'columna1',
                                    dataType:'string',
                                    columnData: "name"
                                },
                                {
                                    title:'columna2',
                                    dataType:'number',
                                    width : 150,
                                    columnData: "lastName",
                                    sortable: true
                                },
                                {
                                    title:'columna3',
                                    dataType:'button',
                                    width : "200px",
                                    onButtonClick: function(row, grid) {
                                        console.log(row, grid);
                                    }
                                }
                            ],
                            items: [
                                {
                                    name: "Daniel",
                                    lastName: "Canedo"
                                }, {
                                    name: "John",
                                    lastName: "McAllister"
                                }, {
                                    name: "Ximena",
                                    lastName: "Jimenez"
                                }, {
                                    name: "Fernando",
                                    lastName: "Fernandez"
                                }, {
                                    name: "Rodrigo",
                                    lastName: "Rodriguez"
                                }, {
                                    name: "Andrea",
                                    lastName: "Postigo"
                                }, {
                                    name: "Hernando",
                                    lastName: "Hernandez"
                                }, {
                                    name: "Ramiro",
                                    lastName: "Ramirez"
                                }, {
                                    name: "Pedro",
                                    lastName: "Pedraza"
                                }, {
                                    name: "Domingo",
                                    lastName: "Dominguez"
                                }, {
                                    name: "Oliva",
                                    lastName: "Olivares"
                                }/*, {
                                    name: "Gonzalo",
                                    lastName: "Gonzales"
                                }, {
                                    name: "Enrique",
                                    lastName: "Enriquez"
                                }*/
                            ]                        
                        }
                    );
        b = new PMUI.grid.GridPanel(
                        {
                            draggable: true,
                            pageSize:10,
                            columns:[
                                {
                                    title:'columna1',
                                    dataType:'string',
                                    columnData: "name"
                                },
                                {
                                    title:'columna2',
                                    dataType:'number',
                                    width : 150,
                                    columnData: "lastName",
                                    sortable: true
                                },
                                {
                                    title:'columna3',
                                    dataType:'button',
                                    width : "200px",
                                    onButtonClick: function(row, grid) {
                                        console.log(row, grid);
                                    }
                                }
                            ],
                            items: [
                                {
                                    name: "Daniel",
                                    lastName: "Canedo"
                                }, {
                                    name: "John",
                                    lastName: "McAllister"
                                }, {
                                    name: "Ximena",
                                    lastName: "Jimenez"
                                }, {
                                    name: "Fernando",
                                    lastName: "Fernandez"
                                }, {
                                    name: "Rodrigo",
                                    lastName: "Rodriguez"
                                }, {
                                    name: "Andrea",
                                    lastName: "Postigo"
                                }, {
                                    name: "Hernando",
                                    lastName: "Hernandez"
                                }, {
                                    name: "Ramiro",
                                    lastName: "Ramirez"
                                }, {
                                    name: "Pedro",
                                    lastName: "Pedraza"
                                }, {
                                    name: "Domingo",
                                    lastName: "Dominguez"
                                }, {
                                    name: "Oliva",
                                    lastName: "Olivares"
                                }/*, {
                                    name: "Gonzalo",
                                    lastName: "Gonzales"
                                }, {
                                    name: "Enrique",
                                    lastName: "Enriquez"
                                }*/
                            ]                        
                        }
                    );
        c = new PMUI.grid.GridPanel(
                        {
                            draggable: true,
                            pageSize:10,
                            columns:[
                                {
                                    title:'columna1',
                                    dataType:'string',
                                    columnData: "name"
                                },
                                {
                                    title:'columna2',
                                    dataType:'number',
                                    width : 150,
                                    columnData: "lastName",
                                    sortable: true
                                },
                                {
                                    title:'columna3',
                                    dataType:'button',
                                    width : "200px",
                                    onButtonClick: function(row, grid) {
                                        console.log(row, grid);
                                    }
                                }
                            ],
                            items: [
                                {
                                    name: "Daniel",
                                    lastName: "Canedo"
                                }, {
                                    name: "John",
                                    lastName: "McAllister"
                                }, {
                                    name: "Ximena",
                                    lastName: "Jimenez"
                                }, {
                                    name: "Fernando",
                                    lastName: "Fernandez"
                                }, {
                                    name: "Rodrigo",
                                    lastName: "Rodriguez"
                                }, {
                                    name: "Andrea",
                                    lastName: "Postigo"
                                }, {
                                    name: "Hernando",
                                    lastName: "Hernandez"
                                }, {
                                    name: "Ramiro",
                                    lastName: "Ramirez"
                                }, {
                                    name: "Pedro",
                                    lastName: "Pedraza"
                                }, {
                                    name: "Domingo",
                                    lastName: "Dominguez"
                                }, {
                                    name: "Oliva",
                                    lastName: "Olivares"
                                }/*, {
                                    name: "Gonzalo",
                                    lastName: "Gonzales"
                                }, {
                                    name: "Enrique",
                                    lastName: "Enriquez"
                                }*/
                            ]                        
                        }
                    );
        d = new PMUI.grid.GridPanel(
                        {
                            draggable: true,
                            pageSize:10,
                            columns:[
                                {
                                    title:'columna1',
                                    dataType:'string',
                                    columnData: "name"
                                },
                                {
                                    title:'columna2',
                                    dataType:'number',
                                    width : 150,
                                    columnData: "lastName",
                                    sortable: true
                                },
                                {
                                    title:'columna3',
                                    dataType:'button',
                                    width : "200px",
                                    onButtonClick: function(row, grid) {
                                        console.log(row, grid);
                                    }
                                }
                            ],
                            items: [
                                {
                                    name: "Daniel",
                                    lastName: "Canedo"
                                }, {
                                    name: "John",
                                    lastName: "McAllister"
                                }, {
                                    name: "Ximena",
                                    lastName: "Jimenez"
                                }, {
                                    name: "Fernando",
                                    lastName: "Fernandez"
                                }, {
                                    name: "Rodrigo",
                                    lastName: "Rodriguez"
                                }, {
                                    name: "Andrea",
                                    lastName: "Postigo"
                                }, {
                                    name: "Hernando",
                                    lastName: "Hernandez"
                                }, {
                                    name: "Ramiro",
                                    lastName: "Ramirez"
                                }, {
                                    name: "Pedro",
                                    lastName: "Pedraza"
                                }, {
                                    name: "Domingo",
                                    lastName: "Dominguez"
                                }, {
                                    name: "Oliva",
                                    lastName: "Olivares"
                                }/*, {
                                    name: "Gonzalo",
                                    lastName: "Gonzales"
                                }, {
                                    name: "Enrique",
                                    lastName: "Enriquez"
                                }*/
                            ]                        
                        }
                    );
                    
        e= new PMUI.core.Panel({
                        layout:"hbox",
                       
                        
                    });
                    e.addItem(a);
                    e.addItem(b);
                    e.addItem(c);
                    e.addItem(d);
		document.body.appendChild(e.getHTML());
		//a.defineEvents();
	});
	afterEach(function () {
		//$(a.getHTML()).remove();
		//a = null;
	});
	describe('class behavior', function () {
		
	});
	describe('method "applyLayout"', function () {
		it('available objects horizontally', function () {
			var html = e.html;
			expect(html).toBeDefined();
		    expect(e.items).toBeDefined();
		    expect(e.items.asArray().length).toBeTruthy();
            expect(e.items.asArray()[0].html.style.display == "inline-block").toBeTruthy();
            expect(e.items.asArray()[1].html.style.display == "inline-block").toBeTruthy();
            expect(e.items.asArray()[2].html.style.display == "inline-block").toBeTruthy();
            expect(e.items.asArray()[3].html.style.display == "inline-block").toBeTruthy();
            e.setLayout("vbox");
            expect(e.items.asArray()[0].html.style.display == "block").toBeTruthy();
            expect(e.items.asArray()[1].html.style.display == "block").toBeTruthy();
            expect(e.items.asArray()[2].html.style.display == "block").toBeTruthy();
            expect(e.items.asArray()[3].html.style.display == "block").toBeTruthy();
            


		});
	});
	
});