//***********************other class******************************************************//
describe('PMUI.grid.GridPanelColumn', function () {
    var a, b, grid;
    beforeEach(function () {
        grid = new PMUI.grid.GridPanel({   draggable:true,
                                        filterable : true,
                                        pageSize:6,
                                        columns:[
                                            {
                                                title:'name',
                                                dataType:'string',
                                                columnData:'name' 
                                            },
                                            {
                                                title:'lastName',
                                                dataType:'string',
                                                columnData:'lastName',
                                                sortable:true
                                            },
                                            {
                                                title:'columna3',
                                                dataType:'button',
                                                width:'200px',
                                                onButtonClick: function (row, grid) {
                                                    console.log(row,grid);
                                                }
                                            }
                                        ],
                                        dataItems: [
                                            {
                                                name:'Carlos',
                                                lastName:'Alvarez'

                                            },
                                            {
                                                name:'Sergio',
                                                lastName:'Benitez'
                                            },
                                            {
                                                name:'Rodrigo',
                                                lastName:'Garcia'
                                            },
                                            {
                                                name:'Alvaro',
                                                lastName:'Gonzales'
                                            },
                                            {
                                                name:'Fernando',
                                                lastName:'Antezana'
                                            },
                                            {
                                                name:'Orlando',
                                                lastName:'Mendoza'
                                            },
                                            {
                                                name:'Maria',
                                                lastName:'Perez'
                                            }
                                        ]
                                        });
        document.body.appendChild(grid.getHTML());
        grid.defineEvents();
        a = grid.getColumns()[0];
    });
    afterEach(function () {
        $(grid.getHTML()).remove();
        grid = null;
        a = null;
        b = null;
    });
    describe('method "setAlignment"', function () {
        it('should set the alignment of the column', function () {
            expect(function () {a.setAlignment(234)}).toThrow();
            expect(function () {a.setAlignment({data: "data1"})}).toThrow();
            expect(function () {a.setAlignment(new PMUI.event.Event())}).toThrow();
            expect(function () {a.setAlignment("north")}).toThrow();
            expect(function () {a.setAlignment('center')}).not.toThrow();
            expect(a.alignment).toEqual('center');
            expect(function () {a.setAlignment('left')}).not.toThrow();
            expect(a.alignment).toEqual('left');
            expect(function () {a.setAlignment('right')}).not.toThrow();
            expect(a.alignment).toEqual('right');
        });
    });
    describe('method "getAlignment"', function () {
        it('should return the alignment of the column', function () {
            a.setAlignment('center');
            expect(a.getAlignment()).toEqual('center');
            a.setAlignment('left');
            expect(a.getAlignment()).toEqual('left');
            a.setAlignment('right');
            expect(a.getAlignment()).toEqual('right');
        });
    });
    describe('method "isSearchable"', function () {
        it('should return a boolean value to show if the column has the searchable property', function () {
            expect(a.isSearchable()).toBeTruthy();
            a.disableSearch();
            expect(a.isSearchable()).toBeFalsy();
        });
    });
    describe('method "enableSearch"', function () {
        it('should enable the searchable property', function () {
            a.enableSearch();
            expect(a.searchable === true).toBeTruthy();
            expect(a.isSearchable()).toBeTruthy();
        });
    });
    describe('method "disableSearch"', function () {
        it('should disable the searchable property', function () {
            a.disableSearch();
            expect(a.searchable === false).toBeTruthy();
            expect(a.isSearchable()).toBeFalsy();
        });
    });
    describe('method "setColumnData"', function () {
        it('should set the columnt data, this must be a string, null or a function', function () {
            expect(function () {a.setColumnData(234)}).toThrow();
            expect(function () {a.setColumnData({data:"data1"})}).toThrow();
            expect(function () {a.setColumnData(new PMUI.event.Event())}).toThrow();
            expect(function () {a.setColumnData(null)}).not.toThrow();
            expect(a.getColumnData()).toBeNull();
            expect(function () {a.setColumnData("lastaname")}).not.toThrow();
            expect(a.getColumnData()).toEqual("lastaname");
            expect(function () {a.setColumnData(function () {
                                    alert('hello world!');
                                })}).not.toThrow();
            expect(typeof a.getColumnData()).toEqual('function');
        });
    });
    describe('method "getColumnData"', function () {
        it('should return the column data', function () {
            a.setColumnData("name");
            expect(a.getColumnData() === "name").toBeTruthy();
            a.setColumnData(null);
            expect(a.getColumnData()).toBeNull();
            a.setColumnData(function () {
                                    alert('hello world!');
                                });
            expect(typeof a.getColumnData() === "function").toBeTruthy();
        });
    });
    describe('method "setTitle"', function () {
        it('should set the title for the column', function () {
            expect(a.title === "name").toBeTruthy();
            expect(function () {a.setTitle("nombre")}).not.toThrow();
            expect(a.title === "nombre").toBeTruthy();
            expect($(a.html).text()).toEqual("nombre");
        });
    });
    describe('method "setDataType"', function () {
        it('should set the data type of the column', function () {
            expect(function () {a.setDataType(234)}).toThrow();
            expect(function () {a.setDataType({data: "data1"})}).toThrow();
            expect(function () {a.setDataType(new PMUI.event.Event())}).toThrow();
            expect(function () {a.setDataType('string')}).not.toThrow();
            expect(a.getDataType()).toEqual("string");
            expect(function () {a.setDataType('number')}).not.toThrow();
            expect(a.getDataType()).toEqual("number");
            expect(function () {a.setDataType('index')}).not.toThrow();
            expect(a.getDataType()).toEqual("index");
            expect(function () {a.setDataType('date')}).not.toThrow();
            expect(a.getDataType()).toEqual("date");
            expect(function () {a.setDataType('button')}).not.toThrow();
            expect(a.getDataType()).toEqual("button");
        });
    });
    describe('method "getDataType"', function () {
        it('should return the data type of the column', function () {
            expect(a.getDataType()).toEqual("string");
            a.setDataType("number");
            expect(a.getDataType()).toEqual("number");
        });
    });
    describe('method "setSortable"', function () {
        it('should set the sortable property, this must be a boolean value', function () {
            expect(function () {a.setSortable(234)}).toThrow();
            expect(function () {a.setSortable("any string")}).toThrow();
            expect(function () {a.setSortable(new PMUI.core.Base())}).toThrow();
            expect(function () {a.setSortable(true)}).not.toThrow();
            expect(a.sortable).toBeTruthy();
            expect(function () {a.setSortable(false)}).not.toThrow();
            expect(a.sortable).toBeFalsy();
        });
    });
    describe('method "setOnButtonClick"',function () {
        it('should set the function for click on the button', function () {
            expect(function () {a.setOnButtonClick(function () {})}).not.toThrow();
            expect(typeof a.onButtonClick).toEqual('function');
        });
    });
    describe('method "getOnButtonClick"',function () {
        it('should return the function for click on the button', function () {
            a.setOnButtonClick(function () {});
            expect(typeof a.getOnButtonClick()).toEqual('function');
        });
    });
    describe('method "setButtonLabel"', function () {
        it('should set the button label for the column', function () {
            expect(function () {a.setButtonLabel("new button")}).not.toThrow();
            expect(a.buttonLabel === "new button").toBeTruthy();
        });
    });
    describe('method "createHTML"', function () {
        it('should create a new HTML element', function () {
            var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
        });
    });
});
