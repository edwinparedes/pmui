//***********************other class******************************************************//
describe('PMUI.item.CheckboxItem', function () {
    var menuItem,
        menuButton,
        menu,
        objMenu;
    beforeEach(function () {
            menuItem = new PMUI.item.CheckboxItem(); 
            menuItem.createHTML();
    });
    afterEach(function () {
        
    }); 
    describe('class behavior', function () {
      
    });
    describe('method "setActiveItem"', function () {
        it('should active class to the item active', function () {
            menuItem.setActiveItem(true);
            expect(menuItem.style.cssClasses[0] == "pmui-item-active").toBeTruthy();
            menuItem.setActiveItem(false);
            expect(menuItem.style.cssClasses.length == 0).toBeTruthy();
        });
    });
    
    describe('method "setListeners"', function () {
        it('should sets listeners to the item', function () {
            menuItem.setListeners(function (){
                console.log("test!!!");
            });
            expect(menuItem.listeners != null).toBeTruthy();
            expect(menuItem.listeners != undefined).toBeTruthy();             
        });
    });

    describe('method "toggleCheckbox"', function () {
        it('should sets toggle property to the item', function () {
            expect(!menuItem.checked).toBeTruthy();
            menuItem.toggleCheckbox();
            expect(menuItem.checked).toBeTruthy();
            
        });
    });

    describe('method "setChecked"', function () {
        it('should sets checked property to the item', function () {
            menuItem.setChecked(true);          
            expect(menuItem.checked).toBeTruthy();
            menuItem.setChecked(false);           
            expect(!menuItem.checked).toBeTruthy();            
        });
    });
    describe('method "createHTML"', function () {
        it('should create a new HTML element', function () {
            var html = menuItem.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
        });
    });
});