//***********************other class******************************************************//
describe('PMUI.control.PasswordControl', function () {
	var a, b, x = 0;
	beforeEach(function () {
		a = new PMUI.control.PasswordControl(
												{
													maxLength: 524288,
            										onKeyUp: null,
            									}
            );
		document.body.appendChild(a.getHTML());
		a.defineEvents();
	});
	afterEach(function () {
		$(a.html).remove();
		a = null;
	});
	describe('class behavior', function () {
		it('[US-]', function () {
			expect(1 === 1).toBeTruthy();
		});
	});
	describe('method "setOnKeyUpHandler"', function () {
		it('should set the hanlder for the keyup event', function () {
			expect(function () {a.setOnKeyUpHandler(function () {x=x+1})}).not.toThrow();
			$(a.html).trigger('keyup');
			expect(x === 1).toBeTruthy();
			$(a.html).trigger('keyup');
			expect(x === 2).toBeTruthy();
		});
	});
	describe('method "setMaxLength"', function () {
		it('should set the max length for the control', function () {
			expect(function () {a.setMaxLength({})}).toThrow();
			expect(function () {a.setMaxLength("34")}).toThrow();
			expect(function () {a.setMaxLength(34)}).not.toThrow();
			expect(a.maxLength === 34).toBeTruthy();
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});