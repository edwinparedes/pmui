//***********************other class******************************************************//

describe('PMUI.form.Validator', function () {
    var v, validator;

    describe('class behavior', function () {
       it('[US-5] should have a class Validator to perform validation functions for each object Field', function () {
            tf = new PMUI.field.TextField({ pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name"});
            
            validator = new PMUI.form.Validator({parent:tf})
            expect(validator).toBeDefined();
            expect(validator instanceof PMUI.form.Validator).toBeTruthy();
            expect(validator).toBeTruthy();

        });  

       it('[US-5,a]should have a property that can define an error message', function () {

            v = new PMUI.form.Validator({parent: new PMUI.field.TextField()});
            expect(v.errorMessage).toBeDefined();
            expect(v.errorMessage).toBeTruthy();

        });  

       it('[US-5,b] should have a property that points to the field to which it belongs', function () {
            v = new PMUI.form.Validator({parent: new PMUI.field.TextField()});
            expect(v.parent).toBeDefined();
            expect(v.parent).toBeTruthy();

        });

       it('[US-5,c] should have a method in which validation is performed', function () {
            v = new PMUI.form.Validator({parent: new PMUI.field.TextField()});
            expect(v.isValid()).toBeDefined();
            expect(v.valid).toBeTruthy();
        });  
    });

    describe('method "setErrorMessage"', function () {
        it('should be able to set the validation error message to display if validation fails', function () {
        var message='invalid data';
            v = new PMUI.form.Validator({parent: new PMUI.field.TextField()});
            v.setErrorMessage(message);
            expect(v.errorMessage===message).toBeTruthy();
        });
    });
    
    describe('method "getErrorMessage"', function () {
        it('should be able to get the validation error message to display if validation fails', function () {
          v = new PMUI.form.Validator({parent: new PMUI.field.TextField(), errorMessage:'invalid data'});
           expect(v.getErrorMessage()===v.errorMessage).toBeTruthy();
        });
    });
    
    describe('method "setCriteria"', function () {
        it('should be able of sets the validation criteria', function () {
        var criteria = {
            maxLength: 6
        };
           v = new PMUI.form.Validator({parent: new PMUI.field.TextField()})
           v.setCriteria(criteria);
           expect(v.criteria.maxLength===criteria.maxLength).toBeTruthy();
        });
    });    

    describe('method "setParent"', function () {
        it('should be able to set the value of the parent property and this should be a field', function () {
          
          var t = new PMUI.field.TextField({ pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name"});

           v = new PMUI.form.Validator({parent:t})
           expect(v.getParent().type=='TextField').toBeTruthy();
        });
        /*
        it('a validator class should be throws an exception when the object parent is not a Field', function () {
          var b = new PMUI.ui.Button({text:'submit'});
          expect(new PMUI.form.Validator({parent:b})).toThrow();
        });*/
    });
    
});