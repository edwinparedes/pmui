//***********************other class******************************************************//
describe('PMUI.form.TextLengthValidator', function () {
    var tf, v;
    describe('class behavior', function () {
        it('[US-22,a]Should validate a string length', function () {
            tf = new PMUI.field.TextField();
            //document.body.appendChild(tf.getHTML());
            v = new PMUI.form.TextLengthValidator({parent: tf, criteria:{maxLength:5}});
            tf.setValue("abcde");
            expect(v.isValid()).toBeTruthy();
            tf.setValue("abcdefgh");
            expect(v.isValid()).toBeFalsy();
        });
        it('[US-22,b]Should not take spaces as a string in order to get a String length', function () {
            tf = new PMUI.field.TextField();
            document.body.appendChild(tf.getHTML());
            v = new PMUI.form.TextLengthValidator({parent: tf, criteria:{maxLength:5}});
            tf.setValue("   abcde");
            v.criteria.trim = true ;
            expect(v.isValid()).toBeTruthy();
        });
    });
});