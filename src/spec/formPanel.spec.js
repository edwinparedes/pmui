//***********************other class******************************************************//
describe('PMUI.form.FormPanel', function () {
    var a, b, c, x, y, fp;
    describe('class behavior', function () {
        it('[US-10,a]should add items to a FormPanel object', function () {
            a = new PMUI.form.FormPanel();
            b = new PMUI.field.TextField();
            x = a.getItems().length;
            a.addItem(b);
            y = a.getItems().length;
            expect(x === y).toBeFalsy(); 
        });
        it('[US-10,b]should clear items added from a FormPanel object', function () {
            a = new PMUI.form.FormPanel();
            b = new PMUI.field.TextField();
            a.addItem(b);
            x = a.getItems().length;
            a.clearItems();
            y = a.getItems().length;
            expect(x === y).toBeFalsy(); 
        });
        it('[US-10,c]should get all formPanels from the main FromPanel', function () {
            a = new PMUI.form.FormPanel();
            b = new PMUI.form.FormPanel();
            a.addItem(b);
            expect(a.getItems().length === 1).toBeTruthy();
        });  
    });
    
    describe('method "setLegend"', function () {
        it('should be able to set the caption text displayed when there fildset', function () {
            fp = new PMUI.form.FormPanel({
                width: 600, 
                height: 150,
                fieldset: true
            });
            fp.createHTML();
            fp.setLegend('richard')
            expect(fp.legend==='richard').toBeTruthy();
            expect(!!fp.legendElement).toBeTruthy();
            expect(fp.legendElement.textContent===fp.legend).toBeTruthy();
        });
    });
    
    describe('method "createHTML"', function () {
        it('should create a new html element for formPanel', function () {
            fp = new PMUI.form.FormPanel({
                width: 600, 
                height: 150,
                fieldset: true,
                legend: 'fieldset'
            });
            var fp1 = fp.createHTML();
            expect(fp1.tagName).toBeDefined();
            expect(fp1.nodeType).toBeDefined();
            expect(fp1.nodeType).toEqual(document.ELEMENT_NODE);
        });
    });

    describe('method "setFactory"', function () {
        it('should set the factory property to a FormPanel object', function () {
            a = new PMUI.form.FormPanel();
            c = new PMUI.util.Factory();
            x = a.factory.defaultProduct;
            a.setFactory(c);
            y = a.factory.defaultProduct;
            expect(x === y).toBeFalsy();
        });
    });
    
    describe('method "addItem"', function () {
        it('should be able to add an item to the object.', function () {
            var fp,t;
            fp = new PMUI.form.FormPanel({
                width: 600, 
                height: 130,
                fieldset: true,
                legend: "my fieldset panel",
            });
            t = new PMUI.field.TextField({
                            pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name"
                        });
            fp.addItem(t);
            expect((fp.getItems()[0].id)===('123')).toBeTruthy();
            expect(fp.isDirectParentOf(t)).toBeTruthy();
        });
    });     

    describe('method "getUsableHeight"', function () {

        it('should get the highest usable for my order form with fieldset activated Panel', function () {
            fp = new PMUI.form.FormPanel({
                width: 600, 
                height: 150,
                fieldset: true,
                legend: "my fieldset panel",
            });
            var paddingHeight = parseInt(fp.getPadding()[0])+parseInt(fp.getPadding()[2]);
            var border = parseInt(fp.getBorderWidth())*2;
            expect((fp.height-paddingHeight-border-fp.legendSize)===(fp.getUsableHeight())).toBeTruthy();

        });
        it('should get the highest usable for my order form fieldset Panel with disabled', function () {
            fp = new PMUI.form.FormPanel({
                width: 600, 
                height: 150,
                fieldset: false,
                padding: "0px",
                borderWidth: "0px"
            });
            var paddingHeight = parseInt(fp.getPadding()[0])+parseInt(fp.getPadding()[2]);
            var border = parseInt(fp.getBorderWidth())*2;
            expect((fp.height-paddingHeight-border)===(fp.getUsableHeight())).toBeTruthy();
        });
    }); 
    


});