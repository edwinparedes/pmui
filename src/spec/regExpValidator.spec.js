//***********************other class******************************************************//
describe('PMUI.form.RegExpValidator', function () {
    var a, b, tf;
    describe('class behavior', function () {
        it('[US-21,a] Should accept a regular expression to make a Validation', function () {
            tf = new PMUI.field.TextField();
            v = new PMUI.form.RegExpValidator({parent: tf, criteria:/^\d$/});
            tf.setValue("4");
            expect(v.isValid()).toBeTruthy();
            tf.setValue("456");
            expect(v.isValid()).toBeFalsy();            
        });
        it('[US-21,b] Should make a validation, a string with only numbers', function () {
            tf = new PMUI.field.TextField();
            //document.body.appendChild(tf.getHTML());
            v = new PMUI.form.RegExpValidator({parent: tf, criteria:/[0-9]/});
            tf.setValue("45645645654");
            expect(v.isValid()).toBeTruthy();
            tf.setValue("any_string");
            expect(v.isValid()).toBeFalsy();
        });
        it('[US-21,b] Should make a validation, a string with a email', function () {
            tf = new PMUI.field.TextField();
            //document.body.appendChild(tf.getHTML());
            v = new PMUI.form.RegExpValidator({parent: tf, criteria:/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/});
            tf.setValue("any_string");
            expect(v.isValid()).toBeFalsy();
            tf.setValue("any_string.com");
            expect(v.isValid()).toBeFalsy();
            tf.setValue("any_string@string");
            expect(v.isValid()).toBeFalsy();
            tf.setValue("any_string@string.st");
            expect(v.isValid()).toBeTruthy();    
        });
    }); 
});