//***********************other class******************************************************//
describe('PMUI.ui.Window', function () {
    var classWindow, classWindow2, button1, button2, padding=18;
    describe('class behavior',function(){
       it("[US-16,d] should be able to instantiate a class Window Default",function() {
            classWindow = new PMUI.ui.Window({height:300});
            expect(classWindow).toBeDefined();
            expect(classWindow instanceof PMUI.ui.Window).toBeTruthy();
            expect(classWindow.type).toEqual('Window');
            expect(classWindow.family).toEqual('ui');
            expect(classWindow.title).toEqual('[Untitled window]');
            expect(classWindow.modal).toEqual(true);
            expect(classWindow.height).toEqual(300-padding);
            expect(classWindow.width).toEqual(400-padding);
        });

         it("[US-16,e] should be able to create buttons for use on windows",function() {
        
            classWindow2 = new PMUI.ui.Window ({
                width:600,
                height:'auto',
                footerHeight:'auto',
                bodyHeight:300,
                footerItems: [  
                        {
                            pmType: 'button',
                            text: 'button1',
                            handler: function() {
                                alert("hi buddy!");
                            }
                        },
                        {
                            pmType: 'button',
                            text: "button2",
                            handler : function () {
                                w.close();
                            }
                        }
                    ]
            });
            for (var i=0 ;i<classWindow2.footer.getItems().length ; i++){
                expect(classWindow2.footer.getItem(i).text).toEqual('button'+(i+1));
            }
        });
        it("[US-16,f] should be able to create an object of type Window Modal or No Modal",function() {
            var classWindow3,classWindow4;
            classWindow3 = new PMUI.ui.Window ();
            classWindow4 = new PMUI.ui.Window ({
                modal: false
            });
            expect(classWindow3.modal).toEqual(true);
            expect(classWindow4).toBeDefined();
            expect(classWindow4 instanceof PMUI.ui.Window).toBeTruthy();
            expect(classWindow4.modal).toEqual(false);
             
        });
        
        it("[US-16,g] should be able to define the title on the header of my window",function() {
            var classWindow5;
            classWindow5 = new PMUI.ui.Window ({
                title : '[US-6,c]',
                showCloseButton : false
            });

            expect(classWindow5.getTitle()).toEqual('[US-6,c]');
            classWindow5.createHTML();
            expect(classWindow5.titleContainer.textContent === '[US-6,c]').toBeTruthy();
        });
        it("[US-16,h] should be able to manipulate the height of the window, body and footer independently of the other sections",function() {
            var classWindow6, b;    
            classWindow6 = new PMUI.ui.Window();
            classWindow6.createHTML();
            classWindow6.setBodyHeight(100);
            classWindow6.setFooterHeight(30);
            expect(classWindow6.bodyHeight).toEqual(100);
            expect(classWindow6.footerHeight === 30).toBeTruthy();
            expect(typeof(classWindow6.footerHeight)).toEqual('number');
        });
        it('[US-16,i] should be able to apply defineEvents to the items that the window is containing', function () {
            var f = new PMUI.form.Form({
                    onSubmit: function() {
                        console.log("submitting...");
                    },
                    onChange: function(field, newValue, previousValue) {
                        console.log("The field " + field.getName() + " has changed from \"" + previousValue + "\" to \"" + newValue + "\"");
                    },
                    items: [
                        {
                            pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name",
                            helper: "Introduce your name",
                            required: true
                        }, 
                        {
                            pmType: "dropdown",
                            label: "age",
                            options: [
                                {
                                    label: "",
                                    value: ""
                                },
                                {
                                    label: "from 0 to 7 years old",
                                    value: "0-7"
                                },
                                {
                                    label: "from 8 to 13 years old",
                                    value: "8-13"
                                },
                                {
                                    label: "from 14 to 19 years old",
                                    value: "14-19"
                                },
                                {
                                    label: "from 20 to 30 years old",
                                    value: "20-30"
                                },
                                {
                                    label: "from 31 to 45 years old",
                                    value: "31-45"
                                },
                                {
                                    label: "older than 45",
                                    value: "46+"
                                }
                            ],
                            name: "age",
                            helper: "Select one of the options",
                            required: true
                        },
                        {
                            pmType: "radio",
                            label: "Gender",
                            value: "m",
                            name: "gender",
                            required: true,
                            options: [
                                {
                                    label: "Male",
                                    value: "m"
                                },
                                {
                                    label: "Female",
                                    value: "f"
                                }
                            ]
                        }, 
                        {
                            pmType: "text",
                            label: "E-mail",
                            value: "",
                            name: "email",
                            helper: "you email here",
                            required: true,
                            validators: [
                                {
                                    pmType: "regexp",
                                    criteria: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                    errorMessage: "Please enter a valid email address"
                                }
                            ]
                        }, 
                        {
                            pmType: "checkbox",
                            label: "Things you like to do",
                            value: "[\"music\"]",
                            name: "hobbies",
                            helper: "Check up to 3 options",
                            options: [
                                {
                                    label: "Music",
                                    value: "music"
                                },
                                {
                                    label: "Programming",
                                    value: "programming"
                                },
                                {
                                    label: "Bike ridding",
                                    value: "bike"
                                },
                                {
                                    label: "Gastronomy",
                                    value: "gastronomy"
                                },
                                {
                                    label: "Movies",
                                    value: "movies"
                                }
                            ],
                            validators: [],
                            required: true
                        },
                        {   
                            pmType: "datetime",    
                            label:'Calendar',
                            helper: 'This is calendar Gregorian',
                            value: new Date(), 
                            datetime : true,
                            dateFormat: 'M dd yy',
                            minDate: -90,
                            maxDate: "+1y -1m -4d",
                            firstDay: 1,
                            months: {
                                "january": {
                                    name: "Enero",
                                    shortname: "Ene"
                                },
                                "february": {
                                    name: "Febrero",
                                    shortname: "Feb"
                                },
                                "march": {
                                    name: "Marzo",
                                    shortname: "Mar"
                                },
                                "april": {
                                    name: "Abril",
                                    shortname: "Abr"
                                },
                                "may": "May",
                                "june": "Junio",
                                "july": "July",
                                "august": "Agosto",
                                "september": "Septiembre",
                                "october": "Octubre",
                                "november": "Noviembre",
                                "december": "Diciembre"
                            },
                            days: {
                                "sunday": {
                                    name: "Domingo",
                                    shortname: "Do"
                                },
                                "monday": {
                                    name: "Lunes",
                                    shortname: "Lu"
                                },
                                "tuesday": {
                                    name: "Martes",
                                    shortname: "Ma"
                                },
                                "wednesday": {
                                    name: "Miércoles",
                                    shortname: "Mi"
                                },
                                "thursday": {
                                    name: "Jueves",
                                    shortname: "Ju"
                                },
                                "friday": "Viernes",
                                "saturday": "Sábado"
                            }

                        }
                    ],
                    buttons: [
                        {
                            text: "Submit",
                            handler: function() {
                                f.submit();
                                console.log("submitting form...");
                            }
                        }
                    ]
                });
                var a = new PMUI.panel.TabPanel({
                    height : 400,
                    width : 600,
                    items:[
                        {   icon :'pmui-gridpanel-engranaje',
                            title:'Form',
                            panel:f
                        }
                    ]
                }); 
            w = new PMUI.ui.Window({
                title: "FORM", 
                width: 650, 
                height: 'auto',
                bodyHeight: 'auto', 
                modal:true,
                footerItems: [  
                    {
                        pmType: 'button',
                        text: 'new field',
                        handler: function() {
                            f.addItem({pmType:'text', label: 'xxx'});
                            f.addItem({pmType: "dropdown",
                            label: "temp",
                            options: [
                            ],
                            name: "temp",
                            helper: "Select one of the options",
                            required: true});
                        }
                    },
                    {
                        pmType: 'label',
                        text: "and then"
                    },
                    {
                        pmType: 'button',
                        text: "Close",
                        handler : function () {
                            w.close();
                        }
                    }
                ],
                footerAlign: 'center'
            });
            w.addItem(a);
            w.open(); 
            w.defineEvents();
            for(var i = 0 ; i<f.getItems().length ; i++){
                    expect(f.getItems()[i].eventsDefined).toBeTruthy();
                    expect(f.getItems()[i].events).not.toBeNull();
                for(var g = 0; g<f.getItems()[i].controls.length ; g++){
                    expect(f.getItems()[i].controls[g].eventsDefined).toBeTruthy();
                    expect(f.getItems()[i].controls[g].events).not.toBeNull();
                }
            }
            $($(w.footer.html).find('a')[0]).trigger('click')
            expect(f.getItems()[6].eventsDefined).toBeTruthy();
            expect(f.getItems()[6].events).not.toBeNull();
            expect(f.getItems()[7].eventsDefined).toBeTruthy();
            expect(f.getItems()[7].events).not.toBeNull();
            expect(f.getItems()[6].controls[0].eventsDefined).toBeTruthy();
            expect(f.getItems()[6].controls[0].events).not.toBeNull();
            expect(f.getItems()[7].controls[0].eventsDefined).toBeTruthy();
            expect(f.getItems()[7].controls[0].events).not.toBeNull();
            f = null;
            a = null;
            $(w.getHTML()).remove();
            w = null;
        });
    });
    describe('method "createHTML"', function () {
        it('should create a new html element', function () {
            classWindow1 = new PMUI.ui.Window();
            var h = classWindow1.createHTML();
            expect(h.tagName).toBeDefined();
            expect(h.nodeType).toBeDefined();
            expect(h.nodeType).toEqual(document.ELEMENT_NODE);
        });
    });
    describe('method "updateDimensions"', function () {
        it('should update Window Dimensions', function () {
            classWindow6 = new PMUI.ui.Window();
            classWindow6.setBodyHeight(10);
            classWindow6.setFooterHeight(30);
            expect(classWindow6.bodyHeight).toEqual(10);
            expect(classWindow6.footerHeight === 30).toBeTruthy();
        });
    });
    describe('method "centerWindow"', function () {
        it('should set Window location in the center of the display', function () {
            classWindow1 = new PMUI.ui.Window();
            classWindow1.open();
            expect(function () {classWindow1.centerWindow();}).not.toThrow();
            expect(classWindow1.style.cssProperties.left === "50%").toBeTruthy();
            expect(classWindow1.style.cssProperties.top === "50%").toBeTruthy();
        });
    });
    describe('method "open"',function () {
        it ('should be able to display the window in screen', function(){
            classWindow1 = new PMUI.ui.Window();
            expect(classWindow1.html).toBeFalsy();
            expect(classWindow1.opened).toBeFalsy();
            expect(classWindow1.isOpen()).toBeFalsy();
            classWindow1.open();
            expect(classWindow1.html).toBeTruthy();
            expect(classWindow1.opened).toBeTruthy();
            expect(classWindow1.isOpen()).toBeTruthy();
        });
    });

    describe('method "close"',function () {
        it ('should be able to close the window of screen', function(){
            classWindow1 = new PMUI.ui.Window();
            classWindow1.open();
            expect(classWindow1.opened).toBeTruthy();
            expect(classWindow1.isOpen()).toBeTruthy();
            classWindow1.close();
            expect(classWindow1.opened).toBeFalsy();
            expect(classWindow1.isOpen()).toBeFalsy();
        });
    });

    describe('method "getItems"',function () {
        it ('should be able to add objects in the body of the window', function(){
            var panel = new PMUI.core.Panel();
            classWindow1 = new PMUI.ui.Window();
            classWindow1.addItem(panel);
            classWindow1.open();
            expect($('.pmui-window-body').children()[0].className).toEqual(panel.getHTML().className);
        });
    });
});
