//***********************other class******************************************************//
describe('PMUI.grid.GridPanelRow', function () {
	var a, b, c, j, i;
	beforeEach(function () {
		a = new PMUI.grid.GridPanelRow();
		document.body.appendChild(a.getHTML());
	});
	afterEach(function () {
		a = null;
		b = null;
		c = null;
	});
	describe('class behavior', function () {
		it('[US-44,b]should set the data for GridPanelRow, this must be a PMUI.data.DataField type', function () {
			j = {"val1":"dat1","val2":"dat2","val3":"dat3"};
			c = new PMUI.data.DataField();
			i = a.data.id;
			expect(a.data.id).toBeDefined();
			a.setData(c);
			expect(a.data.id === i).toBeFalsy();
			a.setData(j);
			expect(a.data.customKeys.val1 === "dat1").toBeTruthy();
		});
	});
	describe('method "setParent"', function () {
		it('should set the parent for GridPanelRow', function () {
			b = new PMUI.draw.Canvas();
			c = new PMUI.grid.GridPanel();
			expect(function () {a.setParent(b);}).toThrow();
			expect(function () {a.setParent(c);}).not.toThrow();
		});
	});
	describe('method "getParent"', function () {
		it('should get the parent from GridPanelRow', function () {
			c = new PMUI.grid.GridPanel();
			i = c.id;
			a.setParent(c);
			expect(a.getParent().id === i).toBeTruthy();
		});
	});
});