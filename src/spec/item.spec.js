//***********************other class******************************************************//
describe('PMUI.core.Item', function () {
	var a, b, c;
	beforeEach(function () {
		a = new PMUI.core.Item();
	});
	afterEach(function () {
		a = null;
		b = null;
	});
	describe('method "setParent"', function () {
		it('should set the parent for the Item', function () {
			b = new PMUI.data.DataSet();
			expect(function(){a.setParent(b);}).not.toThrow();
			expect(a.parent.type === "DataSet").toBeTruthy();
		});
	});
	describe('method "getParent"', function () {
		it('should get the parent from the Item', function () {
			b = new PMUI.data.DataSet();
			a.setParent(b);
			expect(a.getParent().type === "DataSet").toBeTruthy();
		});
	});
});