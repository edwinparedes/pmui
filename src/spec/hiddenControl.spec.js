/*--------------- HiddenControl -----------------*/
describe('PMUI.control.HiddenControl', function() {
    var control;

    beforeEach(function() {
        control = new PMUI.control.HiddenControl();
    });

    afterEach(function() {
        jQuery(control.getHTML()).remove();
        control = null;
    });

    describe('class behavior', function() {
        it('[US-31,a] its HTML should be an input element with the type attribute set to "hidden"', function() {
            var html = control.getHTML();
            expect(html.tagName.toLowerCase()).toBe("input");
            expect(html.type).toBe("hidden");
        });
    });
});