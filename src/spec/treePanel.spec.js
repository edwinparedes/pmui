//***********************other class******************************************************//
describe('PMUI.panel.TreePanel', function() {
    var tree;
    beforeEach(function () {
        tree = new PMUI.panel.TreePanel({
                id: "myTest",
                dataItems: [
                    {
                        id: "elementId",
                        text: "Element1"
                    },
                    {
                        text: "Element2"
                    }
                ]
            });
        document.body.appendChild(tree.getHTML());
    });
    afterEach(function () {
        $(tree.getHTML()).remove();
        tree = null;
    });
    describe('class behavior', function() {
        it("[US-14, a] Should be able to create a TreePanel with a hieranchical parent-child", function(){
            var i,
                items;
            tree = new PMUI.panel.TreePanel({
                id: "myTest",
                dataItems: [
                    {
                        id: "elementId",
                        text: "Element1"
                    },
                    {
                        text: "Element2"
                    }
                ]
            });

            expect(tree).toBeDefined();
            expect(tree.type).toEqual("TreePanel");
            expect(tree.family).toEqual('Panel');

            items = tree.getItems();
            expect(items.length).toBeGreaterThan(0);
            for (i = 0; i < items.length; i++) {
                expect(items[i] instanceof PMUI.item.TreeNode).toBeTruthy();
            }
          
        });
        it("[US-14, b] Should be able to add new items to TreePanel", function() {
            var treeExample = tree;
            spyOn(treeExample, 'addItem');
            treeExample.addItem(
                "myTest",
                [
                    {   
                        id: "elementAddedId",
                        text: "Folder added",
                        children: [
                            {
                                text: "element1 added"
                            },
                            {
                                text: "element2 added"
                            }
                        ]   
                    }
                ]
            );
            expect(treeExample.addItem).toHaveBeenCalled();
        });
        it("[US-14, c] Should be able to remove items from TreePanel with an identifier of the object", function() {
            var treeExample = tree;
            spyOn(treeExample, 'removeItem');
            treeExample.removeItem("myTest");
            expect(treeExample.removeItem).toHaveBeenCalled();
        });
        it("[US-14, d] Should be able to get an object with a identifier as parameter", function() {
            var that = tree;
            item = tree.getItem(0);
            expect(typeof item).toEqual("object");
        });
    });
    describe('method "addItem"', function() {
        it("should add elements to TreePanel", function() {
            var arrayObj,
                items;

            arrayObj = [
                {
                    text: "Elem1"
                }
            ];

            tree.addItem(arrayObj);
            items = tree.getItems();
            expect(items.length).toEqual(3);
            expect(items[0] instanceof PMUI.item.TreeNode).toBeTruthy();
        });

    });
    describe('method "createHTML"', function () {      
        it("should create a new HTML element", function() {
            var h = tree.createHTML();
            expect(h).toBeDefined();
            expect(h.tagName).toBeDefined();
            expect(h.nodeType).toBeDefined();
            expect(h.nodeType).toEqual(document.ELEMENT_NODE);
        });

    });
});