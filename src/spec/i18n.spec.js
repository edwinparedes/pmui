//***********************other class******************************************************//
describe('PMUI.lang.I18N', function () {
	var a, b, t1, t2;
	beforeEach(function () {
		t1 = {
	'LBL_1':'1a',
	'LBL_2':'1b',
	'LBL_3':'1c',
	'LBL_4':'1d',
	'LBL_5':'1e',
	'LBL_6':'1f',
	'LBL_7':'1g',
	'LBL_8':'1h',
	'LBL_9':'1i',
	'LBL_10':'1j',
	'LBL_11':'1a',
	'LBL_12':'1a',
	'LBL_13':'1b',
	'LBL_14':'1b',
	'LBL_15':'1b',
	'LBL_16':'1c'
		}

	t2 = {
	'LBL_1':'2a',
	'LBL_2':'2b',
	'LBL_3':'2c',
	'LBL_4':'2d',
	'LBL_5':'2e',
	'LBL_6':'2f',
	'LBL_7':'2g',
	'LBL_8':'2h',
	'LBL_9':'2i',
	'LBL_10':'2j',
	'LBL_11':'2k',
	'LBL_12':'2l',
	'LBL_13':'2m',
	'LBL_14':'2n',
	'LBL_15':'2o',
	'LBL_16':'2p'
		}
	PMUI.init();
	});
	describe('class behavior', function () {
		it('[US-61,a]should be able to use short instructions in order to translate strings', function () {
			expect(function () {PMUI.loadLanguage(t1,'en')}).not.toThrow();
			expect(function () {PMUI.loadLanguage(t2,'es')}).not.toThrow();
			expect(function () {PMUI.setCurrentLanguage('es')}).not.toThrow();
			expect(function () {PMUI.setDefaultLanguage('en')}).not.toThrow();
			expect(function () {'2p'.translate()}).not.toThrow();
		});
		it('[US-61,b]should be able to set the Current and default language', function () {
			expect(function () {PMUI.setCurrentLanguage('es')}).not.toThrow();
			expect(PMUI.lang.I18N.currentLanguage === 'es').toBeTruthy();
			expect(function () {PMUI.setDefaultLanguage('en')}).not.toThrow();
			expect(PMUI.lang.I18N.defaultLanguage === 'en').toBeTruthy();
		});
		it('[US-61,c]should be able to translate strings in many contexts', function () {
			PMUI.loadLanguage(t1,'en');
			PMUI.loadLanguage(t2,'es');
			PMUI.setDefaultLanguage('en');
			PMUI.setCurrentLanguage('es');
			expect('1e'.translate()).toEqual("2e");
			expect('1f'.translate()).toEqual("2f");
			expect('1g'.translate()).toEqual("2g");
			expect('1h'.translate()).toEqual("2h");
			expect('1c'.translateContext(0)).toEqual("2c");
			expect('1c'.translateContext(1)).toEqual("2p");
		});
	});
});