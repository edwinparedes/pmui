//***********************other class******************************************************//
describe('PMUI.item.MenuItem', function () {
    var menuItem,
        menuButton,
        menu,
        objMenu;
    beforeEach(function () {
            menuItem = new PMUI.item.MenuItem(); 
            menuItem.createHTML();
    });
    afterEach(function () {
        
    }); 
    describe('class behavior', function () {
      
    });
    describe('method "setActiveItem"', function () {
        it('should active class to the item active', function () {
            menuItem.setActiveItem(true);
            expect(menuItem.style.cssClasses[0] == "pmui-item-active").toBeTruthy();
            menuItem.setActiveItem(false);
            expect(menuItem.style.cssClasses.length == 0).toBeTruthy();
        });
    });
    
    describe('method "setListeners"', function () {
        it('should sets listeners to the item', function () {
            menuItem.setListeners(function (){
                console.log("test!!!");
            });
            expect(menuItem.listeners != null).toBeTruthy();
            expect(menuItem.listeners != undefined).toBeTruthy();             
        });
    });

    describe('method "setHasMenuActive"', function () {
        it('should sets hasMenuActive property to the item', function () {
            menuItem.setHasMenuActive(true);
            expect(menuItem.hasMenuActive).toBeTruthy();
            menuItem.setHasMenuActive(false);          
            expect(!menuItem.hasMenuActive).toBeTruthy();
        });
    });
});