//***********************other class******************************************************//
describe('PMUI.layout.Layout', function () {
	var a, obj;
	describe('method "setContainer"', function () {
		it ('should set the parent container', function () {
			a = new PMUI.layout.Layout();
			obj = {}
			expect(a.belongsTo).toBeNull();
			a.setContainer(obj);
			expect(a.belongsTo).not.toBeNull();
		});
	});
});