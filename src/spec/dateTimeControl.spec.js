//***********************other class******************************************************//
describe('PMUI.control.DateTimeControl', function () {
	var a, b, c, d, m, y;
	beforeEach(function () {
		a = new PMUI.control.DateTimeControl();
		document.body.appendChild(a.getHTML());
		a.defineEvents();
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
		d = null;
		m = null;
		y = null;
	});
	describe('class behavior', function () {
		it('[US-33,a]should create a calendar with all days of the week', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
            expect(a.dom.calendar).toBeDefined();
            expect(a.dom.calendar.nodeName === "DIV").toBeTruthy();
            expect($(a.dom.calendar).find('a[data-date="22"]').length === 1).toBeTruthy();
            expect($(a.dom.calendar).find('div[class="pmui-datepicker-header"]')).toBeDefined();
		});
		it('[US-33,b]should set the date format that the control will display', function () {
			//Default is 'yy-mm-dd'
			expect(a.dateFormat === 'yy-mm-dd').toBeTruthy();
			//Setting a new format: 'dd-mm-yy'
			a.setDateFormat('dd-mm-yy');
			expect(a.dateFormat === 'dd-mm-yy').toBeTruthy();
			a.setDateFormat('dd/mm/yy');
			expect(a.dateFormat === 'dd/mm/yy').toBeTruthy();
			a.setDateFormat('D/M/yy');
			expect(a.dateFormat === 'D/M/yy').toBeTruthy();
		});
		it('[US-33,c]should set the first day of the week described by numbers', function () {
			//Default Sunday 0
			expect(a.firstDay === 0).toBeTruthy();
			//Setting up on Monday
			a.setFirstDay(1);
			expect(a.firstDay === 1).toBeTruthy();
		});
		it('[US-33,d]should return current date', function () {
			d = new Date();
			a.setValue(d);
			expect(a.value).toBeDefined();
		});
		it('[US-33,e]should set names and shortnames for the days to be used in the calendar', function () {
			c = {
             "sunday": {
                 name: "Domingo",
                 shortname: "Do"
             },
             "monday": {
                 name: "Lunes",
                 shortname: "Lu"
             },
             "tuesday": {
                 name: "Martes",
                 shortname: "Ma"
             },
             "wednesday": {
                 name: "Miércoles",
                 shortname: "Mi"
             },
             "thursday": {
                 name: "Jueves",
                 shortname: "Ju"
             },
             "friday": "Viernes",
             "saturday": "Sábado"
         };
         	a.setDays(c)
         	expect(a.days.sunday.name === "Domingo").toBeTruthy();
         	expect(a.days.sunday.shortname === "Do").toBeTruthy();
         	expect(a.days.tuesday.name === "Martes").toBeTruthy();
         	expect(a.days.tuesday.shortname === "Ma").toBeTruthy();
		});
		it('[US-33,f]should set the months names/shortnames to be used by the calendar', function () {
			var m = {
             "january": {
                 name: "Enero",
                 shortname: "Ene"
             },
             "february": {
                 name: "Febrero",
                 shortname: "Feb"
             },
             "march": {
                 name: "Marzo",
                 shortname: "Mar"
             },
             "april": {
                 name: "Abril",
                 shortname: "Abr"
             },
             "may": "May",
             "june": "Junio",
             "july": "July",
             "august": "Agosto",
             "september": "Septiembre",
             "october": "Octubre",
             "november": "Noviembre",
             "december": "Diciembre"
         	};
        	a.setMonths(m);
        	expect(a.months.february.name === "Febrero").toBeTruthy();
        	expect(a.months.february.shortname === "Feb").toBeTruthy();
        	expect(a.months.march.name === "Marzo").toBeTruthy();
        	expect(a.months.march.shortname === "Mar").toBeTruthy();
		});
//S
		it('[US-33,g]should get the value from the datetime control with a standar format', function () {
			d = new Date("July 21, 1983 01:15:00");
			a.setValue(d);
			expect(a.getValue()).toBeDefined();
			expect(a.getValue() === "1983-07-21T01:15:00-04:00").toBeTruthy();
		});
	});
	describe('method "isLeapYear"', function () {
		it('should ask if the current year is a leap year', function () {
			//2013 is not a leap year so it should return false
			expect(a.isLeapYear(2013)).toBeFalsy();
			//2012 was a leap year, so it should return true
			expect(a.isLeapYear(2012)).toBeTruthy();
		});
	});
	describe('method "setValue"', function () {
		it('should set the value for the date time Control', function () {
			d = new Date();
			a.setValue(d);
			expect(a.value).toBeDefined();
			//expect(a.value === d).toBeTruthy();
			a.setValue(1385123462);
			expect(a.value).toBeDefined();
			a.setValue("2013-11-18");
			expect(a.value).toBeDefined();
		});
	});
	describe('method "getFormatedDate"', function () {
		it('should get the date using the format set by the programmer', function () {
			d = new Date("July 21, 1983 01:15:00");
			a.setValue(d);
			a.setDateFormat('dd-mm-yy');
			expect(a.getFormatedDate() === "21-07-1983").toBeTruthy();
		});
	});
//s
	describe('method "getValue"', function () {
		it('should get the value from the datetime control', function () {
			d = new Date("July 21, 1983 01:15:00");
			a.setValue(d);
			expect(a.getValue()).toBeDefined();
			expect(a.getValue() === "1983-07-21T01:15:00-04:00").toBeTruthy();
		});
	});
	describe('method "isValidDateTime"', function () {
		it('should return true or false from a bunch of arguments', function () {
			//month > 12
			expect(a.isValidDateTime(2014,13,18,5,0,0)).toBeFalsy();
			//day > 31
			expect(a.isValidDateTime(2014,12,50,5,0,0)).toBeFalsy();
			//all parameters valid
			expect(a.isValidDateTime(2014,12,18,5,0,0)).toBeTruthy();
		});
	});
	describe('method "parseDate"', function () {
		it('should parse the argument into a date', function () {
			d = new Date();
			var s = a.parseDate("+1y -4m +3d").toString();
			expect(a.parseDate("+1y -4m +3d")).toBeDefined();
			expect(typeof a.parseDate("+1y -4m +3d") === "object").toBeTruthy();
			//one more year from now
			expect(s.substring(11,15) === d.getFullYear().toString()).toBeTruthy();
			//three more days from now
			//expect(s.substring(8,10) === (d.getDate()+3).toString()).toBeTruthy();
		});
	});
	describe('method "setMinDate"', function () {
		it('should set the mininum date the control can accept as a selectable one', function () {
			d = new Date("July 21, 2009 01:15:00");
			expect(a.minDate).toBeDefined();
			//expect(a.minDate.toDateString() === "Sun Nov 25 2012").toBeTruthy();
			a.setMinDate(d);
			expect(a.minDate).toBeDefined();
			expect(a.minDate.toDateString() === "Tue Jul 21 2009").toBeTruthy();
		});
	});
	describe('method "setMaxDate"', function () {
		it('should set the maximum date the control can accept as a selectable one', function () {
			d = new Date("July 21, 2015 01:15:00");
			expect(a.maxDate).toBeDefined();
			//expect(a.maxDate.toDateString() === "Tue Nov 25 2014").toBeTruthy();
			a.setMaxDate(d);
			expect(a.maxDate).toBeDefined();
			expect(a.maxDate.toDateString() === "Tue Jul 21 2015").toBeTruthy();
		});
	});
	describe('method "daysInMonth"', function () {
		it('should return the days in a month', function () {
			expect(a.daysInMonth(2013,11) === 30).toBeTruthy();
			expect(a.daysInMonth(2013,12) === 31).toBeTruthy();
			//leap year
			expect(a.daysInMonth(2012,2) === 29).toBeTruthy();
		});
	});
	describe('method "whichDay"', function () {
		it('should return the number of the day of a week', function () {
			expect(a.whichDay(2013,11,22) === 5).toBeTruthy();
			expect(a.whichDay(2013,11,17) === 0).toBeTruthy();
		});
	});
	describe('method "hideCalendar"', function () {
		it('should hide the control´s calendar', function () {
			c = document.body.childElementCount;
			a.showCalendar();
			a.hideCalendar();
			expect(document.body.childElementCount === c).toBeTruthy();
		});
	});
	describe('method "getDayOfYear"', function () {
		it('should get Day number of the year', function () {
			expect(a.getDayOfYear(2013,11,22) === 322).toBeTruthy();
			expect(a.getDayOfYear(2013,01,01) === 1).toBeTruthy();
			expect(a.getDayOfYear(2013,12,01) === 342).toBeTruthy();
		});
	});
	describe('method "formatDate"', function () {
		it('should format a date using the specified format string', function () {
			d = new Date("July 21, 1983 01:15:00");
			expect(a.formatDate(d,"dd-mm-yy") === "21-07-1983").toBeTruthy();
			expect(a.formatDate(d,"mm-dd-yy") === "07-21-1983").toBeTruthy();
			expect(a.formatDate(d,"dd/mm/yy") === "21/07/1983").toBeTruthy();
		});
	});
	describe('method "updateValue"', function () {
		it('should udpate the control´s date value', function () {
			d = new Date();
			m = d.getMonth() + 1;
			y = d.getFullYear();
			$(a.dom.calendar).find('a[data-date="25"]').trigger('click');
			a.updateValue();
			expect(a.getValue() === y +"-0"+m+""+"-25T00:00:00-04:00").toBeTruthy();
		});
	});
	describe('method "getValueFromRawElement"', function () {
		it('should get the control´s value directly from the element', function () {
			d = new Date();
			m = d.getMonth() + 1;
			y = d.getFullYear();
			$(a.dom.calendar).find('a[data-date="25"]').trigger('click');
			expect(a.getValueFromRawElement() === y +"-0"+m+""+"-25T00:00:00-04:00").toBeTruthy();
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});