//***********************other class******************************************************//
describe('PMUI.field.TextField', function () {
    var a, b, c;
    describe('class behavior', function () {
        it('[US-19,h]should set a placeholder to a textfield created', function () {
            a = new PMUI.field.TextField();
            a.setPlaceholder("placeholder1");
            expect(a.controls[0].placeholder).toBeDefined();
            expect(a.controls[0].placeholder === "placeholder1").toBeTruthy();
        });
        it('[US-19,i]should set maximal length to a Text Field', function () {
            a = new PMUI.field.TextField();
            expect(a.controls[0].maxLength === 0).toBeTruthy();
            a.setMaxLength(10);
            expect(a.controls[0].maxLength === 10).toBeTruthy();
        });
 
        it('[US-19,j]should set function Trim on Blur to a text field', function () {
            a = new PMUI.field.TextField();
            a.setTrimOnBlur(true);
            expect(a.trimOnBlur).toBeDefined();
            expect(a.trimOnBlur === true).toBeTruthy();
        });
        it('[US-19,j]should get the TrimOnBlur value from a text Field', function () {
            a = new PMUI.field.TextField();
            a.setTrimOnBlur(true);
            expect(a.getTrimOnBlur() === true).toBeTruthy();
        });
    });
    describe('method "defineEvents"', function () {
        it('should set the events for a text Field', function () {
            a = new PMUI.field.TextField();
            a.defineEvents();
            expect(a.events).not.toBeNull();
            expect(a.events).toBeDefined();
        });
    });
});