//***********************other class******************************************************//
describe('PMUI.event.Event', function () {
	var a, b, bhtml, f;
	describe('method "setElement"', function () {
		it('should set the HTML element', function () {	
			b = new PMUI.ui.Button({
                    handler : function () {        
                    },
                    text : 'closed',
                    iconClass:"pmui-gridpanel-pager_previous",
                    iconPosition : "left",
                    messageTooltip : 'this button to close the window'
            });
			a = new PMUI.event.Event();		
			b.createHTML();
			bhtml = b.html;
			a.setElement(bhtml);
            expect(a.element).toBeDefined();
            expect(a.element.tagName).toBeDefined();
            expect(a.element.nodeType).toBeDefined();
            expect(a.element.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
	describe('method "setHandler"', function () {
		it('should set the handler, this must be function', function () {
			b = new PMUI.ui.Button({
                    handler : function () {        
                    },
                    text : 'closed',
                    iconClass:"pmui-gridpanel-pager_previous",
                    iconPosition : "left",
                    messageTooltip : 'this button to close the window'
            });
			a = new PMUI.event.Event();	
			f = function () {
                   alert('Action has been called!');
            	};
            expect(function () {a.setHandler(f)}).not.toThrow();
            expect(a.handler).toEqual(f);
		});
	});
	describe('method "setEventName"', function () {
		it('should "set the event name"', function () {
			b = new PMUI.ui.Button({
                    handler : function () {        
                    },
                    text : 'closed',
                    iconClass:"pmui-gridpanel-pager_previous",
                    iconPosition : "left",
                    messageTooltip : 'this button to close the window'
            });
			a = new PMUI.event.Event();	
			a.setEventName("new event");
			expect(a.eventName).toEqual("new event");
		});
	});
});