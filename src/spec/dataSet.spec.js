//***********************other class******************************************************//
describe('PMUI.data.DataSet', function () {
	var d, f, g, h, i;
	describe('class behavior', function () {
		it('[US-23,d] Should create a new DataSet object and insert items', function () {
			f = new PMUI.data.DataField();
			g = new PMUI.data.DataField();
			h = new PMUI.data.DataField();
			d = new PMUI.data.DataSet();
			d.setItems([f,g,h]);
			expect(d.items.asArray().length === 3).toBeTruthy();
			expect(d.items.asArray()[0] === f).toBeTruthy();
		});
		it('[US-23,e] Should create a new DataSet object and add a new item', function () {
			f = new PMUI.data.DataField();
			d = new PMUI.data.DataSet();
			expect(d.items.asArray().length === 0).toBeTruthy();
			d.addItem(f);
			expect(d.items.asArray().length === 1).toBeTruthy();
			expect(d.items.asArray()[0] === f).toBeTruthy();
		});
		it('[US-23,f] Should create a new DataSet object and get all data that it contains', function () {
			f = new PMUI.data.DataField();
			g = new PMUI.data.DataField();
			h = new PMUI.data.DataField();
			d = new PMUI.data.DataSet();
			d.setItems([f,g,h]);
			expect(d.items.asArray()[0] === f).toBeTruthy();
			expect(d.items.asArray()[1] === g).toBeTruthy();
			expect(d.items.asArray()[2] === h).toBeTruthy();
			expect(d.items.asArray()[1].typeField === "string").toBeTruthy();
			expect(d.items.asArray()[1].custom).toBeFalsy();

		});
	});
});
