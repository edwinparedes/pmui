//***********************other class******************************************************//
describe('PMUI.field.TextAreaField', function () {
	var a, b;
	beforeEach(function() {
		a = new PMUI.field.TextAreaField();
	});
	describe('class behavior', function () {
		it('[US-30,a]should be able to edit and hide the label', function () {
			expect(a.label === "[field]").toBeTruthy();
			a.setLabel("new_label");
			expect(a.label === "new_label").toBeTruthy();
			expect(a.labelVisible).toBeTruthy();
			a.hideLabel();
			expect(a.labelVisible).toBeFalsy();
		});
		it('[US-30,b]should be able to edit and hide the Helper', function () {
			expect(a.helper.message).not.toBeNull();
			expect(a.helper.message).toBeDefined();
			a.setHelper("hello");
			expect(a.helper.message === "hello").toBeTruthy();
			a.showHelper();
			expect(a.helperIsVisible).toBeTruthy();
			a.hideHelper();
			expect(a.helperIsVisible).toBeFalsy();
		});
		it('[US-30,c]should be able to edit the position of the Label', function () {
			expect(a.labelPosition === "left").toBeTruthy();
			a.setLabelPosition("top");
			expect(a.labelPosition === "top").toBeTruthy();
			a.setLabelPosition("left");
			expect(a.labelPosition === "left").toBeTruthy();
			expect(a.labelVerticalPosition === "top").toBeTruthy();
			a.setLabelVerticalPosition("center");
			expect(a.labelVerticalPosition === "center").toBeTruthy();
			a.setLabelVerticalPosition("bottom");
			expect(a.labelVerticalPosition === "bottom").toBeTruthy();
		});
		it('[US-30,c]should be able to edit the position of the Helper', function () {
			expect(a.tooltipPosition === "top").toBeTruthy();
			a.setTooltipPosition("bottom");
			expect(a.tooltipPosition === "bottom").toBeTruthy();
		});
	});
	describe('method "setControlHeight"', function () {
		it('should set the control Height', function () {
			expect(a.controls[0].height === "auto").toBeTruthy();
			a.setControlHeight(100);
			expect(a.controls[0].height === 100).toBeTruthy();
			a.setControlHeight("200px");
			expect(a.controls[0].height === 200).toBeTruthy();
		});
	});
	describe('method "setControlWidth"', function () {
		it('should set the control width', function () {
			expect(a.controls[0].width === "auto").toBeTruthy();
			a.setControlWidth(100);
			expect(a.controls[0].width === 100).toBeTruthy();
			a.setControlWidth("200px");
			expect(a.controls[0].width === 200).toBeTruthy();
		});
	});
	describe('method "setControls"', function () {
		it('should set the controls for the TextArea Field', function () {
			a.setControls();
			expect(a.controls.length === 1).toBeTruthy();
		});
	});
	/*describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});*/
});