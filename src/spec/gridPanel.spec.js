//***********************other class******************************************************//
describe('PMUI.grid.GridPanel', function () {
	var a, b, c, f, g, h;
    beforeEach(function () {
        a = new PMUI.grid.GridPanel({   draggable:true,
                                        filterable : true,
                                        pageSize:6,
                                        columns:[
                                            {
                                                title:'name',
                                                dataType:'string',
                                                columnData:'name' 
                                            },
                                            {
                                                title:'lastName',
                                                dataType:'string',
                                                columnData:'lastName',
                                                sortable:true
                                            },
                                            {
                                                title:'columna3',
                                                dataType:'button',
                                                width:'200px',
                                                onButtonClick: function (row, grid) {
                                                    console.log(row,grid);
                                                }
                                            }
                                        ],
                                        dataItems: [
                                            {
                                                name:'Carlos',
                                                lastName:'Alvarez'

                                            },
                                            {
                                                name:'Sergio',
                                                lastName:'Benitez'
                                            },
                                            {
                                                name:'Rodrigo',
                                                lastName:'Garcia'
                                            },
                                            {
                                                name:'Alvaro',
                                                lastName:'Gonzales'
                                            },
                                            {
                                                name:'Fernando',
                                                lastName:'Antezana'
                                            },
                                            {
                                                name:'Orlando',
                                                lastName:'Mendoza'
                                            },
                                            {
                                                name:'Maria',
                                                lastName:'Perez'
                                            }
                                        ]
                                        });
        document.body.appendChild(a.getHTML());
        a.defineEvents();
        b = new PMUI.grid.GridPanel();
        document.body.appendChild(b.getHTML());
    });
    afterEach(function () {
        $(a.getHTML()).remove();
        a = null;
        $(b.getHTML()).remove();
        b = null;
        f = null;
        g = null;
        h = null;
    });
    describe('method "indexInPage"', function () {
        it('should return the index number where the row is located in the current page', function () {
            a.setPageSize(2);
            expect(a.indexInPage(0)==0).toBeTruthy();
            expect(a.indexInPage(1)==1).toBeTruthy();
            expect(a.indexInPage(2)==0).toBeTruthy();
            expect(a.indexInPage(3)==1).toBeTruthy();
            expect(a.indexInPage(4)==0).toBeTruthy();
            expect(a.indexInPage(5)==1).toBeTruthy();
            expect(a.indexInPage(6)==0).toBeTruthy();
        });
    });
    describe('method "setColumns"', function () {
        it('should set the columns in a empty grid', function () {
            expect($(b.getHTML()).find('thead').contents().length === 0).toBeTruthy();
            f = [   {
                        title:'a',
                        dataType:'string',
                        columnData:'a' 
                    },
                    {
                        title:'b',
                        dataType:'string',
                        columnData:'b',
                        sortable:true
                    },
                    {
                        title:'c',
                        dataType:'button',
                        width:'200px',
                        onButtonClick: function (row, grid) {
                            console.log(row,grid);
                        }
                    }
                ]
            expect(function () {b.setColumns(f);}).not.toThrow();
            expect(b.getColumns().length === 3).toBeTruthy();
            expect($(b.getHTML()).find('thead').contents().length === 3).toBeTruthy();
            f = new PMUI.grid.GridPanelColumn(
                    {
                        grid:b,
                        title:'a',
                        dataType:'string',
                        columnData:'a'
                    }
                );
            g = new PMUI.grid.GridPanelColumn(
                    {
                        grid:b,
                        title:'b',
                        dataType:'string',
                        columnData:'b'
                    }
                );
            expect(function () {b.setColumns([f,g]);}).not.toThrow();
            expect(b.getColumns().length === 2).toBeTruthy();
            expect($(b.getHTML()).find('thead').contents().length === 2).toBeTruthy();
            expect(function () {b.setColumns("anyString")}).toThrow();
            expect(function () {b.setColumns(234)}).toThrow();
            expect(function () {b.setColumns(new PMUI.event.Event())}).toThrow();
        });
    });
    describe('method "getColumns"', function () {
        it('should get the columns as Array', function () {
            expect(a.getColumns()).not.toBeNull();
            expect(a.getColumns()).toBeDefined();
            expect(a.getColumns().length === 3).toBeTruthy();
            expect(b.getColumns().length === 0).toBeTruthy();
        });
    });
    describe('method "addColumn"', function () {
        it('should add a new Column into a grid', function () {
            f = new PMUI.grid.GridPanelColumn(
                    {
                        grid:b,
                        title:'a',
                        dataType:'string',
                        columnData:'a'
                    }
                );
            expect(function () {a.addColumn(
                {
                    title: 'MiddleName',
                    columnData: 'MidName'
                });}).not.toThrow();
            expect(a.getColumns().length === 4).toBeTruthy();
            expect($(a.getHTML()).find('thead').contents()[3].textContent === 'MiddleName').toBeTruthy();
            expect(function () {a.addColumn(f)}).not.toThrow();
            expect(a.getColumns().length === 5).toBeTruthy();
            expect($(a.getHTML()).find('thead').contents()[4].textContent === 'a').toBeTruthy();
            expect(function () {a.addColumn(34)}).toThrow();
            expect(function () {a.addColumn("anyString")}).toThrow();
            expect(function () {a.addColum(new PMUI.event.Event())}).toThrow();
        });
    });
    describe('method "clearColumns"', function () {
        it('should clear all column data', function () {
            expect($(a.dom.tbody).find('td').length).not.toEqual(0);
            a.clearColumns();
            expect($(a.dom.tbody).find('td').length).toEqual(0);
        });
    });
    describe('method "removeColumn"', function () {
        it('should remove a column, the parameter must be a string, a number or a PMUI.grid.GridPanelColumn', function () {
            var i = a.getColumns().length, j;
            expect(i).not.toEqual(0);
            expect($(a.dom.thead).find('th').contents().eq(-3).text() === "name").toBeTruthy();
            expect($(a.dom.thead).find('th').contents().eq(-2).text() === "lastName").toBeTruthy();
            expect($(a.dom.thead).find('th').contents().eq(-1).text() === "columna3").toBeTruthy();
            a.removeColumn(1);
            j = a.getColumns().length;
            expect(i).not.toEqual(j);
            expect(i-1).toEqual(j);
            expect($(a.dom.thead).find('th').contents().eq(-2).text() === "name").toBeTruthy();
            expect($(a.dom.thead).find('th').contents().eq(-1).text() === "columna3").toBeTruthy();
        });
    });
    describe('method "addItem"', function () {
        it('should add a new Row, it mus be a JSON or a PMUI.grid.GridPanelRow', function () {
            i = a.getItems().length;
            var row = new PMUI.grid.GridPanelRow({
                   data: {
                       name: "Alan",
                       lastName: "Wake"
                   }
                });
            var json = {data:{name:"Martin", lastName:"Smith"}};
            expect(function () {a.addItem(row,2);}).not.toThrow();
            expect($(a.dom.tbody).find('tr')[2].cells[0].textContent === "Alan").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[2].cells[1].textContent === "Wake").toBeTruthy();
            expect(function () {a.addItem(json,4);}).not.toThrow();
            expect($(a.dom.tbody).find('tr')[4].cells[0].textContent === "Martin").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[4].cells[1].textContent === "Smith").toBeTruthy();
        });
    });
    describe('method "removeItem"', function () {
        it('should remove a gridpanel Item, the item must be a PMUI.grid.GridPanelRow object', function () {
            f = a.items.asArray()[3];
            expect($(a.dom.tbody).find('tr')[3].cells[0].textContent === "Alvaro").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[4].cells[0].textContent === "Fernando").toBeTruthy();
            expect(typeof f === "object").toBeTruthy();
            expect(function () {a.removeItem(f)}).not.toThrow();
            expect($(a.dom.tbody).find('tr')[3].cells[0].textContent === "Alvaro").toBeFalsy();
            expect($(a.dom.tbody).find('tr')[3].cells[0].textContent === "Fernando").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[3].cells[1].textContent === "Antezana").toBeTruthy();
        });
        it('should remove a gridpanel Item, the item must be a number(index)', function () {
            expect($(a.dom.tbody).find('tr')[2].cells[0].textContent === "Rodrigo").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[3].cells[0].textContent === "Alvaro").toBeTruthy();
            expect(function () {a.removeItem(2)}).not.toThrow();
            expect($(a.dom.tbody).find('tr')[2].cells[0].textContent === "Rodrigo").toBeFalsy();
            expect($(a.dom.tbody).find('tr')[2].cells[0].textContent === "Alvaro").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[2].cells[1].textContent === "Gonzales").toBeTruthy();
        });
        it('should remove a gridpanel Item, the item must be a string(item´s id)', function () {
            f = a.items.asArray()[5];
            expect($(a.dom.tbody).find('tr')[5].cells[0].textContent === "Orlando").toBeTruthy();
            a.goToPage(1);
            expect($(a.dom.tbody).find('tr')[0].cells[0].textContent === "Maria").toBeTruthy();
            a.goToPage(0);
            expect(typeof f.id === "string").toBeTruthy();
            //expect(function () {a.removeItem(f.id)}).not.toThrow();
            //expect($(a.dom.tbody).find('tr')[5].cells[0].textContent === "Orlando").toBeFalsy();
            //expect($(a.dom.tbody).find('tr')[5].cells[0].textContent === "Maria").toBeTruthy();
            //expect($(a.dom.tbody).find('tr')[5].cells[1].textContent === "Perez").toBeTruthy();
        });
    });
    describe('method "moveItem"', function () {
        it('should move the item to another index, the item must be a PMUI.grid.GridPanelRow object', function () {
            f = a.items.asArray()[3];
            expect($(a.dom.tbody).find('tr')[3].cells[0].textContent === "Alvaro").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[5].cells[0].textContent === "Orlando").toBeTruthy();
            expect(typeof f === "object").toBeTruthy();
            expect(function () {a.moveItem(f, 5)}).not.toThrow();
            expect($(a.dom.tbody).find('tr')[5].cells[0].textContent === "Alvaro").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[4].cells[0].textContent === "Orlando").toBeTruthy();
        });
        it('should move the item to another index, the item must be a number(index)', function () {
            expect($(a.dom.tbody).find('tr')[2].cells[0].textContent === "Rodrigo").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[4].cells[0].textContent === "Fernando").toBeTruthy();
            expect(function () {a.moveItem(2,4)}).not.toThrow();
            expect($(a.dom.tbody).find('tr')[4].cells[0].textContent === "Rodrigo").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[4].cells[1].textContent === "Garcia").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[3].cells[0].textContent === "Fernando").toBeTruthy();
        });
        it('should move the item to another index, the item must be a string(item´s id)', function () {
            f = a.items.asArray()[5];
            expect($(a.dom.tbody).find('tr')[5].cells[0].textContent === "Orlando").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[0].cells[0].textContent === "Carlos").toBeTruthy();
            expect(typeof f.id === "string").toBeTruthy();
            expect(function () {a.moveItem(f.id, 0)}).not.toThrow();
            expect($(a.dom.tbody).find('tr')[0].cells[0].textContent === "Orlando").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[0].cells[1].textContent === "Mendoza").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[5].cells[0].textContent === "Fernando").toBeTruthy();
        });
    });
    describe('method "getData"', function () {
        it('should return all the gridpanel data, expressed in objects', function () {
            g = a.items.asArray().length;
            expect(a.getData().length === g).toBeTruthy();
            expect(a.getData()[1].name === "Sergio").toBeTruthy();
            expect(a.getData()[1].lastName === "Benitez").toBeTruthy();
            expect(a.getData()[6].name === "Maria").toBeTruthy();
            expect(a.getData()[6].lastName === "Perez").toBeTruthy();
        });
    });
    describe('method "createHTML"', function () {
        it('should create a new HTML element', function () {
            var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.className === "pmui-gridpanel").toBeTruthy();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
            expect($(html).find('table').length).not.toEqual(0);
            expect($(html).find('tr').length).not.toEqual(0);
            expect($(html).find('td').length).not.toEqual(0);
        });
    });
    describe('method "goToPage"', function () {
        it('should go to another page, the parameter must be a number', function () {
            f = a.items.asArray().length;
            expect(f).not.toEqual(0);
            g = a.pageSize;
            expect($(a.dom.tbody).find('tr').length === g).toBeTruthy();
            expect(function () {a.goToPage(1)}).not.toThrow();
            expect($(a.dom.tbody).find('tr').length === f-g).toBeTruthy();
            expect(function () {a.goToPage(5)}).not.toThrow();
            expect($(a.dom.tbody).find('tr').length === f-g).toBeTruthy();
        });
        it('should go to another page, the other parameter must be a boolean to go to a page even if there is no data', function () {
            a.removeItem(2);
            expect($(a.dom.tbody).find('tr').length).not.toEqual(0);
            a.goToPage(1, true);
            expect($(a.dom.tbody).find('tr').length).toEqual(1);
            expect($(a.dom.tbody).find('td').text()).toEqual("There are no items to show.");
            a.goToPage(0);
            expect($(a.dom.tbody).find('tr').length).not.toEqual(0);  
        });
    });
    describe('method "setPageSize"', function () {
        it('should set the page size, it must be a number', function () {
            expect(function () {a.setPageSize("45")}).toThrow();
            f = a.items.asArray().length;
            g = 2;
            expect(function () {a.setPageSize(g)}).not.toThrow();
            h = parseInt(f/g);
            i = parseInt(f%g);
            expect(a.getTotalPages() === h+i).toBeTruthy();
            expect($(a.dom.tbody).find('tr')[0].cells[0].textContent === "Carlos").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[1].cells[0].textContent === "Sergio").toBeTruthy();
            a.goToPage(1);
            expect($(a.dom.tbody).find('tr')[0].cells[0].textContent === "Rodrigo").toBeTruthy();
            a.goToPage(2);
            expect($(a.dom.tbody).find('tr')[0].cells[0].textContent === "Fernando").toBeTruthy();
            a.goToPage(3);
            expect($(a.dom.tbody).find('tr')[0].cells[0].textContent === "Maria").toBeTruthy();
        });
    });
    describe('method "getTotalPages"', function () {
        it('should return the total Pages number', function () {
            f = a.items.asArray().length;
            g = 2;
            expect(function () {a.setPageSize(g)}).not.toThrow();
            h = parseInt(f/g);
            i = parseInt(f%g);
            expect(a.getTotalPages() === h+i).toBeTruthy();
        });
        it('should return the total pages number, the parameter must be a boolean(filtered results)', function () {
            expect(function () {a.setPageSize(2)}).not.toThrow();
            expect(a.getTotalPages() === 4).toBeTruthy();
            a.filter("ez");
            expect(a.getTotalPages(true) === 2).toBeTruthy();
        });
    });
    describe('method "setCurrentPage"', function () {
        it('should set the current page, it must be a number', function () {
            f = a.items.asArray().length;
            expect(f).not.toEqual(0);
            g = 2;
            a.setPageSize(g);
            expect($(a.dom.tbody).find('tr').length === g).toBeTruthy();
            a.setCurrentPage(1);
            expect($(a.dom.tbody).find('tr').length === g).toBeTruthy();
            a.setCurrentPage(2);
            expect($(a.dom.tbody).find('tr').length === g).toBeTruthy();
            a.setCurrentPage(3);
            expect($(a.dom.tbody).find('tr').length === g-1).toBeTruthy();
        });
    });
    describe('method "clearFilter"', function () {
        it('should clear the filter and shows a complete gridPanel', function () {
            a.filter("ez");
            expect($(a.dom.tbody).find('tr').length === 4).toBeTruthy();
            expect(a.filteredItems.asArray().length === 4).toBeTruthy();
            a.clearFilter();
            expect($(a.dom.tbody).find('tr').length === 6).toBeTruthy();
            a.goToPage(1);
            expect($(a.dom.tbody).find('tr').length === 1).toBeTruthy();
        });
    });
    describe('method "sort"', function () {
        it('should execute sorting, the parameters musts a criteria and a sorting type', function () {
            a.sort("name","asc");
            expect($(a.dom.tbody).find('tr')[0].cells[0].textContent === "Alvaro").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[1].cells[0].textContent === "Carlos").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[2].cells[0].textContent === "Fernando").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[3].cells[0].textContent === "Maria").toBeTruthy();
            a.sort("name", "desc");
            expect($(a.dom.tbody).find('tr')[0].cells[0].textContent === "Sergio").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[1].cells[0].textContent === "Rodrigo").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[2].cells[0].textContent === "Orlando").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[3].cells[0].textContent === "Maria").toBeTruthy();
            a.sort("lastName", "asc");
            expect($(a.dom.tbody).find('tr')[0].cells[1].textContent === "Alvarez").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[1].cells[1].textContent === "Antezana").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[2].cells[1].textContent === "Benitez").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[3].cells[1].textContent === "Garcia").toBeTruthy();
            a.sort("lastName", "desc");
            expect($(a.dom.tbody).find('tr')[0].cells[1].textContent === "Perez").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[1].cells[1].textContent === "Mendoza").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[2].cells[1].textContent === "Gonzales").toBeTruthy();
            expect($(a.dom.tbody).find('tr')[3].cells[1].textContent === "Garcia").toBeTruthy();
        });
    });
    describe('method "filter"', function () {
        it('should set a filter that makes a row filtering using a criteria', function () {
            expect($(a.dom.tbody).find('tr').length === 6).toBeTruthy();
            a.filter("Carlos");
            expect($(a.dom.tbody).find('tr').length === 1).toBeTruthy();
            expect(a.filteredItems.asArray().length === 1).toBeTruthy();
            a.clearFilter();
            expect($(a.dom.tbody).find('tr').length === 6).toBeTruthy();
        });
    });
    describe('method "disableFiltering"', function () {
        it('should disable gridpanel filtering, removing the search control', function () {
            expect($(a.dom.toolbar)[0].style.cssText).not.toContain("display: none");
            expect(a.filterable).toBeTruthy();
            a.disableFiltering();
            expect(a.filterable).toBeFalsy();
            expect($(a.dom.toolbar)[0].style.cssText).toContain("display: none");
        });
    });
    describe('method "enableFiltering"', function () {
        it('should enable gridpanel filtering, adding the search control', function () {
            expect($(a.dom.toolbar)[0].style.cssText).not.toContain("display: none");
            expect(a.filterable).toBeTruthy();
            a.disableFiltering();
            expect(a.filterable).toBeFalsy();
            expect($(a.dom.toolbar)[0].style.cssText).toContain("display: none");
            a.enableFiltering();
            expect(a.filterable).toBeTruthy();
            expect($(a.dom.toolbar)[0].style.cssText).not.toContain("display: none");
        });
    });
});