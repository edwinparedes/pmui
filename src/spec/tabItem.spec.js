//***********************other class******************************************************//
describe('PMUI.item.TabItem', function () {
	var a, b, c;
	beforeEach(function () {
		a = new PMUI.item.TabItem({panel: new PMUI.panel.TreePanel()});
		document.body.appendChild(a.getHTML());
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
	});
	describe('class behavior', function () {
		it('[US-39,a]should set the title for the TabItem', function () {
			//expect(a.title === "[untitle]").toBeTruthy();
			a.setTitle("new_title");
			expect(a.title === "new_title").toBeTruthy();
		});
		it('[US-39,a]should get the title from the TabItem', function () {
			//expect(a.getTitle() === "[untitle]").toBeTruthy();
			a.setTitle("new_title");
			expect(a.getTitle() === "new_title").toBeTruthy();
		});
		it('[US-39,b]should set the panel for the TabItem', function () {
			//expect(a.panel).toBeNull();
			c = new PMUI.panel.TreePanel();
			a.setPanel(c);
			expect(a.panel === c).toBeTruthy();
		});
		it('[US-39,b]should get the panel from the TabItem', function () {
			//expect(a.getPanel()).toBeNull();
			c = new PMUI.panel.TreePanel();
			a.setPanel(c);
			expect(a.getPanel() === c).toBeTruthy();
		});
		it('[US-39,c]should return a boolean value of selected property', function () {
			expect(a.isSelected()).toBeNull();
			a.select();
			expect(a.isSelected()).toBeTruthy();
			a.deselect();
			expect(a.isSelected()).toBeFalsy();
		});
		it('[US-49,a]should be able to insert an image icon to a TabItem type by css', function () {
			a.setIcon('pmui-gridpanel-engranaje');
			var html = a.createHTML();
			expect(a.icon).toEqual(a.classNameIcon.className);
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
			expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
	describe('method "select"', function () {
		it('should set the selected property to true', function () {
			expect(a.isSelected()).toBeNull()
			a.select();
			expect(a.isSelected()).toBeTruthy();
		});
	});
	describe('method "deselect"', function () {
		it('should set the selected property to true', function () {
			expect(a.isSelected()).toBeNull()
			a.deselect();
			expect(a.isSelected()).toBeFalsy();
		});
	});

});