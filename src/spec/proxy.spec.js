//***********************other class******************************************************//
describe('PMUI.proxy.Proxy', function  () {
	var a, d;
	beforeEach(function () {
		a = new PMUI.proxy.Proxy();
	});
	afterEach(function () {
		a = null;
	});
	describe('method "setData"', function () {
		it('should set data for the proxy', function () {
			d = {data1:"data1", data2: "data2"};
			expect(function () {a.setData(d);}).not.toThrow();
			expect(a.data).toEqual(d);
			expect(typeof a.data === 'object').toBeTruthy();
		});
	});
	describe('method "getData"', function () {
		it('should raturn data for the proxy', function () {
			d = {data1:"data1", data2: "data2"};
			a.setData(d);
			expect(a.getData()).toEqual(d);
			expect(typeof a.getData() === 'object').toBeTruthy();
		});
	});
});