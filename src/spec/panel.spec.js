//***********************other class******************************************************//
describe('PMUI.core.Panel', function () {
    var a, b, c;
    describe('class Behavior', function () {
        it("[US-1,f] Constructor should create a new instance of the Class", function () {
            panel1 = new Panel();
            expect(panel1).toBeDefined();
            expect(panel1.type).toEqual('Panel');
        });
        it('[US-1,g] Should accept to create and remove a new container', function () {
            panel1 = new Panel();
            panel2 = new Panel();
            var i = panel1.getItems().length;
            panel1.addItem(panel2);
            expect(panel1.getItems().length).toEqual(i + 1);
            panel1.removeItem(panel2);
            expect(panel1.getItems().length).toEqual(i);
        });

    });
    describe('method "isDirectParentOf"', function () {
        it('should return a false boolean value', function () {
            panel1 = new Panel();
            panel2 = new Panel();
            expect(panel1.isDirectParentOf(panel2)).not.toBeTruthy();
        });
        it('should return a true boolean value', function () {
            panel1 = new Panel();
            panel2 = new Panel();
            panel1.addItem(panel2);
            expect(panel1.isDirectParentOf(panel2)).toBeTruthy();
        });
    });
    describe('method "clearItems"', function () {
        it('should remove all the child items', function () {
            panel1 = new Panel();
            panel2 = new Panel();
            panel3 = new Panel();
            var i = panel1.getItems().length;
            panel1.addItem(panel2);
            panel1.addItem(panel3);
            expect(panel1.getItems().length).toEqual(i + 2);
            panel1.clearItems();
            expect(panel1.getItems().length).toEqual(i);
        });
    });
    describe('method "setItems"', function () {
        it('should remove all current child items and set a new child', function () {
            var panel1 = new Panel(),
                panel2 = new Panel(),
                panel3 = new Panel();
            panel1.addItem(panel2);
            panel1.setItems(panel3);
            expect(panel1.getItems().length).toEqual(1);
        });
    });
    describe('method "createHTML"', function () {
        it('should create a new HTML element', function () {
            var html = panel1.createHTML();
            panel1 = new Panel();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
        });
    });
    describe('method "getItems"', function () {
        it('should return an array with all items defined as child elements', function () {
            var a = panel1.getItems();
            panel1 = new Panel();
            panel2 = new Panel();
            panel3 = new Panel();
            panel1.addItem(panel2);
            panel1.addItem(panel3);
            expect(a).toBeDefined();
        });
    });
    describe('method "getItem"', function () {
        it('should return an specific item from the array already created', function () {
            var a;
            panel1 = new Panel();
            panel2 = new Panel();
            panel3 = new Panel();
            panel1.addItem(panel2);
            panel1.addItem(panel3);
            expect(panel1.getItem(1)).toBeDefined();
            expect(panel1.getItem(1)).toBe(panel3);
        });
    });
    
    describe('method "setPanel"', function () {
        it('should set a new Panel object', function () {
            a = new PMUI.core.Panel();
            b = new PMUI.core.Panel();
            expect(a.panel).toBeNull();
            a.setPanel(b);
            expect(a.panel).not.toBeNull();
            expect(a.panel).toBeDefined();
            expect(a.panel === b).toBeTruthy();
        });
    });
    describe('method "setParent"', function () {
        it('should set a parent for the object', function () {
            a = new PMUI.core.Panel();
            b = new PMUI.core.Panel();
            expect(a.parent).toBeNull();
            a.setParent(b);
            expect(a.parent).not.toBeNull();
            expect(a.parent).toBeDefined();
            expect(a.parent === b).toBeTruthy();
        });
    });
    describe('method "setLayout"', function () {
        it('should set a Layout for the Object', function () {
            a = new PMUI.core.Panel();
            a.setLayout('box')
            expect(a.layout).not.toBeNull();
            expect(a.layout).toBeDefined();
            expect(a.layout.type === "Box").toBeTruthy();
        });
    });
    describe('method "getParent"', function () {
        it('should get the parent from the object', function () {
            a = new PMUI.core.Panel();
            b = new PMUI.core.Panel();
            a.setParent(b);
            expect(a.getParent()).not.toBeNull();
            expect(a.getParent()).toBeDefined();
            expect(a.getParent() === b).toBeTruthy();
        });
    });
    describe('method "setPadding"',function(){
        
        it ('should be able to assign the values ​​of the padding with a numerical value'+
            ' assigning it the height and width of the panel',function(){
            var p = new PMUI.core.Panel();
            p.setWidth(100);
            p.setHeight(100);
            p.setPadding(5);
            for(var i=0;i<=3;i++) {
               expect(p.getPadding()[i]==('5px')).toBeTruthy();
            }
            expect(p.topPadding).toEqual('5px');
            expect(p.rightPadding).toEqual('5px');
            expect(p.leftPadding).toEqual('5px');
            expect(p.bottomPadding).toEqual('5px');

        });

        it ('should be able to assign the values ​​of the padding with a parameter '+
            'string assigning it the height and width of the panel',function(){
            var p = new PMUI.core.Panel();
            p.setWidth(100);
            p.setHeight(100);
            p.setPadding('5px');
            for(var i=0;i<=3;i++) {
               expect(p.getPadding()[i]==('5px')).toBeTruthy();
            }
            expect(p.topPadding).toEqual('5px');
            expect(p.rightPadding).toEqual('5px');
            expect(p.leftPadding).toEqual('5px');
            expect(p.bottomPadding).toEqual('5px');

        });

        it ('should be able to assign the values ​​of the padding with a string of'+
            ' two parameters assigning it to the panel width and height respectively',function(){
            p = new PMUI.core.Panel();
            p.setWidth(100);
            p.setHeight(100);
            p.setPadding('10px 5px');
            for(var i=0;i<=3;i++) {
                if(i%2==0){
                    expect(p.getPadding()[i]==('10px')).toBeTruthy();
                }else{
                    expect(p.getPadding()[i]==('5px')).toBeTruthy();
                }
               
            }
            expect(p.topPadding).toEqual('10px');
            expect(p.leftPadding).toEqual('5px');
            expect(p.bottomPadding).toEqual('10px');
            expect(p.rightPadding).toEqual('5px');
        });

        it ('should be able to assign the values ​​of the padding with a string of three '+
            'parameters the first to top the second to (left-right) and third to bottom',function(){
            p = new PMUI.core.Panel();
            p.setWidth(100);
            p.setHeight(100);
            p.setPadding('5px 10px 15px');

            expect(p.getPadding()[0]==('5px')).toBeTruthy();
            expect(p.getPadding()[1]==('10px')).toBeTruthy();
            expect(p.getPadding()[2]==('15px')).toBeTruthy();
            expect(p.getPadding()[3]==('10px')).toBeTruthy();

            expect(p.topPadding).toEqual('5px');
            expect(p.leftPadding).toEqual('10px');
            expect(p.bottomPadding).toEqual('15px');
            expect(p.rightPadding).toEqual('10px');
        });

        it ('should be able to assign the values ​​of the padding with a string of four parameters'+
            ' the first for the top, right, bottom and left respectively',function(){
            p = new PMUI.core.Panel();
            p.setWidth(100);
            p.setHeight(100);
            p.setPadding('5px 10px 15px 20px');
            document.body.appendChild(p.getHTML());
            expect(p.getPadding()[0]==('5px')).toBeTruthy();
            expect(p.getPadding()[1]==('10px')).toBeTruthy();
            expect(p.getPadding()[2]==('15px')).toBeTruthy();
            expect(p.getPadding()[3]==('20px')).toBeTruthy();

            expect(p.topPadding).toEqual('5px');
            expect(p.rightPadding).toEqual('10px');
            expect(p.bottomPadding).toEqual('15px');
            expect(p.leftPadding).toEqual('20px');  
        });
    });

    describe('method "setBorderWidth"', function(){
        
        it('should be able to assign the values ​​of the edge of the panel with a numeric value', function () {
            p = new PMUI.core.Panel();
            p.setWidth(100);
            p.setHeight(100);
            p.setBorderWidth(5);
            expect(p.getBorderWidth()===('5px')).toBeTruthy();
        });
            
        it('should be able to assign the values of the edge of the panel with a string value', function () {
            p = new PMUI.core.Panel();
            p.setWidth(100);
            p.setHeight(100);
            p.setBorderWidth('5px');
            expect(p.getBorderWidth()===('5px')).toBeTruthy();
        });

    });
});