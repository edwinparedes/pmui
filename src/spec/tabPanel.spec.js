//***********************other class******************************************************//
describe('PMUI.panel.TabPanel', function () {
	var a, b, c, e, i, p, q, r, t;
	beforeEach(function () {
		p = new PMUI.form.FormPanel({
                    width: 604, 
                    height: 130,
                    fieldset: true,
                    legend: "my fieldset panel",
                    items: [
                        {
                            pmType: "text",
                            label: "Name",
                            id: "123",
                            value: "",
                            placeholder: "insert your name",
                            name: "name"
                        },{
                            pmType: "text",
                            label: "Last name",
                            value: "",
                            placeholder: "your lastname here asshole!",
                            name: "lastname"
                        }, {
                            pmType: "panel",
                            layout: 'hbox',
                            items: [
                                {
                                    pmType: "text",
                                    label: "E-mail",
                                    value: "",
                                    name: "email"
                                },{
                                    pmType: "text",
                                    label: "Phone",
                                    value: "555",
                                    name: "phone"
                                }
                            ]
                        }
                    ],
                    layout: "vbox"
                });

                //Layout Panel *-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
                q = new PMUI.form.FormPanel({
                    width: 604, 
                    height: 130,
                    fieldset: true,
                    legend: "my fieldset panel",
                    items: [
                        {
                            pmType: "panel",
                            layout: 'hbox',
                            items: [
                                {
                                    pmType: "text",
                                    label: "E-mail",
                                    value: "",
                                    name: "email"
                                },{
                                    pmType: "text",
                                    label: "Phone",
                                    value: "555",
                                    name: "phone"
                                }
                            ]
                        }
                    ],
                    layout: "vbox"
                });                

                //FormPanel2 *-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
                 var layout, layoutOptions;

                         layoutOptions = {
                             id: "layout-id",
                             height: 450,
                             eastConfig: {
                                 size: 150,
                                 cssProperties: {
                                     "background-color" : "#800000"
                                 }
                             }
                         };

                         lp = new PMUI.panel.LayoutPanel(layoutOptions);
                //Tab Item*--**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
                a = new PMUI.panel.TabPanel({
                                            height : 400,
                                            width : 600,
                                            items:[
                                                        {   
                                                            title:'Form Panel',
                                                            panel:p
                                                        },
                                                        {   
                                                            title:'Form Panel2',
                                                            panel:q
                                                        },
                                                        {   
                                                            title:'Layout Panel',
                                                            panel:lp
                                                        }
                                                    ],
                                            //itemsPosition : {position:"left",percentageWidth : 22}
                                            itemsPosition : 'top'
                                        });
        document.body.appendChild(a.getHTML());
        a.defineEvents();

	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
		i = null;
		e = null;
	});
	describe('class behavior', function () {
		it('[US-35,a]should add a new Item for a tabPanel', function () {
			b = new PMUI.panel.TabPanel();
			document.body.appendChild(b.getHTML());
			expect(b.items.getSize() === 0).toBeTruthy();
			expect(b.html.getElementsByTagName("li").length === 0).toBeTruthy();
			expect(function () {b.addItem({
                							title: 'Form Panel',
                							panel: new PMUI.form.FormPanel()
										})}).not.toThrow();
			expect(b.items.getSize() === 1).toBeTruthy();
			expect($(b.dom.listContainer).find('span')[0].textContent === "Form Panel").toBeTruthy();
			i = new PMUI.item.TabItem({
										title:'tab1', 
										panel: new PMUI.form.FormPanel()
									});
			expect(function () {b.addItem(i)}).not.toThrow();
			expect(b.items.getSize() === 2).toBeTruthy();
			expect($(b.dom.listContainer).find('span')[1].textContent === "tab1").toBeTruthy();
			expect(function () {b.addItem("new item")}).toThrow();
			expect(function () {b.addItem(new PMUI.event.Event())}).toThrow();
			$(b.getHTML()).remove();
			b = null;
		});
		it('[US-35,a]should set tabs from an array with parameters, if the tabPanel has items, this method erase them', function () {
			b = new PMUI.panel.TabPanel();
			document.body.appendChild(b.getHTML());
			expect(b.items.getSize() === 0).toBeTruthy();
			expect(b.html.getElementsByTagName("li").length === 0).toBeTruthy();
			p = new PMUI.item.TabItem({
										title:'tab1', 
										panel: new PMUI.form.FormPanel()
									});
			q = new PMUI.item.TabItem({
										title:'tab2', 
										panel: new PMUI.form.FormPanel()
									});
			r = new PMUI.item.TabItem({
										title:'tab3', 
										panel: new PMUI.form.FormPanel()
									});
			expect(function () {b.setItems([p, q, r])}).not.toThrow();
			expect(b.items.getSize() === 3).toBeTruthy();
			expect($(b.dom.listContainer).find('span').length === 3).toBeTruthy();
			expect(function () {b.setItems([
												{
													title: 'new tab 1',
													panel: new PMUI.form.FormPanel()
												},
												{
													title: 'new tab 2',
													panel: new PMUI.form.FormPanel()
												}
				])}).not.toThrow();
			expect(b.items.getSize() === 2).toBeTruthy();
			expect($(b.dom.listContainer).find('span').length === 2).toBeTruthy();
			p = null;
			q = null;
			r = null;
		});
		it('[US-35,b]should remove a tab from the tabpanel, the parameter must be the index', function () {
			expect(a.items.getSize() === 3).toBeTruthy();
			expect(a.html.getElementsByTagName("li").length === 3).toBeTruthy();
			expect($(a.dom.listContainer).find('span')[0].textContent === "Form Panel").toBeTruthy();
			expect($(a.dom.listContainer).find('span')[1].textContent === "Form Panel2").toBeTruthy();
			expect($(a.dom.listContainer).find('span')[2].textContent === "Layout Panel").toBeTruthy();
			expect(function () {a.removeItem(1);}).not.toThrow();
			expect(a.items.getSize() === 2).toBeTruthy();
			expect(a.html.getElementsByTagName("li").length === 2).toBeTruthy();
			expect($(a.dom.listContainer).find('span')[0].textContent === "Form Panel").toBeTruthy();
			expect($(a.dom.listContainer).find('span')[1].textContent === "Layout Panel").toBeTruthy();
		});
		it('[US-35,c]should set the height for the tabpanel', function () {
			expect(a.height === 400).toBeTruthy();
			expect(a.html.style.height === "400px").toBeTruthy();
			expect(function () {a.setHeight(300);}).not.toThrow();
			expect(a.height === 300).toBeTruthy();
			expect(a.html.style.height === "300px").toBeTruthy();
			expect(function () {a.setHeight('500px');}).not.toThrow();
			expect(a.height === 500).toBeTruthy();
			expect(a.html.style.height === "500px").toBeTruthy();
		});
		it('[US-49,b]I should be able to adjust the position of TabPanel both top and left', function () {
			expect(function () {a.setItemsPosition({position: 'left', percentageWidth: 22})}).not.toThrow();
			expect(a.itemsPosition === "left").toBeTruthy();
			expect(a.dom.panelContainer.style.display).toEqual('inline-block');
			expect(a.dom.listContainer.style.display).toEqual('inline-block');
			expect(function () {a.setItemsPosition('top')}).not.toThrow();
			expect(a.dom.panelContainer.style.display).toEqual('block');
			expect(a.dom.listContainer.style.display).toEqual('block');
			expect(a.itemsPosition == 'top').toBeTruthy();
			expect(function () {a.setItemsPosition({position: 'right', percentageWidth: 22})}).not.toThrow();
			expect(a.itemsPosition === "right").toBeTruthy();
			expect(a.dom.panelContainer.style.display).toEqual('inline-block');
			expect(a.dom.listContainer.style.display).toEqual('inline-block');
			expect(function () {a.setItemsPosition('bottom')}).not.toThrow();
			expect(a.dom.panelContainer.style.display).toEqual('block');
			expect(a.dom.listContainer.style.display).toEqual('block');
			expect(a.itemsPosition == 'bottom').toBeTruthy();
			expect(function () {a.setItemsPosition({position: 'top', percentageWidth: 20})}).toThrow();
			expect(function () {a.setItemsPosition({position: 'bottom', percentageWidth: 20})}).toThrow();
			expect(function () {a.setItemsPosition('left')}).toThrow();
			expect(function () {a.setItemsPosition('right')}).toThrow();
		});
		it('[US-49,c]I should be able to have a TabPanel that has the collapsible property', function () {
			var x;
			a.setItemsPosition({position: 'right', percentageWidth: 22})
			expect(a.visible).toBeTruthy();
			expect(parseInt(a.dom.collapsibleBar.style.height)==a.getHeight()).toBeTruthy();
			x = parseInt($(a.dom.listContainer).find('li')[0].style.width);
			$(a.html).find('div[class="pmui-collapsibleBar"]').trigger('click');
			expect($(a.dom.listContainer).find('li')[0].style.width).not.toEqual(x);
			expect(parseInt($(a.dom.listContainer).find('li')[0].style.width)).toBeLessThan(x);
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			a = new PMUI.panel.TabPanel();
			var html = a.createHTML();
			expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
	describe('method "defineEvents"', function () {
		it('should define the events for TabPanel', function () {
			a.defineEvents();
			expect(a.eventDefine).toBeTruthy();
		});
	});
});