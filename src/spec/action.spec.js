//***********************other class******************************************************//
describe('PMUI.event.Action', function () {
	var a, b;
	describe('method "setActionIcon"', function () {
		it('should set the Action icon', function () {
			a = new PMUI.event.Action({
               text: 'Save Document',
               icon: 'pm-icon-save',
               disabled: false,
               handler: function () {
                   alert('Action has been called!');
            	}
        	});
			b = a.actionIcon;
			expect(a.actionIcon).toEqual(b);
			a.setActionIcon("newActionIcon");
			expect(a.actionIcon).not.toEqual(b);
			expect(a.actionIcon).toEqual("newActionIcon");
		});
	});
	describe('method "setActionText"', function () {
		it('should set action text', function () {
			a = new PMUI.event.Action({
               text: 'Save Document'
        	});
        	expect(a.actionText).toEqual('Save Document');
        	a.setActionText("another text");
        	expect(a.actionText).toEqual('another text');
		});
	});
	describe('method "setDisable"', function () {
		it('should set the disable property', function () {
			a = new PMUI.event.Action({
                disabled: false
        	});
        	expect(a.disabled).toBeFalsy();
        	a.setDisable(true);
        	expect(a.disabled).toBeTruthy();
		});
	});
	describe('method "getIcon"', function () {
		it('should return the action Icon', function () {
			a = new PMUI.event.Action({
               text: 'Save Document',
               icon: 'pm-icon-save'
        	});
			b = a.actionIcon;
			expect(a.getIcon()).toEqual(b);
			a.setActionIcon("newActionIcon");
			expect(a.getIcon()).toEqual("newActionIcon");
		});
	});
	describe('method "getText"', function () {
		it('should return the action text', function () {
			a = new PMUI.event.Action({
               text: 'Save Document',
               icon: 'pm-icon-save'
        	});
			b = a.actionText;
			expect(a.getText()).toEqual(b);
			a.setActionText("newActiontext");
			expect(a.getText()).toEqual("newActiontext");
		});
	});
	describe('method "setHandler"', function () {
		it('should set the handler', function () {
			a = new PMUI.event.Action();
			b = function () {
				alert('action has been called!');
			};
			a.setHandler(b);
			expect(typeof a.handler === 'function').toBeTruthy();
			expect(a.handler).toEqual(b);
		});
	});
	describe('method "enable"', function () {
		it('should enable the action', function () {
			a = new PMUI.event.Action({
               disabled: true,
               handler: function () {
                   alert('Action has been called!');
            	}
        	});
        	expect(a.disabled).toBeTruthy();
        	a.enable();
        	expect(a.disabled).toBeFalsy();
		});
	});
	describe('method "disable"', function () {
		it('should disable the action', function () {
			a = new PMUI.event.Action({
               disabled: false,
               handler: function () {
                   alert('Action has been called!');
            	}
        	});
        	expect(a.disabled).toBeFalsy();
        	a.disable();
        	expect(a.disabled).toBeTruthy();
		});
	});
	describe('method "isEnabled"', function () {
		it('should enable the action', function () {
			a = new PMUI.event.Action({
               disabled: true,
               handler: function () {
                   alert('Action has been called!');
            	}
        	});
        	expect(a.isEnabled()).toBeFalsy();
        	a.enable();
        	expect(a.isEnabled()).toBeTruthy();
		});
	});
	describe('method "execute"', function () {
		it('should execute the action that is enabled', function () {
			a = new PMUI.event.Action({
               text: 'Save Document',
               icon: 'pm-icon-save',
               disabled: false,
               handler: function () {
                   alert('Action has been called!');
            	}
        	});
        	expect(a.disabled).toBeFalsy();
        	expect(typeof a.handler === 'function').toBeTruthy();
		});
	});
});