//***********************other class******************************************************//
describe('PMUI.field.UploadField', function () {
	var a, b;
	beforeEach(function () {
		a = new PMUI.field.UploadField(
											{
												label: 'Upload',
												multiple: false,
												accept : 'application/pdf',
											}
			);
		document.body.appendChild(a.getHTML());
	});
	afterEach(function () {
		$(a.getHTML()).remove();
		a = null;
	});
	describe('method "setAcceptedFiles"', function () {
		it('should set the type of accepted files', function () {
			expect(a.controls[0].accept === 'application/pdf').toBeTruthy();
			expect(a.controls[0].html.accept.toString() === 'application/pdf').toBeTruthy();
			expect(function () {a.setAcceptedFiles(2)}).toThrow();
			expect(function () {a.setAcceptedFiles({type: 'application/pdf'})}).toThrow();
			expect(function () {a.setAcceptedFiles('application/msword')}).not.toThrow();
			expect(a.controls[0].accept === 'application/msword').toBeTruthy();
			expect(a.controls[0].html.accept.toString() === 'application/msword').toBeTruthy();
		});
		it('should accept many type of files separated with a colon(,)', function () {
			expect(function () {a.setAcceptedFiles('application/msword, application/pdf, image/x-icon')}).not.toThrow();
			expect(a.controls[0].accept === 'application/msword, application/pdf, image/x-icon');
			b = a.controls[0].accept.split(",");
			expect(b[0]).toContain('application/msword');
			expect(b[1]).toContain('application/pdf');
			expect(b[2]).toContain('image/x-icon');
		});
	});
	describe('method "setMultipleFiles"', function () {
		it('should set the multiple property, the parameter must be a boolean', function () {
			expect(a.controls[0].multiple).toBeFalsy();
			expect(a.controls[0].html.multiple).toBeFalsy();
			a.setMultipleFiles(true);
			expect(a.controls[0].multiple).toBeTruthy();
			expect(a.controls[0].html.multiple).toBeTruthy();
			a.setMultipleFiles(false);
			expect(a.controls[0].multiple).toBeFalsy();
			expect(a.controls[0].html.multiple).toBeFalsy();
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
            expect(a.html.nodeName === "DIV").toBeTruthy();
		});
	});
})