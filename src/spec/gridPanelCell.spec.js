//***********************other class******************************************************//
describe('PMUI.grid.GridPanelCell', function () {
	var a, b, c, d, e, cell;
	beforeEach(function () {
		a = new PMUI.grid.GridPanelCell({content: "new_string"});
		document.body.appendChild(a.getHTML());
	});
	afterEach(function () {
		a = null;
		b = null;
		c = null;
	});
	describe('class behavior', function () {
		it('[US-44,a]should accept to contain a string or an Element object', function () {
			// Not Element object
			c = new PMUI.event.Event();
			//String, number, Element Object 
			b = new PMUI.ui.Button();
			b.setText("button");
			d = "new_string";
			e = 34;
			new PMUI.grid.GridPanelCell({content: b});
			expect(function (){cell = new PMUI.grid.GridPanelCell({content: c});}).toThrow();
			expect(function (){cell = new PMUI.grid.GridPanelCell({content: b});
											document.body.appendChild(cell.getHTML());}).not.toThrow();
			expect($(cell.html).text() === "button").toBeTruthy();
			expect(function (){cell = new PMUI.grid.GridPanelCell({content: d});
											document.body.appendChild(cell.getHTML());}).not.toThrow();
			expect($(cell.html).text() === "new_string").toBeTruthy();
			expect(function (){cell = new PMUI.grid.GridPanelCell({content: e});
											document.body.appendChild(cell.getHTML());}).not.toThrow();
			expect($(cell.html).text() === "34").toBeTruthy();
		});	
	});
	describe('method "getContent"', function () {
		it('should get the content of the gridPanelCell', function () {
			b = new PMUI.ui.Button({text:""});
			a.setContent(b);
			expect(a.getContent().type === "Button").toBeTruthy();
			expect(a.getContent().text === "").toBeTruthy();
		});
	});
	describe('method "setColumnIndex"', function () {
		it('should set the column index', function () {
			expect(function (){a.setColumnIndex(any_text);}).toThrow();
			expect(function (){a.setColumnIndex(1);}).not.toThrow();
			expect(a.columnIndex === 1).toBeTruthy();
			expect(function (){a.setColumnIndex("index_1");}).not.toThrow();
			expect(a.columnIndex === "index_1").toBeTruthy();
		});
	});
	describe('method "createHTML"', function () {
		it('should create a new HTML element', function () {
			var html = a.createHTML();
            expect(html).toBeDefined();
            expect(html.tagName).toBeDefined();
            expect(html.nodeType).toBeDefined();
            expect(html.nodeType).toEqual(document.ELEMENT_NODE);
		});
	});
});