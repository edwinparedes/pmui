(function () {
    /**
     * @class PMUI.panel.TreePanel
     * A panel that contains {@link PMUI.item.TreeNode TreeNode items}.
     * @extends {PMUI.core.Panel}
     *
     * An example of a TreePanel with descendant nodes (using the items {@link #cfg-items config option}):
     *
     *      @example
     *      var t = new PMUI.panel.TreePanel({
     *          style: {
     *              cssClasses: ['xxxxxxxx']
     *          },
     *          items: [
     *              {
     *                  label: "America",
     *                  items: [
     *                      {
     *                          label: "North America",
     *                          items: [
     *                              {
     *                                  label: "Canada"
     *                              },
     *                              {
     *                                  label: "USA"
     *                              },
     *                              {
     *                                  label: "Mexico"
     *                              }
     *                          ]
     *                      },
     *                      {
     *                          label: "Central America and Caribbean",
     *                          items: [
     *                              {
     *                                  label: "Guatemala"
     *                              },
     *                              {
     *                                  label: "Cuba"
     *                              },
     *                              {
     *                                  label: "Costa Rica"
     *                              }
     *                          ]
     *                      },
     *                      {
     *                          label: "South America",
     *                          items: [
     *                              {
     *                                  label: "Argentina"
     *                              },
     *                              {
     *                                  label: "Bolivia"
     *                              },
     *                              {
     *                                  label: "Brasil"
     *                              }
     *                          ]
     *                      }
     *                  ]
     *              }
     *          ]
     *      });
     *      document.body.appendChild(t.getHTML());
     *      t.defineEvents();
     *
     * An example of a TreePanel with descendant nodes (using the {@link #cfg-dataItems dataItems} config option):
     *
     *      @example
     *      t2 = new PMUI.panel.TreePanel({
     *          nodeDefaultSettings: {
     *              labelDataBind: 'name',
     *              itemsDataBind: 'regions',
     *              recursiveChildrenDefaultSettings: true,
     *              childrenDefaultSettings: {
     *                  labelDataBind: 'name',
     *                  itemsDataBind: 'countries',
     *                  autoBind: true
     *              },
     *              autoBind: true
     *          },
     *          dataItems: [
     *              {
     *                  name: 'America',
     *                  regions: [
     *                      {
     *                          name: 'North America',
     *                          countries: [
     *                              {
     *                                  name: 'Canada'
     *                              },
     *                              {
     *                                  name: 'USA'
     *                              },
     *                              {
     *                                  name: 'Mexico'
     *                              }
     *                          ]
     *                      },
     *                      {
     *                          name: 'Central America and Caribbean',
     *                          countries: [
     *                              {
     *                                  name: 'Guatemala'
     *                              },
     *                              {
     *                                  name: 'Cuba'
     *                              }, 
     *                              {
     *                                  name: 'Costa Rica'
     *                              }
     *                          ]
     *                      },
     *                      {
     *                          name: 'South America',
     *                          countries: [
     *                              {
     *                                  name: 'Argentina'
     *                              },
     *                              {
     *                                  name: 'Bolivia'
     *                              }, 
     *                              {
     *                                  name: 'Brasil'
     *                              }
     *                          ]
     *                      }
     *                  ]
     *              }
     *          ]
     *      });
     *      document.body.appendChild(t2.getHTML());
     *      t2.defineEvents();
     *
     * @constructor
     * Creates a new instance of the class.
     * @param {Object} settings The config options.
     *
     * @cfg {Object|null} [nodeDefaultSettings=null] The default settings for all the direct child nodes of the tree panel.
     * It is an object with the config options for {@link PMUI.item.TreeNode TreeNode}.
     * @cfg {Object|null} [dataItems=null] An object with the data for the items in the tree panel.
     * @cfg {Array} [data=[]] An array in which each element is an object with the config options for
     * {@link PMUI.item.TreeNode TreeNode} an instance of it. If you use both dataItems and this config option, this
     * config option will be ignored and the dataItems will be used instead.
     * @cfg {String} [filterPlaceholder="search"] The text to show as placeholder for the text control for filtering.
     * @cfg {Function|String|null} [emptyMessage=null] The message to show when the grid is showing no items. It
     * can be:
     *
     * - A String, in this case the string will be displayed in both cases, when the treepanel has no items and when no
     * treepanel's item meet the filter.
     * - A Function, in this case the function must return a string or an HTML Element to be displayed. The function
     * will receive two parameters: the {@link PMUI.panel.TreePanel TreePanel}, a boolean If it is true it means the returned
     * value will be used when a filter is applied, otherwise it means that the returned value will be used when
     * there are no items in the treepanel.
     * - null, in this case a default message will be used for each situation.
     * @cfg {Boolean} [visibleTitle=false] If the title will be displayed.
     * @cfg {String} [title=""] The text for the title.
     * @cfg {Function} [onNodeClick=null] The callback function to be executed everytime the
     * {@link #event-onNodeClick onNodeClick} event fires. For info about the parameters sent to the callback please
     * read the {@link #event-onNodeClick onNodeClick} event documentation.
     * @cfg {Function|null} [onBeforeAppend=null] A callback function to be called everytime the
     * {@link #event-onBeforeAppend onBeforeAppend} event is fired. For info about the parameters used for this
     * callback please read the documentation for this event.
     * @cfg {Function|null} [onAppend=null] A callback function to be called everytime the
     * {@link #event-onAppend onAppend} event is fired. For info about the parameters used for this callback please
     * read the documentation for this event.
     */
    var TreePanel = function (settings) {
        TreePanel.superclass.call(this, jQuery.extend(settings, {
            factory: {
                products: {
                    'node': PMUI.item.TreeNode
                },
                defaultProduct: 'node'
            }
        }));
        /**
         * The default settings for all the direct child nodes of the tree panel.
         * @type {Object}
         */
        this.nodeDefaultSettings = null;
        /**
         * The filter criteria being applied currently.
         * @type {String}
         * @readonly
         */
        this.filterCriteria = "";
        /**
         * If the tree panel is filterable or not.
         * @type {Boolean}
         * @readonly
         */
        this.filterable = null;
        /**
         * The text control in which the user can introduce a text to filter.
         * @type {PMUI.control.TextControl}
         * @private
         */
        this.filterControl = null;
        /**
         * A string or a function or null. Determines the text/HTML Element to show when there are no items to show.
         * @type {Function|String|null}
         * @readonly
         */
        this.emptyMessage = null;
        /**
         * The title for the TreePanel
         * @type {String}
         */
        this.title = null;
        /**
         * If the title is visible or not.
         * @type {Boolean}
         * @readonly
         */
        this.visibleTitle = null;
        /**
         * @event onItemClick
         * Fired everytime an tree item is clicked.
         * @param {PMUI.panel.TreePanel} treePanel The TreePanel in which the event was fired.
         * @param {PMUI.item.TreeNode} node The node that was clicked.
         */
        this.onNodeClick = null;
        /**
         * An private object to store the DOM elements that compose the object's HTML.
         * @type {Object}
         * @private
         */
        this.dom = {};
        /**
         * @event onBeforeAppend
         * Fired before a child node is appended to the current node or to any of its children nodes.
         * @param {PMUI.item.TreeNode} node The node in which the event is being fired.
         * @param {PMUI.item.TreeNode} newNode The new node to be appended.
         */
        this.onBeforeAppend = null;
        /**
         * @event onAppend
         * Fired everytime a child node is appended to the current node or to any of its children nodes.
         * @param {PMUI.item.TreeNode} node The node in which the event is being fired.
         * @param {PMUI.item.TreeNode} newNode The appended node.
         */
        this.onAppend = null;
        TreePanel.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Panel', TreePanel);
    /**
     * The object's type.
     * @type {String}
     */
    TreePanel.prototype.type = 'TreePanel';
    /**
     * The class family.
     * @type {String}
     */
    TreePanel.prototype.family = 'Panel';
    /**
     * Initializes the object.
     * @param  {Object} settings The config options for the object.
     * @private
     */
    TreePanel.prototype.init = function (settings) {
        var defaults = {
            nodeDefaultSettings: null,
            dataItems: null,
            data: [],
            filterPlaceholder: 'search',
            filterable: false,
            emptyMessage: null,
            visibleTitle: false,
            title: "",
            onNodeClick: null,
            onAppend: null,
            onBeforeAppend: null,
            collapse: false,
            onStop : function(){console.log("trepanel")}
        };

        jQuery.extend(true, defaults, settings);

        this.setNodeDefaultSettings(defaults.nodeDefaultSettings)
            .setEmptyMessage(defaults.emptyMessage)
            .setTitle(defaults.title)
            .setOnNodeClickHandler(defaults.onNodeClick)
            .setOnAppendHandler(defaults.onAppend)
            .setOnBeforeAppendHandler(defaults.onBeforeAppend);
        this.filterControl = new PMUI.control.TextControl({
            onKeyUp: this.onFilterControlChangeHandler(),
            placeholder: defaults.filterPlaceholder
        });

        if (jQuery.isArray(defaults.dataItems)) {
            this.setDataItems(defaults.dataItems);
        } else {
            this.setItems(defaults.items);
        }
        if (defaults.filterable) {
            this.enableFiltering();
        } else {
            this.disableFiltering();
        }
        if (defaults.visibleTitle) {
            this.showTitle();
        } else {
            this.hideTitle();
        }
        if (defaults.collapse) {
            this.collapse(true);
        } else {
            this.expand(true);
        }
    };
    /**
     * Sets the callback function to be called everytime the {@link #event-onBeforeAppend onBeforeAppend} event is
     * fired. For info about the parameters used for this callback please read the documentation for this event.
     * @param {Function|null} handler
     */
    TreePanel.prototype.setOnBeforeAppendHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnBeforeAppendHandler(): The parameter must be a function or null.");
        }
        this.onBeforeAppend = handler;
        return this;
    };
    /**
     * Sets the callback function to be called everytime the
     * {@link #event-onAppend onAppend} event is fired. For info about the parameters used for this callback please
     * read the documentation for this event.
     * @param {Function|null} handler
     */
    TreePanel.prototype.setOnAppendHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnAppendHandler(): The parameter must be a function or null.");
        }
        this.onAppend = handler;
        return this;
    };
    /**
     * Sets the callback function to be executed everytime the {@link #event-onNodeclick onNodeClick} is fired. For
     * info about the parameters sent to the callback read the document for that event.
     * @param {Function} handler It can be a function or null.
     */
    TreePanel.prototype.setOnNodeClickHandler = function (handler) {
        if (!(handler === null || typeof handler === 'function')) {
            throw new Error("setOnNodeClickHandler(): The parameter must be a function.");
        }
        this.onNodeClick = handler;
        return this;
    };
    /**
     * @inheritdoc
     */
    TreePanel.prototype.clearItems = function () {
        TreePanel.superclass.prototype.clearItems.call(this);
        return this.showEmptyMessage();
    };
    /**
     * Hides the title bar.
     * @chainable
     */
    TreePanel.prototype.hideTitle = function () {
        this.visibleTitle = false;
        if (this.dom.title) {
            this.dom.title.style.display = 'none';
        }
        return this;
    };
    /**
     * Shows the title.
     * @chainable
     */
    TreePanel.prototype.showTitle = function () {
        this.visibleTitle = true;
        if (this.dom.title) {
            this.dom.title.style.display = '';
        }
        return this;
    };
    /**
     * Sets the title for the tree panel.
     * @param {String} title
     * @chainable
     */
    TreePanel.prototype.setTitle = function (title) {
        if (typeof title !== 'string') {
            throw new Error('showTitle(): The parameter must be a string.');
        }
        this.title = jQuery.trim(title);
        if (this.dom.title) {
            this.dom.title.textContent = this.title;
        }
        return this;
    };
    /**
     * Sets the message to display when there are no items to display in the tree panel.
     * @param {String|Function|null} emptyMessage It can be:
     *
     * - A String, in this case the string will be displayed in both cases, when the tree has no items and when no
     * tree's items meet the filter.
     * - A Function, in this case the function must return a string or an HTML Element to be displayed. The function
     * will receive two parameters: the {@link PMUI.panel.TreePanel tree panel}, a boolean If it is true it means the returned
     * value will be used when a filter is applied, otherwise it means that the returned value will be used when
     * there are no items in the tree panel.
     * - null, in this case a default message will be used for each situation.
     * @chainable
     */
    TreePanel.prototype.setEmptyMessage = function (emptyMessage) {
        if (!(emptyMessage === null || typeof emptyMessage === 'string' || typeof emptyMessage === 'function')) {
            throw new Error("setEmptyMessage(): the parameter must be a string, a function or null.");
        }
        this.emptyMessage = emptyMessage;
        return this;
    };
    /**
     * Sets the filter for the text control's placeholder.
     * @param {String} placeholder The text for the placeholder.
     * @chainable
     */
    TreePanel.prototype.setFilterPlaceholder = function (placeholder) {
        this.filterControl.setPlaceholder(placeholder);
        return this;
    };
    /**
     * Returns the placeholder for the text control for filtering.
     * @return {String}
     */
    TreePanel.prototype.getFilterPlaceholder = function () {
        return this.filterControl.placeholder;
    };
    /**
     * Returns the handler to be executed when the filter changes.
     * @return {Function}
     * @private
     */
    TreePanel.prototype.onFilterControlChangeHandler = function () {
        var that = this;
        return function () {
            var nextFilter = this.getHTML().value;
            if (that.filterCriteria !== nextFilter) {
                that.filter(nextFilter);
            }
        };
    };
    /**
     * Enables the filtering functionality.
     * @chainable
     */
    TreePanel.prototype.enableFiltering = function () {
        this.filterable = true;
        this.clearFilter().filterControl.setValue("");
        if (this.dom.toolbar) {
            this.dom.toolbar.style.display = '';
        }
        return this;
    };
    /**
     * Disables the filtering functionality.
     * @chainable
     */
    TreePanel.prototype.disableFiltering = function () {
        this.filterable = false;
        this.clearFilter().filterControl.setValue("");
        if (this.dom.toolbar) {
            this.dom.toolbar.style.display = 'none';
        }
        return this;
    };
    /**
     * Sets the default settings for the direct child nodes of the tree panel.
     * @param {Object} settings The config options to be applied to new child nodes to be added in case they are
     * missing any config option.
     * @chainable
     */
    TreePanel.prototype.setNodeDefaultSettings = function (settings) {
        if (typeof settings !== 'object') {
            throw new Error('setNodeDefaultSettings(): The parameter must be an object.');
        }
        this.nodeDefaultSettings = settings;

        return this;
    };
    /**
     * @inheritdoc
     */
    TreePanel.prototype.setFactory = function () {
        this.factory = new PMUI.util.Factory({
            products: {
                'node': PMUI.item.TreeNode
            },
            defaultProduct: 'node'
        });

        return this;
    };
    /**
     * The private handler fot the {@link #event-onBeforeAppend onBeforeAppend} event.
     * @private
     * @chainable
     */
    TreePanel.prototype.onBeforeAppendHandler = function (treeNode) {
        if (typeof this.onBeforeAppend === 'function') {
            this.onBeforeAppend(this, treeNode);
        }
        return this;
    };
    /**
     * The private handler for the {@link #event-onAppend} event.
     * @private
     * @chainable
     */
    TreePanel.prototype.onAppendHandler = function (treeNode) {
        if (typeof this.onAppend === 'function') {
            this.onAppend(this, treeNode);
        }
        return this;
    };
    /**
     * Adds a new node to the tree panel.
     * @param {Object|PMUI.item.TreeNode} item An instance of {@link PMUI.item.TreeNode TreeNode}, or an object with
     * the config options to create a new one. In the latter case, if any config option is missing then will be used
     * the respective one in the set by the {@link #method-setNodeDefaultSettings setNodeDefaultSettings()} method.
     * @param {Number} [index] The position index in which will be added the new item. If not specified the will be
     * added to the end.
     * @chainable
     */
    TreePanel.prototype.addItem = function (item, index) {
        var settings = {},
            itemToAdd;

        if (item instanceof PMUI.item.TreeNode) {
            item.setParent(this);
        } else if (typeof item === 'object') {
            jQuery.extend(true, settings, this.nodeDefaultSettings || {}, item);
            settings.parent = this;
            item = settings;
        } else {
            throw new Error('addItem(): The parameter must be an instance of PMUI.item.TreeNode or an object.');
        }
        itemToAdd = this.factory.make(item);
        if (itemToAdd) {
            this.hideEmptyMessage();
            this.onBeforeAppendHandler(itemToAdd);
            TreePanel.superclass.prototype.addItem.call(this, itemToAdd, index);
            this.onAppendHandler(itemToAdd);
        }
        return this;
    };
    /**
     * @inheritdoc
     */
    TreePanel.prototype.removeItem = function (item) {
        TreePanel.superclass.prototype.removeItem.call(this, item);
        if (!this.items.getSize()) {
            this.showEmptyMessage();
        }
        return this;
    };
    /**
     * @inheritdoc
     */
    TreePanel.prototype.setItems = function (items) {
        if (this.onAppend !== undefined) {
            TreePanel.superclass.prototype.setItems.call(this, items);
        }
        return this;
    };
    /**
     * Expands the child nodes.
     * @param  {Boolean} [expandAll=false] If the expand function will be applied to all the sub child items or not.
     * @chainable
     */
    TreePanel.prototype.expand = function (expandAll) {
        var i,
            items = this.items.asArray();

        for (i = 0; i < items.length; i += 1) {
            items[i].expand(expandAll);
        }
        return this;
    };
    /**
     * Collapse the child nodes.
     * @param  {Boolean} [collapseAll=false] If the collapse function will be applied to all the sub child items or
     * not.
     * @chainable
     */
    TreePanel.prototype.collapse = function (collapseAll) {
        var i,
            items = this.items.asArray();

        for (i = 0; i < items.length; i += 1) {
            items[i].collapse(collapseAll);
        }
        return this;
    };
    /**
     * Hides the message to show when there are no items to display.
     * @chainable
     * @private
     */
    TreePanel.prototype.hideEmptyMessage = function () {
        jQuery(this.html).find('.pmui-treepanel-emptymessage').hide();
        return this;
    };
    /**
     * Shows an empty message when there are no items to display.
     * @chainable
     * @private
     */
    TreePanel.prototype.showEmptyMessage = function () {
        var node,
            message;

        if (!this.html) {
            return this;
        }
        node = jQuery(this.html).find('.pmui-treepanel-emptymessage').empty().get(0);
        if (!node) {
            node = PMUI.createHTMLElement('div');
            node.className = 'pmui-treepanel-emptymessage';
        }
        if (typeof this.emptyMessage === 'function') {
            message = this.emptyMessage(this, !!this.filterCriteria);
        } else if (typeof this.emptyMessage === 'string') {
            message = this.emptyMessage;
        } else {
            if (this.filterCriteria) {
                message = 'No matches found for \"' + this.filterCriteria + '\"';
            } else {
                message = 'No items found.';
            }
        }
        if (typeof message === 'string') {
            node.appendChild(document.createTextNode(message));
        } else if (PMUI.isHTMLElement(message)) {
            node.appendChild(message);
        }
        node.style.display = '';
        this.html.appendChild(node);
        return this;
    };
    /**
     * Clears the current filter.
     * @chainable
     */
    TreePanel.prototype.clearFilter = function () {
        return this.filter("");
    };
    /**
     * Filters the tree using a criteria.
     * @param  {String} filterCriteria The criteria.
     * @chainable
     */
    TreePanel.prototype.filter = function (filterCriteria) {
        var i,
            childNodes = this.items.asArray(),
            partialRes,
            res = false;

        filterCriteria = filterCriteria.toString(10);
        this.filterCriteria = filterCriteria;
        jQuery(this.html).find('.pmui-treepanel-emptymessage').remove();
        for (i = 0; i < childNodes.length; i += 1) {
            childNodes[i].setVisible(partialRes = childNodes[i].filter(filterCriteria));
            res = res || partialRes;
        }
        if (!res) {
            this.showEmptyMessage();
        }
        return this;
    };
    /**
     * @inheritdoc
     */
    TreePanel.prototype.defineEvents = function () {
        TreePanel.superclass.prototype.defineEvents.call(this);
        this.filterControl.defineEvents();
        return this;
    };
    /**
     * @inheritdoc
     */
    TreePanel.prototype.createHTML = function () {
        var list,
            toolbar,
            title;

        if (this.html) {
            return this.html;
        }

        html = PMUI.createHTMLElement(this.elementTag || "div");
        html.id = this.id;
        PMUI.linkToPMUIObject(html, this);
        this.html = html;
        this.actionHTML = html;
        this.applyStyle();
        this.containmentArea = this.html;

        title = PMUI.createHTMLElement('h2');
        title.className = 'pmui-treepanel-title';
        this.html.appendChild(title);
        this.dom.title = title;
        list = PMUI.createHTMLElement('ul');
        list.className = 'pmui-treepanel-list';
        this.dom.list = list;
        this.containmentArea = list;
        toolbar = PMUI.createHTMLElement('div');
        toolbar.className = 'pmui-treepanel-toolbar';
        toolbar.style.display = 'none';
        toolbar.appendChild(this.filterControl.getHTML());
        this.dom.toolbar = toolbar;
        this.html.appendChild(toolbar);
        this.html.appendChild(list);
        this.setTitle(this.title);
        if(this.items.getSize() > 0){
            this.paintItems();
        }
        if (this.filterable) {
            this.enableFiltering();
        } else {
            this.disableFiltering();
        }
        if (this.visibleTitle) {
            this.showTitle();
        } else {
            this.hideTitle();
        }
        return this.html;
    };

    PMUI.extendNamespace('PMUI.panel.TreePanel', TreePanel);

    if (typeof exports !== 'undefined') {
        module.exports === TreePanel;
    }
}());