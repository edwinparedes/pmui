(function () {
    /**
     * @class PMUI.panel.AccordionPanel
     * @extends PMUI.core.Panel
     *
     * Class to handle an Accordion component, this is a container for {@link PMUI.item.AccordionItem AccordionItem}.
     *
     * The way of usage the class is showed on the example above
     *
     *              @example
     *               accordion   = new PMUI.panel.AccordionPanel({
     *                   width: 400,
     *                   height: 500,
     *                   hiddenTitle: true,
     *                   //multipleSelection : true,
     *                   title: "My accordion",
     *                   items: [
     *                       {
     *                           iconClass: "",
     *                           title: "First item",
     *                           selected: true
     *                       },
     *                       {
     *                           iconClass: "pmui-icon pmui-icon-warning",
     *                           title: "Second item",
     *                           body: "<a href=\"http://www.google.com\">Google Link</a>",
     *                           style: {
     *                               cssProperties: {
     *                                   "background-color": "#f2eaea"
     *                               }
     *                           }
     *                       },
     *                       {
     *                           title: "Third item <span class=\"classname-accordion\"></span>"
     *                       },
     *                       {
     *                           title: "Fourth"
     *                        }, new PMUI.item.AccordionItem({
     *                           title: "Five"
     *                        })
     *                   ],
     *                   listeners: {
     *                       select: function (obj, event) {
     *                           console.log("Selected...", obj);
     *                       }
     *                   }
     *               });
     *               document.body.appendChild(accordion.getHTML());
     *               accordion.defineEvents();
     *
     *
     * @constructor
     * Creates a new component
     * @param {Object} [settings] settings The configuration options may be specified as the follow sentence
     *      {
     *          items: [
     *              {
     *                  title: "First element",
     *                  body: item // This item can be any element that inherit from {@link PMUI.core.Panel Panel}
     *              },
     *              {
     *                  title: "Second element"
     *              }
     *          ]
     *      }
     * @cfg {String} name The name for the field.
     *
     * @cfg {Boolean} [hiddenTitle=true] Represents wether the title of the component will be showed, the
     * otherwise the panel will rendered without title.
     * @cfg {Array} [items=[]] The array contains the basic elements for {@PMUI.panel.AccordionPanel AccordionPanel class}
     * multipleSelection.
     * @cfg {String} [title=""] The title for the panel.
     * @cfg {Number} [heightItem = 'current height * 0.3'] Represents the height for every item
     * @cfg {Object} factory If it necessary is possible replace the current elements by default.
     *      factory: {
     *          products: {
     *              'accordionitem': PMUI.item.AccordionItem
     *          },
     *          defaultProduct: "accordionitem"
     *      }
     *
     */

    var AccordionPanel = function (settings) {
        AccordionPanel.superclass.call(this, settings);
        /**
         * @property {String} [title= ""] The text represents the title of the panel,
         * Whether the {PMUI.panel.AccordionPanel this.hiddenTitle} is false.
         */
        this.title = null;
        /**
         * @property {Boolean} [hiddenTitle=false] The property represents whether the title
         * will be showed.
         */
        this.hiddenTitle = null;
        /**
         * @property {Boolean} [multipleSelection=false] If the property is enabled, the {@link PMUI.item.AccordionItem items} can
         * be selected more than one time
         */
        this.multipleSelection = null;
        /**
         * @property {Object} [listeners] A config object containing one or more handlers to be added
         * to this object during initialization. by default the 'select' handler is enabled
         */
        this.listeners = {
            /**
             * @event select
             * Fires when a {@link PMUI.item.AccordionItem component} has been selected.
             * @param {Object} accordionItem {@link PMUI.item.AccordionItem AccordionItem}
             * @param {Object} event event
             */
            select: function () {
            }
        };
        /**
         * @property {HTMLElement} [header=null]
         * The property encapsulate the HTMLElement header for the panel.
         */
        this.header = null;
        /**
         * @property {HTMLElement} [body=null]
         * The property is related to HTMLElement body that will be used by
         * {@link PMUI.item.AccordionItem items}
         */
        this.body = null;
        /**
         * @property {HTMLElement} [footer=null]
         * The property encapsulate the HTMLElement footer for the panel.
         */
        this.footer = null;
        /**
         * @property {String} [iconClass='pmui-accordion-panel-icon']
         * Represents the class name for the icon.
         * It can be replaced on the config JSON if it is necessary
         * @private
         */
        this.iconClass = null;
        /**
         * @property {String} [headerClass='pmui-accordion-panel-header']
         * Represents the class name for the header of the panel.
         * It can be replaced on the config JSON if it is necessary
         * @private
         */
        this.headerClass = null;
        /**
         * @property {String} [bodyClass='pmui-accordion-panel-body']
         * Represents the class name for the body of the panel.
         * It can be replaced on the config JSON if it is necessary
         * @private
         */
        this.bodyClass = null;
        /**
         * @property {String} [footerClass='pmui-accordion-panel-footer']
         * Represents the class name for footer of the panel.
         * It can be replaced on the config JSON if it is necessary
         * @private
         */
        this.footerClass = null;
        /**
         * @property {String} [containerClass='pmui-accordion-panel-container']
         * Represents the class name for the father container.
         * It can be replaced on the config JSON if it is necessary
         * @private
         */
        this.containerClass = null;
        /**
         * @property {Number} [heightItem = 'current height * 0.3']
         * Represents the size height for every {@link PMUI.item.AccordionItem item}
         */
        this.heightItem = null;
        /**
         * determines whether the items of the accordion panel is automatically set when adding a new item
         * @type {Boolean}
         */
        this.selfAdjusting = null;
        AccordionPanel.prototype.init.call(this, settings);

    };

    PMUI.inheritFrom('PMUI.core.Panel', AccordionPanel);
    /**
     * Defines the object type of the element
     */
    AccordionPanel.prototype.type = 'Accordion';
    /**
     * Defines the object family of the element
     */
    AccordionPanel.prototype.family = 'Panel';

    AccordionPanel.prototype.init = function (settings) {
        var defaults = {
            multipleSelection: false,
            hiddenTitle: false,
            title: '',
            iconClass: 'pmui-accordion-panel-icon',
            headerClass: 'pmui-accordion-panel-header',
            bodyClass: 'pmui-accordion-panel-body',
            footerClass: 'pmui-accordion-panel-footer',
            containerClass: 'pmui-accordion-panel-container',
            heightItem: this.height * 0.3,
            items: [],
            factory: {
                products: {
                    'accordionitem': PMUI.item.AccordionItem
                },
                defaultProduct: "accordionitem"
            },
            listeners: {
                select: function () {
                }
            },
            selfAdjusting: false
        };

        jQuery.extend(true, defaults, settings);

        this.setFactory(defaults.factory)
            .setMultipleSelection(defaults.multipleSelection)
            .setHiddenTitle(defaults.hiddenTitle)
            .setTitle(defaults.title)
            .setIconClass(defaults.iconClass)
            .setHeaderClass(defaults.headerClass)
            .setBodyClass(defaults.bodyClass)
            .setFooterClass(defaults.footerClass)
            .setContainerClass(defaults.containerClass)
            .setHeightItem(defaults.heightItem)
            .setChildren(defaults.items)
            .setListeners(defaults.listeners)
            .setSelfAdjusting(defaults.selfAdjusting);
    };

    AccordionPanel.prototype.adjustHeightItems = function () {
        var maxHeight = this.height,
            headPanel = jQuery(this.header).outerHeight(),
            footerPanel = jQuery(this.footer).outerHeight(),
            headItems = 0,
            footerItems = 0,
            bodyHeight = 0,
            items,
            i,
            j;

        items = this.getItems();

        for (i = 0; i < items.length; i += 1) {
            headItems += jQuery(items[i].header.html).outerHeight() + 2;
            if (items[i].footer) {
                footerItems += jQuery(items[i].footer.html).outerHeight();
            }
        }

        bodyHeight = maxHeight - (headPanel + footerPanel + headItems + footerItems);
        for (j = 0; j < items.length; j += 1) {
            jQuery(items[j].body.html).height(bodyHeight);
        }
        return this;
    };
    /**
     * Clear all the object's current child items and add new ones
     * @param {Array} items An array where each element can be a {Element} object or a JSON object
     * where is specified the setting for the child item to be added
     * @chainable
     */
    AccordionPanel.prototype.setChildren = function (items) {
        var i;

        if (jQuery.isArray(items)) {
            this.clearItems();
            for (i = 0; i < items.length; i += 1) {
                this.addItem(items[i]);
            }
        }
        return this;
    };
    /**
     * Defines and sets whether the {@link PMUI.item.AccordionItem items} from {@link PMUI.panel.AccordionPanel Accordion}
     * will be selectable
     * @param {Boolean} parameter
     * @return {PMUI.panel.AccordionPanel} this
     */
    AccordionPanel.prototype.setMultipleSelection = function (parameter) {
        if (typeof parameter === 'boolean') {
            this.multipleSelection = parameter;
        }
        return this;
    };
    /**
     * Sets whether the title will be rendered or not inside the header
     * @param {Boolean} enabled
     */
    AccordionPanel.prototype.setHiddenTitle = function (enabled) {
        if (typeof enabled === 'boolean') {
            this.hiddenTitle = enabled;

            if (this.html) {
                if (this.hiddenTitle === true) {
                    this.header.setAttribute("style", "display:none")
                } else {
                    this.header.removeAttribute("style");
                }
                this.adjustHeightItems();
            }
        }
        return this;
    };
    /**
     * Sets the label for the Accordion title
     * @param {String} title
     */
    AccordionPanel.prototype.setTitle = function (title) {
        if (typeof title === "string") {
            this.title = title;

            if (this.html) {
                this.header.children[1].innerHTML = title;
            }
        }

        return this;
    };
    /**
     * Sets the class name for the header
     * @param {String} iconCls
     * @private
     */
    AccordionPanel.prototype.setIconClass = function (iconCls) {
        this.iconClass = iconCls;
        return this;
    };
    /**
     * Sets the class name for the Accordion's header
     * @param {String} class name
     * @private
     */
    AccordionPanel.prototype.setHeaderClass = function (className) {
        if (typeof className === 'string') {
            this.headerClass = className;
        }
        return this;
    };
    /**
     * Sets the class name for the Accordion's body
     * @param {String} class name
     * @private
     */
    AccordionPanel.prototype.setBodyClass = function (className) {
        if (typeof className === 'string') {
            this.bodyClass = className;
        }
        return this;
    };
    /**
     * Sets the class name for the Accordion's footer
     * @param {String} class name
     * @private
     */
    AccordionPanel.prototype.setFooterClass = function (className) {
        if (typeof className === 'string') {
            this.footerClass = className;
        }
        return this;
    };
    /**
     * Sets the class name for the object's container
     * @param {String} class name
     * @private
     */
    AccordionPanel.prototype.setContainerClass = function (className) {
        if (typeof className === 'string') {
            this.containerClass = className;
        }
        return this;
    };
    /**
     * Sets the height for every {@link PMUI.item.AccordionItem item}
     * @param {String} height
     */
    AccordionPanel.prototype.setHeightItem = function (height) {
        this.heightItem = height;
        return this;
    };
    /**
     * Sets the listeners for the object
     * @param {Object} events
     */
    AccordionPanel.prototype.setListeners = function (events) {
        if (typeof events === 'object') {
            this.listeners = events;
        }
        return this;
    };
    /**
     * Creates all the elements with their corresponding attributes
     * for make a container that encapsulate {@link PMUI.item.AccordionItem items}
     * @return  {HTMLElement} HTMLElement
     */
    AccordionPanel.prototype.createHTML = function () {
        var container,
            header,
            body,
            footer,
            title,
            icon,
            itemsLen,
            i;

        if (this.html) {
            return this.html;
        }
        container = AccordionPanel.superclass.prototype.createHTML.call(this);
        header = PMUI.createHTMLElement("div");
        header.setAttribute('class', this.headerClass);
        body = PMUI.createHTMLElement("div");
        body.setAttribute('class', this.bodyClass);
        footer = PMUI.createHTMLElement("div");
        footer.setAttribute('class', this.footerClass);

        title = PMUI.createHTMLElement("span");
        title.innerHTML = this.title;
        icon = PMUI.createHTMLElement("span");
        icon.setAttribute('class', this.iconClass);
        header.appendChild(icon);
        header.appendChild(title);
        if (this.hiddenTitle) {
            header.setAttribute("style", "display:none;");
        }
        itemsLen = this.items.getSize();
        if (typeof itemsLen === 'number') {
            for (i = 0; i < itemsLen; i += 1) {
                body.appendChild(this.getItems()[i].getHTML());
            }
        }

        this.header = header;
        this.body = body;
        this.footer = footer;

        container.appendChild(header);
        container.appendChild(body);
        container.appendChild(footer);
        container.className = this.containerClass;
        this.containmentArea = body;
        this.html = container;
        return this.html;
    };

    /**
     * Defines events related to the component and call to every DefineEvents method from
     * {@link PMUI.item.AccordionItem items}
     * @return {Object} {@link PMUI.panel.AccordionPanel Accordion}
     */
    AccordionPanel.prototype.defineEvents = function () {
        var j,
            children,
            that = this;

        if (that.items.getSize() > 0 && this.eventsDefined !== true) {
            children = that.getItems();
            for (j = 0; j < children.length; j += 1) {
                children[j].defineEvents();
            }
            if (this.selfAdjusting) {
                this.adjustHeightItems();
            }
            this.eventsDefined = true;
        }

        return this;
    };

    AccordionPanel.prototype.setSelfAdjusting = function (value) {
        if (typeof value === "boolean") {
            this.selfAdjusting = value;
        }
        return this;
    };

    PMUI.extendNamespace('PMUI.panel.AccordionPanel', AccordionPanel);
    if (typeof exports !== "undefined") {
        module.exports = AccordionPanel;
    }
}());