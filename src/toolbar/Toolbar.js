(function () {
    var Toolbar = function (settings) {
        Toolbar.superclass.call(this, settings);
        Toolbar.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Container', Toolbar);

    Toolbar.prototype.type = "Toolbar";

    Toolbar.prototype.init = function (settings) {
        var defaults = {
            elementTag: 'ul'
        };

        jQuery(true, defaults, settings);

        this.setElementTag(defaults.elementTag);
    };

    Toolbar.prototype.setFactory = function () {
        this.factory = new PMUI.util.Factory({
            products: {
                'toolbaraction': PMUI.toolbar.ToolbarAction
            },
            defaultProduct: 'toolbaraction'
        });
        return this;
    };

    PMUI.extendNamespace('PMUI.toolbar.Toolbar', Toolbar);
}());