(function () {
    var ToolbarAction = function (settings) {
        ToolbarAction.superclass.call(this, settings);
        this.action = null;
        this.visibleIcon = null;
        this.iconClass = null;
        ToolbarAction.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.toolbar.ToolbarItem', ToolbarAction);

    ToolbarAction.prototype.init = function (settings) {
        var defaults = {
            action: null,
            visibleIcon: true,
            iconClass: null,
            elementTag: 'li'
        };

        jQuery.extend(true, defaults, settings);
        this.setElementTag(defaults.elementTag)
            .setAction(defaults.action)
            .setIconClass(defaults.iconClass);
        if (defaults.visibleIcon) {
            this.showIcon();
        } else {
            this.hideIcon();
        }
    };

    ToolbarAction.prototype.setIconClass = function (iconClass) {
        if (!(iconClass === null || typeof iconClass === 'string')) {
            throw new Error("setIconClass(): the parameter must be a string or null.");
        }
        this.iconClass = iconClass;
        if (this.dom.iconContainer) {
            this.dom.iconContainer.className = 'pmui-toolbaraction-icon ' + (iconClass || "");
        }
        return this;
    };

    ToolbarAction.prototype.setText = function (text) {
        if (typeof text !== 'string') {
            throw new Error('setText(): The parameter must be a string.');
        }
        this.text = text;
        if (this.dom.textContainer) {
            this.dom.textContainer.textContent = text;
        }
        return this;
    };

    ToolbarAction.prototype.setAction = function (action) {
        if (!(action === null || typeof action === 'function')) {
            throw new Error("setAction(): the parameter must be a function or null.");
        }
        this.action = action;
        return this;
    };

    ToolbarAction.prototype.showIcon = function () {
        this.visibleIcon = true;
        if (this.dom.iconContainer) {
            this.dom.iconContainer.style.display = '';
        }
        return this;
    };

    ToolbarAction.prototype.hideIcon = function () {
        this.visibleIcon = false;
        if (this.dom.iconContainer) {
            this.dom.iconContainer.style.display = 'none';
        }
        return this;
    };

    ToolbarAction.prototype.attachEventListeners = function () {
        var that = this;

        if (this.html) {
            this.removeEvents();
            this.addEvent('click').listen(this.dom.link, function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (typeof that.action === 'function') {
                    that.action(that);
                }
            });
        }
        return this;
    };

    ToolbarAction.prototype.createHTML = function () {
        var link,
            iconContainer,
            textContainer;

        if (this.html) {
            return this.html;
        }

        ToolbarAction.superclass.prototype.createHTML.call(this);
        link = PMUI.createHTMLElement('a');
        link.className = 'pmui-toolbaraction-link';
        link.href = "#";
        iconContainer = PMUI.createHTMLElement('i');
        iconContainer.className = 'pmui-toolbaraction-icon';
        textContainer = PMUI.createHTMLElement('span');
        textContainer.className = 'pmui-toolbaraction-text';

        link.appendChild(iconContainer);
        link.appendChild(textContainer);

        this.dom.iconContainer = iconContainer;
        this.dom.link = link;
        this.dom.textContainer = textContainer;

        this.html.appendChild(link);

        this.setText(this.text)
            .setIconClass(this.iconClass);

        this.attachEventListeners();

        return this.html;
    };

    PMUI.extendNamespace('PMUI.toolbar.ToolbarAction', ToolbarAction);
}());