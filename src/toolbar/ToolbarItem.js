(function () {
    var ToolbarItem = function (settings) {
        ToolbarItem.superclass.call(this, settings);
        this.text = null;
        this.dom = {};
        ToolbarItem.prototype.init.call(this, settings);
    };

    PMUI.inheritFrom('PMUI.core.Item', ToolbarItem);

    ToolbarItem.prototype.type = 'ToolbarItem';

    ToolbarItem.prototype.init = function (settings) {
        var defaults = {
            text: ''
        };
        jQuery.extend(true, defaults, settings);
        this.setText(defaults.text);
    };

    ToolbarItem.prototype.setText = function (text) {
        if (typeof text !== 'string') {
            throw new Error('setText(): The parameter must be a string.');
        }
        this.text = text;
        if (this.dom.textContainer) {
            this.dom.textContainer = text;
        }
        return this;
    };

    ToolbarItem.prototype.createHTML = function () {
        if (this.html) {
            return this.html;
        }
        ToolbarItem.superclass.prototype.createHTML.call(this);
        this.dom.textContainer = this.html;
        return this.html;
    };

    PMUI.extendNamespace('PMUI.toolbar.ToolbarItem', ToolbarItem);
}());