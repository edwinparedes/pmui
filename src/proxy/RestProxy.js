(function () {
    /**
     * @class PMUI.proxy.RestProxy
     * @extend PMUI.proxy.Proxy
     *
     * How used the class
     * The {@link PMUI.proxy.RestClient Restclient} class encapsulate all the funcionality about the communicate
     * with the server.
     *
     *          @example
     *          restclient = new PMUI.proxy.RestProxy({
     *              url: "url.php",
     *              method: "GET",
     *              data: {},
     *              dataType: "json",
     *              success: function () {
     *                  ...
     *              },
     *              failure: function () {
     *                  ...
     *              },
     *              complete: function() {
     *                  ...
     *              }
     *          });
     *
     *
     * @constructor
     * Creates a new component
     * @param {Object} [settings] settings The configuration options are specified on the config options:
     *
     * @cfg {String} [url=""] The url property defines the target for the RestClient.
     * @cfg {String} method
     * The available methods to consume are the follow:
     *
     *          'create', 'read', 'update', 'delete'
     *
     * These have a directly relation to:
     *
     *          'create': 'POST',
     *          'read'  : 'GET',
     *          'update': 'PUT',
     *          'delete': 'DELETE'
     *
     * Based in the {@link RestClient RestClient} library
     * @cfg {Object} data Object that will be sent through the proxy.
     * @cfg {String} [dataType='json'] The available options are the following sentences:
     *
     *      json: 'application/json',
     *      plain: 'text/plain',
     *      form: 'application/x-www-form-urlencoded',
     *      html: 'text/html'
     *
     * @cfg {String} [authorizationType= "none"] The options availables for the property:
     *
     *      "none"
     *      "basic"
     *      "oauth2"
     *
     * @cfg {Boolean} [authorizationOAuth=false] For enable the communicate between client-server
     * through Oauth protocol the property must to be true
     *
     */
    var RestProxy = function (options) {
        RestProxy.superclass.call(this, options);
        this.url = null;
        this.method = null;
        this.rc = null;
        this.data = null;
        RestProxy.prototype.init.call(this, options);
    };

    PMUI.inheritFrom('PMUI.proxy.Proxy', RestProxy);
    /**
     * @property {String}
     * @readonly
     */
    RestProxy.prototype.type = "RestProxy";
    RestProxy.prototype.init = function (options) {
        var defaults = {
            url: null,
            method: 'GET',
            data: {},
            dataType: 'json',
            authorizationType: 'none',
            authorizationOAuth: false,
            success: function () {
            },
            failure: function () {
            },
            complete: function () {
            }
        };
        jQuery.extend(true, defaults, options);
        this.setRestClient()
            .setUrl(defaults.url)
            .setAuthorizationOAuth(defaults.authorizationOAuth)
            .setMethod(defaults.method)
            .setData(defaults.data)
            .setDataType(defaults.dataType)
            .setSuccessAction(defaults.success)
            .setFailureAction(defaults.failure)
            .setCompleteAction(defaults.complete);
    };

    /**
     * Creates an instance of the {@link RestClient ResClient} and
     * stores the object inside of {@link RestProxy#rc rc property}
     */
    RestProxy.prototype.setRestClient = function () {
        if (this.rc instanceof RestClient === false) {
            this.rc = new RestClient();
        }
        return this;
    };
    /**
     * Sets the url that will be used for send or get the data
     * from server, It's a basic element for {@link RestClient RestClient}
     * @param {String} url A string that represents the connection between
     * Client and server
     *
     *
     */
    RestProxy.prototype.setUrl = function (url) {
        this.url = url;
        return this;
    };
    /**
     * Sets the OAuth protocol to 'false' or 'true' for enable or disable the property.
     *
     *      //Example
     *      obj = new PMUI.proxy.RestProxy();
     *      obj.setAuthorizationOAuth(true);
     *
     * @param {Boolean} option Defines the boolean option
     */
    RestProxy.prototype.setAuthorizationOAuth = function (option) {
        if (typeof option === 'boolean') {
            this.rc.setSendBearerAuthorization(option);
        }
        return this;
    };
    /**
     * This method is useful for update the current action.
     * The available methods to consume are the follow:
     *      'create', 'read', 'update', 'delete'
     * These have a directly relation to:
     *      'create': 'POST',
     *      'read'  : 'GET',
     *      'update': 'PUT',
     *      'delete': 'DELETE'
     * Based in the {@link RestClient RestClient} library
     * @param {String} A string that reporesents
     */
    RestProxy.prototype.setMethod = function (method) {
        this.method = method;
        return this;
    };
    RestProxy.prototype.setSuccessAction = function (action) {
        RestProxy.prototype.success = action;
        return this;
    };
    RestProxy.prototype.setFailureAction = function (action) {
        RestProxy.prototype.failure = action;
        return this;
    };
    RestProxy.prototype.setCompleteAction = function (action) {
        RestProxy.prototype.complete = action;
        return this;
    };
    /**
     *
     * @param {Object} data Data for send to server
     * @return {PMUI.proxy.RestProxy} this The current component
     *
     */
    RestProxy.prototype.setData = function (data) {
        this.data = data;
        return this;
    };
    /**
     * Sets the Data type used for request data.
     * the types availables for the methods are the following:
     *
     *      json: 'application/json',
     *      plain: 'text/plain',
     *      form: 'application/x-www-form-urlencoded',
     *      html: 'text/html'
     *
     *      //For example
     *      restclient = new PMUI,proxy.RestProxy();
     *      restclient.setDataType("json");
     *      ...
     *
     * @param {String} dataType Type
     */
    RestProxy.prototype.setDataType = function (dataType) {
        this.rc.setDataType(dataType);
        return this;
    };
    RestProxy.prototype.setCredentials = function (usr, pass) {
        this.rc.setBasicCredentials(usr, pass);
        return this;
    };
    RestProxy.prototype.setContentType = function () {
        this.rc.setContentType();
        return this;
    };
    /**
     * Sets the authorization type and credentials for the RestClient.
     *
     * The options availables about authorization types are:
     *
     *      'none'
     *      'basic'
     *      'oauth2'
     *
     *      The credentials must have two keys: The username and password
     *
     *      //For example
     *      restclient = new PMUI.proxy.RestProxy();
     *      restclient.setAuthorizationType('oauth2', credentials);
     *
     *      Is possible sets the property while instances the class like to sentence below:
     *
     *      restclient = new PMUI.proxy.RestProxy({
     *          ...
     *          authorizationOAuth: true
     *          ...
     *      });
     *
     *
     * @param {Object} type Authorization type
     * @param {Object} credentials These are the keys that represent the username and password
     */
    RestProxy.prototype.setAuthorizationType = function (type, credentials) {
        this.rc.setAuthorizationType(type);
        switch (type) {
            case 'none':
                break;
            case 'basic':
                this.rc.setBasicCredentials(credentials.client, credentials.secret);
                break;
            case 'oauth2':
                this.rc.setAccessToken(credentials);
                break;
        }

        return this;
    };
    /**
     * Send data to server using POST method from RestClient
     * @param {Object} settings Configuration parameters
     *
     *      //For example
     *      this.post({
     *          url: "url.php",
     *          data: {
     *              grant_type: "authorization_code",
     *              code: keys.authorizedCode
     *          },
     *          success: function (xhr, response) {
     *              console.log(response);
     *          },
     *          failure: function (xhr, response) {
     *              console.log(response);
     *          },
     *          complete: function (xhr, response) {
     *              console.error(response);
     *          }
     *      });
     *
     */
    RestProxy.prototype.post = function (settings) {
        var that = this;
        if (settings !== undefined) {
            that.init(settings);
        }
        if (this.rc) {
            that.rc.postCall({
                url: that.url,
                id: that.uid,
                data: that.data,
                success: function (xhr, response) {
                    that.success.call(that, xhr, response);
                },
                failure: function (xhr, response) {
                    that.failure.call(that, xhr, response);
                },
                complete: function (xhr, response) {
                    that.complete.call(that, xhr, response);
                }
            });
            that.rc.setSendBearerAuthorization(false);

        } else {
            throw new Error("the RestClient was not defined, please verify the property 'rc' for continue.");
        }
    };
    /**
     * Updates data from the server using UPDATE method from RestClient
     * @param  {Object} settings
     * If it necessary is possible overwrite all the data instantiated on the RestClient
     *
     *      //For example
     *      this.update({
     *          url: "url.php",
     *          authorizationOAuth: true,
     *          success: function (xhr, response) {
     *              console.log(response);
     *          },
     *          failure: function (xhr, response) {
     *              console.error(response);
     *          },
     *          complete: function (xhr, response) {
     *              console.error(response);
     *          }
     *      });
     *
     */
    RestProxy.prototype.update = function (settings) {
        var that = this;

        if (settings !== undefined) {
            that.init(settings);
        }
        if (this.rc) {
            this.rc.putCall({
                url: this.url,
                id: this.uid,
                data: this.data,
                success: function (xhr, response) {
                    that.success.call(this, xhr, response);
                },
                failure: function (xhr, response) {
                    that.failure.call(this, xhr, response);
                },
                complete: function (xhr, response) {
                    that.complete.call(that, xhr, response);
                }
            });
        } else {
            throw new Error("the RestClient was not defined, please verify the property 'rc' for continue.");
        }
    };
    /**
     * Gets data from the server using GET method from RestClient
     * @param  {Object} settings
     * If it necessary is possible overwrite all the data instantiated on the RestClient
     *
     *      //For example
     *      this.get({
     *          url: "url.php",
     *          authorizationOAuth: true,
     *          success: function (xhr, response) {
     *              console.log(response);
     *          },
     *          failure: function (xhr, response) {
     *              console.error(response);
     *          },
     *          complete: function (xhr, response) {
     *              console.error(response);
     *          }
     *      });
     *
     */
    RestProxy.prototype.get = function (settings) {
        var that = this;

        if (settings !== undefined) {
            that.init(settings);
        }
        if (this.rc) {
            that.rc.getCall({
                url: that.url,
                id: that.uid,
                data: that.data,
                success: function (xhr, response) {
                    that.success.call(that, xhr, response);
                },
                failure: function (xhr, response) {
                    that.failure.call(that, xhr, response);
                },
                complete: function (xhr, response) {
                    that.complete.call(that, xhr, response);
                }
            });
            that.rc.setSendBearerAuthorization(false);

        } else {
            throw new Error("the RestClient was not defined, please verify the property 'rc' for continue.");
        }
    };
    /**
     * Removes data using DELETE method from RestClient
     * @param  {Object} settings
     * If it necessary is possible overwrite all the data instantiated on the RestClient
     *
     *      //For example
     *      this.remove({
     *          url: "url.php",
     *          authorizationOAuth: true,
     *          success: function (xhr, response) {
     *              console.log(response);
     *          },
     *          failure: function (xhr, response) {
     *              console.error(response);
     *          },
     *          complete: function (xhr, response) {
     *              console.error(response);
     *          }
     *      });
     *
     */
    RestProxy.prototype.remove = function (settings) {
        var that = this;

        if (settings !== undefined) {
            that.init(settings);
        }
        if (this.rc) {
            this.rc.deleteCall({
                url: this.url,
                id: this.uid,
                data: this.data,
                success: function (xhr, response) {
                    that.success.call(this, xhr, response);
                },
                failure: function (xhr, response) {
                    that.failure.call(this, xhr, response);
                },
                complete: function (xhr, response) {
                    that.complete.call(that, xhr, response);
                }
            });
        } else {
            throw new Error("the RestClient was not defined, please verify the property for continue.");
        }
    };
    /**
     * This is the callback for {@link RestClient RestClient} and it will be actived
     * and executed when the {@link RestClient RestClient} petition was success.
     * @param {Object} xhr
     * @param {Object} response
     */
    RestProxy.prototype.success = function (xhr, response) {
    };
    /**
     * If something happend with the connection or maybe the server is
     * down, this callback will be executed
     * @param {Object} xhr
     * @param {Object} response
     */
    RestProxy.prototype.failure = function (xhr, response) {
    };
    /**
     * This is the callback for {@link RestClient RestClient} and it will be actived
     * and executed when the {@link RestClient RestClient} petition finishes.
     * @param {Object} xhr
     * @param {Object} response
     */
    RestProxy.prototype.complete = function (xhr, response) {
    };

    PMUI.extendNamespace('PMUI.proxy.RestProxy', RestProxy);

    if (typeof exports !== 'undefined') {
        module.exports = RestProxy;
    }

}());