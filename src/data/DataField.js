(function () {
    var DataField = function (options) {
        options = options || {};
        DataField.superclass.call(this, options.key, options.value);
        this.typeField = null;
        this.custom = null;
        this.customKeys = {};
        DataField.prototype.init.call(this, options);
    };

    PMUI.inheritFrom('PMUI.data.DataItem', DataField);

    DataField.prototype.type = "DataField";

    DataField.prototype.dataTypes = {
        "string": "native",
        "number": "native",
        "date": "native",
        "boolean": "native",
        "object": "native"
    };

    DataField.prototype.basicKeys = {
        "key": "native",
        "value": "native",
        "type": "class",
        "val": "private",
        "custom": "native",
        "customKeys": "native",
        "typeField": "native",
        "family": "class"
    };

    DataField.prototype.defaultType = "string";

    DataField.prototype.init = function (options) {
        var that = this;
        if (options) {
            this.setKey(options.key || null);
            this.setValue(options.value || null);
            this.setType(options.type || null);
        }
        jQuery.each(options, function (k, v) {
            if (!that.basicKeys[k]) {
                if (typeof v !== 'object' && typeof v !== 'function') {
                    that[k] = v;
                    that.customKeys[k] = 'custom';
                }
            }
        });
    };

    DataField.prototype.setType = function (type) {
        if (type) {
            if (this.dataTypes[type]) {
                this.typeField = type;
                this.custom = false;
            } else {
                this.typeField = type;
                this.custom = true;
            }
        } else {
            this.custom = false;
            this.typeField = this.defaultType;
        }
        return this;
    };

    DataField.prototype.addAttribute = function (key, value) {
        this.customKeys[key] = value;
        return this;
    };

    DataField.prototype.getAttribute = function (key) {
        if (key === 'val' || key === 'key') {
            return this[key];
        }
        return this.customKeys[key];
    };

    DataField.prototype.existsKey = function (key) {
        if (key === "val" || key === "key") {
            return true;
        }
        return this.customKeys.hasOwnProperty(key);
    };

    DataField.prototype.getRecord = function (format) {
        var castType = format || this.typeField,
            out = {
                key: this.key,
                type: this.typeField,
                value: PMUI.castValue(this.val, castType)
            };
        jQuery.each(this.customKeys, function (key, value) {
            out[key] = value;
        });
        return out;
    };

    DataField.prototype.clear = function () {
        var key;
        DataField.superclass.prototype.clear.call(this);
        for (key in this.customKeys) {
            if (this.customKeys.hasOwnProperty(key)) {
                delete this.customKeys[key];
            }
        }
        return this;
    };

    PMUI.extendNamespace('PMUI.data.DataField', DataField);

    if (typeof exports !== 'undefined') {
        module.exports = DataField;
    }

}());