(function () {
    var DataSet = function (options) {
        this.items = null;
        this.factory = null;
        DataSet.prototype.init.call(this, options);
    };

    DataSet.prototype.type = "DataSet";

    DataSet.prototype.family = "DataSet";

    DataSet.prototype.init = function (options) {
        var defaults = {
            factory: {
                products: {
                    "datafield": PMUI.data.DataField,
                    "dataitem": PMUI.data.DataItem
                },
                defaultProduct: "dataitem"
            },
            items: []
        };
        jQuery.extend(true, defaults, options);
        this.items = new PMUI.util.ArrayList();
        this.factory = new PMUI.util.Factory(defaults.factory);
        this.setItems(defaults.items);
    };

    DataSet.prototype.setItems = function (items) {
        var that = this;
        jQuery.each(items, function (k, item) {
            that.items.insert(that.factory.make(item));
        });
        return this;
    };

    DataSet.prototype.addItem = function (item) {
        if (item) {
            this.items.insert(this.factory.make(item));
        }
        return this;
    };

    DataSet.prototype.getData = function () {
        var items,
            that = this,
            out = [];
        items = this.items.asArray();
        jQuery.each(items, function (k, item) {
            out.push(item.getRecord());
        });
        return out;
    };

    DataSet.prototype.getJSON = function () {
        return JSON.stringify(this.getData());
    };

    DataSet.prototype.getXML = function () {
        return PMUI.json2xml(this.getJSON());
    };

    DataSet.prototype.removeItem = function (item) {
        this.items.remove(item);
    };

    DataSet.prototype.getValue = function (key) {
        var item = this.items.find('key', key);
        return item.getValue();
    };

    DataSet.prototype.setValue = function (key, value) {
        var item = this.items.find('key', key);
        item.setValue(value);
        return this;
    };

    DataSet.prototype.keys = function () {
        var items,
            out = [];
        items = this.items.asArray();
        jQuery.each(items, function (k, item) {
            out.push(item.getKey());
        });
        return out;
    };

    DataSet.prototype.values = function () {
        var items,
            out = [];
        items = this.items.asArray();
        jQuery.each(items, function (k, item) {
            out.push(item.getValue());
        });
        return out;
    };

    PMUI.extendNamespace('PMUI.data.DataSet', DataSet);

    if (typeof exports !== 'undefined') {
        module.exports = DataSet;
    }
}());